import bpy
#import Debug
import sys

input1 = [
(
(-2, -2, 0),
(-2, -1.5, 0),
(-1.5, -1, 0.18212))
,
(
(2, -2, 0),
(1.5, -2, 0),
(2, -1.5, 0))
,
(
(2, -2, 0),
(1.5, -1.5, 0),
(1.5, -2, 0))
,
(
(2, -1.5, 0),
(1.5, -1.5, 0),
(2, -2, 0))
,
(
(1.5, -1.5, 0),
(1, -2, 0),
(1.5, -2, 0))
,
(
(1.5, -1.5, 0),
(1, -1.5, 0),
(1, -2, 0))
,
(
(1.5, -1.5, 0),
(0, -0.5, 0.38726),
(1, -1.5, 0))
,
(
(1, -1.5, 0),
(0, -0.5, 0.38726),
(1, -2, 0))
,
(
(1.5, -1.5, 0),
(1, 0.5, 0.48041),
(0, -0.5, 0.38726))
,
(
(0, -0.5, 0.38726),
(0.5, -2, 0),
(1, -2, 0))
,
(
(1.5, -1.5, 0),
(2, -1.5, 0),
(1, 0.5, 0.48041))
,
(
(1, 0.5, 0.48041),
(0.5, 0.5, 0.2402),
(0, -0.5, 0.38726))
,
(
(0, -0.5, 0.38726),
(0, -1, 0.19363),
(0.5, -2, 0))
,
(
(2, -1.5, 0),
(2, -1, 0),
(1, 0.5, 0.48041))
,
(
(0, -0.5, 0.38726),
(-0.5, -1, 0.25149),
(0, -1, 0.19363))
,
(
(0, -1, 0.19363),
(0, -2, 0),
(0.5, -2, 0))
,
(
(2, -1, 0),
(2, -0.5, 0),
(1, 0.5, 0.48041))
,
(
(0, -0.5, 0.38726),
(-1, -1, 0.36424),
(-0.5, -1, 0.25149))
,
(
(-0.5, -1, 0.25149),
(0, -2, 0),
(0, -1, 0.19363))
,
(
(2, -0.5, 0),
(1.5, 0.5, 0.2402),
(1, 0.5, 0.48041))
,
(
(0, -0.5, 0.38726),
(-0.5, -0.5, 0.26578),
(-1, -1, 0.36424))
,
(
(-1, -1, 0.36424),
(0, -2, 0),
(-0.5, -1, 0.25149))
,
(
(2, -0.5, 0),
(2, 0, 0),
(1.5, 0.5, 0.2402))
,
(
(1.5, 0.5, 0.2402),
(2, 1.5, 0),
(1, 0.5, 0.48041))
,
(
(-0.5, -0.5, 0.26578),
(-1, -0.5, 0.18212),
(-1, -1, 0.36424))
,
(
(-1, -1, 0.36424),
(-1, -1.5, 0.18212),
(0, -2, 0))
,
(
(2, 0, 0),
(2, 0.5, 0),
(1.5, 0.5, 0.2402))
,
(
(1.5, 0.5, 0.2402),
(2, 1, 0),
(2, 1.5, 0))
,
(
(2, 1.5, 0),
(2, 2, 0),
(1, 0.5, 0.48041))
,
(
(-0.5, -0.5, 0.26578),
(-2, 1, 0),
(-1, -0.5, 0.18212))
,
(
(-1, -0.5, 0.18212),
(-2, 0.5, 0),
(-1, -1, 0.36424))
,
(
(-1, -1, 0.36424),
(-2, -2, 0),
(-1, -1.5, 0.18212))
,
(
(-1, -1.5, 0.18212),
(-0.5, -2, 0),
(0, -2, 0))
,
(
(2, 0.5, 0),
(2, 1, 0),
(1.5, 0.5, 0.2402))
,
(
(2, 2, 0),
(1.5, 2, 0),
(1, 0.5, 0.48041))
,
(
(-0.5, -0.5, 0.26578),
(0, -0.5, 0.38726),
(-2, 1, 0))
,
(
(-2, 1, 0),
(-2, 0.5, 0),
(-1, -0.5, 0.18212))
,
(
(-2, 0.5, 0),
(-2, 0, 0),
(-1, -1, 0.36424))
,
(
(-1, -1, 0.36424),
(-1.5, -1, 0.18212),
(-2, -2, 0))
,
(
(-2, -2, 0),
(-1.5, -2, 0),
(-1, -1.5, 0.18212))
,
(
(-1, -1.5, 0.18212),
(-1, -2, 0),
(-0.5, -2, 0))
,
(
(1.5, 2, 0),
(1, 2, 0),
(1, 0.5, 0.48041))
,
(
(0, -0.5, 0.38726),
(-1, 2, 0),
(-2, 1, 0))
,
(
(-2, 0, 0),
(-1.5, -1, 0.18212),
(-1, -1, 0.36424))
,
(
(-1.5, -1, 0.18212),
(-2, -1.5, 0),
(-2, -2, 0))
,
(
(-1.5, -2, 0),
(-1, -2, 0),
(-1, -1.5, 0.18212))
,
(
(1, 2, 0),
(0.5, 2, 0),
(1, 0.5, 0.48041))
,
(
(0, -0.5, 0.38726),
(0.5, 0.5, 0.2402),
(-1, 2, 0))
,
(
(-1, 2, 0),
(-1.5, 2, 0),
(-2, 1, 0))
,
(
(-2, 0, 0),
(-2, -0.5, 0),
(-1.5, -1, 0.18212))
,
(
(0.5, 2, 0),
(0, 2, 0),
(1, 0.5, 0.48041))
,
(
(0.5, 0.5, 0.2402),
(1, 0.5, 0.48041),
(-1, 2, 0))
,
(
(-1.5, 2, 0),
(-2, 1.5, 0),
(-2, 1, 0))
,
(
(-2, -0.5, 0),
(-2, -1, 0),
(-1.5, -1, 0.18212))
,
(
(0, 2, 0),
(-0.5, 2, 0),
(1, 0.5, 0.48041))
,
(
(1, 0.5, 0.48041),
(-0.5, 2, 0),
(-1, 2, 0))
,
(
(-1.5, 2, 0),
(-2, 2, 0),
(-2, 1.5, 0))
]


def old():
    name = "t2"
    faces = [(0, 1, 2)]
    for i in range(len(input1)):
       objName = name + str(i)
       mesh = bpy.data.meshes.new(objName)
       obj = bpy.data.objects.new(objName, mesh)
       obj.location = bpy.context.scene.cursor_location
       bpy.context.scene.objects.link(obj)
       mesh.from_pydata(input1[i],[],faces)
       mesh.update(calc_edges=True)
def RemoveCube():
    bpy.ops.object.select_all(action='DESELECT')
    # selection
    bpy.data.objects['Cube'].select = True
    # remove it
    bpy.ops.object.delete()

def makeMaterial(name, diffuse=(0.8, 0.8, 0.8), specular=(1, 1, 1), alpha=1):
    mat = bpy.data.materials.new(name)
    mat.diffuse_color = diffuse
    mat.diffuse_shader = 'LAMBERT'
    mat.diffuse_intensity = 1.0
    mat.specular_color = specular
    mat.specular_shader = 'COOKTORR'
    mat.specular_intensity = 0.5
    mat.alpha = alpha
    mat.ambient = 1
    return mat

def GenerateMesh(points, faces, colours):
    print("Generating Mesh")
    name = "GeneratedMesh"

    objName = name
    mesh = bpy.data.meshes.new(objName)
    obj = bpy.data.objects.new(objName, mesh)
    obj.location = (0, 0, 0)
    bpy.context.scene.objects.link(obj)
    mesh.from_pydata(points,[],faces)
    mesh.update(calc_edges=True)


    material = makeMaterial("baseMaterial")
    obj.data.materials.append(material)
    #bpy.data.objects[name].select = True
    #bpy.ops.object.mode_set(mode = 'EDIT')
    #bpy.ops.object.select_all(action='DESELECT')
    #obj = bpy.context.scene.objects.get(objName)
    #scene = bpy.context.scene
    #scene.objects.active = obj
    #bpy.ops.object.mode_set(mode='EDIT')
    #bpy.ops.mesh.select_all(action = 'DESELECT')
    #bpy.ops.mesh.select_mode(type="FACE")
    #bpy.context.tool_settings.mesh_select_mode = (False, False, True)
    matDic = {}

    for i in range(0, len(colours)):
        if colours[i] in matDic:
            obj.data.polygons[i].material_index = matDic[colours[i]]
        else:
            a = ((colours[i] & 0xFF000000) >> 24) / 255.
            r = ((colours[i] & 0x00FF0000) >> 16) / 255.
            g = ((colours[i] & 0x0000FF00) >> 8)  / 255.
            b = ((colours[i] & 0x000000FF))       / 255.
            material = makeMaterial("M" + str(len(obj.data.materials)), (r, g, b), (1, 1, 1), 1)
            obj.data.materials.append(material)
            index = len(obj.data.materials) - 1
            matDic[colours[i]] = index
            obj.data.polygons[i].material_index = index
        #obj.data.polygons[i].select = False
    print('Length: ', len(obj.data.materials))
    print(type(obj))
    #for face in obj.faces:
    #    #print(face)
    #    pass
    #bpy.ops.object.mode_set(mode = 'OBJECT')


if __name__ == '__main__':
    argv = sys.argv
    argv = argv[argv.index("--") :]
    if len(argv) < 1:
        print("Please provide the following")
        print("\t File")
    generateFile = argv[1]
    f = open(generateFile)
    points = []
    faces = []
    colours = []
    numberOfPoints = 0
    numberOfFaces = 0
    pointsCounter = 0
    faceCounter = 0
    GetPoints = False
    GetFaces = False
    for line in f:
        if line.startswith("#"):
            continue
        elif line.startswith("Points"):
            numberOfPoints = int(line.strip().split(" ")[1])
            GetPoints = True
            GetFaces = False
        elif line.startswith("Faces"):
            numberOfFaces = int(line.strip().split(" ")[1])
            GetPoints = False
            GetFaces = True
        else:
            if GetPoints:
                x, y, z = line.strip().split(" ")
                points.append((float(x), float(y), float(z)))
                pointsCounter = pointsCounter + 1
            elif GetFaces:
                #i0, i1, i2, colour = line.strip().split(" ")
                i = line.strip().split(" ")
                faces.append((int(i[0]), int(i[1]), int(i[2])))
                if(len(i) == 4):
                    colours.append(int(i[3]))
                faceCounter = faceCounter + 1
    RemoveCube()
    GenerateMesh(points, faces, colours)
    #old()
