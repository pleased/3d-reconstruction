/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/05/30.
//

#include "FileParser.hpp"
namespace IO {
    FileParser::FileParser(std::string &baseDir, std::string &rgbFile, std::string &depthFile, double tol) {
        _baseDir = baseDir;
        _rgbFileLocation = baseDir + rgbFile;
        _depthFileLocation = baseDir + depthFile;

        _rgbFileStream.open(_rgbFileLocation);
        _depthFileStream.open(_depthFileLocation);

        // TODO check if files exist
        if (!_rgbFileStream.good()) {
            std::cerr << " Couldn't open rgb file '" << _rgbFileLocation << std::endl;
            std::cerr << "Please ensure you aren't using os specific symbols" << std::endl;
            exit(0);
        }
        if (!_depthFileStream.good() ) {
            std::cerr << " Couldn't open depth file '" << _depthFileLocation << std::endl;
            std::cerr << "Please ensure you aren't using os specific symbols" << std::endl;
            exit(0);
        }

        _tolerance = tol;
    }

    FileParser::~FileParser() {

    }

    void FileParser::EnableGroundTruth(std::string &groundTruthFile) {
        _groundTruthFileLocation = _baseDir + groundTruthFile;

        _groundTruthFileStream.open(_groundTruthFileLocation);
        //TODO check if file exists

        if (!_groundTruthFileStream.good() ) {
            _groundTruthFileStream.close();
            std::cerr << " Couldn't open groundTruth file '" << _groundTruthFileLocation << std::endl;
            std::cerr << "Please ensure you aren't using os specific symbols" << std::endl;
        } else {
            _groundTruthEnabled = true;
        }
    }

    void FileParser::DisableGroundTruth() {
        _groundTruthEnabled = false;
    }

    void FileParser::EnableIMU(std::string &imuFile) {
        _imuFileLocation = _baseDir + imuFile;

        _imuFileStream.open(_imuFileLocation);
        //TODO check if file exists
        if (!_imuFileStream.good()) {
            std::cerr << " Couldn't open Imu file '" << _imuFileLocation << std::endl;
            std::cerr << "Please ensure you aren't using os specific symbols" << std::endl;
        } else {
            _imuEnabled = true;
        }
    }

    void FileParser::DisableIMU() {
        _imuEnabled = false;
    }

    void FileParser::GetImageTimeAndLocation(std::string &in, double &time , std::string &location) {
        std::istringstream iss(in);
        iss >> time;
        iss >> location;
    }

    void FileParser::GetGroundTruthTimeAndRotationMatrix(std::string &in, double &time, Eigen::Matrix4d &rotationMatrix) {
        double x, y, z, xi, yj, zk, w;
        std::istringstream iss(in);
        iss >> time;
        iss >> x;
        iss >> y;
        iss >> z;
        iss >> xi;
        iss >> yj;
        iss >> zk;
        iss >> w;
        rotationMatrix = Utility::QuaternionToRotationMatrix(xi, yj, zk, w, x, y, z);
    }

    int FileParser::ReadData(std::vector<std::string>& data) {
        data.resize(4);
        double time = 0;
        if (ReadSynchronisedRGBDImages(data[RGB], data[Depth], time) == -1) {
            std::cout << "Couldn't sync RGBDImages eof detected" << std::endl;
            return -1;
        }

        if (_imuEnabled) {
            if (ReadSynchronisedIMU(time, data[IMU]) == -1) {
                std::cout << "Couldn't sync at end of file" << std::endl;
                return -1;
            }
        }
        if (_groundTruthEnabled) {
            if (ReadSynchronisedGroundTruth(time, data[GrouthTruth]) == -1 ) {
                std::cout << "Couldn't sync at end of file" << std::endl;
                return -1;
            }
        }
        return 1;
    }

    int FileParser::ReadSynchronisedRGBDImages(std::string& rgb, std::string& depth, double& time) {
        double rgbCurrentTime = 0, rgbNextTime = 0, depthCurrentTime = 0, depthNextTime = 0, currentTimeDifference = 0,
        nextTimeDifference;

        std::string rgbCurrentLine, depthCurrentLine, rgbNextLine, depthNextLine;

        if (ReadRGB(rgbCurrentLine) == -1 || ReadDepth(depthCurrentLine) == -1) {
            std::cout << "Couldn't sync at end of file" << std::endl;
            return -1;
        }

        GetTime(rgbCurrentLine, rgbCurrentTime);
        GetTime(depthCurrentLine, depthCurrentTime);

        currentTimeDifference = std::abs(rgbCurrentTime - depthCurrentTime);
        while (_tolerance < currentTimeDifference) {

            if ( rgbCurrentTime > depthCurrentTime) {
                //Move depthAhead
                if (PeekLine(_depthFileStream, depthNextLine) == -1) {
                    std::cout << "Couldn't sync at end of file" << std::endl;
                    return -1;
                }
                GetTime(depthNextLine, depthNextTime);

                nextTimeDifference = std::abs(rgbCurrentTime -  depthNextTime);

                if (ReadDepth(depthCurrentLine) == -1) {
                    std::cout << "Couldn't sync at end of file" << std::endl;
                    return -1;
                }
                GetTime(depthCurrentLine, depthCurrentTime);
                if (nextTimeDifference < currentTimeDifference) {
                    //Found better sync point
                } else {
                    //Couldn't sync
                    std::cerr << "Couldn't find better sync Point, move depth time Ahead" << std::endl;
                }
            } else {
                //Move rgbAhead
                if (PeekLine(_rgbFileStream, rgbNextLine) == -1) {
                    std::cout << "Couldn't sync at end of file" << std::endl;
                    return -1;
                }
                GetTime(rgbNextLine, rgbNextTime);

                nextTimeDifference = std::abs(rgbNextTime -  depthCurrentTime);

                if (ReadRGB(rgbCurrentLine) == -1) {
                    std::cout << "Couldn't sync at end of file" << std::endl;
                    return -1;
                }
                GetTime(rgbCurrentLine, rgbCurrentTime);
                if (nextTimeDifference < currentTimeDifference) {
                    //Found better sync point
                } else {
                    //Couldn't sync
                    std::cerr << "Couldn't find better sync Point, move rgb time Ahead" << std::endl;
                }
            }
            currentTimeDifference = std::abs(rgbCurrentTime - depthCurrentTime);
        }

        StripLine(rgbCurrentLine);
        StripLine(depthCurrentLine);
        BuildDirectory(rgbCurrentLine);
        BuildDirectory(depthCurrentLine);
        time = (rgbCurrentTime + depthCurrentTime)/2.;
        rgb = rgbCurrentLine;
        depth = depthCurrentLine;
    }

    int FileParser::ReadSynchronisedGroundTruth(double time, std::string &groundTruth) {
        double currentTime, nextTime, difference, newDifference;
        std::string groundTruthNext;
        if (ReadGroundTruth(groundTruth) != 1) {
            std::cout << "Couldn't sync at end of file" << std::endl;
            return -1;
        }
        GetTime(groundTruth, currentTime);
        difference = std::abs(currentTime - time);
        if (PeekLine(_groundTruthFileStream, groundTruthNext) != 1) {
            std::cout << "End of file giving point" << std::endl;
            return 1;
        }
        GetTime(groundTruthNext, nextTime);
        newDifference = std::abs(nextTime - time);
        while(difference >= newDifference) {
            ReadGroundTruth(groundTruth);
            GetTime(groundTruth, currentTime);
            difference = std::abs(currentTime - time);
            if (PeekLine(_groundTruthFileStream, groundTruthNext) != 1) {
                std::cout << "Couldn't sync at end of file" << std::endl;
                return 1;
            }
            GetTime(groundTruthNext, nextTime);
            newDifference = std::abs(nextTime - time);

        }
        return 1;
    }

    int FileParser::ReadSynchronisedIMU(double time, std::string &imu) {
        double currentTime, nextTime, difference, newDifference;
        std::string imuNext;
        if (ReadIMU(imu) != 1) {
            std::cout << "Couldn't sync at end of file" << std::endl;
            return -1;
        }
        GetTime(imu, currentTime);
        difference = std::abs(currentTime - time);
        while(_tolerance < difference) {
            if(PeekLine(_imuFileStream, imuNext) != 1) {
                std::cout << "Couldn't sync at end of file" << std::endl;
                return -1;
            }
            GetTime(imuNext, nextTime);
            newDifference = std::abs(nextTime - time);
            if ( difference > newDifference) {
                //Better
                if(ReadIMU(imu) != 1) {

                    std::cout << "Logical ERROR" << std::endl;
                    exit(0);
                    return -1;
                }
                GetTime(imu, currentTime);
                difference = std::abs(currentTime - time);
            } else {
                //worse
                std::cerr << "Couldn't find better imu sync point" << std::endl << imu << std::endl;
                break;
            }
        }
        return 1;
    }

    int FileParser::ReadRGB(std::string & rgb) {
        char buffer[512];
        _rgbFileStream.getline(buffer, 512);
        if (_rgbFileStream.eof()) {
            return -1;
        }
        rgb = buffer;
        while(rgb.find('#') == 0) {
            _rgbFileStream.getline(buffer, 512);
            if (_rgbFileStream.eof()) {
                return -1;
            }
            rgb = buffer;
        }
        return 1;
    }

    int FileParser::ReadDepth(std::string & depth) {
        char buffer[512];
        _depthFileStream.getline(buffer, 512);
        if (_depthFileStream.eof()) {
            return -1;
        }
        depth = buffer;
        while( depth.find('#') == 0) {
            _depthFileStream.getline(buffer, 512);
            if (_depthFileStream.eof()) {
                return -1;
            }
            depth = buffer;
        }
        return 1;
    }

    int FileParser::ReadGroundTruth(std::string &groundTruth) {
        char buffer[512];
        _groundTruthFileStream.getline(buffer, 512);
        if (_groundTruthFileStream.eof()) {
            return -1;
        }
        groundTruth = buffer;
        while( groundTruth.find('#') == 0) {
            _groundTruthFileStream.getline(buffer, 512);
            if (_groundTruthFileStream.eof()) {
                return -1;
            }
            groundTruth = buffer;
        }
        return 1;
    }

    int FileParser::ReadIMU(std::string &imu) {
        char buffer[512];
        _imuFileStream.getline(buffer, 512);
        if (_imuFileStream.eof()) {
            return -1;
        }
        imu = buffer;
        while( imu.find('#') == 0) {
            _imuFileStream.getline(buffer, 512);
            if (_imuFileStream.eof()) {
                return -1;
            }
            imu = buffer;
        }
        return 1;
    }

    int FileParser::PeekLine(std::ifstream &stream, std::string &store) {
        std::streampos streamPos = stream.tellg(); // Holds the current stream position
        char buffer[512];
        stream.getline(buffer, 512);
        if (stream.eof()) {
            return -1;
        }
        store = buffer;
        stream.seekg(streamPos);
        return 1;
    }

    void FileParser::GetTime(std::string &in, double &time) {

        std::istringstream iss(in);
        iss >> time;
//        std::cout << "TIME: " << std::fixed << time << std::endl;
    }

    void FileParser::StripLine(std::string & str) {
        str.erase(std::remove(str.begin(), str.end(), '\n'), str.end());
        boost::trim(str);
    }

    void FileParser::BuildDirectory(std::string &imageLine) {
        std::string rebuiltString, temp;
        std::istringstream iss(imageLine);
        iss >> temp;
        rebuiltString = temp + " " + _baseDir;
        iss >> temp;
        rebuiltString += temp;
        imageLine = rebuiltString;
    }


};
















