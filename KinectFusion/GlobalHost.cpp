/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/07/27.
//
#include "DeviceTypes.hpp"
namespace gpu {

    namespace host {
        //General Gpu
        matrix33 *_gpuRotation = 0;
        matrix33 *_gpuInverseRotation = 0;
        float3 *_gpuTranslation = 0;
        matrix33* _gpuPredictedRotation = 0;
        matrix33* _gpuPredictedInverseRotation = 0 ;
        float3* _gpuPredictedTranslation = 0 ;
        matrix33 *_gpuCameraMatrix = 0;

        //Image Stuff
        DepthMap *_gpuDepthMapContainer = 0;
        int rows = 0, cols = 0;
        float *_gpuDepthMap = 0;
        Image *_gpuColourImageContainer = 0;
        uchar* _gpuColourImage = 0;
        int channels = 0;

        //ICP stuff
        int numICPLevels = 0;
        DepthMap* _gpuFilteredSourceDepthMapContainersICP = 0;
        DepthMap* _gpuFilteredTargetDepthMapContainersICP = 0;
        float3* _gpuSourceVertexMap = 0;
        float3* _gpuSourceNormalMap = 0;
        uchar* _gpuSourceColourImage = 0;
        float3* _gpuLocalTargetVertexMap = 0;
        float3* _gpuLocalTargetNormalMap = 0;
        float3* _gpuGlobalTargetVertexMap = 0;
        float3* _gpuGlobalTargetNormalMap = 0;
        float* _gpuFilteredSourceImages = 0;
        float* _gpuFilteredTargetImages = 0;

        float7* _gpuICPEquations = 0;
        bool* _gpuICPMask = 0;
        int* _gpuICPMaskCount = 0;
        float* _gpuICPAstarA = 0;
        float* _gpuICPAstarB = 0;

        float* _gpuICPReductionMatrix = 0;
        int maxReductionBlocks = 0;
        float* _gpuICPError = 0;
        float* _gpuICPErrorBlocks = 0;


        //Tsdf Vars
        int3 *_gpuVoxelCenter = 0;
        int3 *_gpuVoxelGridSize = 0;
        float3 *_gpuVoxelCellSize = 0;
        //tsdfVolume
        int* _gpuTsdfVolume = 0;
        int* _gpuTsdfColourVolume = 0;

        //KinectV2
        int3* _gpuPointIndexSize = 0;
        int* _gpuPointIndex = 0;
        float3* _gpuPointIndexNormal = 0;
        int* _gpuPointIndexColour = 0;

    };
};