/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/07/28.
//

#ifndef VSLAM_GLOBALDEVICE_HPP
#define VSLAM_GLOBALDEVICE_HPP
#include <cuda.h>
#include <cuda_runtime.h>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>
#include <device_functions.h>
#include <math_constants.h>
#include "DeviceTypes.hpp"

namespace gpu {
    namespace device {
        // Indexing Functions
        __device__ inline int Index2D(int x, int y, int xLength) {
            return y * xLength + x;
        }

        /**
         * Takes in positive voxel values, local Voxel only
         */
        __device__ inline int Index3DBasic(int x, int y, int z, int3 *voxelSize) {
//            return x * ((int)voxelSize->y) * ((int)voxelSize->z) + y * ((int)voxelSize->z) + z;
            return x + y * (voxelSize->x) + z * (voxelSize->x * voxelSize->y);
        }
        /**
         * Takes in global Voxel location +/-
         */
        __device__ inline int Index3D(int x, int y, int z, int3* volumeCenter, int3 *volumeSize) {

//            int index = Index3DBasic((x + volumeCenter->x) % volumeSize->x, (y + volumeCenter->y) % volumeSize->y, (z + volumeCenter->z) % volumeSize->z, volumeSize); //TODO Change input to int
//            if(index < 0) {
//                index = volumeSize->x * volumeSize->y * volumeSize->z + index; //TODO This is probably the problem
//            }

            int testx = (x + volumeCenter->x) % volumeSize->x;
            int testy = (y + volumeCenter->y) % volumeSize->y;
            int testz = (z + volumeCenter->z) % volumeSize->z;
            if(testx < 0) {
                testx += volumeSize->x;
            }
            if(testy < 0) {
                testy += volumeSize->y;
            }
            if(testz < 0) {
                testz += volumeSize->z;
            }
            return Index3DBasic(testx, testy, testz, volumeSize);
        }

        //Tsdf Functions
        __device__ static short MAX_SHORT = 32767;
        __device__ inline  short *GetTsdfPointer(int *ptr, int x, int y, int z, int3* volumeCenter, int3 *volumeSize) {
//            int index = (x * ((int) voxelSize->y) * ((int) voxelSize->z) + y * ((int) voxelSize->z) + z);
            int index = Index3D(x, y, z, volumeCenter, volumeSize);
//            printf("Index: %d\n", index);
            short *newptr = (short *) (ptr + index);
            return newptr;
        }
        __device__ inline  void PackTsdf(short *ptr, float weight, float tsdf) {
            (*ptr) = (short) weight;
            (*(ptr + 1)) = (short) fmaxf(-MAX_SHORT, fminf(MAX_SHORT, tsdf * MAX_SHORT));
        }

        __device__ inline  void UnpackTsdf(short *ptr, float &weight, float &tsdf) {
            weight = (float) (*ptr);
            tsdf = ((float) *(ptr + 1)) / MAX_SHORT;
        }
        __device__ inline  float readTsdf(int x, int y, int z, int* ptr, int3* volumeCenter, int3* volumeSize) {
            float tsdf, w;
            UnpackTsdf(GetTsdfPointer(ptr, x, y, z, volumeCenter, volumeSize), w, tsdf);
            return tsdf;
        }
        //Following ARGB
        __device__ inline  int *GetTsdfColourPointer(int *ptr, int x, int y, int z, int3* volumeCenter, int3 *volumeSize) {
//            int index = (x * ((int) voxelSize->y) * ((int) voxelSize->z) + y * ((int) voxelSize->z) + z);
            int index = Index3D(x, y, z, volumeCenter, volumeSize);
            return ptr + index;
        }

        __device__ inline  void PackColourValue(int *ptr, uchar r, uchar g, uchar b) {
            (*ptr) = 0;
            uchar *pack = (uchar *) &(*ptr);
            pack[0] = 0;// Alpha value
            pack[1] = r;
            pack[2] = g;
            pack[3] = b;
        }
        /**
         * Unpacks colour
         */
        __device__ inline  void UnpackColourValue(int *ptr, uchar &r, uchar &g, uchar &b) {
            uchar *unpacked = (uchar *) ptr;
            //a = unpacked[0]
            r = unpacked[1];
            g = unpacked[2];
            b = unpacked[3];
        }

        /**
         * Returns in ARGB format with A = 0
         */
        __device__ inline  int readColourTsdf(int x, int y, int z, int* ptr, int3* volumeCenter, int3* volumeSize) {
            int values = 0;
            uchar* packedValues = ((uchar*)(&values));
            UnpackColourValue(GetTsdfColourPointer(ptr, x, y, z, volumeCenter, volumeSize), packedValues[1], packedValues[2], packedValues[3]);
            return values;
        }



//            __device__ __host__ double ReadTsdf(int x, int y, int z) {
//                float weight, tsdf;
//                UnpackTsdf((short*)&(_voxelGrid[x][y][z]), weight, tsdf);
//                return tsdf;
//            }
//        __device__ inline  float3 VoxelToWorld(int x, int y, int z, float3* _voxelGridSize, float3* _voxelCenter, float3* _voxelCellSize) {
//            float3 voxel(x, y, z);
//            voxel = (voxel - (*_voxelCenter)) * (*_voxelCellSize);
//            return voxel;
//        }
//
//        __device__ inline  float3 VoxelToWorldShiftedToCenter(int x, int y, int z, float3* _voxelGridSize, float3* _voxelCenter, float3* _voxelCellSize) {
//            float3 voxel(x, y, z);
//            float3 shifted(0.5, 0.5, 0.5);
//            voxel = (voxel - (*_voxelCenter) + shifted) * (*_voxelCellSize);
//            return voxel;
//        }
//
//        __device__ inline  int3 WorldToVoxel(float3 world, float3* _voxelGridSize, float3* _voxelCenter, float3* _voxelCellSize) {
//
//            world = (world/(*_voxelCellSize)) + (*_voxelCenter);
////            int3 r(__float2int_rn(world.x), __float2int_rn(world.y), __float2int_rn(world.z));
//            int3 r((int)(world.x), (int)(world.y), (int)(world.z));
//            return r;
//        }
//        __device__ inline  int3 WorldToVoxel(float x, float y, float z, float3* _voxelGridSize, float3* _voxelCenter, float3* _voxelCellSize) {
//            float3 world(x, y, z);
//            return WorldToVoxel(world, _voxelGridSize, _voxelCenter, _voxelCellSize);
//        }
//        __device__ inline int3 GetVoxel(float3& pos, float3* voxelCellSize) {
//            float3 rs;
//            int3 r;
//            rs = pos / (*voxelCellSize);
//            r.x = (int)rs.x;
//            r.y = (int)rs.y;
//            r.z = (int)rs.z;
//            return r;
//        }

        //New Methods
        /**
         * Takes in global Voxel
         */
        __device__ inline  float3 VoxelToWorld(int x, int y, int z, int3* _voxelGridSize, int3* _voxelCenter, float3* _voxelCellSize) {
            float3 voxel(x, y, z);
//            voxel = (voxel + (*_voxelCenter) - ((*_voxelGridSize) * 0.5f)) * (*_voxelCellSize);
            voxel = (voxel) * (*_voxelCellSize);
            return voxel;
        }
        /**
        * Takes in global Voxel
        */
        __device__ inline  float3 VoxelToWorldShiftedToCenter(int x, int y, int z, int3* _voxelGridSize, int3 *_voxelCenter,
                                                              float3 *_voxelCellSize) {
            float3 voxel(x, y, z);
            float3 shifted(0.5, 0.5, 0.5);
//            voxel = (voxel + (*_voxelCenter) + shifted - ((*_voxelGridSize)/2)) * (*_voxelCellSize);
            voxel = (voxel + shifted) * (*_voxelCellSize);
            return voxel;
        }
        /**
         * Takes in World position 'Global' position
         */
        __device__ inline  int3 WorldToVoxel(float3 world, int3 *_voxelGridSize, int3 *_voxelCenter,
                                             float3 *_voxelCellSize) {

            world = (world * (1.  /(*_voxelCellSize)));
//            int3 r(__float2int_rn(world.x), __float2int_rn(world.y), __float2int_rn(world.z));
            int3 r((int)(world.x), (int)(world.y), (int)(world.z));
            return r;
        }
        /*
         *  Takes in world position
         */
        __device__ inline  int3 WorldToVoxel(float x, float y, float z, int3* _voxelGridSize, int3* _voxelCenter, float3* _voxelCellSize) {
            float3 world(x, y, z);
            return WorldToVoxel(world, _voxelGridSize, _voxelCenter, _voxelCellSize);
        }
        /*
         * Takes in local Voxel
         */
        __device__ inline void LocalVoxelToGlobalVoxel(int x, int y, int z, int3* voxelCenter, int3* voxelGridSize, float3* voxelCellSize, int& gx, int& gy, int& gz) {
            // 0,
            gx = x + voxelCenter->x - voxelGridSize->x/2;
            gy = y + voxelCenter->y - voxelGridSize->y/2;
            gz = z + voxelCenter->z - voxelGridSize->z/2;
        }
        __device__ inline int3 LocalVoxelToGlobalVoxel(int3 voxel, int3* voxelCenter, int3* voxelGridSize, float3* voxelCellSize) {
            int3 global;
            LocalVoxelToGlobalVoxel(voxel.x, voxel.y , voxel.z, voxelCenter, voxelGridSize, voxelCellSize, global.x, global.y, global.z);
            return global;
        }
        __device__ inline void GlobalVoxelToLocalVoxel(int gx, int gy, int gz, int3* voxelCenter, int3* voxelGridSize, float3* voxelCellSize, int& x, int& y, int& z) {
            x = gx - voxelCenter->x + voxelGridSize->x/2;
            y = gy - voxelCenter->y + voxelGridSize->y/2;
            z = gz - voxelCenter->z + voxelGridSize->z/2;
        }

//        __device__ inline int3 GetVoxel(float3& pos, float3* voxelCellSize) {
//            float3 rs;
//            int3 r;
//            rs = pos / (*voxelCellSize);
//            r.x = (int)rs.x;
//            r.y = (int)rs.y;
//            r.z = (int)rs.z;
//            return r;
//        }
    };
};
#endif //VSLAM_GLOBALDEVICE_HPP
