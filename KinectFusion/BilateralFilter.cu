/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/07/01.
//

#include "BilateralFilter.hpp"
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <math_functions.h>
#include <iostream>
#include "DeviceTypes.hpp"
#include "GlobalHost.hpp"
#include "DeviceTypes.hpp"
#include "GlobalDevice.hpp"

namespace gpu {
    namespace device {
        namespace BilateralFilter {
            //Finish TODO
            __global__ void ImageResize(DepthMap* depthMap, DepthMap* dst, int level) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;

                if(x >= dst->cols || y >= dst->rows) {
                    return;
                }
//                x = 319;
//                y = 239;
//                x =  97;
//                y = 232;
//                dst->data[Index2D(x, y, cols)] = USHRT_MAX;
//                return;
                int factor = (int)powf(2, level);
//                printf("Factor %d\n", factor);
//                dst->rows = rows;
//                dst->cols = cols;
                int boxSize = factor/2;
//                printf("Box size %d\n", boxSize);
                int xmin = max(0, x * factor - boxSize);
                int ymin = max(0, y * factor - boxSize);
                int xmax = min(depthMap->cols, x * factor + boxSize);
                int ymax = min(depthMap->rows, y * factor + boxSize);
//                printf("Compare x %d %d %d\n", depthMap->cols, x * factor + boxSize, dst->cols);
//                printf("Compare y %d %d %d\n", depthMap->rows, y * factor - boxSize, dst->rows);
                int xpos, ypos;
                float value = 0;
                int counter = 0;
                int index = 0;
//                printf("x stuff %d %d\n", xmin, xmax);
//                printf("y stuff %d %d\n", ymin, ymax);
                for (xpos = xmin; xpos < xmax; xpos++ ) {
                    for(ypos = ymin; ypos < ymax; ypos++) {
                        index = Index2D(xpos, ypos, depthMap->cols);
                        value += depthMap->data[index];
                        counter++;
                    }
                }
//                printf("Struct r %d, c %d\n", rows, cols);
//                printf("Index %d\n", index);
//                printf("Index %d %d\n", xpos, ypos);
//                __syncthreads();
                if (counter > 0) {
                    dst->data[Index2D(x, y, dst->cols)] = (value/ counter);
                } else {
                    printf("This has been hit %d %d?\n", x, y);
                }

            }

            __global__ void BilateralFilterKernel(DepthMap* src, DepthMap* dst) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
//                x = 639;
//                y = 479;
                if(x >= src->cols || y >= src->rows) {
                    return;
                }
//                printf("Max %d %d\n", src->cols, src->rows);
                int blockSize = 13;

                int endRows = min(src->cols, x + blockSize/2 + 1);
                int endCols = min(src->rows, y + blockSize/2 + 1);
                float totalWeightedPixel = 0, totalWeight = 0;
                float sigmaSpace = 0.5f/(4.5f * 4.5f);
                float sigmaDepth = 0.5f/(30.f * 30.f);

//                printf("memory add %p \n", src->data);
                int i, j;
                for (i = max(0, x - blockSize/2); i < endRows; i++ ) {
                    for (j = max(0, y - blockSize/2) ; j < endCols; j++ ) {
//                        std::cout << i  << " " << endRows << std::endl;
//                        std::cout << j  << " " << endCols << std::endl;
//                        exit(0);
                        float euclideanDistance = (x - i) * (x - i) + (y - j ) * (y - j);
                        float depthDifference = (*src)(y, x) - (*src)(j, i);
                        float weight;
//                        printf("SRC %f\n", (*src)(y, x));
//                        d += Gaussian(euclideanDistance, g) * Gaussian(depthDifference, g) * depth(i, j);
                        weight =  __expf( - (euclideanDistance * sigmaSpace + depthDifference * sigmaDepth));
//                        printf("Values %f %f %f %f\n", euclideanDistance, depthDifference, - (euclideanDistance * sigmaSpace + depthDifference * sigmaDepth), weight);
//                        printf("Weight %f %f\n", weight, (euclideanDistance * sigmaSpace + depthDifference * sigmaDepth));
                        totalWeightedPixel += weight * (*src)(j, i);
                        totalWeight += weight;
                    }
                }
//                printf("Other stuff %f %f\n", totalWeight, totalWeightedPixel);
//                printf("Stuff %f\n", (totalWeightedPixel/ totalWeight));
//                printf("Values %f %f\n", totalWeightedPixel, totalWeight);
//                printf("Value %d\n", max(0, min((int)(totalWeightedPixel/ totalWeight), USHRT_MAX)));
                //If the original point is 0 keep it zero
//                if((*src)(y, x) == 0) {
//                    (*dst)(y, x) = 0;
//                } else {
//                    (*dst)(y, x) = (float) max(0, min((int)(totalWeightedPixel/ totalWeight), USHRT_MAX));
//                }
                (*dst)(y, x) = (float) max(0, min((int)(totalWeightedPixel/ totalWeight), USHRT_MAX));


            }
            __global__ void test(DepthMap* ptr) {
                printf("CHECKING VALUES \n");
//                int x;
//                for( x = 0; x < number;x++ ){
                    printf("Dimensions %d %d\n", (*ptr).rows, (*ptr).cols);
//                }

            }


        }
    }
    namespace host {
        namespace BilateralFilter {
            void BilateralFilter() {
                // Seems to work
                dim3 dimBlock = DivUp(GetImageCols(), GetImageRows());
                dim3 dimGrid = GetGrid();
//                dim3 dimBlock(1, 1);
//                dim3 dimGrid(1, 1);
//                printf("Dimensions %d %d %d %d\n", dimBlock.x, dimBlock.y, dimGrid.x, dimGrid.y);
                device::BilateralFilter::BilateralFilterKernel<<<dimBlock, dimGrid>>>(GetGPUDepthMapContainer(), GetGPUTargetDepthMapContainersICP());
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
            }
            void ResizeImageIntoPyramid() {
                int i = 0;
                int r = GetImageRows()/2, c = GetImageCols()/2;
                for(i = 1; i < GetICPLevels(); i++) {
                    dim3 dimBlock = DivUp(c, r);
                    dim3 dimGrid = GetGrid();
                    device::BilateralFilter::ImageResize<<<dimBlock, dimGrid>>>(GetGPUTargetDepthMapContainersICP(), GetGPUTargetDepthMapContainersICP() + i, i);
                    r /= 2;
                    c /= 2;
                    cudaDeviceSynchronize();
                    CudaErrorCheck(__FILE__, __LINE__);
                }

            }

        }
    }
}
