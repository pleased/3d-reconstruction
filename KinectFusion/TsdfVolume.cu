/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/07/19.
//

//#include "TsdfVolume.hpp"
#include <cuda.h>
// includes CUDA Runtime
#include <cuda_runtime_api.h>
#include <device_functions.h>
#include <device_launch_parameters.h>
#include <math_constants.h>
#include <iostream>
#include "DeviceTypes.hpp"
#include "GlobalDevice.hpp"
#include "GlobalHost.hpp"
//Prototypes

namespace gpu {
    namespace device {
        namespace Tsdf{
            __global__ void ProjectiveTsdf(int* tsdfVolume, int* colourTsdfVolume, matrix33* cameraMatrix, matrix33* rotation, matrix33* inverseRotation,
                                           float3* translation, int3* voxelCenter, int3* voxelGridSize,
                                           float3* voxelCellSize, DepthMap* depthTarget, Image* colourImage, float WrK=1.f) {
                float distanceThreshold = 0.1;
//                float maxTruncation = 30; //mm
//                float minTruncation = 3; //mm
                float MAX_WEIGHT = 1 << 7;
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if (x >= voxelGridSize->x|| y >= voxelGridSize->y)
                    return;
                float fx = cameraMatrix->row0.x, fy = cameraMatrix->row1.y, cx = cameraMatrix->row0.z, cy = cameraMatrix->row1.z;
                int z = 0;
                for (z = 0; z < voxelGridSize->z; z++) {
                    int gx, gy, gz;
                    LocalVoxelToGlobalVoxel(x, y, z, voxelCenter, voxelGridSize, voxelCellSize, gx, gy, gz);
//                    float3 worldPos = VoxelToWorldShiftedToCenter(gx, gy, gz, voxelGridSize, voxelCenter, voxelCellSize), worldPosTransformed;
                    float3 worldPos = VoxelToWorldShiftedToCenter(gx, gy, gz, voxelGridSize, voxelCenter, voxelCellSize), worldPosTransformed;
                    worldPosTransformed = (*inverseRotation) * (worldPos - (*translation));
                    int3 img( __float2int_rn(worldPosTransformed.x * fx / worldPosTransformed.z + cx),
                                __float2int_rn(worldPosTransformed.y * fy / worldPosTransformed.z + cy),
                                 1);
//                    if(x == 259 && y == 256 && z == 272) {
//                        printf("Shifted Voxel %f %f %f\n", worldPos.x, worldPos.y, worldPos.z);
//                        printf("Projective TSDF %d %d %f\n", img.x, img.y, worldPosTransformed.z);
//                        printf("eval %d %d %d %d\n", worldPosTransformed.z > 0,
//                                           img.x >= 0 && img.x < depthTarget->cols,
//                                           img.y >= 0 && img.y < depthTarget->rows,
//                                           (*depthTarget)(img.y, img.x) != 0);
//                        printf("Depth value %f\n", (*depthTarget)(img.y, img.x));
//                    }
                    if (worldPosTransformed.z > 0 &&
                            img.x >= 0 && img.x < depthTarget->cols &&
                            img.y >= 0 && img.y < depthTarget->rows &&
                            (*depthTarget)(img.y, img.x) != 0
                        && (*depthTarget)(img.y, img.x) < 3000.f) {
                        float depth = (*depthTarget)(img.y, img.x) / 1000.f;
                        float3 surface(depth * (img.x - cx)/fx,
                                        depth * (img.y - cy)/fy,
                                        depth);
                        surface = (*rotation) * surface + (*translation);
                        //TODO DO NOT UPDATE ALL THE VOXELS Update the voxels that are within a certain distance otherwise this causes interference with other surfaces behind it
                        float sdf = (surface - (*translation)).norm() - (worldPos - (*translation)).norm();
                        if(sdf < -distanceThreshold) {
                            continue;
                        }
//                        if(-sdf < -distanceThreshold) {
//                            continue;
//                        }
//                        if(std::abs(sdf) > distanceThreshold) {
//                            continue;
//                        }
                        float tsdf = 0;
                        if (sdf > 0) {
                            tsdf = fminf(1.f, sdf);
//                            tsdf = fminf(1.f, sdf/maxTruncation);
                        } else {
                            tsdf = fmaxf(-1.f, sdf);
//                            tsdf = fmaxf(-1.f, sdf/minTruncation);
                        }
                        float prevWeight = 0 , prevTsdf = 0, w = 0;

                        gx = gx - voxelCenter->x;
                        gy = gy - voxelCenter->y;
                        gz = gz - voxelCenter->z;
                        UnpackTsdf(GetTsdfPointer(tsdfVolume, gx, gy, gz, voxelCenter, voxelGridSize), prevWeight, prevTsdf);

                        tsdf = (prevTsdf * prevWeight + tsdf * WrK) / (prevWeight + WrK);
                        w = fminf(MAX_WEIGHT, prevWeight + WrK);


                        PackTsdf(GetTsdfPointer(tsdfVolume, gx, gy, gz, voxelCenter, voxelGridSize), w, tsdf);

                        //Colour avg
                        //TODO Colour averaging FIX if the r,g,b values are 0 I MUST NOT TAKE AVERAGE (INITIALSATION) this causes the image to start off dark and slowly come up to colour
                        uchar r, g, b;
                        UnpackColourValue(GetTsdfColourPointer(colourTsdfVolume, gx, gy, gz, voxelCenter, voxelGridSize), r, g, b);
                        uchar* colourPtr = (*colourImage)(img.y, img.x);
                        uchar rr = (uchar)(r/2 + colourPtr[2]/2); //BGR format
                        uchar gg = (uchar)(g/2 + colourPtr[1]/2);
                        uchar bb = (uchar)(b/2 + colourPtr[0]/2);
                        if(r == 0 && g == 0 && b == 0) {
                            rr = (uchar)(colourPtr[2]); //BGR format
                            gg = (uchar)(colourPtr[1]);
                            bb = (uchar)(colourPtr[0]);
                        } else {
                            rr = (uchar)(r/2 + colourPtr[2]/2); //BGR format
                            gg = (uchar)(g/2 + colourPtr[1]/2);
                            bb = (uchar)(b/2 + colourPtr[0]/2);
                        }

                        PackColourValue(GetTsdfColourPointer(colourTsdfVolume, gx, gy, gz, voxelCenter, voxelGridSize), (uchar) rr, (uchar) gg, (uchar) bb);
                    }
                }
            }
            __global__ void ResetVolume(int* tsdfVolume, int* colourVolume, int3* volumeCenter, int3* volumeSize, int3 start, int3 end) {
                int x = blockDim.x * blockIdx.x + threadIdx.x, y = blockDim.y * blockIdx.y + threadIdx.y;
//                printf("%d %d %d\n", x, y, z);
                if(x >= volumeSize->x || y >= volumeSize->y) {
                    return;
                }
                start = LocalVoxelToGlobalVoxel(start, volumeCenter, volumeSize, NULL);
                end = LocalVoxelToGlobalVoxel(end, volumeCenter, volumeSize, NULL);
                start = start - (*volumeCenter);
                end = end - (*volumeCenter);
                int xx, yy, zz;
                yy = x;
                zz = y;
                for(xx = start.x; xx < end.x; xx++) {
                    tsdfVolume[Index3D(xx, yy, zz, volumeCenter, volumeSize)] = 0;
                    colourVolume[Index3D(xx, yy, zz, volumeCenter, volumeSize)] = 0;
                }

                xx = x;
                zz = y;
                for(yy = start.y; yy < end.y; yy++) {
                    tsdfVolume[Index3D(xx, yy, zz, volumeCenter, volumeSize)] = 0;
                    colourVolume[Index3D(xx, yy, zz, volumeCenter, volumeSize)] = 0;
                }

                xx = x;
                yy = y;
                for(zz = start.z; zz < end.z; zz++) {
                    tsdfVolume[Index3D(xx, yy, zz, volumeCenter, volumeSize)] = 0;
                    colourVolume[Index3D(xx, yy, zz, volumeCenter, volumeSize)] = 0;
                }

            }
        };
    };
    namespace host {
        //TODO Move to a general Cuda Header
//        void CudaInitMemoryAssignments() {
//            dim3 dimBlock(1);
//            dim3 dimGrid(1);
//            if(GetGPUDepthMap() == 0 || GetGPUDepthMapContainer() == 0) {
//                printf("Allocated never happens\n" );
//            }
//            std::cout << "Assigning image to data struct" << std::endl;
//            device::CudaInitMemoryAssignments<<<dimGrid, dimBlock>>>(GetGPUDepthMapContainer(), GetGPUDepthMap());
//            cudaDeviceSynchronize();
//            CudaErrorCheck(__FILE__, __LINE__);
//        }
        namespace Tsdf {
            void ProjectiveTSDFKernel(float Wrk) {
//                std::cout << "Starting projective TSDF" << std::endl;
                dim3 dimBlock = DivUp(512, 512);
                dim3 dimGrid = GetGrid();
//                int* array;
//                cudaMalloc((void**)&array, 2 * 2 * 2 * 2 * sizeof(int));
//                cudaMemset(array, 0, 2 * 2 * 2 * 2 * sizeof(int));
//                dim3 dimBlock(1, 1); // Threads
//                dim3 dimGrid(1, 1); //blocks
//                printf("\t\tKernel ProjectiveTsdf\n");
                device::Tsdf::ProjectiveTsdf<<<dimGrid, dimBlock>>>(GetGPUTsdfVolume(), GetGPUTsdfColourVolume(), GetGPUCameraMatrix(), GetGPURotation(), GetGPUInverseRotation(),
                        GetGPUTranslation(), GetGPUVoxelCenter(), GetGPUVoxelGridSize(),
                        GetGPUVoxelCellSize(), GetGPUDepthMapContainer(), GetGPUColourImageContainer(), Wrk);
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
//                int* recv = (int*)calloc(2 * 2 * 2 * 2, sizeof(int));
//                cudaMemcpy(recv, array, sizeof(int) * 2 * 2 * 2 * 2, cudaMemcpyDeviceToHost);
//
//                int i;
//                for(i = 0; i < 2 * 2 * 2 * 2; i++) {
//                    if (recv[i] != -1) {
//                        printf("Pos %d\n", i);
//                    }
//                }
//                printf("Pass ?\n");


//                std::cout << "Ending projective TSDF" << std::endl;
            }

            //KinectV2 Method
            void ResetVolume(int3 start, int3 end) {
                //64
//                std::cout << "WUT" << std::endl;
//                std::cout << "Resetting" << "\x1B[31m" << std::endl;
//                std::cout << start.x << " " << start.y << " " << start.z << std::endl;
//                std::cout << end.x << " " << end.y << " " << end.z << "\x1B[0m" <<std::endl;
                //TODO Make this Generic at some point
                dim3 blocks(512/THREADS, 512/THREADS);
                dim3 threads(THREADS, THREADS);
                gpu::device::Tsdf::ResetVolume<<<blocks, threads>>>(GetGPUTsdfVolume(), GetGPUTsdfColourVolume(), GetGPUVoxelCenter(), GetGPUVoxelGridSize(), start, end);
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
            }
        };
    };
};

