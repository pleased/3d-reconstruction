/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/07/19.
//

#ifndef VSLAM_GLOBALHOST_HPP
#define VSLAM_GLOBALHOST_HPP
#include <cuda.h>
#include <cuda_runtime.h>
#include "DeviceTypes.hpp"
#define THREADS 32
#define TOTALTHREADS THREADS * THREADS * 7
#define REDUCTIONTHREADS 1024
namespace gpu {
    namespace host {

        //Gpu memory location
        //TSDF
        extern matrix33* _gpuRotation ;
        extern matrix33* _gpuInverseRotation ;
        extern float3* _gpuTranslation ;

        extern matrix33* _gpuPredictedRotation ;
        extern matrix33* _gpuPredictedInverseRotation ;
        extern float3* _gpuPredictedTranslation ;



        extern matrix33* _gpuCameraMatrix ;

        //Target Image container
        extern DepthMap* _gpuDepthMapContainer;
        extern float* _gpuDepthMap ;
        extern Image* _gpuColourImageContainer;
        extern uchar* _gpuColourImage;
        extern int rows, cols, channels;

        //ICP
        extern int numICPLevels;
        extern DepthMap* _gpuFilteredSourceDepthMapContainersICP;
        extern DepthMap* _gpuFilteredTargetDepthMapContainersICP;
        extern float* _gpuFilteredSourceImages;//image data
        extern float* _gpuFilteredTargetImages;//image data
        extern float3* _gpuSourceVertexMap;
        extern float3* _gpuSourceNormalMap;
        extern uchar* _gpuSourceColourImage;
        extern float3* _gpuLocalTargetVertexMap;
        extern float3* _gpuLocalTargetNormalMap;
        extern float3* _gpuGlobalTargetVertexMap;
        extern float3* _gpuGlobalTargetNormalMap;
        extern float7* _gpuICPEquations;
        extern bool* _gpuICPMask;
        extern int* _gpuICPMaskCount;
        extern float* _gpuICPAstarA;
        extern float* _gpuICPAstarB;

        extern float* _gpuICPReductionMatrix;
        extern int maxReductionBlocks;
        extern float* _gpuICPError;
        extern float* _gpuICPErrorBlocks;



        //Tsdf Volume
        extern int3* _gpuVoxelCenter ;
        extern int3* _gpuVoxelGridSize ;
        extern float3* _gpuVoxelCellSize ;
        extern int* _gpuTsdfVolume ;
        extern int* _gpuTsdfColourVolume ;
        //KinectV2
        extern int3* _gpuPointIndexSize;
        extern int* _gpuPointIndex;
        extern int* _gpuPointIndexColour;
        extern float3* _gpuPointIndexNormal;

        //RayCaster GpuMemory locations
//        extern float3* _gpuVertexMap ;
//        extern float3* _gpuNormalMap ;

        //Gets
        inline matrix33* GetGPURotation() {
            return _gpuRotation;
        }
        inline matrix33* GetGPUInverseRotation() {
            return _gpuInverseRotation;
        }
        inline float3* GetGPUTranslation() {
            return _gpuTranslation;
        }
        inline matrix33* GetGPUPredictedRotation() {
            return _gpuPredictedRotation;
        }
        inline matrix33* GetGPUPredictedInverseRotation() {
            return _gpuPredictedInverseRotation;
        }
        inline float3* GetGPUPredictedTranslation() {
            return _gpuPredictedTranslation;
        }

        inline int3* GetGPUVoxelGridSize() {
            return _gpuVoxelGridSize;
        }
        inline float3* GetGPUVoxelCellSize() {
            return _gpuVoxelCellSize;
        }
        inline int3* GetGPUVoxelCenter() {
            return _gpuVoxelCenter;
        }
        inline DepthMap* GetGPUDepthMapContainer() {
            return _gpuDepthMapContainer;
        }
        inline float* GetGPUDepthMap() {
            return _gpuDepthMap;
        }
        inline Image* GetGPUColourImageContainer() {
            return _gpuColourImageContainer;
        }
        inline uchar* GetGPUColourImage() {
            return _gpuColourImage;
        }
        inline int GetImageChannels() {
            return channels;
        }
        inline matrix33* GetGPUCameraMatrix() {
            return _gpuCameraMatrix;
        }
        inline int* GetGPUTsdfVolume() {
            return _gpuTsdfVolume;
        }
        inline int* GetGPUTsdfColourVolume() {
            return _gpuTsdfColourVolume;
        }
        inline int GetImageRows() {
            return rows;
        }
        inline int GetImageCols() {
            return cols;
        }

        //ICP
        inline int GetICPLevels() {
            return numICPLevels;
        }
        inline DepthMap* GetGPUSourceDepthMapContainersICP() {
            return _gpuFilteredSourceDepthMapContainersICP;
        }
        inline DepthMap* GetGPUTargetDepthMapContainersICP() {
            return _gpuFilteredTargetDepthMapContainersICP;
        }
        inline float3* GetGPUSourceVertexMap() {
            return _gpuSourceVertexMap;
        }
        inline uchar* GetGPUSourceColourMap() {
            return _gpuSourceColourImage;
        }
        inline float3* GetGPULocalTargetVertexMap() {
            return _gpuLocalTargetVertexMap;
        }
        inline float3* GetGPUSourceNormalMap() {
            return _gpuSourceNormalMap;
        }
        inline float3* GetGPULocalTargetNormalMap() {
            return _gpuLocalTargetNormalMap;
        }
        inline float3* GetGPUGlobalTargetVertexMap() {
            return _gpuGlobalTargetVertexMap;
        }
        inline float3* GetGPUGlobalTargetNormalMap() {
            return _gpuGlobalTargetNormalMap;
        }

        inline float* GetGPUSourceImages() {
            return _gpuFilteredSourceImages;
        }
        inline float* GetGPUTargetImages() {
            return _gpuFilteredTargetImages;
        }
        // 6 by 6 float
        inline float* GetGPUICPMatrixAstarA() {
            return _gpuICPAstarA;
        }
        // 6 by 1 float
        inline float* GetGPUICPMatrixAstarB() {
            return _gpuICPAstarB;
        }

        //6 by 7 float contains AstarA and AstarB
        inline float* GetGPUICPReductionMatrix() {
            return _gpuICPReductionMatrix;
        }
        inline int GetReductionBlocks() {
            return maxReductionBlocks;
        }


//        inline float3* GetGPUVertexMap() {
//            return _gpuVertexMap;
//        }
//        inline float3* GetGPUNormalMap() {
//            return _gpuNormalMap;
//        }
        inline float7* GetGPUICPEquations() {
            return _gpuICPEquations;
        }
        inline bool* GetGPUICPMask() {
            return _gpuICPMask;
        }
        inline int* GetGPUICPMaskCount() {
            return _gpuICPMaskCount;
        }
        inline float* GetGPUICPErrorBlocks() {
            return _gpuICPErrorBlocks;
        }
        inline float* GetGPUICPError() {
            return _gpuICPError;
        }

        //Kinect V2
        inline int3* GetGPUPointIndexGridSize() {
            return _gpuPointIndexSize;
        }
        inline int* GetGPUPointIndex() {
            return _gpuPointIndex;
        }
        inline int* GetGPUPointIndexColour() {
            return _gpuPointIndexColour;
        }
        inline gpu::float3* GetGPUPointIndexNormal() {
            return _gpuPointIndexNormal;
        }

        inline void CudaErrorCheck(std::string file, int line) {
            cudaError err = cudaGetLastError();
            /*  Check for and display Error  */
            if ( cudaSuccess != err )
            {
                fprintf( stderr, "Cuda error in file '%s' in line %i : %s.\n",
                         file.c_str(), line, cudaGetErrorString( err) );
                exit(0);
            }
            err = cudaThreadSynchronize();
            /*  Check for and display Error  */
            if ( cudaSuccess != err )
            {
                fprintf( stderr, "Cuda error in file '%s' in line %i : %s.\n",
                         file.c_str(), line, cudaGetErrorString( err) );
                exit(0);
            }
        }
        //Memory Allocations/Deallocations
        inline void DeallocatePointer(void* ptr) {
            cudaFree(ptr);
        }
        inline matrix33* AllocateMatrix33(){
            matrix33* ptr;
            cudaMalloc((void**) &ptr, sizeof(matrix33));
            CudaErrorCheck(__FILE__, __LINE__);
            return ptr;
        }
        inline float3* AllocateFloat3() {
            float3* ptr;
            cudaMalloc((void**) &ptr, sizeof(float3));
            CudaErrorCheck(__FILE__, __LINE__);
            return ptr;
        }
        inline int3* AllocateInt3() {
            int3* ptr;
            cudaMalloc((void**) &ptr, sizeof(int3));
            CudaErrorCheck(__FILE__, __LINE__);
            return ptr;
        }
        inline void AllocateMemory() {
            //Transformation
            _gpuRotation = AllocateMatrix33();
            _gpuInverseRotation = AllocateMatrix33();
            _gpuTranslation = AllocateFloat3();
            _gpuPredictedRotation = AllocateMatrix33();
            _gpuPredictedInverseRotation = AllocateMatrix33();
            _gpuPredictedTranslation = AllocateFloat3();
            //VolumeProperties
            _gpuVoxelCenter = AllocateInt3();
            _gpuVoxelGridSize = AllocateInt3();
            _gpuVoxelCellSize = AllocateFloat3();
            //Camera
            _gpuCameraMatrix = AllocateMatrix33();
        }
        inline void AllocateTsdf(int x, int y, int z) {
            if ( x <= 0 || y <= 0 || z <= 0) {
                std::cerr << "Error Allocating Tsdf volume of size " << x << " " << y << " " << z << std::endl;
            }
            unsigned int size = (unsigned int) x * y * z;
            std::cout << "size of unsigned is " << size << std::endl;
            cudaMalloc((void**) &_gpuTsdfVolume, size * sizeof(int));
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuTsdfVolume, 0, size * sizeof(int));
            CudaErrorCheck(__FILE__, __LINE__);

            cudaMalloc((void**) &_gpuTsdfColourVolume, size * sizeof(int));
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuTsdfColourVolume, 0, size * sizeof(int));
            CudaErrorCheck(__FILE__, __LINE__);

        }
        inline void AllocateTsdf(int3& s) {
            AllocateTsdf(s.x, s.y, s.z);
        }
        inline void AllocateDepthImage(int rows1, int cols1) {
            rows = rows1;
            cols = cols1;
            int size = rows * cols * sizeof(float);
            cudaMalloc((void**) &_gpuDepthMapContainer, sizeof(DepthMap));
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMalloc((void**) &_gpuDepthMap, size);
            CudaErrorCheck(__FILE__, __LINE__);
        }
        inline void AllocateColourImage() {
            int size = rows * cols * channels;
            cudaMalloc((void**) &_gpuColourImageContainer, sizeof(Image));
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMalloc((void**) &_gpuColourImage, size * sizeof(uchar));
            CudaErrorCheck(__FILE__, __LINE__);
        }

        inline void AllocateRaycastedColourImage() {
            int i;
            int rows1 = rows, cols1 = cols;
            int size = 0;
            for(i = 0; i < numICPLevels; i++) {
                size += rows1 * cols1 * channels;
                rows1 /= 2;
                cols1 /= 2;
            }
            cudaMalloc((void**) &_gpuSourceColourImage, size * sizeof(uchar));
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuSourceColourImage, 0, size * sizeof(uchar));
        }

//        inline void AllocateImageMaps(int rows1, int cols1) {
//            rows = rows1;
//            cols = cols1;
//            int size = rows * cols * sizeof(float3);
//            cudaMalloc((void**) &_gpuVertexMap, size);
//            cudaMemset(_gpuVertexMap, 0, size);
//            CudaErrorCheck(__FILE__, __LINE__);
//            cudaMalloc((void**) &_gpuNormalMap, size);
//            cudaMemset(_gpuNormalMap, 0, size);
//            CudaErrorCheck(__FILE__, __LINE__);
//        }

        inline void AllocateICPMemory(int rows, int cols, int numLevels) {
//            extern DepthMap* _gpuSourceDepthMapContainersICP;
//            extern DepthMap* _gpuTargetDepthMapContainersICP;
//            extern float3* _gpuSourceVertexMap;
//            extern float3* _gpuSourceNormalMap;
//            extern float3* _gpuTargetVertexMap;
//            extern float3* _gpuTargetNormalMap;

            cudaMalloc((void**) &_gpuFilteredSourceDepthMapContainersICP, sizeof(DepthMap) * numLevels);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuFilteredSourceDepthMapContainersICP, 0, sizeof(DepthMap) * numLevels);
            cudaMalloc((void**) &_gpuFilteredTargetDepthMapContainersICP, sizeof(DepthMap) * numLevels);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuFilteredTargetDepthMapContainersICP, 0, sizeof(DepthMap) * numLevels);
            int i = 0;
            int size = 0;
            int r = rows, c = cols;
            for(i = 0; i < numLevels; i++) {
                size += r * c;
                r = r/2;
                c = c/2;
            }
            cudaMalloc((void**) &_gpuSourceVertexMap, sizeof(float3) * size);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuSourceVertexMap, 0, sizeof(float3) * size);

            cudaMalloc((void**) &_gpuSourceNormalMap, sizeof(float3) * size);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuSourceNormalMap, 0, sizeof(float3) * size);

            cudaMalloc((void**) &_gpuLocalTargetVertexMap, sizeof(float3) * size);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuLocalTargetVertexMap, 0, sizeof(float3) * size);

            cudaMalloc((void**) &_gpuLocalTargetNormalMap, sizeof(float3) * size);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuLocalTargetNormalMap, 0, sizeof(float3) * size);

            cudaMalloc((void**) &_gpuGlobalTargetVertexMap, sizeof(float3) * size);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuLocalTargetVertexMap, 0, sizeof(float3) * size);

            cudaMalloc((void**) &_gpuGlobalTargetNormalMap, sizeof(float3) * size);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuLocalTargetNormalMap, 0, sizeof(float3) * size);

            cudaMalloc((void**) &_gpuFilteredSourceImages, sizeof(float) * size);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuFilteredSourceImages, 0, sizeof(float) * size);

            cudaMalloc((void**) &_gpuFilteredTargetImages, sizeof(float) * size);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuFilteredTargetImages, 0, sizeof(float) * size);

            cudaMalloc((void**) &_gpuICPEquations, sizeof(float7) * size);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuICPEquations, 0, sizeof(float7) * size);

            cudaMalloc((void**) &_gpuICPMaskCount, sizeof(int));
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuICPMaskCount, 0, sizeof(int));

            cudaMalloc((void**) &_gpuICPMask, sizeof(bool) * size);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuICPMask, 0, sizeof(bool) * size);

            // TODO Remove AstarA and AstarB
            cudaMalloc((void**) &_gpuICPAstarA, sizeof(float) * 6 * 6);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuICPAstarA, 0, sizeof(float) * 6 * 6);

            cudaMalloc((void**) &_gpuICPAstarB, sizeof(float) * 6);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuICPAstarB, 0, sizeof(float) * 6);

            maxReductionBlocks = (rows * cols) / REDUCTIONTHREADS + 1;
            cudaMalloc((void**) &_gpuICPReductionMatrix, sizeof(float) * 6 * 7 * maxReductionBlocks);
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(_gpuICPReductionMatrix, 0, sizeof(float) * 6 * 7 * maxReductionBlocks);

            cudaMalloc((void**) &_gpuICPError, sizeof(float));
            cudaMemset(_gpuICPError, 0, sizeof(float));

            cudaMalloc((void**) &_gpuICPErrorBlocks, sizeof(float) * maxReductionBlocks);
            cudaMemset(_gpuICPErrorBlocks, 0, sizeof(float) * maxReductionBlocks);

        }
        inline void AllocatePointGenerationBuffer(int3& gridSize) {
            unsigned int size = (unsigned int) gridSize.x * gridSize.y * gridSize.z;
            cudaMalloc((void**) &_gpuPointIndex, size * sizeof(int));
            CudaErrorCheck(__FILE__, __LINE__);

            cudaMalloc((void**) &_gpuPointIndexColour, size * sizeof(int));
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(GetGPUPointIndexColour(), 0, size * sizeof(int));


            cudaMalloc((void**) &_gpuPointIndexNormal, size * sizeof(gpu::float3));
            CudaErrorCheck(__FILE__, __LINE__);
            cudaMemset(GetGPUPointIndexNormal(), 0, size * sizeof(gpu::float3));

        }



        //TODO must be completed
        inline void DeallocateAll() {
            DeallocatePointer(GetGPUCameraMatrix());
            CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPURotation());
            CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUInverseRotation());
            CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUTranslation());
            CudaErrorCheck(__FILE__, __LINE__);

            DeallocatePointer(GetGPUPredictedRotation());
            CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUPredictedInverseRotation());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUPredictedTranslation());CudaErrorCheck(__FILE__, __LINE__);


            //Target Image container
            DeallocatePointer(GetGPUDepthMapContainer());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUDepthMap());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUColourImageContainer());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUColourImage());CudaErrorCheck(__FILE__, __LINE__);

            //ICP
            DeallocatePointer(GetGPUSourceDepthMapContainersICP());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUSourceImages());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUTargetDepthMapContainersICP());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUTargetImages());CudaErrorCheck(__FILE__, __LINE__);

            DeallocatePointer(GetGPUSourceVertexMap());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUSourceNormalMap());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUSourceColourMap());CudaErrorCheck(__FILE__, __LINE__);

            DeallocatePointer(GetGPULocalTargetVertexMap());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPULocalTargetNormalMap());CudaErrorCheck(__FILE__, __LINE__);

            DeallocatePointer(GetGPUGlobalTargetVertexMap());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUGlobalTargetNormalMap());CudaErrorCheck(__FILE__, __LINE__);

            DeallocatePointer(GetGPUICPEquations());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUICPMask());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUICPMaskCount());CudaErrorCheck(__FILE__, __LINE__);

            DeallocatePointer(GetGPUICPMatrixAstarA());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUICPMatrixAstarB());CudaErrorCheck(__FILE__, __LINE__);
//            //Tsdf Volume
            DeallocatePointer(GetGPUVoxelCellSize());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUVoxelGridSize());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUVoxelCenter());CudaErrorCheck(__FILE__, __LINE__);
//            extern int* _gpuTsdfVolume ;
//            extern int* _gpuTsdfColourVolume ;
            DeallocatePointer(GetGPUTsdfVolume());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUTsdfColourVolume());CudaErrorCheck(__FILE__, __LINE__);

//            //KinectV2
            DeallocatePointer(GetGPUPointIndex());CudaErrorCheck(__FILE__, __LINE__);
            DeallocatePointer(GetGPUPointIndexColour());CudaErrorCheck(__FILE__, __LINE__);
//            extern bool* _gpuTsdfBoolVolume;
        }
        //Memory Uploads
        inline void UploadMatrix33(matrix33* src, matrix33* gpuPtr) {
            cudaMemcpy(gpuPtr, src, sizeof(matrix33), cudaMemcpyHostToDevice);
            CudaErrorCheck(__FILE__, __LINE__);
        }
        inline void UploadFloat3(float3 *src, float3 *gpuPtr) {
            cudaMemcpy(gpuPtr, src, sizeof(float3), cudaMemcpyHostToDevice);
            CudaErrorCheck(__FILE__, __LINE__);
        }
        inline void UploadInt3(int3 *src, int3 *gpuPtr) {
            cudaMemcpy(gpuPtr, src, sizeof(int3), cudaMemcpyHostToDevice);
            CudaErrorCheck(__FILE__, __LINE__);
        }

        inline void UploadImageContainer(DepthMap* ptr, DepthMap& depthMap) {
            cudaMemcpy(ptr, &depthMap, sizeof(DepthMap),cudaMemcpyHostToDevice);
            CudaErrorCheck(__FILE__, __LINE__);
        }
        inline void UploadImageData(float *data, int rows, int cols) {
            int size = rows * cols * sizeof(float);
            cudaMemcpy(_gpuDepthMap, data, size, cudaMemcpyHostToDevice);
            CudaErrorCheck(__FILE__, __LINE__);
        }
        inline void UploadColourImageContainer(Image* ptr, Image& image) {
            cudaMemcpy(ptr, &image, sizeof(Image),cudaMemcpyHostToDevice);
            CudaErrorCheck(__FILE__, __LINE__);
        }
        inline void UploadColourImageData(uchar* ptr, int rows, int cols, int channels) {
            int size = rows * cols * channels;
            cudaMemcpy(GetGPUColourImage(), ptr, size, cudaMemcpyHostToDevice);
        }
        inline void UploadTransformation(matrix33& rotationMatrix, matrix33& inverseRotationMatrix, float3& translation,
                                         matrix33* gpuRotation, matrix33* gpuInverseRotation, float3* gpuTranslation) {
            UploadMatrix33(&rotationMatrix, gpuRotation);
            UploadMatrix33(&inverseRotationMatrix, gpuInverseRotation);
            UploadFloat3(&translation, gpuTranslation);
        }

        inline void UploadVolumeProperties(int3& voxelCenter, int3& voxelGridSize, float3& voxelCellSize) {
            UploadInt3(&voxelCenter, _gpuVoxelCenter);
            UploadInt3(&voxelGridSize, _gpuVoxelGridSize);
            UploadFloat3(&voxelCellSize, _gpuVoxelCellSize);
        }
        inline void UploadCamera(matrix33& cameraMatrix) {
            UploadMatrix33(&cameraMatrix, _gpuCameraMatrix);
        }
        inline void UploadColourImage(uchar* ptr, Image& image) {
            cudaMemcpy(ptr, image.data, image.rows * image.cols * image.channels * sizeof(uchar), cudaMemcpyHostToDevice);
            CudaErrorCheck(__FILE__, __LINE__);
        }
        //ICP
        inline void UploadImage(float* ptr, DepthMap& image) {
            cudaMemcpy(ptr, image.data, image.rows * image.cols * sizeof(float), cudaMemcpyHostToDevice);
            CudaErrorCheck(__FILE__, __LINE__);
        }
        inline void SetupICP(int rows, int cols, int numLevels) {
            int i = 0;
//            int size = 0;
            int skip = 0;
            int r = rows, c = cols;
            DepthMap baseContainer;
            baseContainer.allocated = true;
            for(i = 0; i < numLevels; i++) {
                baseContainer.rows = r;
                baseContainer.cols = c;
                printf("Allocating with r %d c %d\n", r, c);
                baseContainer.data = GetGPUSourceImages() + skip;
                UploadImageContainer(GetGPUSourceDepthMapContainersICP() + i, baseContainer);
                //upload
                baseContainer.data = GetGPUTargetImages() + skip;
                UploadImageContainer(GetGPUTargetDepthMapContainersICP() + i, baseContainer);
                //upload
                printf("Memaddress %p \n", baseContainer.data);
                skip += r * c;
                r = r/2;
                c = c/2;
            }
            baseContainer.allocated = false; // to prevent the destructor from free'ing since all the memory is technically on the gpu
        }
        //
        inline void Download(void* src, void* dst, int bytes) {
            cudaMemcpy(dst, src, bytes,cudaMemcpyDeviceToHost);
        }

        inline dim3 DivUp(int x, int y) {
            return dim3((unsigned int)(x/THREADS) + 1, (unsigned int)(y/THREADS) + 1);
        }
        inline dim3 GetGrid() {
            return dim3(THREADS, THREADS);
        }
        //TODO Release Memory of volumeProperties and camera


        //Swap
        inline void SwitchSourceAndTarget() {
            printf("Switch not implemented\n");
            //Switch DepthMap containers
            //TODO Shouldn't have to switch since we are using raycaster
//            DepthMap* temp = _gpuFilteredSourceDepthMapContainersICP;
//            _gpuFilteredSourceDepthMapContainersICP = _gpuFilteredTargetDepthMapContainersICP;
//            _gpuFilteredTargetDepthMapContainersICP = temp;
//
//
//
//            float* tempFloat = _gpuFilteredSourceImages;
//            _gpuFilteredSourceImages = _gpuFilteredTargetImages;
//            _gpuFilteredTargetImages = tempFloat;
//
//            float3* tempFloat3 = _gpuSourceVertexMap;
//            _gpuSourceVertexMap = _gpuGlobalTargetVertexMap;
//            _gpuGlobalTargetVertexMap = tempFloat3;
//
//            tempFloat3 = _gpuSourceNormalMap;
//            _gpuSourceNormalMap = _gpuGlobalTargetNormalMap;
//            _gpuGlobalTargetNormalMap = tempFloat3;






        }


        //Entry
        inline void init(matrix33& cameraMatrix, matrix33& rotation, matrix33& inverseRotation,
                         float3& translation, int3& voxelCenter, int3& voxelGridSize,
                         float3& voxelCellSize, int3& pointIndexGridSize, int rows, int cols, int colourChannels, int numLevels) {
            numICPLevels = numLevels;
            AllocateMemory();
            UploadTransformation(rotation, inverseRotation, translation, GetGPURotation(), GetGPUInverseRotation(), GetGPUTranslation());
            UploadCamera(cameraMatrix);
            UploadVolumeProperties(voxelCenter, voxelGridSize, voxelCellSize);
            AllocateDepthImage(rows, cols);
            DepthMap depthMap;
            depthMap.rows = rows;
            depthMap.cols = cols;
            depthMap.data = GetGPUDepthMap();
            UploadImageContainer(_gpuDepthMapContainer, depthMap);
            AllocateTsdf(voxelGridSize);

            //Colour image upload
            channels = colourChannels;
            std::cout << channels << " " << colourChannels << std::endl;
            AllocateColourImage();
            AllocateRaycastedColourImage();
            Image img;
            img.rows = rows;
            img.cols = cols;
            img.allocated = true;
            img.channels = colourChannels;
            img.data = GetGPUColourImage();
            UploadColourImageContainer(GetGPUColourImageContainer(), img);
            img.allocated = false;

            if(GetGPUDepthMap() == 0) {
                printf("Error 1\n");
            } else if (GetGPUDepthMapContainer() == 0) {
                printf("Error 2\n");
            } else if (GetGPUVoxelCellSize() == 0) {
                printf("Error 3\n");
            } else if (GetGPUVoxelGridSize() == 0) {
                printf("Error 4\n");
            } else if (GetGPUVoxelCenter() == 0) {
                printf("Error 5\n");
            } else if (GetGPUCameraMatrix() == 0) {
                printf("Error 6\n");
            } else if (GetGPUInverseRotation() == 0) {
                printf("Error 7\n");
            } else if (GetGPURotation() == 0) {
                printf("Error 8\n");
            } else if (GetGPUTranslation() == 0) {
                printf("Error 9\n");
            } else if(GetGPUTsdfVolume() == 0 ) {
                printf("Error 10\n");

            }

            //Allocations for RayCaster
//            AllocateImageMaps(rows, cols);


            //ICP

            AllocateICPMemory(rows, cols, numICPLevels);
            UploadTransformation(rotation, inverseRotation, translation,
                                 GetGPUPredictedRotation(), GetGPUPredictedInverseRotation(), GetGPUPredictedTranslation());
            SetupICP(rows, cols, numICPLevels);


            //Kinect V2
            _gpuPointIndexSize = AllocateInt3();
            cudaMemcpy(GetGPUPointIndexGridSize(), &pointIndexGridSize, sizeof(int3), cudaMemcpyHostToDevice);
            AllocatePointGenerationBuffer(pointIndexGridSize);
        }
        extern float* v;

        inline void _test() {
            cudaMalloc((void**)&v, sizeof(float));
            float test = 12345.123f;
            cudaMemcpy(v, (void*)&test, sizeof(float), cudaMemcpyHostToDevice);

            float download =0;

            cudaMemcpy((void*)&download, (void*) v, sizeof(float), cudaMemcpyDeviceToHost);
            printf("Downloaded %f \n", download);
        }

    }
}
#endif //VSLAM_GLOBALHOST_HPP
