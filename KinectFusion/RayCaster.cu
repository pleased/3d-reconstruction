/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/07/20.
//

#include "RayCaster.hpp"
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <math_functions.h>
#include <iostream>
#include "DeviceTypes.hpp"
#include "GlobalHost.hpp"
#include "GlobalDevice.hpp"
//#include "GlobalDevice.hpp"

namespace gpu {
    namespace device {

        namespace RayCaster {


            // Volume max is distance in meters
            // if this is negative it means that the camera is inside the volume
            // later we will take the minimum between this and 0 to ensure it is greater or equal to 0
            __device__ float GetMinTime (const float3& volumeMin, const float3& volume_max, const float3& rayStart, const float3& rayDir)
            {
                float txmin = ( (rayDir.x > 0 ? volumeMin.x : volume_max.x) - rayStart.x) / rayDir.x;
                float tymin = ( (rayDir.y > 0 ? volumeMin.y : volume_max.y) - rayStart.y) / rayDir.y;
                float tzmin = ( (rayDir.z > 0 ? volumeMin.z : volume_max.z) - rayStart.z) / rayDir.z;
                return fmaxf ( fmaxf (txmin, tymin), tzmin);
            }
            // Volume max is distance in meters
            __device__ float GetMaxTime (const float3& volumeMin, const float3& volume_max, const float3& rayStart, const float3& rayDir)
            {
                float txmax = ( (rayDir.x > 0 ? volume_max.x :  volumeMin.x) - rayStart.x) / rayDir.x;
                float tymax = ( (rayDir.y > 0 ? volume_max.y :  volumeMin.y) - rayStart.y) / rayDir.y;
                float tzmax = ( (rayDir.z > 0 ? volume_max.z :  volumeMin.z) - rayStart.z) / rayDir.z;
                return fminf (fminf (txmax, tymax), tzmax);
            }
            __device__ float3 GetNextRay(int x, int y, float fx, float fy, float cx, float cy) {
                float3 nextRay;
                nextRay.x = (x - cx)/ fx;
                nextRay.y = (y - cy)/ fy;
                nextRay.z = 1;
                return nextRay;
            }
            __device__ float InterpolateTrilineary(int* volumePtr, float3& point, int3* voxelCenter, float3* voxelCellSize, int3* voxelGridSize, int x=-1, int y=-1) {
                int3 g = WorldToVoxel(point.x, point.y, point.z, voxelGridSize, voxelCenter, voxelCellSize);
//                if(x == 277 && y == 371) {
//                    printf("Voxel index %d %d %d\n", g.x, g.y, g.z);
//                }
//                float3 worldFromVoxel = VoxelToWorld(g.x, g.y, g.z, voxelGridSize, voxelCenter, voxelCellSize);
                int3 l ;
                GlobalVoxelToLocalVoxel(g.x, g.y, g.z, voxelCenter, voxelGridSize, voxelCellSize, l.x, l.y, l.z);
//                if (g.x < 0 || g.x >= voxelGridSize->x ||g.y < 0 || g.y >= voxelGridSize->z ||g.z < 0 || g.z >= voxelGridSize->z ) {
                if (l.x < 0 || l.x >= voxelGridSize->x ||l.y < 0 || l.y >= voxelGridSize->y ||l.z < 0 || l.z >= voxelGridSize->z ) {
                    return NAN;
                }
//                if(x == 25 && y == 320) {
//                    printf("Interpolator point %f %f %f\n", point.x, point.y, point.z);
//                    printf("G value %d %d %d\n", g.x, g.y, g.z);
//                }
                float vx = (g.x + 0.5f) * voxelCellSize->x;
                float vy = (g.y + 0.5f) * voxelCellSize->y;
                float vz = (g.z + 0.5f) * voxelCellSize->z;
//                if(x == 25 && y == 320) {
//                    printf("G value %d %d %d\n", g.x, g.y, g.z);
//                    printf("G value Corrected%d %d %d\n", g.x - voxelCenter->x, g.y - voxelCenter->y, g.z - voxelCenter->z);
//                    printf("Values before corr %f\n", readTsdf(g.x - voxelCenter->x, g.y - voxelCenter->y, g.z - voxelCenter->z, volumePtr, voxelCenter, voxelGridSize));
//                    printf("Values before init %f\n", readTsdf(g.x, g.y, g.z, volumePtr, voxelCenter, voxelGridSize));
//                }
                g.x = (point.x < vx) ? (g.x - 1) : g.x;
                g.y = (point.y < vy) ? (g.y - 1) : g.y;
                g.z = (point.z < vz) ? (g.z - 1) : g.z;
                float a = (point.x - (g.x + 0.5f) * voxelCellSize->x) / voxelCellSize->x;
                float b = (point.y - (g.y + 0.5f) * voxelCellSize->y) / voxelCellSize->y;
                float c = (point.z - (g.z + 0.5f) * voxelCellSize->z) / voxelCellSize->z;

                //Use world From voxel in place of g.x
//                float a = (point.x - worldFromVoxel.x) / voxelCellSize->x;
//                float b = (point.y - worldFromVoxel.y) / voxelCellSize->y;
//                float c = (point.z - worldFromVoxel.z) / voxelCellSize->z;
//                if(x == 25 && y == 320) {
//                    printf("Interpolation values %f\n", readTsdf(g.x, g.y, g.z, volumePtr, voxelCenter, voxelGridSize));
//                }
                g = g - (*voxelCenter);

                float res = readTsdf (g.x + 0, g.y + 0, g.z + 0, volumePtr, voxelCenter, voxelGridSize) * (1 - a) * (1 - b) * (1 - c) +
                            readTsdf (g.x + 0, g.y + 0, g.z + 1, volumePtr, voxelCenter, voxelGridSize) * (1 - a) * (1 - b) * c +
                            readTsdf (g.x + 0, g.y + 1, g.z + 0, volumePtr, voxelCenter, voxelGridSize) * (1 - a) * b * (1 - c) +
                            readTsdf (g.x + 0, g.y + 1, g.z + 1, volumePtr, voxelCenter, voxelGridSize) * (1 - a) * b * c +
                            readTsdf (g.x + 1, g.y + 0, g.z + 0, volumePtr, voxelCenter, voxelGridSize) * a * (1 - b) * (1 - c) +
                            readTsdf (g.x + 1, g.y + 0, g.z + 1, volumePtr, voxelCenter, voxelGridSize) * a * (1 - b) * c +
                            readTsdf (g.x + 1, g.y + 1, g.z + 0, volumePtr, voxelCenter, voxelGridSize) * a * b * (1 - c) +
                            readTsdf (g.x + 1, g.y + 1, g.z + 1, volumePtr, voxelCenter, voxelGridSize) * a * b * c;

//                if(x == 277 && y == 371) {
//                    printf("Interpolated Values\n %f\n%f\n%f\n%f\n%f\n%f\n%f\n%f\n",
//                           readTsdf (g.x + 0, g.y + 0, g.z + 0, volumePtr, voxelCenter, voxelGridSize),
//                           readTsdf (g.x + 0, g.y + 0, g.z + 1, volumePtr, voxelCenter, voxelGridSize),
//                           readTsdf (g.x + 0, g.y + 1, g.z + 0, volumePtr, voxelCenter, voxelGridSize),
//                           readTsdf (g.x + 0, g.y + 1, g.z + 1, volumePtr, voxelCenter, voxelGridSize),
//                           readTsdf (g.x + 1, g.y + 0, g.z + 0, volumePtr, voxelCenter, voxelGridSize),
//                           readTsdf (g.x + 1, g.y + 0, g.z + 1, volumePtr, voxelCenter, voxelGridSize),
//                           readTsdf (g.x + 1, g.y + 1, g.z + 0, volumePtr, voxelCenter, voxelGridSize),
//                           readTsdf (g.x + 1, g.y + 1, g.z + 1, volumePtr, voxelCenter, voxelGridSize));
//                    printf("res: %f\n", res);
//                }
//                if(x == 25 && y == 320) {
//                    printf("A: %f\tB: %f\tC:%f\n", a, b, c);
//                    printf("Interpolated Total Value %f\n", res);
//                }

                return res;
            }
            __device__ int InterpolateColourTrilineary(int* volumePtr, float3& point, int3* voxelCenter, float3* voxelCellSize, int3* voxelGridSize) {
                int3 g = WorldToVoxel(point.x, point.y, point.z, voxelGridSize, voxelCenter, voxelCellSize);
//                float3 worldFromVoxel = VoxelToWorld(g.x, g.y, g.z, voxelGridSize, voxelCenter, voxelCellSize);
                int3 l ;
                GlobalVoxelToLocalVoxel(g.x, g.y, g.z, voxelCenter, voxelGridSize, voxelCellSize, l.x, l.y, l.z);
//                if (g.x < 0 || g.x >= voxelGridSize->x ||g.y < 0 || g.y >= voxelGridSize->z ||g.z < 0 || g.z >= voxelGridSize->z ) {
                if (l.x < 0 || l.x >= voxelGridSize->x ||l.y < 0 || l.y >= voxelGridSize->y ||l.z < 0 || l.z >= voxelGridSize->z ) {
                    return 0;
                }


                float vx = (g.x + 0.5f) * voxelCellSize->x;
                float vy = (g.y + 0.5f) * voxelCellSize->y;
                float vz = (g.z + 0.5f) * voxelCellSize->z;

                g.x = (point.x < vx) ? (g.x - 1) : g.x;
                g.y = (point.y < vy) ? (g.y - 1) : g.y;
                g.z = (point.z < vz) ? (g.z - 1) : g.z;



                float ra = (point.x - (g.x + 0.5f) * voxelCellSize->x) / voxelCellSize->x;
                float rb = (point.y - (g.y + 0.5f) * voxelCellSize->y) / voxelCellSize->y;
                float rc = (point.z - (g.z + 0.5f) * voxelCellSize->z) / voxelCellSize->z;
//                float ra = (point.x - worldFromVoxel.x) / voxelCellSize->x;
//                float rb = (point.y - worldFromVoxel.y) / voxelCellSize->y;
//                float rc = (point.z - worldFromVoxel.z) / voxelCellSize->z;

                g = g - (*voxelCenter);
                int zero   = readColourTsdf (g.x + 0, g.y + 0, g.z + 0, volumePtr, voxelCenter, voxelGridSize);
                uchar* zerou = (uchar*) &zero;

                int xyz1   = readColourTsdf (g.x + 0, g.y + 0, g.z + 1, volumePtr, voxelCenter, voxelGridSize);
                uchar* xyz1u = (uchar*) &xyz1;

                int xy1z   = readColourTsdf (g.x + 0, g.y + 1, g.z + 0, volumePtr, voxelCenter, voxelGridSize);
                uchar* xy1zu = (uchar*) &xy1z;

                int xy1z1  = readColourTsdf (g.x + 0, g.y + 1, g.z + 1, volumePtr, voxelCenter, voxelGridSize);
                uchar* xy1z1u = (uchar*) &xy1z1;

                int x1yz   = readColourTsdf (g.x + 1, g.y + 0, g.z + 0, volumePtr, voxelCenter, voxelGridSize);
                uchar* x1yzu = (uchar*) &x1yz;

                int x1yz1  = readColourTsdf (g.x + 1, g.y + 0, g.z + 1, volumePtr, voxelCenter, voxelGridSize);
                uchar* x1yz1u = (uchar*) &x1yz1;

                int x1y1z  = readColourTsdf (g.x + 1, g.y + 1, g.z + 0, volumePtr, voxelCenter, voxelGridSize);
                uchar* x1y1zu = (uchar*) &x1y1z;

                int x1y1z1 = readColourTsdf (g.x + 1, g.y + 1, g.z + 1, volumePtr, voxelCenter, voxelGridSize);
                uchar* x1y1z1u = (uchar*) &x1y1z1;



                uchar red = (uchar) (  zerou[1] * (1 - ra) * (1 - rb) * (1 - rc) +
                                       xyz1u[1] * (1 - ra) * (1 - rb) * rc +
                                       xy1zu[1] * (1 - ra) * rb * (1 - rc) +
                                       xy1z1u[1] * (1 - ra) * rb * rc +
                                       x1yzu[1]* ra * (1 - rb) * (1 - rc) +
                                       x1yz1u[1] * ra * (1 - rb) * rc +
                                       x1y1zu[1] * ra * rb * (1 - rc) +
                                       x1y1z1u[1] * ra * rb * rc);
                uchar green = (uchar) (  zerou[2] * (1 - ra) * (1 - rb) * (1 - rc) +
                                       xyz1u[2] * (1 - ra) * (1 - rb) * rc +
                                       xy1zu[2] * (1 - ra) * rb * (1 - rc) +
                                       xy1z1u[2] * (1 - ra) * rb * rc +
                                       x1yzu[2]* ra * (1 - rb) * (1 - rc) +
                                       x1yz1u[2] * ra * (1 - rb) * rc +
                                       x1y1zu[2] * ra * rb * (1 - rc) +
                                       x1y1z1u[2] * ra * rb * rc);
                uchar blue = (uchar) (  zerou[3] * (1 - ra) * (1 - rb) * (1 - rc) +
                                       xyz1u[3] * (1 - ra) * (1 - rb) * rc +
                                       xy1zu[3] * (1 - ra) * rb * (1 - rc) +
                                       xy1z1u[3] * (1 - ra) * rb * rc +
                                       x1yzu[3]* ra * (1 - rb) * (1 - rc) +
                                       x1yz1u[3] * ra * (1 - rb) * rc +
                                       x1y1zu[3] * ra * rb * (1 - rc) +
                                       x1y1z1u[3] * ra * rb * rc);
//                red = ((uchar*)&zero)[1];
//                green =  0;
//                blue = 0;
                int rgb = 0;
                uchar* ptr = (uchar*) &rgb;
                ptr[0] = 0;
                ptr[1] = red;
                ptr[2] = green;
                ptr[3] = blue;
                return rgb;
            }

            __device__ bool InsideVolume(int x, int y, int z, int3* voxelGridSize) {
                return (x >= 0 && x < voxelGridSize->x && y >= 0 && y < voxelGridSize->y && z >= 0 && z < voxelGridSize->z);
            }


            __global__ void RayCaster(int* ptr, int* colourVolumePtr, int rows, int cols, matrix33* cameraMatrix, matrix33* rotation, matrix33* inverseRotation,
                                      float3* translation, int3* voxelGridSize, float3* voxelCellSize, int3* voxelCenter, float3* vertexMap, float3* normalMap,
                                      uchar* colourMap, int level) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if(x >= cols || y >= rows) {
                    return;
                }
                bool interpolate = false;
                float3 nan(NAN, NAN, NAN);
                colourMap[Index2D(x, y, cols) * 3 + 0] = 0;
                colourMap[Index2D(x, y, cols) * 3 + 1] = 0;
                colourMap[Index2D(x, y, cols) * 3 + 2] = 0;
                normalMap[Index2D(x, y, cols)] = nan;
                vertexMap[Index2D(x, y, cols)] = nan;
                float scale = powf(2, -level);
                float fx = (*cameraMatrix)(0, 0) * scale, fy = (*cameraMatrix)(1, 1) * scale, cx = (*cameraMatrix)(0, 2) * scale, cy = (*cameraMatrix)(1, 2) * scale;
                int3 globalMin, globalMax;
                LocalVoxelToGlobalVoxel(0, 0, 0, voxelCenter, voxelGridSize, voxelCellSize, globalMin.x, globalMin.y, globalMin.z);
                LocalVoxelToGlobalVoxel(voxelGridSize->x - 1, voxelGridSize->y - 1, voxelGridSize->z - 1, voxelCenter, voxelGridSize, voxelCellSize,
                                        globalMax.x, globalMax.y, globalMax.z);
                float3 volumeMax = VoxelToWorld(globalMax.x, globalMax.y, globalMax.z, voxelGridSize, voxelCenter, voxelCellSize);
                float3 volumeMin = VoxelToWorld(globalMin.x, globalMin.y, globalMin.z, voxelGridSize, voxelCenter, voxelCellSize);

                float3 rayStart, rayNext, rayDir;
                rayStart = (*translation);
                rayNext = ((*rotation) * GetNextRay(x, y, fx, fy, cx, cy))+ (*translation);
                rayDir = (rayNext - rayStart).normalized(); // Normalized to 1 meter
                if(rayDir.x == 0.f) {
                    rayDir.x = 1e-15;
                } if(rayDir.y == 0.f) {
                    rayDir.y = 1e-15;
                } if(rayDir.z == 0.f) {
                    rayDir.z = 1e-15;
                }

                float startTime = GetMinTime(volumeMin, volumeMax, rayStart, rayDir);
                float endTime = GetMaxTime(volumeMin, volumeMax, rayStart, rayDir);
                startTime = fmaxf(startTime, 0.f); // required so that if we are in the volume we start with 0 + step
                if(startTime >= endTime) {
                    return;
                }
                float time = startTime;

                float3 ray = rayStart + rayDir * time;
//                int3 g = GetVoxel(ray, voxelCellSize);// TODO in testing Testing
                int3 g = WorldToVoxel(ray, voxelGridSize, voxelCenter, voxelCellSize) , l;
                GlobalVoxelToLocalVoxel(g.x, g.y, g.z, voxelCenter, voxelGridSize, voxelCellSize, l.x, l.y, l.z);
                l.x = max(0, min((voxelGridSize->x) - 1, l.x));
                l.y = max(0, min((voxelGridSize->y) - 1, l.y));
                l.z = max(0, min((voxelGridSize->z) - 1, l.z));
                g = LocalVoxelToGlobalVoxel(l, voxelCenter, voxelGridSize, voxelCellSize) - (*voxelCenter);
                g = g - (*voxelCenter);
                float tsdf = readTsdf(g.x, g.y, g.z, ptr, voxelCenter, voxelGridSize); //TODO
                float prevTsdf = 0;
                const float maxTime = endTime;
                const float timeStep = 0.01;
                //Raycast here
                int iteration = 0;
                for(; time < maxTime; time += timeStep, iteration++) {
//                    25 320
                    prevTsdf = tsdf;
                    ray = rayStart + rayDir * (time + timeStep);
//                    if(x == 25 && y == 320) {
//                        printf("Ray start Pos %f %f %f Previous value: %f Current value: %f\n", ray.x, ray.y, ray.z, prevTsdf, tsdf);
//                    }
//                    g = GetVoxel(ray, voxelCellSize);//TODO in testing Testing
                    g = WorldToVoxel(ray, voxelGridSize, voxelCenter, voxelCellSize) ; // TODO
                    GlobalVoxelToLocalVoxel(g.x, g.y, g.z, voxelCenter, voxelGridSize, voxelCellSize, l.x, l.y, l.z);
                    g = g - (*voxelCenter);
                    if (!InsideVolume(l.x, l.y, l.z, voxelGridSize)) { //TODO
//                        printf("Failed at pos %d %d \n", x, y);
//                        if(x == 433 && y == 283) {
//                            printf("Outsied volume\n");
//                        }
                        break;
                    }
                    tsdf = readTsdf(g.x, g.y, g.z, ptr, voxelCenter, voxelGridSize); //TODO


                    if ( prevTsdf < 0.f && tsdf > 0.f){
                        break;
                    }

                    if ( tsdf < 0.f) {
                        float3 prevRay = rayStart + rayDir * time;


                        float ts = time;
                        if(interpolate) {
                            float ft = InterpolateTrilineary (ptr, prevRay, voxelCenter, voxelCellSize, voxelGridSize, x, y);     //f_{t}
                            float ftdt = InterpolateTrilineary (ptr, ray, voxelCenter, voxelCellSize, voxelGridSize, x, y); //f_{t+dt}
                            if(isnan(ft) || isnan(ftdt)) {
                                break;
                            }
                            ts = ts - timeStep * ft / (ftdt - ft);
                        }
                        vertexMap[Index2D(x, y, cols)] = rayStart + rayDir * (ts);
                        int3 g = WorldToVoxel(prevRay, voxelGridSize, voxelCenter, voxelCellSize); //TODO
                        GlobalVoxelToLocalVoxel(g.x, g.y, g.z, voxelCenter, voxelGridSize, voxelCellSize, l.x, l.y, l.z);
                        g = g - (*voxelCenter);
                        float3 pos;
                        if (l.x > 1 && l.y > 1 && l.z > 1 && l.x < voxelGridSize->x - 2 && l.y < voxelGridSize->y - 2 && l.z < voxelGridSize->z - 2) {
                            if(interpolate) {
                                pos = vertexMap[Index2D(x, y, cols)];
                                pos.x += voxelCellSize->x;
                                float x1 = InterpolateTrilineary(ptr, pos, voxelCenter, voxelCellSize, voxelGridSize);
                                pos = vertexMap[Index2D(x, y, cols)];
                                pos.x -= voxelCellSize->x;
                                float x2 = InterpolateTrilineary(ptr, pos, voxelCenter, voxelCellSize, voxelGridSize);

                                pos = vertexMap[Index2D(x, y, cols)];
                                pos.y += voxelCellSize->y;
                                float y1 = InterpolateTrilineary(ptr, pos, voxelCenter, voxelCellSize, voxelGridSize);

                                pos = vertexMap[Index2D(x, y, cols)];
                                pos.y -= voxelCellSize->y;
                                float y2 = InterpolateTrilineary(ptr, pos, voxelCenter, voxelCellSize, voxelGridSize);

                                pos = vertexMap[Index2D(x, y, cols)];
                                pos.z += voxelCellSize->z;
                                float z1 = InterpolateTrilineary(ptr, pos, voxelCenter, voxelCellSize, voxelGridSize);

                                pos = vertexMap[Index2D(x, y, cols)];
                                pos.z -= voxelCellSize->z;
                                float z2 = InterpolateTrilineary(ptr, pos, voxelCenter, voxelCellSize, voxelGridSize);

                                normalMap[Index2D(x, y, cols)].x = x1 - x2;
                                normalMap[Index2D(x, y, cols)].y = y1 - y2;
                                normalMap[Index2D(x, y, cols)].z = z1 - z2;
                            } else {
                                normalMap[Index2D(x, y, cols)].x = readTsdf(g.x + 1, g.y, g.z, ptr, voxelCenter, voxelGridSize) - readTsdf(g.x - 1, g.y, g.z, ptr, voxelCenter, voxelGridSize);
                                normalMap[Index2D(x, y, cols)].y = readTsdf(g.x, g.y + 1, g.z, ptr, voxelCenter, voxelGridSize) - readTsdf(g.x, g.y - 1, g.z, ptr, voxelCenter, voxelGridSize);
                                normalMap[Index2D(x, y, cols)].z = readTsdf(g.x, g.y, g.z + 1, ptr, voxelCenter, voxelGridSize) - readTsdf(g.x, g.y, g.z - 1, ptr, voxelCenter, voxelGridSize);
                            }
                            normalMap[Index2D(x, y, cols)] = normalMap[Index2D(x, y, cols)].normalized();
//                            printf("norm %f %f %f\n", normalMap[y * cols + x].x, normalMap[y * cols + x].y, normalMap[y * cols + x].z);
                        }

//                        float interpolatedValue = InterpolateTrilineary(ptr, ray, voxelCenter, voxelCellSize, voxelGridSize);
//                        printf("Interpolated %f\n", interpolatedValue);

                        //Colour volume stuff
                        int colour = readColourTsdf(g.x, g.y, g.z, colourVolumePtr, voxelCenter, voxelGridSize);
                        if(interpolate) {
                            colour = InterpolateColourTrilineary(colourVolumePtr, prevRay, voxelCenter, voxelCellSize, voxelGridSize); //TODO Fix Interpolation then use it
                        }
                        uchar* uColour = (uchar*) &colour;
                        colourMap[Index2D(x, y, cols) * 3 + 0] = uColour[1];
                        colourMap[Index2D(x, y, cols) * 3 + 1] = uColour[2];
                        colourMap[Index2D(x, y, cols) * 3 + 2] = uColour[3];
                        break;
                    }
                }
            }


            __global__ void RayCasterBySection(int* tsdf, bool* volume, int3 start, int3 end, int3* volumeCenter, int3* volumeSize, float3* voxelCellSize) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if(x >= 512 || y >= 512) {
                    return;
                }
                int v;
                //Goes along the x axis
                float weight;
                float prev = NAN, value = NAN;
                for(v = start.x; v < end.x; v++) {
                    //index should be z, x, y
                    UnpackTsdf(GetTsdfPointer(tsdf, v, x, y, volumeCenter, volumeSize), weight, value);
                    if( !isnan(prev) && !isnan(value) && ((prev < 0 && value > 0) || (prev > 0 && value < 0 ))) {
                        //Found vertex
                        volume[Index3D(v, x, y, volumeCenter, volumeSize)] = true;
                    }
                    prev = value;
                }

                //Goes along the y axis
                prev = NAN, value = NAN;
                for(v = start.y; v < end.y; v++) {
                    UnpackTsdf(GetTsdfPointer(tsdf, x, v, y, volumeCenter, volumeSize), weight, value);
                    if( !isnan(prev) && !isnan(value) && ((prev < 0 && value > 0) || (prev > 0 && value < 0 ))) {
                        //Found vertex
                        volume[Index3D(x, v, y, volumeCenter, volumeSize)] = true;
                    }
                    prev = value;
                }

                //Goes along the z axis
                prev = NAN, value = NAN;
                for(v = start.z; v < end.z; v++) {
                    //index should be x, y, z
                    UnpackTsdf(GetTsdfPointer(tsdf, x, y, v, volumeCenter, volumeSize), weight, value);
                    if( !isnan(prev) && !isnan(value) && ((prev < 0 && value > 0) || (prev > 0 && value < 0 ))) {
                        //Found vertex
                        volume[Index3D(x, y, v, volumeCenter, volumeSize)] = true;
                    }
                    prev = value;
                }



                //Weird raycasting here

                if (x < start.x || x >= end.x || y < start.y || y >= end.y) {
                    return;
                }

                prev = NAN, value = NAN;
                for(v = 0; v < volumeSize->x; v++) {
                    //index should be x, y, z
                    UnpackTsdf(GetTsdfPointer(tsdf, v, x, y, volumeCenter, volumeSize), weight, value);
                    if( !isnan(prev) && !isnan(value) && ((prev < 0 && value > 0) || (prev > 0 && value < 0 ))) {
                        //Found vertex
                        volume[Index3D(v, x, y, volumeCenter, volumeSize)] = true;
                    }
                    prev = value;
                }
                prev = NAN, value = NAN;
                for(v = 0; v < volumeSize->y; v++) {
                    UnpackTsdf(GetTsdfPointer(tsdf, x, v, y, volumeCenter, volumeSize), weight, value);
                    if( !isnan(prev) && !isnan(value) && ((prev < 0 && value > 0) || (prev > 0 && value < 0 ))) {
                        //Found vertex
                        volume[Index3D(x, v, y, volumeCenter, volumeSize)] = true;
                    }
                    prev = value;
                }
                prev = NAN, value = NAN;
                for(v = 0; v < volumeSize->z; v++) {
                    //index should be x, y, z
                    UnpackTsdf(GetTsdfPointer(tsdf, x, y, v, volumeCenter, volumeSize), weight, value);
                    if( !isnan(prev) && !isnan(value) && ((prev < 0 && value > 0) || (prev > 0 && value < 0 ))) {
                        //Found vertex
                        volume[Index3D(x, y, v, volumeCenter, volumeSize)] = true;
                    }
                    prev = value;
                }


            }
//            __global__ void RayCastVolume(int* tsdf, int* tsdfColour, int3* volumeGrid, int3* volumeCenter,
//                                          int* pointIndex, int* pointColourIndex, int3* pointIndexGridSize,
//                                          int3 start, int3 end) {
//                int x, y, z;
//                x = blockDim.x * blockIdx.x + threadIdx.x;
//                y = blockDim.y * blockIdx.y + threadIdx.y;
//                if (x >= 512 - 1 || y >= 512 - 1) {
//                    return;
//                }
//                //New rayCaster
//                //for CUDA
//                int nextIndex = 0;
//                // now do ray cast
//                uchar r, g, b;
//                for(z = 0; z < volumeGrid->z - 1; z++) {
//                    float myTsdf, x1, y1, z1, w;
//                    UnpackTsdf(gpu::device::GetTsdfPointer(tsdf, x, y, z, volumeCenter, volumeGrid), w, myTsdf);
//                    UnpackTsdf(gpu::device::GetTsdfPointer(tsdf, x + 1, y, z, volumeCenter, volumeGrid), w, x1);
//                    UnpackTsdf(gpu::device::GetTsdfPointer(tsdf, x, y + 1, z, volumeCenter, volumeGrid), w, y1);
//                    UnpackTsdf(gpu::device::GetTsdfPointer(tsdf, x, y, z + 1, volumeCenter, volumeGrid), w, z1);
//                    bool point = (myTsdf > 0 && ((x1 < 0) || y1 < 0 || z1 < 0)) || (myTsdf < 0 && (x1 > 0 || y1 > 0 || z1 > 0));
//                    if(point) {
//                        pointIndex[gpu::device::Index3DBasic(x, y, nextIndex, pointIndexGridSize)] = z;
//
//                        UnpackColourValue(GetTsdfColourPointer(tsdfColour, x, y, z, volumeCenter, volumeGrid), r, g, b);
//                        uchar* pack = (uchar*) &(pointColourIndex[gpu::device::Index3DBasic(x, y, nextIndex, pointIndexGridSize)]);
//                        pack[0] = 0;// Alpha value
//                        pack[1] = r;
//                        pack[2] = g;
//                        pack[3] = b;
//                        nextIndex++;
//                    }
//                }
//            }
            /***********************************************************
             * Note raycaster needs values from 0 -> end or start->512 else it will fail
             **********************************************************/
            __global__ void RayCastVolumeAlongAxis(int *tsdf, int *tsdfColour, int3 *volumeGrid, int3 *volumeCenter,
                                                   int *pointIndex, int *pointColourIndex, gpu::float3 *pointNormal,
                                                   int3 *pointIndexGridSize, int3 localStart, int3 localEnd) {
                int threadX, threadY, x, y, z;
                threadX = blockDim.x * blockIdx.x + threadIdx.x;
                threadY = blockDim.y * blockIdx.y + threadIdx.y;
                if (threadX >= 512 - 1 || threadY >= 512 - 1) {
                    return;
                }


                LocalVoxelToGlobalVoxel(threadX, threadY, 0, volumeCenter, volumeGrid, NULL, x, y, z);
                x -= volumeCenter->x;
                y -= volumeCenter->y;

                //New rayCaster
                //for CUDA
                int nextIndex = 0;
                // now do ray cast
                uchar r, g, b;
                //start.z < end.z
                //Changing
                int3 start = LocalVoxelToGlobalVoxel(localStart, volumeCenter, volumeGrid, NULL);
                int3 end = LocalVoxelToGlobalVoxel(localEnd, volumeCenter, volumeGrid, NULL);
                start -= (*volumeCenter);
                end -= (*volumeCenter);
                if (threadX == 0 && threadY == 0) {
                    printf("Indexing location %d %d z details: %d, %d\n", x, y, start.z, end.z);
                }
                float tsdfValues[27];
                int index;
                bool pointFound;
                gpu::float3 normalVector;
                gpu::float3 tempDir;
                for(z = start.z; z < end.z - 1; z++) {

                    memset(tsdfValues, 0, sizeof(float) * 27);
                    float w;
                    int xDir, yDir, zDir;
                    for(xDir = 0; xDir < 3; xDir++) {
                        for(yDir = 0; yDir < 3; yDir++) {
                            for(zDir = 0; zDir < 3; zDir++) {
                                index = xDir + yDir * 3 + zDir * 9;
                                UnpackTsdf(gpu::device::GetTsdfPointer(tsdf, x + xDir - 1, y + yDir - 1, z + zDir - 1, volumeCenter, volumeGrid), w, tsdfValues[index]);
                            }
                        }
                    }

//                    //Quick check
                    pointFound = false;
                    for(xDir = 0; xDir < 27; xDir++) {
                        if( ((tsdfValues[13] > 0) && (tsdfValues[xDir] < 0)) || ((tsdfValues[13] < 0 && tsdfValues[xDir] > 0))) {
                            pointFound = true;
                            break;
                        }
                    }
                    if(!pointFound) {
                        continue;
                    }

                    normalVector.SetZero();
                    for(xDir = 0; xDir < 3; xDir++) {
                        for(yDir = 0; yDir < 3; yDir++) {
                            for(zDir = 0; zDir < 3; zDir++) {
                                index = xDir + yDir * 3 + zDir * 9;
                                tempDir.x = xDir - 1;
                                tempDir.y = yDir - 1;
                                tempDir.z = zDir - 1;
                                if(tsdfValues[13] > 0 && tsdfValues[index] < 0) {
                                    normalVector = normalVector - tempDir;
                                } else if(tsdfValues[13] < 0 && tsdfValues[index] > 0) {
                                    normalVector = normalVector + tempDir;
                                }
                            }
                        }
                    }
                    pointIndex[gpu::device::Index3D(x, y, nextIndex, volumeCenter, pointIndexGridSize)] = z + volumeCenter->z;
                    UnpackColourValue(GetTsdfColourPointer(tsdfColour, x, y, z, volumeCenter, volumeGrid), r, g, b);
                    uchar* pack = (uchar*) &(pointColourIndex[gpu::device::Index3D(x, y, nextIndex, volumeCenter, pointIndexGridSize)]);
                    pack[0] = 0;// Alpha value
                    pack[1] = r;
                    pack[2] = g;
                    pack[3] = b;
                    pointNormal[gpu::device::Index3D(x, y, nextIndex, volumeCenter, pointIndexGridSize)] = normalVector.normalized();;
                    nextIndex++;
                    if(nextIndex >= 64) {
                        printf("PROC !!!!!!\n");
                        return;
                    }
                }


                int3 startZ(0, 0, 0);
                int3 endZZ(*volumeGrid);
                startZ = LocalVoxelToGlobalVoxel(startZ, volumeCenter, volumeGrid, NULL);
                endZZ = LocalVoxelToGlobalVoxel(endZZ, volumeCenter, volumeGrid, NULL);
                startZ -= (*volumeCenter);
                endZZ -= (*volumeCenter);
                if(threadY >= localStart.y && threadY <= localEnd.y && localStart.y != localEnd.y) {

//                    minz -> maxz
                    z = startZ.z;
                    int endZ = endZZ.z;
                    if ( threadX == 300 && threadY == 450) {
                        printf("start %d end %d\n", z, endZ);
                    }
                    for(; z < endZ - 1; z++) {

                        memset(tsdfValues, 0, sizeof(float) * 27);
                        float w;
                        int xDir, yDir, zDir;
                        for(xDir = 0; xDir < 3; xDir++) {
                            for(yDir = 0; yDir < 3; yDir++) {
                                for(zDir = 0; zDir < 3; zDir++) {
                                    index = xDir + yDir * 3 + zDir * 9;
                                    UnpackTsdf(gpu::device::GetTsdfPointer(tsdf, x + xDir - 1, y + yDir - 1, z + zDir - 1, volumeCenter, volumeGrid), w, tsdfValues[index]);
                                }
                            }
                        }

//                    //Quick check
                        pointFound = false;
                        for(xDir = 0; xDir < 27; xDir++) {
                            if( ((tsdfValues[13] > 0) && (tsdfValues[xDir] < 0)) || ((tsdfValues[13] < 0 && tsdfValues[xDir] > 0))) {
                                pointFound = true;
                                break;
                            }
                        }
                        if(!pointFound) {
                            continue;
                        }

                        normalVector.SetZero();
                        for(xDir = 0; xDir < 3; xDir++) {
                            for(yDir = 0; yDir < 3; yDir++) {
                                for(zDir = 0; zDir < 3; zDir++) {
                                    index = xDir + yDir * 3 + zDir * 9;
                                    tempDir.x = xDir - 1;
                                    tempDir.y = yDir - 1;
                                    tempDir.z = zDir - 1;
                                    if(tsdfValues[13] > 0 && tsdfValues[index] < 0) {
                                        normalVector = normalVector - tempDir;
                                    } else if(tsdfValues[13] < 0 && tsdfValues[index] > 0) {
                                        normalVector = normalVector + tempDir;
                                    }
                                }
                            }
                        }


                        pointIndex[gpu::device::Index3D(x, y, nextIndex, volumeCenter, pointIndexGridSize)] = z + volumeCenter->z;

                        UnpackColourValue(GetTsdfColourPointer(tsdfColour, x, y, z, volumeCenter, volumeGrid), r, g, b);
                        uchar* pack = (uchar*) &(pointColourIndex[gpu::device::Index3D(x, y, nextIndex, volumeCenter, pointIndexGridSize)]);
                        pack[0] = 0;// Alpha value
                        pack[1] = r;
                        pack[2] = g;
                        pack[3] = b;
                        pointNormal[gpu::device::Index3D(x, y, nextIndex, volumeCenter, pointIndexGridSize)] = normalVector.normalized();
                        nextIndex++;
                        if(nextIndex >= 64) {
                            printf("PROC !!!!!!\n");
                            return;
                        }
                    }
                }

                if(threadX >= localStart.x && threadX <= localEnd.x && localStart.x != localEnd.x) {
//                if(x  + volumeCenter->x >= start.x && x  + volumeCenter->x <= end.x) {
//                    z = end.z;
//                    if(end.z != 0) {
//                        z -= 1;
//                    }
                    z = startZ.z;
                    int endZ = endZZ.z;
//                    if (threadX == 0 && threadY == 0) {
//                        printf("Test %d %d\n", threadX, threadY);
//                    }
                    for(; z < endZ - 1; z++) {
                        memset(tsdfValues, 0, sizeof(float) * 27);
                        float w;
                        int xDir, yDir, zDir;
                        for(xDir = 0; xDir < 3; xDir++) {
                            for(yDir = 0; yDir < 3; yDir++) {
                                for(zDir = 0; zDir < 3; zDir++) {
                                    index = xDir + yDir * 3 + zDir * 9;
                                    UnpackTsdf(gpu::device::GetTsdfPointer(tsdf, x + xDir - 1, y + yDir - 1, z + zDir - 1, volumeCenter, volumeGrid), w, tsdfValues[index]);
                                }
                            }
                        }

//                    //Quick check
                        pointFound = false;
                        for(xDir = 0; xDir < 27; xDir++) {
                            if( ((tsdfValues[13] > 0) && (tsdfValues[xDir] < 0)) || ((tsdfValues[13] < 0 && tsdfValues[xDir] > 0))) {
                                pointFound = true;
//                                printf("Found\n");
                                break;
                            }
                        }
                        if(!pointFound) {
                            continue;
                        }

                        normalVector.SetZero();
                        for(xDir = 0; xDir < 3; xDir++) {
                            for(yDir = 0; yDir < 3; yDir++) {
                                for(zDir = 0; zDir < 3; zDir++) {
                                    index = xDir + yDir * 3 + zDir * 9;
                                    tempDir.x = xDir - 1;
                                    tempDir.y = yDir - 1;
                                    tempDir.z = zDir - 1;
                                    if(tsdfValues[13] > 0 && tsdfValues[index] < 0) {
                                        normalVector = normalVector - tempDir;
                                    } else if(tsdfValues[13] < 0 && tsdfValues[index] > 0) {
                                        normalVector = normalVector + tempDir;
                                    }
                                }
                            }
                        }
                        pointIndex[gpu::device::Index3D(x, y, nextIndex, volumeCenter, pointIndexGridSize)] = z + volumeCenter->z;

                        UnpackColourValue(GetTsdfColourPointer(tsdfColour, x, y, z, volumeCenter, volumeGrid), r, g, b);
                        uchar* pack = (uchar*) &(pointColourIndex[gpu::device::Index3D(x, y, nextIndex, volumeCenter, pointIndexGridSize)]);
                        pack[0] = 0;// Alpha value
                        pack[1] = r;
                        pack[2] = g;
                        pack[3] = b;
                        pointNormal[gpu::device::Index3D(x, y, nextIndex, volumeCenter, pointIndexGridSize)] = normalVector.normalized();
                        nextIndex++;
                        if(nextIndex >= 64) {
                            printf("PROC !!!!!!\n");
                            return;
                        }

                    }
                }

            }
            __global__ void ResetPointGenerationVolume(int* ptr, int3* volumeGridSize, int defaultValue) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if(x >= volumeGridSize->x || y >= volumeGridSize->y) {
                    return;
                }
                int z;
                for(z = 0; z < volumeGridSize->z; z++) {
                    ptr[Index3DBasic(x, y, z, volumeGridSize)] = defaultValue;
                }
            }
            __global__ void ResetPointGenerationVolumeNormals(gpu::float3* ptr, int3* volumeGridSize, float3 defaultValue) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if(x >= volumeGridSize->x || y >= volumeGridSize->y) {
                    return;
                }
                int z;
                for(z = 0; z < volumeGridSize->z; z++) {
                    ptr[Index3DBasic(x, y, z, volumeGridSize)] = defaultValue;
                }
            }
        }
    };
    namespace host {
        namespace RayCaster {
            void RayCasterKernel() {
//                printf("Starting RayCaster Kernel\n");
                int i, skip = 0, cols = GetImageCols(), rows = GetImageRows();
                for(i = 0; i < GetICPLevels(); i++) { // TODO uncomment
//                    std::cout << "Raycasting Level: " << i << std::endl;
//                for(i = 0; i < 1; i++) {
                    dim3 dimBlock = DivUp(cols, rows);
                    dim3 dimGrid = GetGrid();
//                    dim3 dimBlock(1, 1);
//                    dim3 dimGrid(1, 1);
    //                printf("Image Size given %d %d \n", GetImageCols(), GetImageRows());
                    device::RayCaster::RayCaster<<<dimBlock, dimGrid>>>(GetGPUTsdfVolume(), GetGPUTsdfColourVolume(), rows, cols, GetGPUCameraMatrix(),
                            GetGPURotation(), GetGPUInverseRotation(), GetGPUTranslation(), GetGPUVoxelGridSize(), GetGPUVoxelCellSize(), GetGPUVoxelCenter(),
                            GetGPUSourceVertexMap() + skip, GetGPUSourceNormalMap() + skip, GetGPUSourceColourMap() + skip * GetImageChannels(), i);
                    skip += (cols * rows);
                    rows /= 2;
                    cols /= 2;
                    cudaDeviceSynchronize();
    //                printf("VertexMap Memory add %p NormalMap memory add %p, %d\n", GetGPUVertexMap(), GetGPUNormalMap(), GetGPUVertexMap());
                    CudaErrorCheck(__FILE__, __LINE__);
                }
//                printf("Ending RayCaster Kernel\n");
            }
            void RayCasterKernel(int3& start, int3& end) {
//                printf("\x1B[31mStart %d %d %d\n", start.x, start.y, start.z);
//                printf("End   %d %d %d\n\x1B[0m", end.x, end.y, end.z);
                dim3 blocks =  dim3((unsigned int)(512/16) + 1, (unsigned int)(512/16) + 1);
                dim3 threads = dim3(16, 16);
                std::cout << "Blocks " << blocks.x << " " << blocks.y << " threads " << threads.x << " " << threads.y << std::endl;
                std::cout << "Start " << start.x << " " << start.y << "  " << start.z << std::endl;
                std::cout << "End " << end.x << " " << end.y << "  " << end.z << std::endl;
                gpu::device::RayCaster::RayCastVolumeAlongAxis<<<blocks, threads>>>(GetGPUTsdfVolume(), GetGPUTsdfColourVolume(), GetGPUVoxelGridSize(), GetGPUVoxelCenter(),
                        GetGPUPointIndex(), GetGPUPointIndexColour(), GetGPUPointIndexNormal(), GetGPUPointIndexGridSize(), start, end);
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
            }
            void ResetPointIndexVolume() {
                dim3 blocks = DivUp(512, 512);
                dim3 threads = GetGrid();
                device::RayCaster::ResetPointGenerationVolume<<<blocks, threads>>>(GetGPUPointIndex(), GetGPUPointIndexGridSize(), -2147483648); //TODO dirty FIX for generation of points
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
                device::RayCaster::ResetPointGenerationVolume<<<blocks, threads>>>(GetGPUPointIndexColour(), GetGPUPointIndexGridSize(), 0);
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
                device::RayCaster::ResetPointGenerationVolumeNormals<<<blocks, threads>>>(GetGPUPointIndexNormal(), GetGPUPointIndexGridSize(), NAN);
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
            }
        };
    };
};