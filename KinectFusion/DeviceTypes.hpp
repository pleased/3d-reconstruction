/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/07/19.
//

#ifndef VSLAM_DEVICETYPES_HPP
#define VSLAM_DEVICETYPES_HPP
#include <iostream>
#include <ostream>
#include <cstring>
#include <cmath>
#include <cassert>
#include <cuda.h>
#include <cuda_runtime.h>

namespace gpu{
    typedef unsigned char uchar;
    struct float3 {
        float x = 0, y = 0, z = 0;
        __host__ __device__ float3(){
            x = 0;
            y = 0;
            z = 0;
        };
        __host__ __device__ float3(float x, float y, float z) {
            this->x = x;
            this->y = y;
            this->z = z;
        }
        __host__ __device__ float3(float v) {
            this->x = v;
            this->y = v;
            this->z = v;
        }
        __host__ __device__ void SetZero() {
            this->x = 0;
            this->y = 0;
            this->z = 0;
        }
        __host__ __device__ float dot(const float3& other) {
            //TODO Test this functions
            return x * other.x + y * other.y + z * other.z;
        }
        __host__ __device__ float norm() {
            return sqrtf(x * x + y * y + z * z);
        }
        __host__ __device__ float3 normalized() {
            float3 norm;
            float normValue = sqrtf(x*x + y*y + z*z);
            norm.x = x/normValue;
            norm.y = y/normValue;
            norm.z = z/normValue;

            return norm;
        }
        __host__ __device__ float3 cross(float3 other) {
            float3 r;
            r.x = y * other.z - z * other.y ;
            r.y = -(x * other.z) + (z * other.x);
            r.z = x * other.y - y * other.x;
            return r;
        }

        //Basic Operators;
        __host__ __device__ float3& operator+=(const float3& rhs) {
            x += rhs.x;
            y += rhs.y;
            z += rhs.z;
            return *this;
        }
        __host__ __device__ float3& operator-=(const float3& rhs) {
            x -= rhs.x;
            y -= rhs.y;
            z -= rhs.z;
            return *this;
        }
        __host__ __device__ float3& operator-() {
            x = -x;
            y = -y;
            z = -z;
            return *this;
        }
        __host__ __device__ float3& operator*=(const float3& rhs) {
            x *= rhs.x;
            y *= rhs.y;
            z *= rhs.z;
            return *this;
        }
        __host__ __device__ float3& operator/=(const float3& rhs) {
            x /= rhs.x;
            y /= rhs.y;
            z /= rhs.z;
            return *this;
        }
    };
    //float3 operators
    __host__ __device__ inline float3 operator+(float3 lhs, const float3& rhs) {
        float3 r;
        r.x = lhs.x + rhs.x;
        r.y = lhs.y + rhs.y;
        r.z = lhs.z + rhs.z;
        return r;
    }
    //TODO Addition of constants

    __host__ __device__ inline float3 operator-(float3 lhs, const float3& rhs) {
        float3 r;
        r.x = lhs.x - rhs.x;
        r.y = lhs.y - rhs.y;
        r.z = lhs.z - rhs.z;
        return r;
    }
    //TODO Sub of constants
    __host__ __device__ inline float3 operator*(float3 lhs, const float3& rhs) {
        float3 r;
        r.x = lhs.x * rhs.x;
        r.y = lhs.y * rhs.y;
        r.z = lhs.z * rhs.z;
        return r;
    }

    __host__ __device__ inline float3 operator*(float3 lhs, const float rhs) {
        float3 r;
        r.x = lhs.x * rhs;
        r.y = lhs.y * rhs;
        r.z = lhs.z * rhs;
        return r;
    }
    __host__ __device__ inline float3 operator*(const float lhs, float3 rhs) {
        float3 r;
        r.x = rhs.x * lhs;
        r.y = rhs.y * lhs;
        r.z = rhs.z * lhs;
        return r;
    }

    __host__ __device__ inline float3 operator/(float3 lhs, const float3 rhs) {
        float3 r;
        r.x = lhs.x / rhs.x;
        r.y = lhs.y / rhs.y;
        r.z = lhs.z / rhs.z;
        return r;
    }
    __host__ __device__ inline bool operator==(const float3& lhs, const float3& rhs) {
        return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z;
    }
    __host__ __device__ inline bool operator!=(const float3& lhs, const float3& rhs) {
        return !(lhs == rhs);
    }
    __host__ __device__ inline float3 operator/(float l, const float3 r) {
        float3 ret;
        ret.x = l / r.x;
        ret.y = l / r.y;
        ret.z = l / r.z;
        return ret;
    }
    __host__ __device__ inline float3 operator/(float3 l, float r) {
        float3 ret;
        ret.x = l.x / r;
        ret.y = l.y / r;
        ret.z = l.z / r;
        return ret;
    }

    struct float7 {
        float values[7];
        float err;
        __host__ __device__ float7() {
            values[0] = 0;
            values[1] = 0;
            values[2] = 0;
            values[3] = 0;
            values[4] = 0;
            values[5] = 0;
            values[6] = 0;
            err = 0;
        }
        __host__ __device__ float7(const float3& other) {

        }
        __host__ __device__ float7(const float3& aOther, const float3& bOther, const float& cOther, float errOther=0) {
            values[0] = aOther.x;
            values[1] = aOther.y;
            values[2] = aOther.z;
            values[3] = bOther.x;
            values[4] = bOther.y;
            values[5] = bOther.z;
            values[6] = cOther;
            err = errOther;
        }
        __host__ __device__ float& operator()(int x) {
            return values[x];
        }

    };
    struct int3 {
        int x = 0, y = 0, z = 0;
        __host__ __device__ int3(){};
        __host__ __device__ int3(int x, int y, int z) {
            this->x = x;
            this->y = y;
            this->z = z;
        }
        __host__ __device__ int3(int v) {
            this->x = v;
            this->y = v;
            this->z = v;
        }
        __host__ __device__ float norm() {
            return sqrtf(x * x + y * y + z * z);
        }
        __host__ __device__ int3 normalized() {
            int3 norm;
            float normValue = sqrtf(x*x + y*y + z*z);
            norm.x = (int)(x/normValue);
            norm.y = (int)(y/normValue);
            norm.z = (int)(z/normValue);

            return norm;
        }
        __host__ __device__ int3& operator+=(const int3 rhs) {
            x += rhs.x;
            y += rhs.y;
            z += rhs.z;
            return *this;
        }
        __host__ __device__ int3& operator-=(const int3 rhs) {
            x -= rhs.x;
            y -= rhs.y;
            z -= rhs.z;
            return *this;
        }
        __host__ __device__ int3& operator*=(const int3 rhs) {
            x *= rhs.x;
            y *= rhs.y;
            z *= rhs.z;
            return *this;
        }
        __host__ __device__ int3& operator/=(const int3 rhs) {
            x /= rhs.x;
            y /= rhs.y;
            z /= rhs.z;
            return *this;
        }
        __host__ __device__ int3& operator=(const int3 other) {
            x = other.x;
            y = other.y;
            z = other.z;
            return (*this);
        }
        __host__ __device__ operator float3() {
            float3 rt(x, y, z);
            return rt;
        }
    };
    //int3 operators
    __host__ __device__ inline int3 operator-(const int3 rhs) {
        int3 r;
        r.x = -rhs.x;
        r.y = -rhs.y;
        r.z = -rhs.z;
        return r;
    }
    __host__ __device__ inline int3 operator+(const int3& lhs, const int3& rhs) {
        int3 r;
        r.x = lhs.x + rhs.x;
        r.y = lhs.y + rhs.y;
        r.z = lhs.z + rhs.z;
        return r;
    }
    __host__ __device__ inline int3 operator-(const int3& lhs, const int3 rhs) {
        int3 r;
        r.x = lhs.x - rhs.x;
        r.y = lhs.y - rhs.y;
        r.z = lhs.z - rhs.z;
        return r;
    }
    __host__ __device__ inline int3 operator*(const int3& lhs, const int3 rhs) {
        int3 r;
        r.x = lhs.x * rhs.x;
        r.y = lhs.y * rhs.y;
        r.z = lhs.z * rhs.z;
        return r;
    }
    __host__ __device__ inline int3 operator/(const int3& lhs, const int3 rhs) {
        int3 r;
        r.x = lhs.x / rhs.x;
        r.y = lhs.y / rhs.y;
        r.z = lhs.z / rhs.z;
        return r;
    }
    __host__ __device__ inline int3 operator/(const int lhs, const int3 rhs) {
        int3 r;
        r.x = lhs / rhs.x;
        r.y = lhs / rhs.y;
        r.z = lhs / rhs.z;
        return r;
    }
    __host__ __device__ inline float3 operator/(const float lhs, const int3 rhs) {
        float3 r;
        r.x = lhs / rhs.x;
        r.y = lhs / rhs.y;
        r.z = lhs / rhs.z;
        return r;
    }
    __host__ __device__ inline int3 operator/(const int3& lhs, const int rhs) {
        int3 r;
        r.x = lhs.x / rhs;
        r.y = lhs.y / rhs;
        r.z = lhs.z / rhs;
        return r;
    }
    __host__ __device__ inline float3 operator/(const int3& lhs, const float rhs) {
        float3 r;
        r.x = lhs.x / rhs;
        r.y = lhs.y / rhs;
        r.z = lhs.z / rhs;
        return r;
    }

    __host__ __device__ inline int3 operator%(const int3& lhs, const int3 rhs) {
        int3 r;
        r.x = lhs.x % rhs.x;
        r.y = lhs.y % rhs.y;
        r.z = lhs.z % rhs.z;
        return r;
    }
    __host__ __device__ inline bool operator==(const int3& lhs, const int3& rhs) {
        return lhs.x == rhs.x && lhs.y == rhs.y && lhs.z == rhs.z;
    }


    //float3 and int3 operators
    __host__ __device__ inline float3 operator+(float3 l, int3 r) {
        gpu::float3 rt(l.x + r.x, l.y + r.y , l.z + r.z);
        return rt;
    }
    __host__ __device__ inline float3 operator+(int3 l, float3 r) {
        return r + l;
    }

    __host__ __device__ inline float3 operator-(float3 l, int3 r) {
        gpu::float3 rt(l.x - r.x, l.y - r.y , l.z - r.z);
        return rt;
    }
    __host__ __device__ inline float3 operator-(int3 l, float3 r) {
        return r - l;
    }

    __host__ __device__ inline float3 operator*(float3 l, int3 r) {
        float3 rt(l.x * r.x, l.y * r.y, l.z * r.z);
        return rt;
    }
    __host__ __device__ inline float3 operator*(int3 l, float3 r) {
        return r * l;
    }
    __host__ __device__ inline float3 operator*(int3 l, float r) {
        float3 rt(l.x * r, l.y * r, l.z * r);
        return rt;
    }
    __host__ __device__ inline float3 operator*(float l, int3 r) {
        float3 rt(l * r.x, l * r.y, l * r.z);
        return rt;
    }

    __host__ __device__ inline float3 operator/(float3 l, int3 r) {
        float3 rt(l.x/r.x, l.y/r.y, l.z/ r.z);
        return rt;
    }
    __host__ __device__ inline float3 operator/(int3 l, float3 r) {
        float3 rt(l.x/r.x, l.y/r.y, l.z/ r.z);
        return rt;
    }
    //Cast operators
//    int3 operator int3(float3 l) {
//        int3 rt((int) l.x, (int) l.y, (int) l.z);
//        return rt;
//    }

    struct matrix33 {
        float3 row0;
        float3 row1;
        float3 row2;
        __host__ __device__ matrix33() {
            row0.x = 0;
            row0.y = 0;
            row0.z = 0;
            row1.x = 0;
            row1.y = 0;
            row1.z = 0;
            row2.x = 0;
            row2.y = 0;
            row2.z = 0;
        }
        __host__ __device__ matrix33(float r0x, float r0y, float r0z,
                 float r1x, float r1y, float r1z,
                 float r2x, float r2y, float r2z) {
            row0.x = r0x;
            row0.y = r0y;
            row0.z = r0z;
            row1.x = r1x;
            row1.y = r1y;
            row1.z = r1z;
            row2.x = r2x;
            row2.y = r2y;
            row2.z = r2z;
        }
        __host__ __device__ matrix33(matrix33* ptr) {
            row0 = ptr->row0;
            row1 = ptr->row1;
            row2 = ptr->row2;
        }
        __host__ __device__ void SetIdentity() {
            row0.x = 1;
            row0.y = 0;
            row0.z = 0;
            row1.x = 0;
            row1.y = 1;
            row1.z = 0;
            row2.x = 0;
            row2.y = 0;
            row2.z = 1;
        }
        __host__ __device__ float operator()(int x, int y) {
            if( x == 0) {
                if( y == 0 ) {
                    return row0.x;
                } else if (y == 1) {
                    return row0.y;
                } else if (y == 2) {
                    return row0.z;
                }
            } else if( x == 1) {
                if( y == 0 ) {
                    return row1.x;
                } else if (y == 1) {
                    return row1.y;
                } else if (y == 2) {
                    return row1.z;
                }
            } else if ( x== 2) {
                if( y == 0 ) {
                    return row2.x;
                } else if (y == 1) {
                    return row2.y;
                } else if (y == 2) {
                    return row2.z;
                }
            }
            return 0;
        }
        __host__ __device__ matrix33 operator-() {
            matrix33 r;
            r.row0 = -this->row0;
            r.row1 = -this->row1;
            r.row2 = -this->row2;
            return r;
        }
        __host__ __device__ float3 operator*(const float3& other) {
            float3 r;
            r.x = row0.x * other.x + row0.y * other.y + row0.z * other.z;
            r.y = row1.x * other.x + row1.y * other.y + row1.z * other.z;
            r.z = row2.x * other.x + row2.y * other.y + row2.z * other.z;
            return r;
        }
        __host__ __device__ matrix33 operator*(const matrix33& other) {
            matrix33 r;
            r.row0.x = row0.x * other.row0.x + row0.y * other.row1.x + row0.z * other.row2.x;
            r.row0.y = row0.x * other.row0.y + row0.y * other.row1.y + row0.z * other.row2.y;
            r.row0.z = row0.x * other.row0.z + row0.y * other.row1.z + row0.z * other.row2.z;

            r.row1.x = row1.x * other.row0.x + row1.y * other.row1.x + row1.z * other.row2.x;
            r.row1.y = row1.x * other.row0.y + row1.y * other.row1.y + row1.z * other.row2.y;
            r.row1.z = row1.x * other.row0.z + row1.y * other.row1.z + row1.z * other.row2.z;

            r.row2.x = row2.x * other.row0.x + row2.y * other.row1.x + row2.z * other.row2.x;
            r.row2.y = row2.x * other.row0.y + row2.y * other.row1.y + row2.z * other.row2.y;
            r.row2.z = row2.x * other.row0.z + row2.y * other.row1.z + row2.z * other.row2.z;


            return r;
        }
        __host__ __device__ matrix33 operator*(float x) {
            matrix33 r;
            r.row0 = row0 * x;
            r.row1 = row1 * x;
            r.row2 = row2 * x;
            return r;
        }
        __host__ __device__ bool operator==(const matrix33& other) {
            return row0 == other.row0 && row1 == other.row1 && row2 == other.row2;
        }
        __host__ __device__ matrix33 transpose() {

            matrix33 r(row0.x, row1.x, row2.x,
                       row0.y, row1.y, row2.y,
                       row0.z, row1.z, row2.z);
            return r;
        }
    };
    struct DepthMap {
        //TODO Write deallocator for depthMap data field
        int rows = 0, cols = 0;
        bool allocated = false;
        bool deepCopy = true;
        float* data = 0;
        DepthMap() { allocated = false; }
        __host__ __device__ DepthMap(int rows, int cols) {
            assert(rows > 0 && cols > 0);
            this->rows = rows;
            this->cols = cols;
            data = (float*)calloc(rows * cols, sizeof(float));
            allocated = true;
        }
        ~DepthMap() {
         if (allocated) {
             printf("Destructor\n");
             free(data);
             allocated = false;
         }
        }
        /**
         * Takes in a 64F image
         *
         */
        __host__ __device__ DepthMap(int rows, int cols, const uchar* datastart) {
            assert(rows > 0 && cols > 0);
            this->rows = rows;
            this->cols = cols;

            data = (float*)calloc(rows * cols, sizeof(float));
            allocated = true;
            double* dataStart = (double*)datastart;
            int i;
            for(i = 0; i < rows * cols; i++) {
                data[i] = (float) dataStart[i];
            }


//                unsigned long copySize = (dataend - datastart) * sizeof(uchar);
//                assert(copySize > 0);
//                data = (uchar*)calloc(copySize, sizeof(uchar));
////                std::cout << "Copying " << copySize << " bytes" << std::endl;
//                std::memcpy(this->data, datastart, copySize);
//                allocated = true;
//                int i = 0;
            //TODO Remove sanity check
//                for(i = 0; i < rows * cols; i++ ) {
//                    if(dataStart[i] != data[i]) {
//                        printf("test %f %f\n", dataStart[i], data[i]);
//                    }
//                    assert(dataStart[i] == data[i]);
//                }
        }
        //size is the number of bytes per element in the pointer
        __host__ __device__ DepthMap(int rows, int cols, const uchar* datastart, const uchar* dataend, int test=0) {
            assert(rows > 0 && cols > 0);
            this->rows = rows;
            this->cols = cols;

            data = (float*)calloc(rows * cols, sizeof(float));
            allocated = true;
            double* dataStart = (double*)datastart;
            int i;
            for(i = 0; i < rows * cols; i++) {
                data[i] = (float) dataStart[i];
            }


//                unsigned long copySize = (dataend - datastart) * sizeof(uchar);
//                assert(copySize > 0);
//                data = (uchar*)calloc(copySize, sizeof(uchar));
////                std::cout << "Copying " << copySize << " bytes" << std::endl;
//                std::memcpy(this->data, datastart, copySize);
//                allocated = true;
//                int i = 0;
            //TODO Remove sanity check
            for(i = 0; i < rows * cols; i++ ) {
                if ( test == 1) {
                    printf("%f %f\n", dataStart[i], data[i]);
                }
//                    assert(dataStart[i] == data[i]);
            }
        }

        __host__ __device__ void Init(int rows, int cols, const uchar* datastart, const uchar* dataend, int test=0) {
            if (allocated) {
                printf("DepthMap already allocated\n");
                return;
            }
            assert(rows > 0 && cols > 0);
            this->rows = rows;
            this->cols = cols;

            data = (float*)calloc(rows * cols, sizeof(float));
            allocated = true;
            double* dataStart = (double*)datastart;
            int i;
            for(i = 0; i < rows * cols; i++) {
                data[i] = (float) dataStart[i];
            }


//                unsigned long copySize = (dataend - datastart) * sizeof(uchar);
//                assert(copySize > 0);
//                data = (uchar*)calloc(copySize, sizeof(uchar));
////                std::cout << "Copying " << copySize << " bytes" << std::endl;
//                std::memcpy(this->data, datastart, copySize);
//                allocated = true;
//                int i = 0;
            //TODO Remove sanity check
            for(i = 0; i < rows * cols; i++ ) {
                if ( test == 1) {
                    printf("%f %f\n", dataStart[i], data[i]);
                }
//                    assert(dataStart[i] == data[i]);
            }
        }
        __host__ __device__ void Init(int rows, int cols) {
            if (allocated) {
                printf("DepthMap already allocated\n");
                return;
            }
            assert(rows > 0 && cols > 0);
            this->rows = rows;
            this->cols = cols;
            data = (float*)calloc(rows * cols, sizeof(float));
            allocated = true;
        }
        __host__ __device__ void Init(int rows, int cols, const uchar* datastart) {
            if (allocated) {
                printf("DepthMap already allocated\n");
                return;
            }
            assert(rows > 0 && cols > 0);
            this->rows = rows;
            this->cols = cols;

            data = (float*)calloc(rows * cols, sizeof(float));
            allocated = true;
            double* dataStart = (double*)datastart;
            int i;
            for(i = 0; i < rows * cols; i++) {
                data[i] = (float) dataStart[i];
            }


//                unsigned long copySize = (dataend - datastart) * sizeof(uchar);
//                assert(copySize > 0);
//                data = (uchar*)calloc(copySize, sizeof(uchar));
////                std::cout << "Copying " << copySize << " bytes" << std::endl;
//                std::memcpy(this->data, datastart, copySize);
//                allocated = true;
//                int i = 0;
            //TODO Remove sanity check
            for(i = 0; i < rows * cols; i++ ) {
//                    assert(dataStart[i] == data[i]);
            }
        }

        __host__ __device__ void reset(int rows, int cols, const uchar* datastart) {
            if(allocated && rows == this->rows && cols == this->cols) {
//                printf("Reusing containers\n");
                double* dataStart = (double*)datastart;
                int i;
                for(i = 0; i < rows * cols; i++) {
                    data[i] = (float) dataStart[i];
                }
            }
        }

        //Normal Operators outside of cuda
        __host__ __device__ float& operator()(int x) {
//                if (!allocated)
//                    std::cerr << "Depth map not allocated Or running in CUDA returning NAN" << std::endl;
            return this->data[x];
        }

        __host__ __device__ float& operator()(int y, int x) {
            return (*this)(y * this->cols + x);
        }
        __host__ __device__ DepthMap& operator=(DepthMap& r) {
            printf("NOT implemented\n");
            exit(0);
//                return 0;
        }

        //CUDA Operators
        __host__ __device__ float operator()(float* ptr, int x) {
            return ptr[x];
        }
        __host__ __device__ float operator()(float* ptr, int y, int x) {
            return (*this)(ptr, y * this->cols + x);
        }

        __host__ __device__ bool operator==(DepthMap& other) {
            if ((rows != other.rows) ||(cols != other.cols)) {
                return false;
            }
            if(!(data == 0 && other.data == 0) && !(data != 0 && other.data != 0)) {
                return false;
            }
            if (data != 0 && other.data != 0) {
                int x;
                for(x = 0; x < rows * cols; x++) {
                    if(data[x] != other.data[x]) {
                        return false;
                    }
                }

            }
            return true;
        }

    };

    struct Image {
        uchar* data = 0;
        int rows = 0;
        int cols = 0;
        bool allocated = false;
        int channels = 3;
        Image() {

        }
        Image(int rows, int cols, int channels) {
            assert(rows > 0 && cols > 0);
            assert(channels >= 1);
            assert(channels == 3);
            this->rows = rows;
            this->cols = cols;

            data = (uchar*)calloc(rows * cols * channels, sizeof(uchar));
            allocated = true;
        }

        Image(int rows, int cols, int channels, const uchar* datastart) {
            assert(rows > 0 && cols > 0);
            assert(channels == 3);
            this->rows = rows;
            this->cols = cols;
            this->channels = channels;
            data = (uchar*)calloc(rows * cols * channels, sizeof(uchar));
            allocated = true;
            memcpy(data, datastart, sizeof(uchar) * rows * cols * channels);
        }
        __host__ __device__ void reset(int rows, int cols, int channels, const uchar* datastart) {
            if(allocated && rows == this->rows && cols == this->cols && channels == this->channels) {
//                printf("Reusing containers\n");
                int i;
                for(i = 0; i < rows * cols * channels; i++) {
                    data[i] = datastart[i];
                }
            } else {
                printf("ERROR Image format incorrect\n");
//                throw "Image format incorrect";
            }
        }

        __host__ __device__ void resetBGRAToBGR(int rows, int cols, int channels, const uchar* datastart) {
            assert(channels == 4);
            int i;
            int counter = 0;
            for(i = 0; i < rows * cols * channels; i++) {
                if(i % 4 == 3) {
                    continue;
                }
                data[counter] = datastart[i];
                counter++;
            }
        }

        ~Image() {
            if (allocated) {
                printf("Destructor\n");
                free(data);
                allocated = false;
            }
        }

        __host__ __device__ int GetChannels() {
            return channels;
        }
        __host__ __device__ uchar* operator()(int x) {
            return &this->data[x * channels];
        }
        __host__ __device__ uchar* operator()(int y, int x) {
            return (*this)(y * this->cols + x);
        }
    };
    //Structure of the namespace
//    namespace device {
//
//    };
//    namespace host {
//
//    };
};

#endif //VSLAM_DEVICETYPES_HPP
