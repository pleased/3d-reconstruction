/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/07/27.
//
#include "IterativeClosestPoint.hpp"
#include <cuda.h>
#include <cuda_runtime_api.h>
#include <device_functions.h>
#include <device_launch_parameters.h>
#include <math_functions.h>
#include <math_constants.h>
#include <iostream>
#include "DeviceTypes.hpp"
#include "GlobalHost.hpp"
#include "GlobalDevice.hpp"
namespace gpu {
    namespace device {
        namespace IterativeClosestPoint {
            __global__ void VertexVectors(DepthMap *depth, float3 *ptr, matrix33* cameraMatrix, int level) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if(x >= depth->cols || y >= depth->rows) {
                    return;
                }
//                x = 0;
//                y = 109;
                float d = 1.f/1000.f;
                float scale = powf(2, -level);
                float2 f;
                float2 c;
                f.x = (*cameraMatrix)(0, 0) * scale;
                f.y = (*cameraMatrix)(1, 1) * scale;

                c.x = (*cameraMatrix)(0, 2) * scale;
                c.y = (*cameraMatrix)(1, 2) * scale;
//                if (x == 0 && y == 0) {
//                    printf("DepthThings %d %d\n", depth->cols, depth->rows);
//                    printf("Camera %f %f\n", c.x, c.y);
//                    printf("focal Length %f %f\n", f.x, f.y);
//                }


                int index = y * depth->cols + x;//Index2D(x, y, depth->cols);
                float depthV = (*depth)(y, x);
//                printf("depthV %f\n", depthV);
                if (depthV > 0) {
                    depthV = depthV * d; // from mm to meters
//                    if(x == 394 && y == 285) {
//                        printf("DeptV has value %f\n", depthV);
//                    }
                    ptr[index].x = depthV * (x - c.x) / f.x;
                    ptr[index].y = depthV * (y - c.y) / f.y;
                    ptr[index].z = depthV;
//                    if(x == 394 && y == 285) {
//                        printf("DeptV has value\n");
//                    }

                } else {
                    ptr[index].x = 0.f;
                    ptr[index].y = 0.f;
                    ptr[index].z = 0.f;
//                    if(x == 393 && y == 286) {
//                        printf("DeptV has no value\n");
//                    }
                }
            }
            __global__ void NormalVectors(DepthMap* depth, float3* vertexMap, float3* normalMap) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if(x >= depth->cols - 1 || y >= depth->rows - 1) {
                    return;
                }
//                x = 393; y = 285;
//                printf("Thread %d %d\n", x, y);
                float3 y1, x1, xy;

                y1.x = vertexMap[Index2D(x, y + 1, depth->cols)].x;
                y1.y = vertexMap[Index2D(x, y + 1, depth->cols)].y;
                y1.z = vertexMap[Index2D(x, y + 1, depth->cols)].z;
                x1.x = vertexMap[Index2D(x + 1, y, depth->cols)].x;
                x1.y = vertexMap[Index2D(x + 1, y, depth->cols)].y;
                x1.z = vertexMap[Index2D(x + 1, y, depth->cols)].z;
                xy.x = vertexMap[Index2D(x, y, depth->cols)].x;
                xy.y = vertexMap[Index2D(x, y, depth->cols)].y;
                xy.z = vertexMap[Index2D(x, y, depth->cols)].z;

//                float3 a = (x1 - xy);
//                float3 b = (y1 - xy);

                float3 aCrossB = (x1 - xy).cross((y1 - xy));
//                float ax = a.y * b.z;
//                float bx = -(a.z * b.y);
                float normalTerm = aCrossB.norm();
//                printf("Normal Term is %f\n", normalTerm);
                if (fabsf(normalTerm) == 0.f) {
//                    printf("Cuda setting Zero\n");
                                normalMap[Index2D(x, y, depth->cols)].x = 0;
                                normalMap[Index2D(x, y, depth->cols)].y = 0;
                                normalMap[Index2D(x, y, depth->cols)].z = 0;
                                return;
                //                        std::cout << "Normal Term is Nan " << normalTerm << std::endl;
                //                        std::cout << normal << std::endl;
                //                        std::cout << normal(0) * normal(0) << " " << normal(1) * normal(1) << " " << normal(2) * normal(2) << std::endl;
                //                        std::cout << normal(0) * normal(0) + normal(1) * normal(1) + normal(2) * normal(2) << std::endl;
                //                        std::cout << sqrt(normal(0) * normal(0) + normal(1) * normal(1) + normal(2) * normal(2)) << std::endl;
                //                        exit(0);
                }
                normalMap[Index2D(x, y, depth->cols)] = aCrossB.normalized();
//                printf("CUDA Normalized Values %g %g %g\n", normalMap[Index2D(x, y, depth->cols)].x, normalMap[Index2D(x, y, depth->cols)].y, normalMap[Index2D(x, y, depth->cols)].z);
                if (!isfinite(normalMap[Index2D(x, y, depth->cols)].x) || !isfinite(normalMap[Index2D(x, y, depth->cols)].y) || !isfinite(normalMap[Index2D(x, y, depth->cols)].z)) {
                //                std::cout << "NAN" << std::endl;
//                    printf("IS NOT FINITE %d %d %f %f %f\n",x,y, aCrossB.x, aCrossB.y, aCrossB.z);
                    normalMap[Index2D(x, y, depth->cols)].x = 0;
                    normalMap[Index2D(x, y, depth->cols)].y = 0;
                    normalMap[Index2D(x, y, depth->cols)].z = 0;
                //                std::cout << "Normal Term" << normalTerm << std::endl;
                //                    std::cout << normalMap[y][x] << std::endl;
                //                exit(0);
                }
//                printf("Ending CUDA\n");

            }





            //SOURCE IS THE NEW DEPTH IMAGE, destination is the raycaster
            // raycast is obtained from raycasting in the previous iteration
            __device__ bool PDASearch(DepthMap* depthTarget, matrix33* rotationMatrix, matrix33* inverseRotation, float3* translation,
                                                       matrix33* predictedRotation, matrix33* predictedInverseRotation, float3* predictedTranslation,
                                                       float3* destinationLocalVertexMap, float3* destinationLocalNormalMap,
                                                       float3* sourceLocalVertexMap, float3* sourceLocalNormalMap,
                                                       float3* sourceGlobalVertexMap, float3* sourceGlobalNormalMap, float7* icpEquations,
                                                       matrix33* cameraMatrix, int level, bool* mask) {

                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                float7 zero;
                float _normalThreshold = 0.65, _vertexThreshold = 0.025;
                if(x >= depthTarget->cols || y >= depthTarget->rows ) {
                    return false;
                }
                mask[Index2D(x, y, depthTarget->cols)] = false;
                icpEquations[Index2D(x, y, depthTarget->cols)] = zero;

                float scale = powf(2., -level);
                float fx = cameraMatrix->row0.x * scale;
                float fy = cameraMatrix->row1.y * scale;
                float cx = cameraMatrix->row0.z * scale;
                float cy = cameraMatrix->row1.z * scale;

                gpu::float3 ncurr = sourceLocalNormalMap[gpu::device::Index2D(x, y, depthTarget->cols)];
                if(isnan(ncurr.x)) {
                    return false;
                }
                gpu::float3 vcurr = sourceLocalVertexMap[gpu::device::Index2D(x, y, depthTarget->cols)];
                gpu::float3 vcurrg = (*predictedRotation) * vcurr + (*predictedTranslation);
                gpu::float3 vcurrPrevCS = (*inverseRotation) * (vcurrg - (*translation));
                gpu::float3 uHat = vcurrPrevCS / vcurrPrevCS.z;

                uHat.x = uHat.x * fx + cx;
                uHat.y = uHat.y * fy + cy;
                if(uHat.x < 0 || uHat.x >= depthTarget->cols - 1 || uHat.y < 0 || uHat.y >= depthTarget->rows - 1) {
                    return false;
                }
                gpu::float3 nm1g = destinationLocalNormalMap[gpu::device::Index2D((int)uHat.x, (int)uHat.y, depthTarget->cols)];
                if(isnan(nm1g.x)) {
                    return false;
                }
                gpu::float3 vm1g = destinationLocalVertexMap[gpu::device::Index2D((int)uHat.x, (int)uHat.y, depthTarget->cols)];
                gpu::float3 ncurrg = (*predictedRotation) * ncurr;
                float diff = (vm1g - vcurrg).norm();
                float sine = ((ncurrg).cross(nm1g)).norm();
                if(sine > _normalThreshold && diff < _vertexThreshold) {
                    gpu::float3 n = nm1g;
                    gpu::float3 d = vm1g;
                    gpu::float3 s = vcurrg;
                    gpu::float3 scn = s.cross(n);
                    float b = n.dot(d - s);
                    gpu::float7 eq(scn, n, b);
                    eq.err = (vm1g - vcurrg).dot(n);
                    eq.err = eq.err * eq.err;
                    mask[Index2D(x, y, depthTarget->cols)] = true;
                    icpEquations[Index2D(x, y, depthTarget->cols)] = eq;
                    return true;
                }
                return false;
            }

            __device__ bool PDASearchCorrected(DepthMap* depthTarget, matrix33* rotationMatrix, matrix33* inverseRotation, float3* translation,
                                      matrix33* predictedRotation, matrix33* predictedInverseRotation, float3* predictedTranslation,
                                      float3* Vm1gMap, float3* Nm1gMap,
                                      float3* VCurrLocalMap, float3* NCurrLocalMap,
                                      float3* sourceGlobalVertexMap, float3* sourceGlobalNormalMap, float7* icpEquations,
                                      matrix33* cameraMatrixOriginal, int level, bool* mask, float3* voxelCellSize) {

                bool newFix = false;
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                float7 zero;
                if(x >= depthTarget->cols || y >= depthTarget->rows ) {
                    return false;
                }
                int rows = depthTarget->rows, cols = depthTarget->cols;


                //Zero things if anything fails
                mask[Index2D(x, y, depthTarget->cols)] = false;
                icpEquations[Index2D(x, y, depthTarget->cols)] = zero;


                float scale = powf(2., -level);
                gpu::matrix33 cameraMatrix = (*cameraMatrixOriginal)* scale;
                cameraMatrix.row2.z = 1;
                zero.err = 0;

                gpu::float3 halfValue(0.5f, 0.5f, 0.f);
                float distanceThres = 0.025;
                float normalThres = 0.65;
                //source = vcurrg, destination = vm1g

                //Building nm1g
                gpu::float3 nm1g;
                nm1g = Nm1gMap[gpu::device::Index2D(x, y, cols)];
                if(isnan(nm1g.x)) {
                    return false;
                }

                gpu::float3 vm1g, vm1, point;
                vm1g = Vm1gMap[gpu::device::Index2D(x, y, cols)];

//            vm1 = inverseRotation * (vm1g - translation);
                vm1 = (*inverseRotation) * vm1g - (*inverseRotation) * (*translation);
                if(vm1.z <= 0.f) {
                    return false;
                }

                point = cameraMatrix * vm1;

                point = point/point.z;
                point = point + halfValue;
                //Persepective projection

                gpu::int3 uHat((int) point.x, (int) point.y, (int) point.z);
                if(uHat.x < 0 || uHat.x >= cols || uHat.y < 0 || uHat.y >= rows) {
                    return false;
                }

                ///Get NM1G
                gpu::float3 ncurr, ncurrg;
                ncurr = NCurrLocalMap[gpu::device::Index2D(uHat.x, uHat.y, cols)];
                if(isnan(ncurr.x)) {
                    return false;
                }
                ncurrg = (*rotationMatrix) * ncurr; //TODO THIS IS A ERROR SHOULD BE THE PREDICATED ROTATION

                gpu::float3 vcurr, vcurrg;
                vcurr = VCurrLocalMap[gpu::device::Index2D(uHat.x, uHat.y, cols)];
                vcurrg = (*predictedRotation) * vcurr + (*predictedTranslation);

                // Convert Vcurrg to voxel then convert from voxel to point
                // Doesnt work since the position is based off the vm1g is based off interpolation of some points to produce a vector close to
                // the voxel position but not directly at the voxel pos
                if(newFix) {
                    int3 voxel = gpu::device::WorldToVoxel(vcurrg, NULL, NULL, voxelCellSize);
                    vcurrg = gpu::device::VoxelToWorld(voxel.x, voxel.y, voxel.z, NULL, NULL, voxelCellSize);
                    printf("New Fix %f %f %f | %f %f %f\n", vcurrg.x, vcurrg.y, vcurrg.z, vm1g.x, vm1g.y, vm1g.z);
                }

                float distance = ((vm1g - vcurrg).norm());
                if(distance > distanceThres) {
                    return false;
                }


//            float normal = (ncurrg.cross(nm1g)).norm();  // must be less than threshold to be vaild
//
//            if(normal > normalThres) { // difference is normal < normalThres vs normal > normalThres
//                continue;
//            }

                float normal = std::abs(ncurrg.dot(nm1g)); //kinect version seems broken 1 is if they are exactly the same
                if(normal < normalThres) { // difference is normal < normalThres vs normal > normalThres
                    return false;
                }

                //TODO Optimised version
                gpu::float3 a = vcurrg.cross(nm1g);
                float b = nm1g.dot(vm1g - vcurrg);
                gpu::float7 equation(a, nm1g, b);
                equation.err = (vcurrg - vm1g).dot(nm1g);


                equation.err = equation.err * equation.err;
                icpEquations[gpu::device::Index2D(x, y, cols)] = equation;
                mask[gpu::device::Index2D(x, y, cols)] = true;
                return true;
            }
            __global__ void ProjectiveDataAssoication(DepthMap *depthTarget, matrix33 *rotationMatrix,
                                                      matrix33 *inverseRotation, float3 *translation,
                                                      matrix33 *predictedRotation, matrix33 *predictedInverseRotation,
                                                      float3 *predictedTranslation,
                                                      float3 *destinationLocalVertexMap,//should be from raycaster
                                                      float3 *destinationLocalNormalMap,//should be from raycaster
                                                      float3 *sourceLocalVertexMap, float3 *sourceLocalNormalMap,//image
                                                      float3 *sourceGlobalVertexMap, float3 *sourceGlobalNormalMap,// not used
                                                      float7 *icpEquations,
                                                      matrix33 *cameraMatrix, int level, bool *mask, float3* voxelCellSize) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                float7 equations;
//                float _normalThreshold = 0.65, _vertexThreshold = 0.025;
                if(x >= depthTarget->cols || y >= depthTarget->rows ) {
                    return;
                }
//                bool pass = PDASearch(depthTarget, rotationMatrix, inverseRotation, translation,
//                predictedRotation, predictedInverseRotation, predictedTranslation,
//                destinationLocalVertexMap, destinationLocalNormalMap,
//                sourceLocalVertexMap, sourceLocalNormalMap,
//                sourceGlobalVertexMap, sourceGlobalNormalMap, icpEquations, cameraMatrix, level, mask);
                bool pass = PDASearchCorrected(depthTarget, rotationMatrix, inverseRotation, translation,
                                      predictedRotation, predictedInverseRotation, predictedTranslation,
                                      destinationLocalVertexMap, destinationLocalNormalMap,
                                      sourceLocalVertexMap, sourceLocalNormalMap,
                                      sourceGlobalVertexMap, sourceGlobalNormalMap, icpEquations, cameraMatrix, level, mask, voxelCellSize);

                //Reduction should follow
            }
            //1024

            /*
             * <blocks> number of CUDA blocks run in the current kernel
             * float7* matrix, array of float7 containing the matrix
             * float* result, a 42 * <blocks> for the reduction to take place, still need to perform block reduction afterwards
             * float* blockError, <blocks> size where the reduction can take place, still need to perform block reduction afterwards
             */
            __global__ void Reductions(float7* matrix, int size, float* result, float* blockError) {
                int x = blockDim.x * blockIdx.x + threadIdx.x; //global x pos in grid
                int skip = blockIdx.x * 42;

                int threadId = threadIdx.x;
                __shared__ float smem[REDUCTIONTHREADS]; // number of threads
                __shared__ float smemError[REDUCTIONTHREADS];
                smem[threadId] = 0;
                smemError[threadId] = 0;
                if(x >= size) {
                    return;
                }
                //Error reduction
                smemError[threadId] = matrix[x].err;
                __syncthreads();
                {
                    unsigned int s;
                    for (s = blockDim.x/2; s>0; s>>=1) {
                        if (threadId < s) {
                            smemError[threadId] += smemError[threadId + s];
                        }
                        __syncthreads();
                    }
                }
                if(threadId == 0) {
                    blockError[blockIdx.x] = smemError[0];
//                    if(blockIdx.x == 0) {
//                        int i;
//                        printf("Block dim %d %d %d\n", blockDim.x, blockDim.x/2, 512>>1);
//                        for(i = 0; i < REDUCTIONTHREADS; i++) {
//                            printf("v %f %d %d\n", smemError[i], i, blockIdx.x);
//                        }
//                    }
                }
                //Matrix reduction
                // for loops
                int i, j;
                for(i = 0; i < 6; i++ ){
                    for(j = i; j < 7; j++) { //should be from j = i
                        __syncthreads();
                        smem[threadId] = matrix[x](i) * matrix[x](j);
                        __syncthreads();
                        // reduce shared mem
                        {
                            unsigned int s;
                            for (s = blockDim.x/2; s>0; s>>=1) {
                                if (threadId < s) {
                                    smem[threadId] += smem[threadId + s];
                                }
                                __syncthreads();
                            }
                        }
                        if(threadId == 0) {
                            result[skip + i * 7 + j] = smem[0]; // per block
                        }
                    }
                }
            }
            __global__ void BlockReductions(float* results, int size, float* blockError, float* totalError) {
                int threadId = threadIdx.x;
                __shared__ float smem[1024]; //TODO BEtter solution required max is 301 for 640 * 480 reduction requires this num
                __shared__ float smemError[1024];
                smem[threadId] = 0;
                smemError[threadId] = 0;
                if(threadId >= size) {
                    return;
                }
                //Error block reduction
                smemError[threadId] = blockError[threadId];
                __syncthreads();
                {
                    unsigned int s;
                    for (s = blockDim.x/2; s>0; s>>=1) {
                        if (threadId < s) {
                            smemError[threadId] += smemError[threadId + s];
                        }
                        __syncthreads();
                    }
                }
                if(threadId == 0) {
                    (*totalError) = smemError[0];
                }
                //Matrix block reduction
                int i, j;
                for(i = 0; i < 6; i++ ){
                    for(j = i; j < 7; j++) { //should be from j = i
                        __syncthreads();
//                        if( i== 0 && j == 0) {
//                            printf("value loading %f\n", results[x * 42 + i * 7 + j]);
//                        }
                        smem[threadId] = results[threadId * 42 + i * 7 + j];
                        __syncthreads();
                        // reduce shared mem
                        {
                            unsigned int s;
                            for (s = blockDim.x/2; s>0; s>>=1) { // TODO Better solution for blockdim.x
                                if (threadId < s) {
                                    smem[threadId] += smem[threadId + s];
                                }
                                __syncthreads();
                            }
                        }
                        if(threadId == 0) {
                            results[i * 7 + j] = smem[0]; // per block
                        }
                    }
                }
            }
        };
    };
    namespace host {
        namespace IterativeClosestPoint {


            void Reduction(float7 *matrix, int size, float *result, float *blockError) {
                dim3 threads(REDUCTIONTHREADS, 1, 1);
                dim3 blocks((size/threads.x) + 1, 1, 1);
//                printf("Running %d threads with %d blocks\n", threads.x, blocks.x);
                CudaErrorCheck(__FILE__, __LINE__);
                gpu::device::IterativeClosestPoint::Reductions<<<blocks, threads>>>(matrix, size, result, blockError);
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
            }
            // size is num blocks to reduce
            void BlockReduction(float* results, int size, float* errorBlocks, float* error) {
                dim3 threads(1024, 1, 1); //TODO Related to shared memory of block reduction
                dim3 blocks(1, 1, 1);
//                printf("Running %d threads with %d blocks (Note: Should always be 1 block)\n", threads.x, blocks.x);
                gpu::device::IterativeClosestPoint::BlockReductions<<<blocks, threads>>>(results, size, errorBlocks, error);
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
            }
            void ReduceEquations(float7* equations, int size, float* results, float* error, float* errorblocks) {

                gpu::host::IterativeClosestPoint::Reduction(equations, size, results, errorblocks);
//                gpu::host::IterativeClosestPoint::BlockReduction(results, GetReductionBlocks()); //TODO should be Get Reduction blocks only for rows * cols
                gpu::host::IterativeClosestPoint::BlockReduction(results, (size/REDUCTIONTHREADS) + 1, errorblocks, error);
            }
            void ReduceEquations(int level) {
                int size = GetImageRows() * GetImageCols();
                int r, c, i;
                int skip = 0;
                r = GetImageRows();
                c = GetImageCols();
                for (i = 0; i < level; i++) {
                    skip += r * c;
                    r /= 2;
                    c /= 2;
                }
//                size = 1023;
                cudaMemset(GetGPUICPReductionMatrix(), 0, sizeof(float) * 6 * 7 * (GetImageRows() * GetImageCols()) / REDUCTIONTHREADS + 1);
                Reduction(GetGPUICPEquations() + skip, size, GetGPUICPReductionMatrix(), GetGPUICPErrorBlocks());
                BlockReduction(GetGPUICPReductionMatrix(), (GetImageRows() * GetImageCols()) / REDUCTIONTHREADS + 1, GetGPUICPErrorBlocks(), GetGPUICPError()); //TODO
            }
//            void reductionTest(float* smem, float* final, int size) {
//                std::cout << "Kernel Launched" << std::endl;
////                dim3 equationsDimBlock = DivUp(150, 150);
////                dim3 equationsDimThreads = GetGrid();
//                dim3 equationsDimBlock(4, 4);
//                dim3 equationsDimThreads(2, 2);
//                //smem 128
//                device::IterativeClosestPoint::reduce2<<<equationsDimBlock, equationsDimThreads>>>(smem, final, 64,64);
//                cudaDeviceSynchronize();
//                CudaErrorCheck(__FILE__, __LINE__);
//
//                equationsDimThreads = equationsDimBlock.x * equationsDimBlock.y;
//                int rows = equationsDimBlock.x, cols = equationsDimBlock.y;
//                equationsDimBlock = dim3(1);
//                device::IterativeClosestPoint::reduceBlocks<<<equationsDimBlock, equationsDimThreads>>>(final, rows, cols);
//                cudaDeviceSynchronize();
//                CudaErrorCheck(__FILE__, __LINE__);
//            }
//            void AstarAReductionKernel(float7* equationsPointer, int num, float* result, bool* mask) {
//                std::cout << "LAunching ASTARAREDUCTIONKERNEL" << std::endl;
//                dim3 equationsDimBlock(6, 6);
//                dim3 equationsDimThreads(1, 1);
//                device::IterativeClosestPoint::reduceAStarA<<<equationsDimBlock, equationsDimThreads>>>(equationsPointer, num, result, mask, GetGPUICPMaskCount());
//                cudaDeviceSynchronize();
//                CudaErrorCheck(__FILE__, __LINE__);
//            }
//            void equationTests(int num) {
//                float7 eqs[num];
//                int x, y, counter = 0;
//                for(x = 0; x < num;x++) {
//                    for(y = 0; y < 7; y++) {
//                        eqs[x].values[y] = counter;
//                        counter++;
//                    }
//                }
//                cudaMemcpy(GetGPUICPEquations(), eqs, sizeof(float7) * num, cudaMemcpyHostToDevice);
//                cudaDeviceSynchronize();
//                CudaErrorCheck(__FILE__, __LINE__);
//                dim3 equationsDimBlock(6, 6);
//                dim3 equationsDimThreads(1, 1);
//                device::IterativeClosestPoint::reduceAStarA<<<equationsDimBlock, equationsDimThreads>>>(GetGPUICPEquations(), num, GetGPUICPMatrixAstarA(), GetGPUICPMask(), GetGPUICPMaskCount());
//                cudaDeviceSynchronize();
//                CudaErrorCheck(__FILE__, __LINE__);
//                dim3 equationsBDimBlock(1);
//                dim3 equationsBDimGrid(6);
//                device::IterativeClosestPoint::reduceAstarB<<<equationsBDimGrid, equationsBDimBlock>>>(GetGPUICPEquations(), num, GetGPUICPMatrixAstarB(), GetGPUICPMask());
//                cudaDeviceSynchronize();
//                CudaErrorCheck(__FILE__, __LINE__);
//            }
            void VertexMap(DepthMap* gpuDepthMap, float3* gpuVertexMap, int level) {
                int rows = (int)(GetImageRows() * pow(2, -level));
                int cols = (int)(GetImageCols() * pow(2, -level));
                dim3 dimBlock = DivUp(cols, rows);
                dim3 dimGrid = GetGrid();
                device::IterativeClosestPoint::VertexVectors<<<dimBlock, dimGrid>>>(gpuDepthMap, gpuVertexMap, GetGPUCameraMatrix(), level);
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
            }
            void NormalMap(DepthMap* gpuDepthMap, float3* gpuVertexMap, float3* gpuNormalMap) {
                dim3 dimBlock = DivUp(cols, rows);
                dim3 dimGrid = GetGrid();
//                dim3 dimBlock(1, 1);// = DivUp(cols, rows);
//                dim3 dimGrid(1, 1);// = GetGrid();
                device::IterativeClosestPoint::NormalVectors<<<dimBlock, dimGrid>>>(gpuDepthMap, gpuVertexMap, gpuNormalMap);
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
            }
            void BuildTargetVertexAndNormalMaps() {
                int level;
                int rows = GetImageRows();
                int cols = GetImageCols();
                int skip = 0;
                for (level = 0; level < GetICPLevels(); level++) {
                    VertexMap(GetGPUTargetDepthMapContainersICP() + level, GetGPULocalTargetVertexMap() + skip, level);
                    NormalMap(GetGPUTargetDepthMapContainersICP() + level, GetGPULocalTargetVertexMap() + skip,
                              GetGPULocalTargetNormalMap() + skip);
//                    VertexMapToGlobal(GetGPULocalTargetVertexMap() + skip, GetGPURotation(), GetGPUTranslation(), GetGPUGlobalTargetVertexMap() + skip, rows, cols);
//                    NormalMapToGlobal(GetGPULocalTargetNormalMap() + skip, GetGPURotation(), GetGPUGlobalTargetNormalMap() + skip, rows, cols);
                    skip += rows * cols;
                    rows /=2;
                    cols /=2;
                }
            }


            void IterativeClosestPoint(int level, int rows, int cols, int skip) {
                dim3 dimBlock = gpu::host::DivUp(cols, rows);
                dim3 dimGrid = gpu::host::GetGrid();
                device::IterativeClosestPoint::ProjectiveDataAssoication<<<dimBlock, dimGrid>>>
                                                                                     (GetGPUTargetDepthMapContainersICP() + level, GetGPURotation(), GetGPUInverseRotation(), GetGPUTranslation(),
                                                                                             GetGPUPredictedRotation(), GetGPUPredictedInverseRotation(), GetGPUPredictedTranslation(),
                                                                                             GetGPUSourceVertexMap() + skip, GetGPUSourceNormalMap() + skip,//RayCaster
                                                                                             GetGPULocalTargetVertexMap() + skip, GetGPULocalTargetNormalMap() + skip,//new Image
                                                                                             GetGPUGlobalTargetVertexMap() + skip, GetGPUGlobalTargetNormalMap() + skip, GetGPUICPEquations() + skip,
                                                                                             GetGPUCameraMatrix(), level, GetGPUICPMask(), GetGPUVoxelCellSize());
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);

                cudaMemset(GetGPUICPReductionMatrix(), 0, sizeof(float) * 6 * 7 * GetReductionBlocks());
                ReduceEquations(GetGPUICPEquations() + skip, rows * cols, GetGPUICPReductionMatrix(), GetGPUICPError(), GetGPUICPErrorBlocks());
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
            }
        };
    };
};