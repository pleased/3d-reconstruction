/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/11/08.
//

#include "KinectFusionViewerMemCopy.hpp"
#include <cuda_runtime.h>
#include <device_launch_parameters.h>
#include <math_functions.h>
#include <iostream>
#include "DeviceTypes.hpp"
#include "GlobalHost.hpp"
#include "GlobalDevice.hpp"
namespace gpu {
    namespace device {
        namespace KinectFusionViewerMemCopy {
            __global__ void CopyLoadedDepthMapImage(float* src, int* dst, int rows, int cols) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if(x >= cols || y >= rows) {
                    return;
                }
                int depth = src[Index2D(x, y, cols)] / 10;
                if(depth > 255) {
                    depth = 255;
                }
                uchar* dstPtr = (uchar*) (dst + Index2D(x, y, cols));
                dstPtr[0] = depth;
                dstPtr[1] = depth;
                dstPtr[2] = depth;
                dstPtr[3] = 255;

            }
            __global__ void CopyLoadedColourImage(uchar* src, int* dst, int rows, int cols, int channels) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if(x >= cols || y >= rows) {
                    return;
                }
                uchar* dstPtr = (uchar*) (dst + Index2D(x, y, cols));
                uchar* srcPtr = (src + Index2D(x, y, cols) * channels);
                dstPtr[0] = srcPtr[2];
                dstPtr[1] = srcPtr[1];
                dstPtr[2] = srcPtr[0];
                dstPtr[3] = 255;
            }
            __global__ void CopyColourImage(uchar* src, int* dst, int rows, int cols) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if(x >= cols || y >= rows) {
                    return;
                }
                //dst must be rgba, src is argb
                uchar* dstPtr = (uchar*) (dst + Index2D(x, y, cols));

                dstPtr[0] = src[Index2D(x, y, cols) * 3 + 0]; // r
                dstPtr[1] = src[Index2D(x, y, cols) * 3 + 1]; // g
                dstPtr[2] = src[Index2D(x, y, cols) * 3 + 2]; // b
                dstPtr[3] = 255; // a
            }
            __global__ void CopyDepthMapImage(float3* src, int* dst, int rows, int cols, matrix33* inverseRotation, float3* translation) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if(x >= cols || y >= rows) {
                    return;
                }
                float3 result = (*inverseRotation) * (src[Index2D(x, y, cols)] - (*translation));
                //Scaling 2.5m should be 250, 0m should be 0 format is RGBA
//                float scale = (255.f/65535.f);
                float scale = (255.f/10000.f);
                int depth = (int)(scale * (5000 * result.z));
//                if(depth > 255) {
//                    depth = 255;
//                }
                uchar* dstptr = (uchar*)&(dst[Index2D(x, y, cols)]);
                dstptr[0] = (uchar)depth;
                dstptr[1] = (uchar)depth;
                dstptr[2] = (uchar)depth;
                dstptr[3] = 255;
            }
            __global__ void CopyBilateralDepthMapImage(DepthMap* depthMap, int* dst, int rows, int cols) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if(x >= cols || y >= rows) {
                    return;
                }
//                float scale = (255.f/65535.f); TODO this is the correct scale, but to see we will scale it for 2m
                float scale = (255.f/10000.f);
                int depth = (int)(scale * (((*depthMap)(y, x) * 5)));
                if(depth > 255) {
                    depth = 255;
                }
                uchar* dstptr = (uchar*)&(dst[Index2D(x, y, cols)]);
                dstptr[0] = (uchar)depth;
                dstptr[1] = (uchar)depth;
                dstptr[2] = (uchar)depth;
                dstptr[3] = 255;
            }

            /**
             * UShort
             * Used for range image and IR Image since they are in the same format
             */
            const float _ShortScaleFactor = ((float)UCHAR_MAX)/USHRT_MAX;
            __global__ void CopyRangeImage(unsigned short* rangeImage, int* ptr, int rows, int cols) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if(x >= cols || y >= rows) {
                    return;
                }
                unsigned short value = (unsigned short)(rangeImage[Index2D(x, y, cols)]* _ShortScaleFactor);
//                uchar value = (uchar)(rangeImage[Index2D(x, y, cols)] * scale);
                if(value > 255) {
                    value = 255;
                }
                if (value < 0 ) {
                    value = 0;
                }
                uchar* dstptr = (uchar*)&(ptr[Index2D(x, y, cols)]);
//                dstptr[0] = (uchar)(rangeImage[Index2D(x, y, cols)]);
//                dstptr[1] = (uchar)(rangeImage[Index2D(x, y, cols)]);
//                dstptr[2] = (uchar)(rangeImage[Index2D(x, y, cols)]);
                dstptr[0] = (uchar)(value);
                dstptr[1] = (uchar)(value);
                dstptr[2] = (uchar)(value);
                dstptr[3] = 255;
            }
            __global__ void CopyICPOutliersImage(bool* mask, int* dst, int rows, int cols) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if(x >= cols || y >= rows) {
                    return;
                }
                uchar* dstptr = (uchar*)&(dst[Index2D(x, y, cols)]);

                if(mask[Index2D(x,y ,cols)]) {
                    dstptr[0] = (uchar)255;
                    dstptr[1] = (uchar)255;
                    dstptr[2] = (uchar)255;
                    dstptr[3] = 255;
                } else {
                    dstptr[0] = (uchar)0;
                    dstptr[1] = (uchar)0;
                    dstptr[2] = (uchar)0;
                    dstptr[3] = 255;
                }
            }
            __global__ void SetZero(int* dst, int rows, int cols) {
                int x = blockDim.x * blockIdx.x + threadIdx.x;
                int y = blockDim.y * blockIdx.y + threadIdx.y;
                if(x >= cols || y >= rows) {
                    return;
                }
                dst[Index2D(x, y, cols)] = 0;
            }
        };
    };
    namespace host {
        namespace KinectFusionViewerMemCopy {
            void CopyRaycasterOnly(int** ptr) {
                //FIrst viewer is colour image
                dim3 dimBlock = gpu::host::DivUp(GetImageCols(), GetImageRows());
                dim3 dimGrid = gpu::host::GetGrid();

//                //Loaded Colour Image
//                device::KinectFusionViewerMemCopy::SetZero<<<dimBlock, dimGrid>>>(GetGPUColourImage(), ptr[1], GetImageRows(), GetImageCols());
//                cudaDeviceSynchronize();
//                CudaErrorCheck(__FILE__, __LINE__);
//
//
//                //Loaded DepthMap
//                device::KinectFusionViewerMemCopy::CopyLoadedDepthMapImage<<<dimBlock, dimGrid>>>(GetGPUDepthMap(), ptr[0], GetImageRows(), GetImageCols());
//                cudaDeviceSynchronize();
//                CudaErrorCheck(__FILE__, __LINE__);
//
//
//
//                //Tsdf Colour Image
//                device::KinectFusionViewerMemCopy::CopyColourImage<<<dimBlock, dimGrid>>>(GetGPUSourceColourMap(), ptr[2], GetImageRows(), GetImageCols());
//
//                cudaDeviceSynchronize();
//                CudaErrorCheck(__FILE__, __LINE__);


                //Tsdf DepthMap
                device::KinectFusionViewerMemCopy::CopyDepthMapImage<<<dimBlock, dimGrid>>>(GetGPUSourceVertexMap(), ptr[3], GetImageRows(),
                        GetImageCols(), GetGPUInverseRotation(), GetGPUTranslation());
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);

            }
            void CopyToViewers(int** ptr) {
                //FIrst viewer is colour image
                dim3 dimBlock = gpu::host::DivUp(GetImageCols(), GetImageRows());
                dim3 dimGrid = gpu::host::GetGrid();

                //Loaded Colour Image
                device::KinectFusionViewerMemCopy::CopyLoadedColourImage<<<dimBlock, dimGrid>>>(GetGPUColourImage(), ptr[1], GetImageRows(), GetImageCols(), GetImageChannels());
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);


                //Loaded DepthMap
//                device::KinectFusionViewerMemCopy::CopyLoadedDepthMapImage<<<dimBlock, dimGrid>>>(GetGPUDepthMap(), ptr[0], GetImageRows(), GetImageCols());
                //BilateralFiltered image
                device::KinectFusionViewerMemCopy::CopyBilateralDepthMapImage<<<dimBlock, dimGrid>>>(GetGPUTargetDepthMapContainersICP(), ptr[0], GetImageRows(), GetImageCols());
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);



                //Tsdf Colour Image
                device::KinectFusionViewerMemCopy::CopyColourImage<<<dimBlock, dimGrid>>>(GetGPUSourceColourMap(), ptr[2], GetImageRows(), GetImageCols());
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);

//                ICP Outliers
//                device::KinectFusionViewerMemCopy::CopyICPOutliersImage<<<dimBlock, dimGrid>>>(GetGPUICPMask(), ptr[2], GetImageRows(), GetImageCols());
//                cudaDeviceSynchronize();
//                CudaErrorCheck(__FILE__, __LINE__);


                //Tsdf DepthMap
                device::KinectFusionViewerMemCopy::CopyDepthMapImage<<<dimBlock, dimGrid>>>(GetGPUSourceVertexMap(), ptr[3], GetImageRows(),
                        GetImageCols(), GetGPUInverseRotation(), GetGPUTranslation());
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);


                // Second viere is depth map
//                dimBlock = gpu::host::DivUp(GetImageCols(), GetImageRows());
//                dimGrid = gpu::host::GetGrid();

//
//                dimBlock = gpu::host::DivUp(GetImageCols(), GetImageRows());
//                dimGrid = gpu::host::GetGrid();

//
//                dimBlock = gpu::host::DivUp(GetImageCols(), GetImageRows());
//                dimGrid = gpu::host::GetGrid();

            }
        };
        namespace KinectCaptureViewerMemCopy {
            // input should be gpu points, ptr is KinectFusionViewer opengl pointers
            void CopyToViewer(unsigned char* colourImage, unsigned short* irImage, unsigned short* depthImage, int** ptr) {
                dim3 dimBlock = gpu::host::DivUp(512, 424);
                dim3 dimGrid = gpu::host::GetGrid();

                //Loaded Colour Image
                device::KinectFusionViewerMemCopy::CopyLoadedColourImage<<<dimBlock, dimGrid>>>(colourImage, ptr[1], 424, 512, 4);
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
                //Loaded Ranged Image
                device::KinectFusionViewerMemCopy::CopyRangeImage<<<dimBlock, dimGrid>>>(irImage, ptr[2], 424, 512);
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);

//              Loaded IR Image
                device::KinectFusionViewerMemCopy::CopyRangeImage<<<dimBlock, dimGrid>>>(depthImage, ptr[0], 424, 512);
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
                //Loaded Colour Image
                device::KinectFusionViewerMemCopy::SetZero<<<dimBlock, dimGrid>>>(ptr[3], 424, 512);
                cudaDeviceSynchronize();
                CudaErrorCheck(__FILE__, __LINE__);
            }
        };
    };
};
