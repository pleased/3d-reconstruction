/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/09/07.
//

#ifndef VSLAM_GLOBALTESTFUNCTIONS_HPP
#define VSLAM_GLOBALTESTFUNCTIONS_HPP
#include "../KinectFusion/DeviceTypes.hpp"
bool inline fequal(float a, float b, float delta=1.e-6) {
    return (fabsf(a - b) < delta);
}
bool inline fequal(gpu::float3 a, gpu::float3 b, float eps=1.e-2) {
    return fequal(a.x, b.x, eps) && fequal(a.y, b.y, eps) && fequal(a.z, b.z, eps);
}
#endif //VSLAM_GLOBALTESTFUNCTIONS_HPP
