/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/07/19.
//

#include "DeviceTypesUnitTests.hpp"

bool fequal(float a, float b) {
    float eps = 1.e-4;
    return (a-b) < eps;
}
void _TestDeviceTypesFloat3() {
    std::cout << "Starting _TestDeviceTypesDouble3 UnitTest" << std::endl;
    gpu::float3 d1(10, 15, 20), d2(25, 40, 1), d3(100, -213, 23);
    assert(fequal(d1.x , 10));
    assert(fequal(d1.y , 15));
    assert(fequal(d1.z , 20));

    assert(fequal(d2.x , 25));
    assert(fequal(d2.y , 40));
    assert(fequal(d2.z , 1));

    assert(fequal(d3.x , 100));
    assert(fequal(d3.y , -213));
    assert(fequal(d3.z , 23));


    //Operator tests

    gpu::float3 d4 = d1 - d2;
    assert(fequal(d4.x , -15));
    assert(fequal(d4.y , -25));
    assert(fequal(d4.z , 19));

    gpu::float3 d5 = d4 + d2;
    assert(fequal(d5.x , d1.x));
    assert(fequal(d5.y , d1.y));
    assert(fequal(d5.z , d1.z));

    gpu::float3 d1d2 = d1 * d2;
    assert(fequal(d1d2.x , d1.x * d2.x));
    assert(fequal(d1d2.y , d1.y * d2.y));
    assert(fequal(d1d2.z , d1.z * d2.z));

    gpu::float3 d11 = d1d2 / d2;
    assert(fequal(d1.x , d11.x));
    assert(fequal(d1.y , d11.y));
    assert(fequal(d1.z , d11.z));
    gpu::float3 d12 = d1d2 / d1;
    assert(fequal(d2.x , d12.x));
    assert(fequal(d2.y , d12.y));
    assert(fequal(d2.z , d12.z));

    gpu::float3 d1d3 = d1 * d3;
    assert(fequal(d1d3.x , d1.x * d3.x));
    assert(fequal(d1d3.y , d1.y * d3.y));
    assert(fequal(d1d3.z , d1.z * d3.z));

    gpu::float3 d31 = d1d3 / d3;
    assert(fequal(d31.x , d1.x));
    assert(fequal(d31.y , d1.y));
    assert(fequal(d31.z , d1.z));

    gpu::float3 d32 = d1d3 / d1;
    assert(fequal(d32.x , d3.x));
    assert(fequal(d32.y , d3.y));
    assert(fequal(d32.z , d3.z));


    //Test Equal method
    gpu::float3 e1(1.34215, 1.3333,4.5534);
    gpu::float3 e2(1.34315, 1.3333,4.5534);
    gpu::float3 e3(1.34215, 1.353,4.5534);
    gpu::float3 e4(1.34215, 1.3333,4.6534);
    gpu::float3 e5(1.34215, 1.3333,4.5534);
    assert(!(e1 == e2));
    assert(!(e1 == e3));
    assert(!(e1 == e4));
    assert(e1 == e5);

    //Cross Product

    gpu::float3 a( 1, 3.f, 9), b(2,12,7);
    gpu::float3 res = a.cross(b);
    assert(fequal(res.x, -87));
    assert(fequal(res.x, 11));
    assert(fequal(res.x, 6));
//    exit(0);
    std::cout << "\tSuccessful" << std::endl << "Ending _TestDeviceTypesDouble3 UnitTest" << std::endl;
}
void _TestDeviceTypesMatrix33() {
    std::cout << "Starting _TestDeviceTypesMatrix33 UnitTest" << std::endl;

    gpu::matrix33 zero;
    assert(fequal(zero.row0.x,0));
    assert(fequal(zero.row0.y , 0));
    assert(fequal(zero.row0.z , 0));

    assert(fequal(zero.row1.x , 0));
    assert(fequal(zero.row1.y , 0));
    assert(fequal(zero.row1.z , 0));

    assert(fequal(zero.row2.x , 0));
    assert(fequal(zero.row2.y , 0));
    assert(fequal(zero.row2.z , 0));

    gpu::matrix33 eye(1, 0, 0, 0, 1, 0, 0, 0, 1);
    assert(fequal(eye.row0.x , 1));
    assert(fequal(eye.row0.y , 0));
    assert(fequal(eye.row0.z , 0));

    assert(fequal(eye.row1.x , 0));
    assert(fequal(eye.row1.y , 1));
    assert(fequal(eye.row1.z , 0));

    assert(fequal(eye.row2.x , 0));
    assert(fequal(eye.row2.y , 0));
    assert(fequal(eye.row2.z , 1));

    gpu::matrix33 count(1.9, 2.8, 3.7, 4.6, 5.5, 6.4, 7.3, 8.2, 9.1);
    assert(fequal(count.row0.x , 1.9));
    assert(fequal(count.row0.y , 2.8));
    assert(fequal(count.row0.z , 3.7));

    assert(fequal(count.row1.x , 4.6));
    assert(fequal(count.row1.y , 5.5));
    assert(fequal(count.row1.z , 6.4));

    assert(fequal(count.row2.x , 7.3));
    assert(fequal(count.row2.y , 8.2));
    assert(fequal(count.row2.z , 9.1));

    gpu::float3 p(2, 5, 7);

    gpu::float3 eyeP = eye * p;
    assert(fequal(eyeP.x , p.x));
    assert(fequal(eyeP.y , p.y));
    assert(fequal(eyeP.z , p.z));

    gpu::float3 zeroP = zero * p;
    assert(fequal(zeroP.x , 0));
    assert(fequal(zeroP.y , 0));
    assert(fequal(zeroP.z , 0));

    gpu::matrix33 counting(1, 2, 3, 4, 5, 6, 7, 8, 9);
    gpu::float3 v(5, 4, 9);

    gpu::float3 countingV = counting * v;
    assert(fequal(countingV.x , 40));
    assert(fequal(countingV.y , 94));
    assert(fequal(countingV.z , 148));
//TODO

    gpu::float3 transform = count *p + eyeP;
    gpu::float3 transform1 = count *(p + eyeP);
    gpu::float3 transform2 = (count *p) + eyeP;
    assert(fequal(transform.x , 45.7));
    assert(fequal(transform.y , 86.5));
    assert(fequal(transform.z , 126.3));

    assert(fequal(transform1.x , 87.4));
    assert(fequal(transform1.y , 163));
    assert(fequal(transform1.z , 238.6));

    assert(fequal(transform2.x , 45.7));
    assert(fequal(transform2.y , 86.5));
    assert(fequal(transform2.z , 126.3));

    //transpose
    gpu::matrix33 cTranspose = count.transpose();

    assert(fequal(cTranspose.row0.x , 1.9));
    assert(fequal(cTranspose.row0.y , 4.6));
    assert(fequal(cTranspose.row0.z , 7.3));

    assert(fequal(cTranspose.row1.x , 2.8));
    assert(fequal(cTranspose.row1.y , 5.5));
    assert(fequal(cTranspose.row1.z , 8.2));

    assert(fequal(cTranspose.row2.x , 3.7));
    assert(fequal(cTranspose.row2.y , 6.4));
    assert(fequal(cTranspose.row2.z , 9.1));

    //Test Equals

    gpu::matrix33 m1(1, 2, 3, 4, 5, 6, 7, 8, 9);
    gpu::matrix33 m2(2, 2, 3, 4, 5, 6, 7, 8, 9);
    gpu::matrix33 m3(1, 2, 3, 4, 6, 6, 7, 8, 9);
    gpu::matrix33 m4(1, 2, 3, 4, 5, 6, 9, 8, 9);
    gpu::matrix33 m5(1, 2, 3, 4, 5, 6, 7, 8, 9);
    assert(!(m1 == m2));
    assert(!(m1 == m3));
    assert(!(m1 == m4));
    assert(m1 == m5);

    gpu::matrix33 n1(2, -4, 3, 4, 3, 5, 6, 5, 7);
    gpu::matrix33 n2(4, 3, 2, 4, 3, 9, 2, 3, 4);
    gpu::matrix33 n1n2 = n1 * n2;
    assert(n1n2.row0.x == -2.);
    assert(n1n2.row0.y == 3.);
    assert(n1n2.row0.z == -20.);
//    std::cout << n1n2.row1.x << " " << n1n2.row1.y << " " << n1n2.row1.z << std::endl;
    assert(n1n2.row1.x == 38.);
    assert(n1n2.row1.y == 36.);
    assert(n1n2.row1.z == 55.);

    assert(n1n2.row2.x == 58.);
    assert(n1n2.row2.y == 54.);
    assert(n1n2.row2.z == 85.);
//    array([[ -2.,   3., -20.],
//    [ 38.,  36.,  55.],
//    [ 58.,  54.,  85.]])


    std::cout << "\tSuccessful" << std::endl << "Ending _TestDeviceTypesMatrix33 UnitTest" << std::endl;
}
void _TestDeviceTypesDepthMapDestructors() {
    gpu::DepthMap depthMap(15, 15);
    gpu::DepthMap depthMap1(2, 9);
    gpu::DepthMap depthMap2(4, 4);
    gpu::DepthMap depthMap3(6, 2);
    gpu::DepthMap depthMap4(7, 1);

}

void _TestDeviceTypesDepthMap() {
    std::cout << "Starting _TestDeviceTypesDepthMap UnitTest" << std::endl;
    cv::Mat_<double> test(480, 640);
    int x, y;
    for(x = 0; x < test.cols; x++) {
        for(y = 0; y < test.rows; y++) {
            test(y, x) = x * y + y;
        }
    }
    for(x = 0; x < test.cols; x++) {
        for(y = 0; y < test.rows; y++) {
            assert(test(y, x) == x * y + y);
        }
    }

    gpu::DepthMap depthMap(test.rows, test.cols, test.datastart, test.dataend);
    for(x = 0; x < depthMap.cols * depthMap.rows; x++) {
        assert(depthMap(x) == ((double*)test.data)[x]);
    }
    for(x = 0; x < depthMap.cols; x++ ) {
        for( y = 0; y < depthMap.rows; y++) {
            assert(depthMap(y, x) == test(y, x));
        }
    }

    //Testing assignment
    int i = 0;
    gpu::DepthMap depthMapOther(test.rows, test.cols);
    for(x = 0; x < depthMapOther.cols; x++ ) {
        for( y = 0; y < depthMapOther.rows; y++) {
            depthMapOther(y, x) = i;
            i--;
        }
    }

    i = 0;
    for(x = 0; x < depthMapOther.cols; x++ ) {
        for( y = 0; y < depthMapOther.rows; y++) {
            assert(depthMapOther(y, x) == i);
            i--;
        }
    }

    // Testing Equals
    gpu::DepthMap depthMap1(5, 5);
    gpu::DepthMap depthMap2(5, 6);
    gpu::DepthMap depthMap3(4, 5);
    gpu::DepthMap depthMap4(5, 5);
    assert(!(depthMap1 == depthMap2));
    assert(!(depthMap1 == depthMap3));
    assert(depthMap1 == depthMap4);

    free(depthMap1.data);
    free(depthMap4.data);
    depthMap1.data = 0;
    depthMap4.data = 0;
    assert(depthMap1 == depthMap4);

    depthMap1.data = (float*) calloc(depthMap1.rows * depthMap1.cols, sizeof(float));
    assert(!(depthMap1 == depthMap4));
    free(depthMap1.data);
    depthMap1.data = 0;

    depthMap4.data = (float*) calloc(depthMap1.rows * depthMap1.cols, sizeof(float));
    assert(!(depthMap1 == depthMap4));
    free(depthMap4.data);

    depthMap1.data = (float*) calloc(depthMap1.rows * depthMap1.cols, sizeof(float));
    depthMap4.data = (float*) calloc(depthMap4.rows * depthMap4.cols, sizeof(float));
    //fill depthmap1 with data
    float value = 0;
    for(x = 0 ; x < depthMap1.rows * depthMap1.cols; x++) {
        depthMap1(x) = value;
        depthMap4(x) = value;
        value = value - 0.01f;
    }
    assert(depthMap1 == depthMap4);
    depthMap4(5) = 20;
    assert(!(depthMap1 == depthMap4));



    std::cout << "\tSuccessful" << std::endl << "Ending _TestDeviceTypesDepthMap UnitTest" << std::endl ;
}

void _TestDeviceTypesDepthMapVector() {
    std::cout << "Starting _TestDeviceTypesDepthMapVector UnitTest" << std::endl;
    std::vector<gpu::DepthMap> vector;
    vector.resize(1);
    vector[0].Init(10, 10);
    std::cout << "\tSuccessful" << std::endl << "Ending _TestDeviceTypesDepthMapVector UnitTest" << std::endl ;
}
void _TestDeviceTypesDepthMapVector2() {
    std::cout << "Starting _TestDeviceTypesDepthMapVector2 UnitTest" << std::endl;
    std::vector<gpu::DepthMap> vector;
    std::cout << "\tSuccessful" << std::endl << "Ending _TestDeviceTypesDepthMapVector2 UnitTest" << std::endl ;
}
void _TestDeviceTypes() {

}

void _TestDeviceTypesImage() {
    std::cout << "Starting _TestDeviceTypesImage UnitTest" << std::endl;
    cv::Scalar s(255, 0, 0);
    cv::Mat img(480, 640, CV_8UC3, s);
    std::cout << "Image cvtype " << img.type() << std::endl;
    std::cout << "Image channels " << img.channels() << std::endl;


    gpu::Image gpuImg(img.rows, img.cols, img.channels(), img.datastart);


    int x, y;
    for(x = 0; x < img.cols; x++) {
        for(y = 0;y < img.rows; y++) {
            uchar b = img.data[img.channels()*(gpu::device::Index2D(x, y, img.cols)) + 0];
            uchar g = img.data[img.channels()*(gpu::device::Index2D(x, y, img.cols)) + 1];
            uchar r = img.data[img.channels()*(gpu::device::Index2D(x, y, img.cols)) + 2];
            assert(gpuImg(gpu::device::Index2D(x, y, img.cols))[0] == b);
            assert(gpuImg(gpu::device::Index2D(x, y, img.cols))[1] == g);
            assert(gpuImg(gpu::device::Index2D(x, y, img.cols))[2] == r);
        }
    }

    //rebuild image
    cv::Mat rebuilt(img.rows, img.cols, CV_8UC3);

    for(x = 0; x < img.cols; x++) {
        for(y = 0; y < img.rows; y++) {
            int index = gpu::device::Index2D(x, y, img.cols) * 3;
            rebuilt.at<uchar>(index + 0) = gpuImg(y, x)[0];
            rebuilt.at<uchar>(index + 1) = gpuImg(y, x)[1];
            rebuilt.at<uchar>(index + 2) = gpuImg(y, x)[2];
        }
    }
    cv::Mat diff;
    cv::absdiff(img, rebuilt, diff);
//    cv::imshow("Rebuilt image", rebuilt);

//    cv::imshow("Diff", diff);
//    cv::waitKey(0);
    std::cout << "\tSuccessful" << std::endl << "Ending _TestDeviceTypesImage UnitTest" << std::endl ;
}
