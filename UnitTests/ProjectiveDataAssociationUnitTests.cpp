/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/08/23.
//

#include "ProjectiveDataAssociationUnitTests.hpp"

gpu::float3 GetNormal(cv::Mat_<double>& img, gpu::matrix33 inverseCameraMatrix, gpu::matrix33 rotation, gpu::float3 translation, int x, int y) {
    gpu::float3 nan(NAN, NAN, NAN);
    if(y + 1 >= img.rows || x + 1 >= img.cols) {
        return nan;
    }

    if(img(y, x) == 0 || img(y+1,x) == 0|| img(y, x+1) == 0) {
        return nan;
    }
    gpu::float3 y1, x1, xy;

    float currentDepth = img(y + 1, x);
    gpu::float3 imgBLocalCameraPos(x, y + 1, 1);
    imgBLocalCameraPos = inverseCameraMatrix * imgBLocalCameraPos * currentDepth / 1000.;
//    y1 = rotation * imgBLocalCameraPos + translation;
    y1 = imgBLocalCameraPos;




    currentDepth = img(y, x + 1);
    imgBLocalCameraPos.x = x + 1;
    imgBLocalCameraPos.y = y;
    imgBLocalCameraPos.z = 1;
    imgBLocalCameraPos = inverseCameraMatrix * imgBLocalCameraPos * currentDepth / 1000.;
//    x1 = rotation * imgBLocalCameraPos + translation;
    x1 = imgBLocalCameraPos;


    currentDepth = img(y, x);
    imgBLocalCameraPos.x = x;
    imgBLocalCameraPos.y = y;
    imgBLocalCameraPos.z = 1;
    imgBLocalCameraPos = inverseCameraMatrix * imgBLocalCameraPos * currentDepth / 1000.;
//    xy = rotation * imgBLocalCameraPos + translation;
    xy = imgBLocalCameraPos;


    gpu::float3 aCrossB = (x1 - xy).cross((y1 - xy));
    float normalTerm = aCrossB.norm();
    if (fabsf(normalTerm) == 0.f) {
        return nan;
    }
    return aCrossB.normalized();
}
gpu::float3* GenerateVertexMap(cv::Mat_<double> img, gpu::matrix33 cameraMatrix, int levels) {


    int x, y;
    int size = 0;
    int rows = img.rows, cols = img.cols;
    gpu::matrix33* iCm = new gpu::matrix33[levels];
    for(x = 0; x < levels; x++) {
        float scale = powf(2, -x);
        float fx = cameraMatrix.row0.x * scale, fy = cameraMatrix.row1.y * scale, cx = cameraMatrix.row0.z * scale, cy = cameraMatrix.row1.z * scale;
        gpu::matrix33 icmC(1.f/fx, 0, -cx/fx, 0, 1.f/fy, -cy/fy,0 ,0, 1);
        iCm[x] = icmC;
        size += rows * cols;
        rows /= 2;
        cols /= 2;
    }
    std::cout << "SIZE is " << size << std::endl;
    gpu::float3* ptr = new gpu::float3[size]();

    std::memset(ptr, 0, sizeof(gpu::float3) * size);
    gpu::float3 nan(0, 0, 0);
    int level;
    float scale = 1;
    int skip = 0;
    for(level = 0; level < levels; level++) {
        cv::Mat_<double> temp;
        cv::resize(img, temp, cv::Size(0, 0), scale, scale);
        for(x = 0; x < temp.cols; x++) {
            for(y = 0 ; y < temp.rows; y++) {
                int index = skip + gpu::device::Index2D(x, y, temp.cols);
//            if(img(y, x) > 0) {
                float depth = temp(y, x);
                gpu::float3 point(x, y, 1);
                ptr[index] = depth * (iCm[level] * point/1000.);
//            } else {
//                ptr[index] = nan;
//            }
            }
        }
        skip += temp.rows * temp.cols;
        scale /= 2.;
    }


//    int x, y;
//    gpu::float3* ptr = new gpu::float3[img.rows * img.cols];
//    float scale = 1;
//
//    for(x = 0; x < img.cols; x++) {
//        for(y = 0 ; y < img.rows; y++) {
//            int index = gpu::device::Index2D(x, y, img.cols);
////            if(img(y, x) > 0) {
//            float depth = img(y, x);
//            gpu::float3 point(x, y, 1);
//            ptr[index] = inverseCameraMatrix * point * depth /1000.;
////            } else {
////                ptr[index] = nan;
////            }
//        }
//    }
    return ptr;
}

gpu::float3* GenerateNormalMap(gpu::float3* vertexMap, int rows, int cols, int levels) {

    int Trows = rows, Tcols = cols;
    int size = 0;
    int level;
    for(level = 0; level < levels; level++) {
        size += Trows * Tcols;

        Trows /= 2.;
        Tcols /= 2.;
    }

    gpu::float3* normalMap = new gpu::float3[size]();

    std::memset(normalMap, 0, sizeof(gpu::float3) * size);
    gpu::float3 nan(NAN, NAN, NAN);
    int x, y, skip = 0;
    Trows = rows, Tcols = cols;
    for(level = 0; level < levels; level++) {
        for(x = 0 ; x < Tcols; x++ ){
            for(y = 0 ; y < Trows; y++) {
                if(x + 1 >= Tcols || y + 1 >= Trows) {
                    normalMap[skip + gpu::device::Index2D(x, y, Tcols)] = nan;
                    continue;
                }
                gpu::float3 y1, x1, xy;
                y1 = vertexMap[skip + gpu::device::Index2D(x, y + 1, Tcols)];
                x1 = vertexMap[skip + gpu::device::Index2D(x + 1, y, Tcols)];
                xy = vertexMap[skip + gpu::device::Index2D(x, y, Tcols)];
    //            if(x == 639 && y ==457) {
    //                std::cout << y1.x << " " << y1.y << " " << y1.z << std::endl;
    //                std::cout << x1.x << " " << x1.y << " " << x1.z << std::endl;
    //                std::cout << xy.x << " " << xy.y << " " << xy.z << std::endl;
    //                exit(0);
    //            }
//                if(x1.z == 0 || y1.z == 0 || xy.z ==0) {
                if(x1.z < 0.3 || y1.z < 0.3 || xy.z < 0.3) {
                    normalMap[skip + gpu::device::Index2D(x, y, Tcols)] = nan;
                    continue;
                }

                gpu::float3 aCrossB = (x1 - xy).cross((y1 - xy));
                float normalTerm = aCrossB.norm();
                if (fabsf(normalTerm) == 0.f) {
    //                    printf("Cuda setting Zero\n");
    //                std::cout << "should never hit this" << std::endl;
    //                exit(0);
                    normalMap[skip + gpu::device::Index2D(x, y, Tcols)] = nan;
                } else {
                    normalMap[skip + gpu::device::Index2D(x, y, Tcols)] = aCrossB.normalized();
                }
            }
        }
        skip += Trows * Tcols;
        Trows /= 2;
        Tcols /= 2;
    }
    return normalMap;

}
void TransformVertexMap(gpu::float3* vertexMap, int rows, int cols, int levels, gpu::matrix33& rotationMatrix, gpu::float3& translation) {
    int x, y;
    int level, skip = 0;
    int Trows = rows, Tcols = cols;
    for(level = 0; level < levels; level++) {
        for(x = 0; x < Tcols; x++) {
            for(y = 0; y < Trows; y++) {
                vertexMap[skip + gpu::device::Index2D(x, y, Tcols)] = rotationMatrix * vertexMap[skip + gpu::device::Index2D(x, y, Tcols)] + translation;
            }
        }
        skip += Trows * Tcols;
        Trows /= 2.;
        Tcols /= 2.;
    }

}
void TransformNormalMap(gpu::float3* normalMap, int rows, int cols, int levels, gpu::matrix33& rotationMatrix) {
    int x, y;
    int Trows = rows, Tcols = cols, skip = 0, level;
    for(level = 0; level < levels; level++) {
        for(x = 0; x < Tcols; x++) {
            for(y = 0; y < Trows; y++) {
                normalMap[skip + gpu::device::Index2D(x, y, Tcols)] = rotationMatrix * normalMap[skip + gpu::device::Index2D(x, y, Tcols)];
            }
        }
        skip += Tcols * Trows;
        Tcols /= 2.;
        Trows /= 2.;
    }
}
void viewICPOutliers(bool* mask, int rows, int cols) {
    int x, y;
    cv::Mat outliers(rows, cols, CV_8UC1);
    for(x = 0; x < cols; x++) {
        for(y = 0; y < rows; y++) {
            if(mask[gpu::device::Index2D(x, y, cols)]) {
                outliers.at<uchar>(y, x) = 255;
            } else {
                outliers.at<uchar>(y, x) = 0;
            }
        }
    }
    cv::imshow("Outliers", outliers);
    cv::waitKey(100);
}
void viewTransform(cv::Mat_<double>& depthImage, gpu::matrix33 cameraMatrix, gpu::matrix33 rotationMatrix, gpu::float3 translation, gpu::matrix33 newRotation, gpu::float3 newTranslation) {
    int x, y;
    float zero = 0;
    cv::Mat_<float> outliers(depthImage.rows, depthImage.cols, zero);
    gpu::float3 inverseTranslation = -rotationMatrix.transpose() * translation;

    //Compute difference in transformations T_{diff} = T^{-1}_{k-1} * T_{k}
    gpu::matrix33 rotationMatrixDifference  = rotationMatrix.transpose() *  newRotation;
    gpu::float3   translationDifference     = inverseTranslation + (rotationMatrix.transpose() * newTranslation);
//    rotationMatrixDifference.SetIdentity();
//    translationDifference.x = 0;
//    translationDifference.y = 0;
//    translationDifference.z = 0;
    std::cout << "Seg " << std::endl;
    for(x = 0; x < depthImage.cols; x++) {
        for(y = 0; y < depthImage.rows; y++) {
            float depth = (float) depthImage(y, x);
            float z = depth / 1000.f;
            gpu::float3 point((x - cameraMatrix.row0.z) * z/cameraMatrix.row0.x,
                              (y - cameraMatrix.row1.z) * z/cameraMatrix.row1.y,
                               z);
            point = rotationMatrixDifference * point + translationDifference;
            //Now back project
            point = cameraMatrix * point;
            point = point/point.z;
            gpu::int3 uHat((int) point.x, (int) point.y, 1);
            if(uHat.x < 0 || uHat.x >= outliers.cols || uHat.y < 0 || uHat.y >= outliers.rows) {
                continue;
            }
            outliers(uHat.y, uHat.x) = depth;
        }
    }
    std::cout << "Seg 2" << std::endl;
    cv::Mat image;
    outliers.convertTo(image, CV_16U, 5);
    cv::imshow("Transform", image);
    cv::waitKey(0);
}


/**
 * Doesnt work because of the logic
 */
//std::vector<gpu::float7> ProjectiveDataAssociation(gpu::float3 *imgAGlobalVertexMap, gpu::float3 *imgAGlobalNormalMap,
//                                                   gpu::matrix33 rotationMatrix, gpu::matrix33 inverseRotation,
//                                                   gpu::float3 translation,
//                                                   gpu::float3 *imgBLocalVertexMap, gpu::float3 *imgBLocalNormalMap,
//                                                   gpu::matrix33 predictedRotation,
//                                                   gpu::matrix33 predictedInverseRotation,
//                                                   gpu::float3 predictedTranslation,
//                                                   gpu::matrix33 cameraMatrix, gpu::matrix33 inverseCameraMatrix,
//                                                   int rows, int cols, bool *mask, int level) {
//    std::cout << "Starting ProjectiveDataAssociation" << std::endl;
//    int x, y;
//    std::vector<gpu::float7> equations;
//    equations.resize(rows * cols);
//    int counter = 0;
//    gpu::float3 zero;
//    Eigen::Matrix4f transformation, transformationInverse, predictedTransformation, predictedTransformationInverse;
//    transformation.setZero();
//    predictedTransformation.setZero();
//    transformation(0, 0) = rotationMatrix.row0.x;
//    transformation(0, 1) = rotationMatrix.row0.y;
//    transformation(0, 2) = rotationMatrix.row0.z;
//
//    transformation(1, 0) = rotationMatrix.row1.x;
//    transformation(1, 1) = rotationMatrix.row1.y;
//    transformation(1, 2) = rotationMatrix.row1.z;
//
//    transformation(2, 0) = rotationMatrix.row2.x;
//    transformation(2, 1) = rotationMatrix.row2.y;
//    transformation(2, 2) = rotationMatrix.row2.z;
//
//    transformation(0, 3) = translation.x;
//    transformation(1, 3) = translation.y;
//    transformation(2, 3) = translation.z;
//    transformation(3, 3) = 1;
//
//    predictedTransformation(0, 0) = predictedRotation.row0.x;
//    predictedTransformation(0, 1) = predictedRotation.row0.y;
//    predictedTransformation(0, 2) = predictedRotation.row0.z;
//
//    predictedTransformation(1, 0) = predictedRotation.row1.x;
//    predictedTransformation(1, 1) = predictedRotation.row1.y;
//    predictedTransformation(1, 2) = predictedRotation.row1.z;
//
//    predictedTransformation(2, 0) = predictedRotation.row2.x;
//    predictedTransformation(2, 1) = predictedRotation.row2.y;
//    predictedTransformation(2, 2) = predictedRotation.row2.z;
//
//    predictedTransformation(0, 3) = predictedTranslation.x;
//    predictedTransformation(1, 3) = predictedTranslation.y;
//    predictedTransformation(2, 3) = predictedTranslation.z;
//    predictedTransformation(3, 3) = 1;
//
//    predictedTransformationInverse = predictedTransformation.inverse();
//    transformationInverse = transformation.inverse();
//    Eigen::Matrix4f transformDifference = transformationInverse * predictedTransformation;
//
//    Eigen::Matrix4f camMatrix;
//    camMatrix.setIdentity();
//    camMatrix(0, 0) = cameraMatrix(0, 0);
//    camMatrix(0, 2) = cameraMatrix(0, 2);
//    camMatrix(1, 1) = cameraMatrix(1, 1);
//    camMatrix(1, 2) = cameraMatrix(1, 2);
//
//    Eigen::Vector3f halfValue;
//    halfValue(0) = 0.5f;
//    halfValue(1) = 0.5f;
//    halfValue(2) = 0;
//    float distanceThres = 0.025;
//    float normalThres = 0.3;
//    for(x = 0; x < cols; x++) {
//        for(y = 0 ; y < rows; y++) {
//            //Zero things if anything fails
//            equations[gpu::device::Index2D(x, y, cols)] = zero;
//            mask[gpu::device::Index2D(x, y, cols)] = false;
//
//            //Building nm1g
//            Eigen::Vector3f nm1g;
//            nm1g(0) = imgAGlobalNormalMap[gpu::device::Index2D(x, y, cols)].x;
//            nm1g(1) = imgAGlobalNormalMap[gpu::device::Index2D(x, y, cols)].y;
//            nm1g(2) = imgAGlobalNormalMap[gpu::device::Index2D(x, y, cols)].z;
//            if(std::isnan(nm1g(0))) {
//                continue;
//            }
//
//            Eigen::Vector4f vm1g, vm1, vm1Projected;
//            vm1g(0) = imgAGlobalVertexMap[gpu::device::Index2D(x, y, cols)].x;
//            vm1g(1) = imgAGlobalVertexMap[gpu::device::Index2D(x, y, cols)].y;
//            vm1g(2) = imgAGlobalVertexMap[gpu::device::Index2D(x, y, cols)].z;
//            vm1g(3) = 1;
//
//
//            vm1 = transformationInverse * vm1g;
//            vm1 = vm1/vm1(3);
//
//            if(vm1(2) < 0.3 || vm1(2) > 5.) { // Ensure the distance is between 0.3m -> 5m
//                continue;
//            }
//
//            vm1Projected = camMatrix * vm1;
//            Eigen::Vector3f point;
//            point(0) = vm1Projected(0);
//            point(1) = vm1Projected(1);
//            point(2) = vm1Projected(2);
//            point = point/point(2);
//            point = point + halfValue;
//            //Persepective projection
//
//            Eigen::Vector3i uHat = point.cast<int>();
//            if(uHat(0) < 0 || uHat(0) >= cols || uHat(1) < 0 || uHat(1) >= rows) {
//                continue;
//            }
//
//            Eigen::Vector4f vcurr, vcurrg;
//            vcurr(0) = imgBLocalVertexMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].x;
//            vcurr(1) = imgBLocalVertexMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].y;
//            vcurr(2) = imgBLocalVertexMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].z;
//            vcurr(3) = 1;
//
//            if(vcurr(2) < 0.3 || vcurr(2) > 5.) { // Ensure the distance is between 0.3m -> 5m
//                continue;
//            }
//
//            vcurrg = predictedTransformation * vcurr;
//            vcurrg = vcurrg/vcurrg(3);
//            //vcurrg == v
//            Eigen::Vector3f ncurr, ncurrg;
//            ncurr(0) = imgBLocalNormalMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].x;
//            ncurr(1) = imgBLocalNormalMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].y;
//            ncurr(2) = imgBLocalNormalMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].z;
//            if(std::isnan(ncurr(0))) {
//                continue;
//            }
////            std::cout << "Hereh " << std::endl;
//            ncurrg = predictedTransformation.block(0, 0, 3, 3) * ncurr;
//            float distance = ((vm1g - vcurrg).norm());
////            float normal = std::abs(ncurrg.dot(nm1g)); //kinect version seems broken 1 is if they are exactly the same
//            float normal = (ncurrg.cross(nm1g)).norm();  // must be less than threshold to be vaild
////            std::cout << "Here2 " << std::endl;
//
//
//            if(distance > distanceThres) {
//                continue;
//            }
//            if(normal > normalThres) { // difference is normal < normalThres vs normal > normalThres
//                continue;
//            }
//            gpu::float3 a;
//            Eigen::Vector3f aa, VCURRG;
//            VCURRG(0) = vcurrg(0);
//            VCURRG(1) = vcurrg(1);
//            VCURRG(2) = vcurrg(2);
//            aa = VCURRG.cross(nm1g);
//            a.x = aa(0);
//            a.y = aa(1);
//            a.z = aa(2);
////            a.x = (vcurrg(2) * nm1g(1)) + (-vcurrg(1) * nm1g(2));
////            a.y = (-vcurrg(2) * nm1g(2)) + (vcurrg(0) * nm1g(2));
////            a.z = (vcurrg(1) * nm1g(0)) + (-vcurrg(0) * nm1g(1));
//
//            Eigen::Vector4f diff = vm1g - vcurrg;
//            Eigen::Vector3f diff2;
//            diff2(0) = diff(0);
//            diff2(1) = diff(1);
//            diff2(2) = diff(2);
//            float b = nm1g.dot(diff2);
//            gpu::float3 n(nm1g(0), nm1g(1), nm1g(2));
//            gpu::float7 equation(a, n, b);
//
//            equations[gpu::device::Index2D(x, y, cols)] = equation;
//            mask[gpu::device::Index2D(x, y, cols)] = true;
//            counter++;
//        }
//    }
////    exit(0);
//    std::cout << "Accepted Solutions " << counter << std::endl;
////    exit(0);
//    return equations;
//
//}
//std::vector<gpu::float7> ProjectiveDataAssociation2(gpu::float3 *imgAGlobalVertexMap, gpu::float3 *imgAGlobalNormalMap,
//                                                   gpu::matrix33 rotationMatrix, gpu::matrix33 inverseRotation,
//                                                   gpu::float3 translation,
//                                                   gpu::float3 *imgBLocalVertexMap, gpu::float3 *imgBLocalNormalMap,
//                                                   gpu::matrix33 predictedRotation,
//                                                   gpu::matrix33 predictedInverseRotation,
//                                                   gpu::float3 predictedTranslation,
//                                                   gpu::matrix33 cameraMatrix, gpu::matrix33 inverseCameraMatrix,
//                                                   int rows, int cols, bool *mask, int level) {
//    std::cout << "Starting ProjectiveDataAssociation" << std::endl;
//    int x, y;
//    std::vector<gpu::float7> equations;
//    equations.resize(rows * cols);
//    int counter = 0;
//    gpu::float3 zero(0, 0, 0);
//    gpu::float7 zero7(zero, zero, 0);
//    zero7.err = 0;
//    Eigen::Matrix4f transformation, transformationInverse, predictedTransformation, predictedTransformationInverse;
//    transformation.setIdentity();
//    predictedTransformation.setIdentity();
//    transformation(0, 0) = rotationMatrix.row0.x;
//    transformation(0, 1) = rotationMatrix.row0.y;
//    transformation(0, 2) = rotationMatrix.row0.z;
//
//    transformation(1, 0) = rotationMatrix.row1.x;
//    transformation(1, 1) = rotationMatrix.row1.y;
//    transformation(1, 2) = rotationMatrix.row1.z;
//
//    transformation(2, 0) = rotationMatrix.row2.x;
//    transformation(2, 1) = rotationMatrix.row2.y;
//    transformation(2, 2) = rotationMatrix.row2.z;
//
//    transformation(0, 3) = translation.x;
//    transformation(1, 3) = translation.y;
//    transformation(2, 3) = translation.z;
//    transformation(3, 3) = 1;
//
//    predictedTransformation(0, 0) = predictedRotation.row0.x;
//    predictedTransformation(0, 1) = predictedRotation.row0.y;
//    predictedTransformation(0, 2) = predictedRotation.row0.z;
//
//    predictedTransformation(1, 0) = predictedRotation.row1.x;
//    predictedTransformation(1, 1) = predictedRotation.row1.y;
//    predictedTransformation(1, 2) = predictedRotation.row1.z;
//
//    predictedTransformation(2, 0) = predictedRotation.row2.x;
//    predictedTransformation(2, 1) = predictedRotation.row2.y;
//    predictedTransformation(2, 2) = predictedRotation.row2.z;
//
//    predictedTransformation(0, 3) = predictedTranslation.x;
//    predictedTransformation(1, 3) = predictedTranslation.y;
//    predictedTransformation(2, 3) = predictedTranslation.z;
//    predictedTransformation(3, 3) = 1;
//
//    predictedTransformationInverse = predictedTransformation.inverse();
//    transformationInverse = transformation.inverse();
//    Eigen::Matrix4f transformDifference = transformationInverse * predictedTransformation;
//
//    Eigen::Matrix4f camMatrix;
//    camMatrix.setIdentity();
//    camMatrix(0, 0) = cameraMatrix(0, 0);
//    camMatrix(0, 2) = cameraMatrix(0, 2);
//    camMatrix(1, 1) = cameraMatrix(1, 1);
//    camMatrix(1, 2) = cameraMatrix(1, 2);
//
//    Eigen::Vector3f halfValue;
//    halfValue(0) = 0.5f;
//    halfValue(1) = 0.5f;
//    halfValue(2) = 0;
//    float distanceThres = 0.025;
//    float normalThres = 0.35;
//    for(x = 0; x < cols; x++) {
//        for(y = 0 ; y < rows; y++) {
//            //Zero things if anything fails
//            equations[gpu::device::Index2D(x, y, cols)] = zero7;
//            mask[gpu::device::Index2D(x, y, cols)] = false;
//
//            //Building nm1g
//            Eigen::Vector3f ncurr;
//            ncurr(0) = imgBLocalNormalMap[gpu::device::Index2D(x, y, cols)].x;
//            ncurr(1) = imgBLocalNormalMap[gpu::device::Index2D(x, y, cols)].y;
//            ncurr(2) = imgBLocalNormalMap[gpu::device::Index2D(x, y, cols)].z;
//            if(std::isnan(ncurr(0))) {
//                continue;
//            }
//
//            Eigen::Vector4f vcurr, vcurrg, vcurrProjected;
//            vcurr(0) = imgBLocalVertexMap[gpu::device::Index2D(x, y, cols)].x;
//            vcurr(1) = imgBLocalVertexMap[gpu::device::Index2D(x, y, cols)].y;
//            vcurr(2) = imgBLocalVertexMap[gpu::device::Index2D(x, y, cols)].z;
//            vcurr(3) = 1;
//
//            vcurrg = predictedTransformation * vcurr;
//            vcurrg = vcurrg/vcurrg(3);
//            vcurrProjected = transformationInverse * vcurrg;
//            vcurrProjected = vcurrProjected/vcurrProjected(3);
//
//            if(vcurrProjected(2) < 0.3f ||vcurrProjected(2) > 5.f) {
//                continue;
//            }
//            vcurrProjected = camMatrix * vcurrProjected;
//            vcurrProjected = vcurrProjected/vcurrProjected(3);
//            Eigen::Vector3f point;
//            point(0) = vcurrProjected(0);
//            point(1) = vcurrProjected(1);
//            point(2) = vcurrProjected(2);
//
//            point = point/point(2);
//            point = point + halfValue;
//            //Persepective projection
//
//            Eigen::Vector3i uHat = point.cast<int>();
//            if(uHat(0) < 0 || uHat(0) >= cols || uHat(1) < 0 || uHat(1) >= rows) {
//                continue;
//            }
//
//            ///Get NM1G
//            Eigen::Vector3f nm1g;
//            nm1g(0) = imgAGlobalNormalMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].x;
//            nm1g(1) = imgAGlobalNormalMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].y;
//            nm1g(2) = imgAGlobalNormalMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].z;
//            if(std::isnan(nm1g(0))) {
//                continue;
//            }
//            Eigen::Vector4f vm1g;
//            vm1g(0) = imgAGlobalVertexMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].x;
//            vm1g(1) = imgAGlobalVertexMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].y;
//            vm1g(2) = imgAGlobalVertexMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].z;
//            vm1g(3) = 1;
//            float distance = ((vm1g - vcurrg).norm());
//            if(distance > distanceThres) {
//                continue;
//            }
//
//            Eigen::Vector3f ncurrg;
//            ncurrg = predictedTransformation.block(0, 0, 3, 3) * ncurr;
//
////            float normal = std::abs(ncurrg.dot(nm1g)); //kinect version seems broken 1 is if they are exactly the same
//            float normal = (ncurrg.cross(nm1g)).norm();  // must be less than threshold to be vaild
//
//
//            if(normal > normalThres) { // difference is normal < normalThres vs normal > normalThres
////                std::cout << vcurr(0) << " " << vcurr(1) << " " << vcurr(2) << std::endl;
////                std::cout << ncurrg(0) << " " << ncurrg(1) << " " << ncurrg(2) << std::endl;
////                std::cout << ncurr(0) << " " << ncurr(1) << " " << ncurr(2) << std::endl;
////
////                std::cout << nm1g(0) << " " << nm1g(1) << " " << nm1g(2) << std::endl;
////                exit(0);
//                continue;
//            }
//
//            //source = vcurrg, destination = vm1g
//            Eigen::Vector3f d1, s1, n1;
//            n1 = nm1g;
//            d1(0) = vm1g(0);
//            d1(1) = vm1g(1);
//            d1(2) = vm1g(2);
//            s1(0) = vcurrg(0);
//            s1(1) = vcurrg(1);
//            s1(2) = vcurrg(2);
//            Eigen::Vector3f a1 = s1.cross(n1);
//            gpu::float3 a(a1(0), a1(1), a1(2)), n(nm1g(0), nm1g(1), nm1g(2));
//            float b = n1.dot(d1 - s1);
//            std::cout << "soloution " << n1 << std::endl <<  (d1 - s1) << std::endl;
////            gpu::float7 equation(a, nm1g, nm1g.dot(vm1g - vcurrg));
//            gpu::float7 equation(a, n, b);
////            eq.err = (vcurrg - vm1g).dot(n);
//
//
//
//            equation.err = (s1 - d1).dot(n1);
//            equation.err = equation.err * equation.err;
//            equations[gpu::device::Index2D(x, y, cols)] = equation;
//            mask[gpu::device::Index2D(x, y, cols)] = true;
//            counter++;
//
////            a.x = (vcurrg(2) * nm1g(1)) + (-vcurrg(1) * nm1g(2));
////            a.y = (-vcurrg(2) * nm1g(2)) + (vcurrg(0) * nm1g(2));
////            a.z = (vcurrg(1) * nm1g(0)) + (-vcurrg(0) * nm1g(1));
////
////            Eigen::Vector4f diff = vm1g - vcurrg;
////            Eigen::Vector3f diff2;
////            diff2(0) = diff(0);
////            diff2(1) = diff(1);
////            diff2(2) = diff(2);
////            float b = nm1g.dot(diff2);
////            gpu::float3 n(nm1g(0), nm1g(1), nm1g(2));
////            gpu::float7 equation(a, n, b);
////
////            equations[gpu::device::Index2D(x, y, cols)] = equation;
////            mask[gpu::device::Index2D(x, y, cols)] = true;
////            counter++;
//        }
//    }
////    exit(0);
//    std::cout << "Accepted Solutions " << counter << std::endl;
////    exit(0);
//    return equations;
//
//}
std::vector<gpu::float7> ProjectiveDataAssociationOpenFusion(gpu::float3 *imgAGlobalVertexMap, gpu::float3 *imgAGlobalNormalMap,
                                                    gpu::matrix33 rotationMatrix, gpu::matrix33 inverseRotation,
                                                    gpu::float3 translation,
                                                    gpu::float3 *imgBLocalVertexMap, gpu::float3 *imgBLocalNormalMap,
                                                    gpu::matrix33 predictedRotation,
                                                    gpu::matrix33 predictedInverseRotation,
                                                    gpu::float3 predictedTranslation,
                                                    gpu::matrix33 cameraMatrix, gpu::matrix33 inverseCameraMatrix,
                                                    int rows, int cols, bool *mask, int level) {
    std::cout << "Starting ProjectiveDataAssociation" << std::endl;
    //Level 0 = 640 * 480, Level 1 = 320 * 240 ...
    float scale = powf(2., -level);
    cameraMatrix = cameraMatrix * scale;
    cameraMatrix.row2.z = 1;

    int x, y;
    std::vector<gpu::float7> equations;
    equations.resize(rows * cols);
    int counter = 0;
    gpu::float3 zero(0, 0, 0);
    gpu::float7 zero7(zero, zero, 0);
    zero7.err = 0;
    Eigen::Matrix4f transformation, transformationInverse, predictedTransformation, predictedTransformationInverse;
    transformation.setIdentity();
    predictedTransformation.setIdentity();
    transformation(0, 0) = rotationMatrix.row0.x;
    transformation(0, 1) = rotationMatrix.row0.y;
    transformation(0, 2) = rotationMatrix.row0.z;

    transformation(1, 0) = rotationMatrix.row1.x;
    transformation(1, 1) = rotationMatrix.row1.y;
    transformation(1, 2) = rotationMatrix.row1.z;

    transformation(2, 0) = rotationMatrix.row2.x;
    transformation(2, 1) = rotationMatrix.row2.y;
    transformation(2, 2) = rotationMatrix.row2.z;

    transformation(0, 3) = translation.x;
    transformation(1, 3) = translation.y;
    transformation(2, 3) = translation.z;
    transformation(3, 3) = 1;

    predictedTransformation(0, 0) = predictedRotation.row0.x;
    predictedTransformation(0, 1) = predictedRotation.row0.y;
    predictedTransformation(0, 2) = predictedRotation.row0.z;

    predictedTransformation(1, 0) = predictedRotation.row1.x;
    predictedTransformation(1, 1) = predictedRotation.row1.y;
    predictedTransformation(1, 2) = predictedRotation.row1.z;

    predictedTransformation(2, 0) = predictedRotation.row2.x;
    predictedTransformation(2, 1) = predictedRotation.row2.y;
    predictedTransformation(2, 2) = predictedRotation.row2.z;

    predictedTransformation(0, 3) = predictedTranslation.x;
    predictedTransformation(1, 3) = predictedTranslation.y;
    predictedTransformation(2, 3) = predictedTranslation.z;
    predictedTransformation(3, 3) = 1;

    predictedTransformationInverse = predictedTransformation.inverse();
    transformationInverse = transformation.inverse();
    Eigen::Matrix4f transformDifference = transformationInverse * predictedTransformation;

    Eigen::Matrix4f camMatrix;
    camMatrix.setIdentity();
    camMatrix(0, 0) = cameraMatrix(0, 0);
    camMatrix(0, 2) = cameraMatrix(0, 2);
    camMatrix(1, 1) = cameraMatrix(1, 1);
    camMatrix(1, 2) = cameraMatrix(1, 2);

    Eigen::Vector3f halfValue;
    halfValue(0) = 0.5f;
    halfValue(1) = 0.5f;
    halfValue(2) = 0;
    float distanceThres = 0.02;
    float normalThres = 0.75;
    for(x = 0; x < cols; x++) {
        for(y = 0 ; y < rows; y++) {
            //source = vcurrg, destination = vm1g

            //Zero things if anything fails
            equations[gpu::device::Index2D(x, y, cols)] = zero7;
            mask[gpu::device::Index2D(x, y, cols)] = false;

            //Building nm1g
            Eigen::Vector3f nm1g;
            nm1g(0) = imgAGlobalNormalMap[gpu::device::Index2D(x, y, cols)].x;
            nm1g(1) = imgAGlobalNormalMap[gpu::device::Index2D(x, y, cols)].y;
            nm1g(2) = imgAGlobalNormalMap[gpu::device::Index2D(x, y, cols)].z;
            if(std::isnan(nm1g(0))) {
                continue;
            }

            Eigen::Vector4f vm1g, vm1, vm1Projected;
            vm1g(0) = imgAGlobalVertexMap[gpu::device::Index2D(x, y, cols)].x;
            vm1g(1) = imgAGlobalVertexMap[gpu::device::Index2D(x, y, cols)].y;
            vm1g(2) = imgAGlobalVertexMap[gpu::device::Index2D(x, y, cols)].z;
            vm1g(3) = 1;

            vm1 = transformationInverse * vm1g;
            if(vm1(2) <= 0.f) {
                continue;
            }

            vm1Projected = camMatrix * vm1;
            vm1Projected = vm1Projected/vm1Projected(3);
            Eigen::Vector3f point;
            point(0) = vm1Projected(0);
            point(1) = vm1Projected(1);
            point(2) = vm1Projected(2);

            point = point/point(2);
            point = point + halfValue;
            //Persepective projection

            Eigen::Vector3i uHat = point.cast<int>();
            if(uHat(0) < 0 || uHat(0) >= cols || uHat(1) < 0 || uHat(1) >= rows) {
                continue;
            }

            ///Get NM1G
            Eigen::Vector3f ncurr, ncurrg;
            ncurr(0) = imgBLocalNormalMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].x;
            ncurr(1) = imgBLocalNormalMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].y;
            ncurr(2) = imgBLocalNormalMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].z;
            if(std::isnan(ncurr(0))) {
                continue;
            }
            ncurrg = predictedTransformation.block(0, 0, 3, 3) * ncurr;

            Eigen::Vector4f vcurr, vcurrg;
            vcurr(0) = imgBLocalVertexMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].x;
            vcurr(1) = imgBLocalVertexMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].y;
            vcurr(2) = imgBLocalVertexMap[gpu::device::Index2D(uHat(0), uHat(1), cols)].z;
            vcurr(3) = 1;
            vcurrg = predictedTransformation * vcurr;
            float distance = ((vm1g - vcurrg).norm());
            if(distance > distanceThres) {
                continue;
            }


//            float normal = (ncurrg.cross(nm1g)).norm();  // must be less than threshold to be vaild
//
//            if(normal > normalThres) { // difference is normal < normalThres vs normal > normalThres
//                continue;
//            }

            float normal = std::abs(ncurrg.dot(nm1g)); //kinect version seems broken 1 is if they are exactly the same
            if(normal < normalThres) { // difference is normal < normalThres vs normal > normalThres
                continue;
            }

            //source = vcurrg, destination = vm1g
            Eigen::Vector3f d1, s1, n1;
            n1 = nm1g;
            d1(0) = vm1g(0);
            d1(1) = vm1g(1);
            d1(2) = vm1g(2);
            s1(0) = vcurrg(0);
            s1(1) = vcurrg(1);
            s1(2) = vcurrg(2);
            Eigen::Vector3f a1 = s1.cross(n1);
            gpu::float3 a(a1(0), a1(1), a1(2)), n(nm1g(0), nm1g(1), nm1g(2));
            float b = n1.dot(d1 - s1);
//            std::cout << "soloution " << n1 << std::endl <<  (d1 - s1) << std::endl;
//            gpu::float7 equation(a, nm1g, nm1g.dot(vm1g - vcurrg));
            gpu::float7 equation(a, n, b);
//            eq.err = (vcurrg - vm1g).dot(n);



            equation.err = (s1 - d1).dot(n1);
            equation.err = equation.err * equation.err;
            equations[gpu::device::Index2D(x, y, cols)] = equation;
            mask[gpu::device::Index2D(x, y, cols)] = true;
            counter++;

//            a.x = (vcurrg(2) * nm1g(1)) + (-vcurrg(1) * nm1g(2));
//            a.y = (-vcurrg(2) * nm1g(2)) + (vcurrg(0) * nm1g(2));
//            a.z = (vcurrg(1) * nm1g(0)) + (-vcurrg(0) * nm1g(1));
//
//            Eigen::Vector4f diff = vm1g - vcurrg;
//            Eigen::Vector3f diff2;
//            diff2(0) = diff(0);
//            diff2(1) = diff(1);
//            diff2(2) = diff(2);
//            float b = nm1g.dot(diff2);
//            gpu::float3 n(nm1g(0), nm1g(1), nm1g(2));
//            gpu::float7 equation(a, n, b);
//
//            equations[gpu::device::Index2D(x, y, cols)] = equation;
//            mask[gpu::device::Index2D(x, y, cols)] = true;
//            counter++;
        }
    }
//    exit(0);
    std::cout << "Accepted Solutions " << counter << std::endl;
//    exit(0);
    return equations;

}
std::vector<gpu::float7> ProjectiveDataAssociationOpenFusionInternalTypes(gpu::float3 *imgAGlobalVertexMap, gpu::float3 *imgAGlobalNormalMap,
                                                             gpu::matrix33 rotationMatrix, gpu::matrix33 inverseRotation,
                                                             gpu::float3 translation,
                                                             gpu::float3 *imgBLocalVertexMap, gpu::float3 *imgBLocalNormalMap,
                                                             gpu::matrix33 predictedRotation,
                                                             gpu::matrix33 predictedInverseRotation,
                                                             gpu::float3 predictedTranslation,
                                                             gpu::matrix33 cameraMatrix, gpu::matrix33 inverseCameraMatrix,
                                                             int rows, int cols, bool *mask, int level) {
    std::cout << "Starting ProjectiveDataAssociation" << std::endl;
    //Level 0 = 640 * 480, Level 1 = 320 * 240 ...
    float scale = powf(2., -level);
    cameraMatrix = cameraMatrix * scale;
    cameraMatrix.row2.z = 1;

    int x, y;
    std::vector<gpu::float7> equations;
    equations.resize(rows * cols);
    int counter = 0;
    gpu::float3 zero(0, 0, 0);
    gpu::float7 zero7(zero, zero, 0);
    zero7.err = 0;

    gpu::float3 halfValue(0.5f, 0.5f, 0.f);
    float distanceThres = 0.025;
    float normalThres = 0.65;
    for(x = 0; x < cols; x++) {
        for(y = 0 ; y < rows; y++) {
            //source = vcurrg, destination = vm1g

            //Zero things if anything fails
            equations[gpu::device::Index2D(x, y, cols)] = zero7;
            mask[gpu::device::Index2D(x, y, cols)] = false;

            //Building nm1g
            gpu::float3 nm1g;
            nm1g = imgAGlobalNormalMap[gpu::device::Index2D(x, y, cols)];
            if(std::isnan(nm1g.x)) {
                continue;
            }

            gpu::float3 vm1g, vm1, point;
            vm1g = imgAGlobalVertexMap[gpu::device::Index2D(x, y, cols)];

//            vm1 = inverseRotation * (vm1g - translation);
            vm1 = inverseRotation * vm1g - inverseRotation * translation;
            if(vm1.z <= 0.f) {
                continue;
            }

            point = cameraMatrix * vm1;

            point = point/point.z;
            point = point + halfValue;
            //Persepective projection

            gpu::int3 uHat((int) point.x, (int) point.y, (int) point.z);
            if(uHat.x < 0 || uHat.x >= cols || uHat.y < 0 || uHat.y >= rows) {
                continue;
            }

            ///Get NM1G
            gpu::float3 ncurr, ncurrg;
            ncurr = imgBLocalNormalMap[gpu::device::Index2D(uHat.x, uHat.y, cols)];
            if(std::isnan(ncurr.x)) {
                continue;
            }
            ncurrg = rotationMatrix * ncurr;

            gpu::float3 vcurr, vcurrg;
            vcurr = imgBLocalVertexMap[gpu::device::Index2D(uHat.x, uHat.y, cols)];
            vcurrg = predictedRotation * vcurr + predictedTranslation;
            float distance = ((vm1g - vcurrg).norm());
            if(distance > distanceThres) {
                continue;
            }


//            float normal = (ncurrg.cross(nm1g)).norm();  // must be less than threshold to be vaild
//
//            if(normal > normalThres) { // difference is normal < normalThres vs normal > normalThres
//                continue;
//            }

            float normal = std::abs(ncurrg.dot(nm1g)); //kinect version seems broken 1 is if they are exactly the same
            if(normal < normalThres) { // difference is normal < normalThres vs normal > normalThres
                continue;
            }

            //TODO Long version
            //source = vcurrg, destination = vm1g
//            gpu::float3 d1, s1, n1;
//            n1 = nm1g;
//            d1 = vm1g;
//            s1 = vcurrg;
//            gpu::float3 a1 = s1.cross(n1);
//            gpu::float3 a(a1.x, a1.y, a1.z), n(nm1g.x, nm1g.y, nm1g.z);
//            float b = n1.dot(d1 - s1);
//            gpu::float7 equation(a, n, b);
//            equation.err = (s1 - d1).dot(n1);
            //TODO Optimised version
            gpu::float3 a = vcurrg.cross(nm1g);
            float b = nm1g.dot(vm1g - vcurrg);
            gpu::float7 equation(a, nm1g, b);
            equation.err = (vcurrg - vm1g).dot(nm1g);


            equation.err = equation.err * equation.err;
            equations[gpu::device::Index2D(x, y, cols)] = equation;
            mask[gpu::device::Index2D(x, y, cols)] = true;
            counter++;
        }
    }
//    exit(0);
    std::cout << "Accepted Solutions " << counter << std::endl;
//    exit(0);
    return equations;

}

Eigen::Matrix<double, 6, 1> solveEquations(std::vector<gpu::float7>& eqs , float& error, bool &pass) {
    std::cout << "Solving equations" << std::endl;
    pass = true;
    Eigen::Matrix<double, Eigen::Dynamic, 6> A;
    A.resize(eqs.size(), Eigen::NoChange);

    Eigen::Matrix<double, Eigen::Dynamic, 1> b;
    b.resize(eqs.size(), Eigen::NoChange);

    int x =0;
    error = 0;
    for(x = 0 ; x < eqs.size(); x++) {
//        std::cout << eqs[x](0) << " " << eqs[x](1) << " " << eqs[x](2) << " " << eqs[x](3) << " " << eqs[x](4) << " " << eqs[x](5) << " " << eqs[x](6) << std::endl;
        A(x, 0) = eqs[x](0);
        A(x, 1) = eqs[x](1);
        A(x, 2) = eqs[x](2);
        A(x, 3) = eqs[x](3);
        A(x, 4) = eqs[x](4);
        A(x, 5) = eqs[x](5);
        b(x, 0) = eqs[x](6);
        error += (eqs[x].err * eqs[x].err);
    }
//    error /= eqs.size();
    Eigen::Matrix<double, 6, 6> AstarA = A.transpose() * A;
    Eigen::Matrix<double, 6, 1> AstarB = A.transpose() * b;
    std::cout << AstarA << std::endl;
    std::cout << AstarB << std::endl;
    if(AstarA.determinant() < 1.e12) {
        std::cout << "Determinant failed" << std::endl;
        pass = false;
    }
    Eigen::Matrix<double, 6, 1> solution = AstarA.llt().solve(AstarB);
//    std::cout << solution << std::endl;
//    std::cout << "Equations: " << eqs.size() << std::endl;
    std::cout << "Error: " << error << std::endl;
    return solution;
}

void printfloat3(gpu::float3* ptr, int size) {
    int i;
    for(i = 0; i < size; i++) {
        std::cout << ptr[i].x << " " << ptr[i].y << " " << ptr[i].z << std::endl;
    }
}
//Image A is previous/ raycasted Image, Image B is new image
void ICP(cv::Mat_<double>& imgA, gpu::matrix33 rotationMatrix, gpu::float3 translation,
         cv::Mat_<double>& imgB, gpu::matrix33& newRotationMatrix, gpu::float3& newTranslation,
         gpu::matrix33 cameraMatrix, gpu::matrix33 inverseCameraMatrix) {
    std::cout << "Starting CPU ICP" << std::endl ;
    int iteration = 0;
    int maxIterations[3] = {10, 5, 4};
    int levels = 3;
    gpu::matrix33 inverseRotationMatrix = rotationMatrix.transpose();
    gpu::matrix33 predictedRotation = rotationMatrix;
    gpu::matrix33 predictedInverseRotation = rotationMatrix.transpose();
    gpu::float3 predictedTranslation = translation;

    //Generate vertexMaps
    std::cout << "Generating Vertex maps" << std::endl;
    gpu::float3* imgAVertexMap = GenerateVertexMap(imgA, cameraMatrix, 3);
    gpu::float3* imgBVertexMap = GenerateVertexMap(imgB, cameraMatrix, 3);
    std::cout << "Generating Normal maps" << std::endl;
    gpu::float3* imgANormalMap = GenerateNormalMap(imgAVertexMap, imgA.rows, imgA.cols, 3);
    gpu::float3* imgBNormalMap = GenerateNormalMap(imgBVertexMap, imgB.rows, imgB.cols, 3);
    std::cout << "Map Gen complete" << std::endl;
    TransformVertexMap(imgAVertexMap, imgA.rows, imgA.cols, 3, rotationMatrix, translation);
    TransformNormalMap(imgANormalMap, imgA.rows, imgA.cols, 3, rotationMatrix);
//    std::cout << "VertexMapA" << std::endl;
//    printfloat3(imgAVertexMap, imgA.rows * imgA.cols);
//    std::cout << "VertexMapA" << std::endl;
//    printfloat3(imgBVertexMap, imgA.rows * imgA.cols);
//    std::cout << "VertexMapA" << std::endl;
//    printfloat3(imgANormalMap, imgA.rows * imgA.cols);
//    std::cout << "VertexMapA" << std::endl;
//    printfloat3(imgBNormalMap, imgA.rows * imgA.cols);
    std::cout << "Transforming to global Complete" << std::endl;
    int level = 0;
    int totalSize = 0;
    int r = imgA.rows, c = imgA.cols;
    int skip = 0;
    while(level < levels) {
        totalSize += r * c;
        if(level < levels - 1) {
            skip += r*c;
        }
        r = r/2;
        c = c/2;
        level++;
    }


    bool* mask = new bool[totalSize];
    bool nullSpaceCheck = true;
    std::memset(mask, 0, totalSize);
    float error = INFINITY;
    float rotationMag = 0.01, translationMag = 0.025;
    std::cout << "Starting Transforms" << std::endl;
    std::cout << rotationMatrix.row0.x << "\t" << rotationMatrix.row0.y << "\t" << rotationMatrix.row0.z << "\t" << translation.x << std::endl;
    std::cout << rotationMatrix.row1.x << "\t" << rotationMatrix.row1.y << "\t" << rotationMatrix.row1.z << "\t" << translation.y << std::endl;
    std::cout << rotationMatrix.row2.x << "\t" << rotationMatrix.row2.y << "\t" << rotationMatrix.row2.z << "\t" << translation.z << std::endl;
    std::cout << "Predicted" << std::endl;
    std::cout << predictedRotation.row0.x << "\t" << predictedRotation.row0.y << "\t" << predictedRotation.row0.z << "\t" << predictedTranslation.x << std::endl;
    std::cout << predictedRotation.row1.x << "\t" << predictedRotation.row1.y << "\t" << predictedRotation.row1.z << "\t" << predictedTranslation.y << std::endl;
    std::cout << predictedRotation.row2.x << "\t" << predictedRotation.row2.y << "\t" << predictedRotation.row2.z << "\t" << predictedTranslation.z << std::endl;

    float scale = powf(2, -(levels-1));
    r = (int)(imgA.rows * scale), c = (int)(imgA.cols * scale);
    for(level = 2; level >= 0 ;level--) {


        std::cout << "Rows " << r << " Cols " << c << std::endl;
        for(iteration = 0; iteration < maxIterations[level]; iteration++) {
            std::cout << "ICP Iteration: " << iteration << " of " << maxIterations[level] << std::endl;
//            std::vector<gpu::float7> equations = ProjectiveDataAssociationOpenFusion(imgAVertexMap + skip, imgANormalMap + skip, rotationMatrix,
//                                                                            inverseRotationMatrix, translation,
//                                                                            imgBVertexMap + skip, imgBNormalMap + skip, predictedRotation,
//                                                                            predictedInverseRotation, predictedTranslation,
//                                                                            cameraMatrix, inverseCameraMatrix, r,
//                                                                            c, mask + skip, level);

            std::vector<gpu::float7> equations = ProjectiveDataAssociationOpenFusionInternalTypes(imgAVertexMap + skip, imgANormalMap + skip, rotationMatrix,
                                                                                     inverseRotationMatrix, translation,
                                                                                     imgBVertexMap + skip, imgBNormalMap + skip, predictedRotation,
                                                                                     predictedInverseRotation, predictedTranslation,
                                                                                     cameraMatrix, inverseCameraMatrix, r,
                                                                                     c, mask + skip, level);
            nullSpaceCheck = true;
            Eigen::Matrix<double, 6, 1> x = solveEquations(equations, error, nullSpaceCheck);
            gpu::matrix33 incRotation = Utility::GenerateRotationMatrix33(x(0), x(1), x(2));
            gpu::float3 incTranslation(x(3), x(4), x(5));

            std::cout << x << std::endl;
            if(!nullSpaceCheck) {
                std::cerr << "\x1b[93;41mNull Space check failed\x1b[0m" << std::endl;
                break;
            }
            //Error checking
            if(x(0) > rotationMag || x(1) > rotationMag || x(2) > rotationMag) {
                std::cerr << "\x1b[93;41mExceeding rotation Magnitude\x1b[0m" << std::endl;
                break;
            }
            if(x(3) > translationMag || x(4) > translationMag || x(5) > translationMag) {
                std::cerr << "\x1b[93;41mExceeding Translation Magnitude\x1b[0m" << std::endl;
                break;
            }
            viewICPOutliers(mask + skip, r, c);

            predictedTranslation = incRotation * predictedTranslation + incTranslation;
            predictedRotation = incRotation * predictedRotation;
            predictedInverseRotation = predictedRotation.transpose();
        }
        r = r * 2;
        c = c * 2;
        skip -= r * c;
    }
//    exit(0);
    newRotationMatrix = predictedRotation;
    newTranslation = predictedTranslation;
    free(imgAVertexMap);
    free(imgBVertexMap);
    free(imgANormalMap);
    free(imgBNormalMap);
//    free(mask);
}

void ICPCuda(cv::Mat_<double>& imgA, gpu::matrix33 rotationMatrix, gpu::float3 translation,
         cv::Mat_<double>& imgB, gpu::matrix33& newRotationMatrix, gpu::float3& newTranslation,
         gpu::matrix33 cameraMatrix, gpu::matrix33 inverseCameraMatrix ) {

    //Fusion stuff
    Slam::KinectFusion kinectFusion;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;

    int icpLevels = 3;
    kinectFusion.SetupCuda(cameraMatrix.row0.x, cameraMatrix.row1.y, cameraMatrix.row0.z, cameraMatrix.row1.z, imgA.rows, imgB.cols,
                           icpLevels, rotationMatrix, translation, 1);
    //End FUsion stuff



    std::cout << "ICP" << std::endl ;
    int iteration = 0;
    int maxIterations[3] = {10, 5, 4};
    gpu::matrix33 inverseRotationMatrix = rotationMatrix.transpose();
    gpu::matrix33 predictedRotation = rotationMatrix;
    gpu::matrix33 predictedInverseRotation = rotationMatrix.transpose();
    gpu::float3 predictedTranslation = translation;


    //Generate vertexMaps
    std::cout << "Generating Vertex maps" << std::endl;
    gpu::float3* imgAVertexMapGlobal = GenerateVertexMap(imgA, cameraMatrix, icpLevels);
    gpu::float3* imgBVertexMap = GenerateVertexMap(imgB, cameraMatrix, icpLevels);

    //visual
    int ii, jj;
    float zero = 0;
    int level = 0;
    int skip = 0;
    int rows = imgA.rows, cols = imgA.cols;
    for(level = 0; level < icpLevels; level++) {
        cv::Mat_<float> img(rows, cols, zero);
        float scale = powf(2, -level);
        gpu::matrix33 sCM = cameraMatrix * scale;
        for(ii = 0 ;ii < cols; ii++) {
            for(jj = 0; jj < rows; jj++) {
    //            std::cout << gpu::device::Index2D(ii, jj, 640) << std::endl;

    //            float depth = img(y, x);
    //            gpu::float3 point(x, y, 1);
    //            ptr[index] = inverseCameraMatrix * point * depth /1000.;

    //            gpu::float3 newCameraLocalPoint = newInverseRotation * (WorldPoint - newTranslation);
    //            gpu::float3 imagePoint = cameraCalibrationMatrix * newCameraLocalPoint;
    //            imagePoint = imagePoint / imagePoint.z;


                gpu::float3 v = imgAVertexMapGlobal[skip + gpu::device::Index2D(ii, jj, cols)];
                float depth = v.z * 1000.f;
                v = v/ v.z;
                v = sCM * v;
                if(v.x < 0 || v.x >= cols || v.y < 0 || v.y >= rows || std::isnan(v.x) || std::isnan(v.y)) {
                    continue;
                }

    //            std::cout << (int)v.y << " " << (int)v.x << v.x << " " << v.y <<  std::endl;
                img((int)v.y, (int)v.x) = depth;
            }
        }
        cv::Mat display;
        img.convertTo(display, CV_16U);
        display = display * 5;
        //display function
//        cv::imshow("TEST A", display);
//        cv::waitKey(0);
        skip += rows * cols;
        rows /= 2;
        cols /= 2;

    }
    skip = 0;
    rows = imgB.rows, cols = imgB.cols;
    for(level = 0; level < icpLevels; level++) {
        cv::Mat_<float> img(rows, cols, zero);
        float scale = powf(2, -level);
        gpu::matrix33 sCM = cameraMatrix * scale;
        for(ii = 0 ;ii < cols; ii++) {
            for(jj = 0; jj < rows; jj++) {
                gpu::float3 v = imgBVertexMap[skip + gpu::device::Index2D(ii, jj, cols)];
                float depth = v.z * 1000.f;
                v = v/ v.z;
                v = sCM * v;
                if(v.x < 0 || v.x >= cols || v.y < 0 || v.y >= rows || std::isnan(v.x) || std::isnan(v.y)) {
                    continue;
                }

                //            std::cout << (int)v.y << " " << (int)v.x << v.x << " " << v.y <<  std::endl;
                img((int)v.y, (int)v.x) = depth;
            }
        }
        cv::Mat display;
        img.convertTo(display, CV_16U);
        display = display * 5;
        //Display function
//        cv::imshow("TEST B", display);
//        cv::waitKey(0);

        skip += rows * cols;
        rows /= 2;
        cols /= 2;

    }


    std::cout << "Generating Normal maps" << std::endl;
    gpu::float3* imgANormalMapGlobal = GenerateNormalMap(imgAVertexMapGlobal, imgA.rows, imgA.cols, icpLevels);
    gpu::float3* imgBNormalMap = GenerateNormalMap(imgBVertexMap, imgB.rows, imgB.cols, icpLevels);
    std::cout << "Map Gen complete" << std::endl;
    TransformVertexMap(imgAVertexMapGlobal, imgA.rows, imgA.cols, icpLevels, rotationMatrix, translation);
    TransformNormalMap(imgANormalMapGlobal, imgA.rows, imgA.cols, icpLevels, rotationMatrix);
    std::cout << "Transforming to global Complete" << std::endl;
//    exit(0);
    int size = 0;
    for(level = 0; level < icpLevels; level++) {
        size += ((int) (imgA.rows * powf(2, -level)) ) * ((int) (imgA.cols * powf(2, -level)));
    }
    cudaMemcpy(gpu::host::GetGPUSourceVertexMap(), imgAVertexMapGlobal, sizeof(gpu::float3) * size, cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUSourceNormalMap(), imgANormalMapGlobal, sizeof(gpu::float3) * size, cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPULocalTargetVertexMap(), imgBVertexMap, sizeof(gpu::float3) * size, cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPULocalTargetNormalMap(), imgBNormalMap, sizeof(gpu::float3) * size, cudaMemcpyHostToDevice);

    gpu::host::UploadTransformation(rotationMatrix, inverseRotationMatrix, translation, gpu::host::GetGPURotation(), gpu::host::GetGPUInverseRotation(), gpu::host::GetGPUTranslation());



    float error = INFINITY;
//    int level = 0;
//    int skip = 0;
//    int rows = imgA.rows, cols = imgA.cols;
    rows = imgA.rows, cols = imgA.cols;
    size = 0;
    skip = 0;
    for(level = 0; level < icpLevels - 1; level++) {
        skip += rows * cols;
        rows /= 2.;
        cols /= 2.;
    }
    size = skip + rows * cols;
    bool *cpuMask = new bool[imgA.rows * imgA.cols];
    std::memset(cpuMask, 0, sizeof(bool) * imgA.rows * imgA.cols);
    gpu::float7 *cudaEquations = new gpu::float7[imgA.rows * imgA.cols];
    bool *mask = new bool[imgA.rows * imgA.cols];
    for(level = icpLevels - 1; level >= 0; level--) {
        rows = (int)(imgA.rows * powf(2, -level));
        cols = (int)(imgA.cols * powf(2, -level));
        std::cout << "Skip value " << skip << std::endl;
        iteration = 0;
        while (iteration < maxIterations[level]) {
            gpu::host::UploadTransformation(predictedRotation, predictedInverseRotation, predictedTranslation,
                                            gpu::host::GetGPUPredictedRotation(),
                                            gpu::host::GetGPUPredictedInverseRotation(),
                                            gpu::host::GetGPUPredictedTranslation());

            //Uploading Transforms

//        std::vector<gpu::float7> equations = ProjectiveDataAssociation(imgA, rotationMatrix, inverseRotationMatrix, translation,
//                                            imgB, predictedRotation, predictedInverseRotation, predictedTranslation, cameraMatrix, inverseCameraMatrix);
            std::cout << "Pointer Start " << std::endl << imgAVertexMapGlobal << std::endl << imgANormalMapGlobal << std::endl << imgBVertexMap << std::endl <<
            imgBNormalMap << std::endl;
            std::vector<gpu::float7> equations ;
//            commented just to compile
//            std::vector<gpu::float7> equations = ProjectiveDataAssociation(skip + imgAVertexMapGlobal, skip + imgANormalMapGlobal,
//                                                                            rotationMatrix, inverseRotationMatrix,
//                                                                            translation,
//                                                                            skip + imgBVertexMap, skip + imgBNormalMap,
//                                                                            predictedRotation, predictedInverseRotation,
//                                                                            predictedTranslation,
//                                                                            cameraMatrix, inverseCameraMatrix,
//                                                                            rows, cols, cpuMask, level);

//        Eigen::Matrix<double, 6, 1> x = solveEquations(equations, error);

            gpu::host::IterativeClosestPoint::IterativeClosestPoint(level, rows, cols, skip);

            //Build Equations manually


            gpu::host::Download(gpu::host::GetGPUICPMask(), mask, sizeof(bool) * rows * cols);
            gpu::host::Download(gpu::host::GetGPUICPEquations() + skip, cudaEquations,
                                sizeof(gpu::float7) * rows * cols);
            int index, vectorIndex = 0, counter = 0;
            for (index = 0; index < rows * cols; index++) {
                if (mask[index] != cpuMask[index]) {
                    std::cout << "Masks failed" << " " << index << " " << mask[index] << " " << cpuMask[index] <<
                    std::endl;
                    exit(0);
                }
                if (mask[index] && cpuMask[index]) {
                    counter++;
                    gpu::float7 cuda = cudaEquations[index];
                    gpu::float7 vector = equations[index];
                    bool compare = (cuda(0) == vector(0)) &&
                                   (cuda(1) == vector(1)) &&
                                   (cuda(2) == vector(2)) &&
                                   (cuda(3) == vector(3)) &&
                                   (cuda(4) == vector(4)) &&
                                   (cuda(5) == vector(5)) &&
                                   (cuda(6) == vector(6));
                    if (!compare) {
                        std::cout << cuda(0) << " " << cuda(1) << std::endl;
                        std::cout << vector(0) << " " << vector(1) << std::endl;
                        std::cout << "Failed " << index << std::endl;
                        exit(0);
                    }
//                vectorIndex++;

//                exit(0);
                }
            }
//        std::cout << "COunters GPU " << counter << " CPU " << equations.size() << std::endl;
            std::cout << "equations accepted " << counter << std::endl;
//        std::cout << "Equations passed" << std::endl;






            kinectFusion.CudaDownloadIterativeClosestPointEquations();
            Eigen::Matrix<float, 6, 6> AstarA = Eigen::Map<Eigen::Matrix<float, 6, 6>>(kinectFusion._icpAstarA);
            Eigen::Matrix<float, 6, 1> AstarB = Eigen::Map<Eigen::Matrix<float, 6, 1>>(kinectFusion._icpAstarB);


            Eigen::Matrix<float, Eigen::Dynamic, 6> A;
            A.resize(counter, Eigen::NoChange);
            Eigen::Matrix<float, Eigen::Dynamic, 6> a;
            a.resize(counter, Eigen::NoChange);

            Eigen::Matrix<float, Eigen::Dynamic, 1> B;
            B.resize(counter, Eigen::NoChange);
            Eigen::Matrix<float, Eigen::Dynamic, 1> b;
            b.resize(counter, Eigen::NoChange);

            int x = 0;
            error = 0;
            index = 0;
            for (x = 0; x < equations.size(); x++) {
                if (cpuMask[x]) {
                    A(index, 0) = equations[x](0);
                    A(index, 1) = equations[x](1);
                    A(index, 2) = equations[x](2);
                    A(index, 3) = equations[x](3);
                    A(index, 4) = equations[x](4);
                    A(index, 5) = equations[x](5);

                    B(index, 0) = equations[x](6);
                    index++;
                    error += equations[x].err;
                }
            }
            index = 0;
            for (x = 0; x < equations.size(); x++) {
                if (mask[x]) {
                    a(index, 0) = cudaEquations[x](0);
                    a(index, 1) = cudaEquations[x](1);
                    a(index, 2) = cudaEquations[x](2);
                    a(index, 3) = cudaEquations[x](3);
                    a(index, 4) = cudaEquations[x](4);
                    a(index, 5) = cudaEquations[x](5);

                    b(index, 0) = cudaEquations[x](6);
                    index++;
                    error += equations[x].err;
                }
            }
            if (A != a) {
//            std::cout << "A" << std::endl;
//            std::cout << A << std::endl;
//            std::cout << "A" << std::endl;
//            std::cout << a << std::endl;
                std::cout << "EROORORROR" << std::endl;
                exit(0);
            }
            Eigen::Matrix<float, 6, 6> ASA = A.transpose() * A;
            Eigen::Matrix<float, 6, 1> ASB = A.transpose() * B;

            error /= equations.size();
            Eigen::Matrix<float, 6, 6> asa = a.transpose() * a;
            Eigen::Matrix<float, 6, 1> asb = a.transpose() * b;
            if (asa != ASA) {
                std::cout << "ERROR 2" << std::endl;
                exit(0);
            }
            if (asb != ASB) {
                std::cout << "ERROR 3" << std::endl;
                exit(0);
            }
            std::cout << ASA << std::endl;
            std::cout << "---------------" << std::endl;
            std::cout << asa << std::endl;
            std::cout << "---------------" << std::endl;
            std::cout << AstarA << std::endl;
            std::cout << "---------------" << std::endl;
            std::cout << AstarA - asa << std::endl;
            std::cout << "---------------" << std::endl;



//        Eigen::Matrix<float, 6, 1> X = AstarA.llt().solve(AstarB);
            Eigen::Matrix<float, 6, 1> X = asa.llt().solve(asb);

            gpu::matrix33 incRotation = Utility::GenerateRotationMatrix33(X(0), X(1), X(2));
            gpu::float3 incTranslation(X(3), X(4), X(5));

            predictedTranslation = incRotation * predictedTranslation + incTranslation;
            predictedRotation = incRotation * predictedRotation;
            predictedInverseRotation = predictedRotation.transpose();

            gpu::host::UploadTransformation(predictedRotation, predictedInverseRotation, predictedTranslation,
                                            gpu::host::GetGPUPredictedRotation(),
                                            gpu::host::GetGPUPredictedInverseRotation(),
                                            gpu::host::GetGPUPredictedTranslation());

            std::cout << predictedRotation.row0.x << "\t" << predictedRotation.row0.y << "\t" <<
            predictedRotation.row0.z << "\t" << predictedTranslation.x << std::endl;
            std::cout << predictedRotation.row1.x << "\t" << predictedRotation.row1.y << "\t" <<
            predictedRotation.row1.z << "\t" << predictedTranslation.y << std::endl;
            std::cout << predictedRotation.row2.x << "\t" << predictedRotation.row2.y << "\t" <<
            predictedRotation.row2.z << "\t" << predictedTranslation.z << std::endl;
//        exit(0);
//        Eigen::Matrix<float, 6, 1> x = AstarA.llt().solve(AstarB);
            iteration++;
            std::cout << "Waiting 1" << std::endl;
        }
        std::cout << "Waiting 2" << std::endl;
        skip -= 4 * rows * cols;
        std::cout << "Waiting 3" << std::endl;

    }
    std::cout << "Waiting 4" << std::endl;
//    exit(0);
    newRotationMatrix = predictedRotation;
    newTranslation = predictedTranslation;
    free(cpuMask);
    free(mask);
    free(imgAVertexMapGlobal);
    free(imgBVertexMap);
    free(imgANormalMapGlobal);
    free(imgBNormalMap);
    std::cout << "Waiting 5" << std::endl;
}
void _TestProjectiveDataAssociationCustomImages() {
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;


    gpu::matrix33 cameraCalibrationMatrix(fx, 0, cx, 0, fy, cy,0 ,0, 1);
    gpu::matrix33 inverseCameraCalibrationMatrix(1.f/fx, 0, -cx/fx, 0, 1.f/fy, -cy/fy,0 ,0, 1);

    gpu::matrix33 rotationMatrix;
    rotationMatrix.SetIdentity();
    rotationMatrix = Utility::GenerateRotationMatrix33(0, 0, 0);
    gpu::float3 translation(2.56, 2.56, 0);
    cv::Mat_<double> imgA = cv::imread("RaycastImageIteration0B.png", -1);
    cv::Mat_<double> imgB = cv::imread("RaycastImageIteration1C.png", -1);
    imgA = imgA * (1./5.);
    imgB = imgB * (1./5.);

    gpu::matrix33 rotationResult;
    gpu::matrix33 inverseRotationResult;
    gpu::float3 translationResult;
//    ICP(imgA, rotationMatrix, translation, imgB, rotationResult, translationResult, cameraCalibrationMatrix, inverseCameraCalibrationMatrix);
    ICPCuda(imgA, rotationMatrix, translation, imgB, rotationResult, translationResult, cameraCalibrationMatrix, inverseCameraCalibrationMatrix);
}
void _TestProjectiveDataAssociationWithTsdfAndRaycaster() {
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;


    gpu::matrix33 cameraCalibrationMatrix(fx, 0, cx, 0, fy, cy,0 ,0, 1);
    gpu::matrix33 inverseCameraCalibrationMatrix(1.f/fx, 0, -cx/fx, 0, 1.f/fy, -cy/fy,0 ,0, 1);

    gpu::matrix33 rotationMatrix;
    rotationMatrix.SetIdentity();
    rotationMatrix = Utility::GenerateRotationMatrix33(0, 0, 0);
    gpu::float3 translation(2.56, 2.56, 2.56);
    cv::Mat_<double> imgA = cv::imread("TestImgA.png", -1);
    cv::Mat_<double> imgB = cv::imread("TestImgB.png", -1);
    imgA = imgA * (1./5.);
    imgB = imgB * (1./5.);

    gpu::matrix33 rotationResult;
    gpu::matrix33 inverseRotationResult;
    gpu::float3 translationResult;



    Slam::KinectFusion kinectFusion;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotationMatrix, translation, 1);

    gpu::DepthMap depthSource(imgA.rows, imgA.cols, imgA.datastart);
    gpu::DepthMap depthTarget(imgB.rows, imgB.cols, imgB.datastart);
    //Upload Image data
    kinectFusion.CudaUploadImageData(depthSource);
    kinectFusion.CudaProjectiveTsdf();
    kinectFusion.CudaRayCaster();
    kinectFusion.CudaDownloadRayCaster();
    kinectFusion.CudaSaveRayCaster(0);

    kinectFusion.CudaUploadImageData(depthTarget);
    cudaMemcpy(gpu::host::GetGPUTargetImages(), depthTarget.data, sizeof(float) * 640 * 480, cudaMemcpyHostToDevice);
//    gpu::host::BilateralFilter::BilateralFilter();
    gpu::host::BilateralFilter::ResizeImageIntoPyramid();
    bool integrationCheckPassed, pointcloudExtractionCheck;
    kinectFusion.CudaIterativeClosestPoint(integrationCheckPassed, pointcloudExtractionCheck);
    kinectFusion.CudaRayCaster();
    kinectFusion.CudaDownloadRayCaster();
    kinectFusion.CudaSaveRayCaster(1);

    //Download Filtered
//    int rows = 480;
//    int cols = 640;
//    int size = 0;
//    int i;
//    int Trows = rows;
//    int Tcols = cols;
//    for(i = 0; i < 3; i++) {
//        size += Trows * Tcols;
//        Trows /= 2;
//        Tcols /= 2;
//    }
//    float* images = new float[size];
//    cudaMemcpy(images, gpu::host::GetGPUTargetImages(), sizeof(float) * size, cudaMemcpyDeviceToHost);
//    double zero = 0;
//    int level, x, y, skip = 0;
//    Trows = rows;
//    Tcols = cols;
//    for(level = 0; level < 3; level++) {
//        cv::Mat_<double> img(Trows, Tcols, zero);
//
//        for (x = 0 ;x < Tcols; x++) {
//            for(y = 0; y < Trows; y++) {
//                img(y, x) = images[skip + gpu::device::Index2D(x, y, Tcols)];
////                img.at<uint>(y, x) = (uint)images[gpu::device::Index2D(x, y, Tcols)];
//            }
//        }
//
//        skip += Trows * Tcols;
//        Trows /= 2;
//        Tcols /= 2;
//        img = img * 5;
//        cv::Mat display;
//        img.convertTo(display, CV_16U);
//
//        cv::imshow("Bilat Image", display);
//        cv::waitKey(0);
//        if(level == 0) {
//            cv::imwrite("Filtered.png", display);
//        }
//    }
//
    //Download and view


    //Now ICP


}


void _TestProjectiveDataAssociation() {
    std::cout << "Starting Test Projective Data association" << std::endl;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;


    gpu::matrix33 cameraCalibrationMatrix(fx, 0, cx, 0, fy, cy,0 ,0, 1);
    gpu::matrix33 inverseCameraCalibrationMatrix(1.f/fx, 0, -cx/fx, 0, 1.f/fy, -cy/fy,0 ,0, 1);

    gpu::matrix33 rotationMatrix;
    rotationMatrix.SetIdentity();
    rotationMatrix = Utility::GenerateRotationMatrix33(0, 0, 0);
    gpu::float3 translation(0, 0, 0);
//    gpu::float3 translation;

    gpu::matrix33 newRotation = Utility::GenerateRotationMatrix33(0.01, 0.01, 0.00);
    gpu::float3 newTranslation(0.015, -0.015, 0.015);
    newTranslation = translation + newTranslation;
    newRotation = rotationMatrix * newRotation;
//    newTranslation =
    gpu::matrix33 newInverseRotation = newRotation.transpose();

//    newRotation = rotationMatrix;
//    newInverseRotation = rotationMatrix.transpose();
//    newTranslation = translation;


    cv::Mat_<double> imgA = cv::imread("test/1D.png", -1);
    imgA = imgA * (1./5.);
    double zero = 0;
    cv::Mat_<double> imgB(imgA.rows, imgA.cols, zero);

    int x, y;
    for(x = 0; x < imgA.cols; x++) {
        for(y = 0; y < imgA.rows; y++) {
            float depth = (float)imgA(y, x);
            if(depth == 0) {
                continue;
            }
            depth /= 1000.f;
            gpu::float3 point(x, y, 1);
            gpu::float3 CameraLocalPoint = inverseCameraCalibrationMatrix * point * depth;

            gpu::float3 WorldPoint = rotationMatrix * CameraLocalPoint + translation;

            gpu::float3 newCameraLocalPoint = newInverseRotation * (WorldPoint - newTranslation);
            gpu::float3 imagePoint = cameraCalibrationMatrix * newCameraLocalPoint;
            imagePoint = imagePoint / imagePoint.z;

            if(imagePoint.x < 0 || imagePoint.x >= imgB.cols || imagePoint.y < 0 || imagePoint.y >= imgB.rows) {
                continue;
            }
            imgB((int)imagePoint.y, (int)imagePoint.x) = imgA(y, x);
        }
    }
//    std::cout << "B value 0,0 is " << imgB(0, 0) << std::endl;
//
//    for(x = 0; x < imgB.cols; x++) {
//        for(y = 0; y < imgB.rows; y++) {
//            if(imgB(y, x) == 0) {
//                float s = imgB(y, x);
//                std::cout << "ZERO " << s <<  " " << (s == 0) << std::endl;
//                exit(0);
//            }
//        }
//    }


    cv::Mat initial, result;
    imgA.convertTo(initial, CV_16U);
    initial = initial * 5;

//    imgB = cv::imread("2D.png", -1);
//    imgB = imgB * (1./5.);
    imgB.convertTo(result, CV_16U);
    result = result * 5;
    cv::imshow("Image", initial);
    cv::imshow("Warped Image", result);
    cv::waitKey(0);
    std::cout << "starting ICP " << std::endl;


    gpu::matrix33 rotationResult;
    gpu::matrix33 inverseRotationResult;
    gpu::float3 translationResult;
//    ICP(imgA, rotationMatrix, translation, imgB, rotationResult, translationResult, cameraCalibrationMatrix, inverseCameraCalibrationMatrix);
    ICPCuda(imgA, rotationMatrix, translation, imgB, rotationResult, translationResult, cameraCalibrationMatrix, inverseCameraCalibrationMatrix);
    inverseRotationResult = rotationResult.transpose();
    cv::Mat_<double> WarpedImage(imgA.rows, imgA.cols);
    for(x = 0; x < imgA.cols; x++) {
        for(y = 0; y < imgA.rows; y++) {
            float depth = (float)imgA(y, x);
            if(depth <= 0) {
                continue;
            }
            depth /= 1000.f;
            gpu::float3 point(x, y, 1);
            gpu::float3 CameraLocalPoint = inverseCameraCalibrationMatrix * point * depth;

            gpu::float3 WorldPoint = rotationMatrix * CameraLocalPoint + translation;

            gpu::float3 newCameraLocalPoint = inverseRotationResult * (WorldPoint - translationResult);
            gpu::float3 imagePoint = cameraCalibrationMatrix * newCameraLocalPoint;
            imagePoint = imagePoint / imagePoint.z;

            if(imagePoint.x < 0 || imagePoint.x >= imgB.cols || imagePoint.y < 0 || imagePoint.y >= imgB.rows) {
                continue;
            }
            WarpedImage((int)imagePoint.y, (int)imagePoint.x) = imgA(y, x);
        }
    }
    cv::Mat showWarpedImage;
    WarpedImage.convertTo(showWarpedImage,CV_16U);
    showWarpedImage = showWarpedImage * 5;
    cv::imshow("original", initial);
    cv::imshow("dest", result);
    cv::imshow("warped", showWarpedImage);
    cv::imshow("diff", result - showWarpedImage);
    cv::waitKey(0);


    std::cout << "Actual Transform" << std::endl;
    std::cout << newRotation.row0.x << "\t" << newRotation.row0.y << "\t" << newRotation.row0.z << "\t" << newTranslation.x << std::endl;
    std::cout << newRotation.row1.x << "\t" << newRotation.row1.y << "\t" << newRotation.row1.z << "\t" << newTranslation.y << std::endl;
    std::cout << newRotation.row2.x << "\t" << newRotation.row2.y << "\t" << newRotation.row2.z << "\t" << newTranslation.z << std::endl;
    std::cout << "Estimated Transform" << std::endl;
    std::cout << rotationResult.row0.x << "\t" << rotationResult.row0.y << "\t" << rotationResult.row0.z << "\t" << translationResult.x << std::endl;
    std::cout << rotationResult.row1.x << "\t" << rotationResult.row1.y << "\t" << rotationResult.row1.z << "\t" << translationResult.y << std::endl;
    std::cout << rotationResult.row2.x << "\t" << rotationResult.row2.y << "\t" << rotationResult.row2.z << "\t" << translationResult.z << std::endl;








    std::cout << "Ending Test Projective Data association" << std::endl;
}
void ShowImages(cv::Mat& imgA, cv::Mat& imgB) {
    cv::Mat a, b, c;
    imgA.convertTo(a, CV_16U, 5);
    imgB.convertTo(b, CV_16U, 5);
    cv::absdiff(a, b, c);
    cv::imshow("Previous Image", a);
    cv::imshow("Current Image", b);
    cv::imshow("Difference in Images", c);

}
void _TestProjectiveDataAssociationWithImageSequence() {
    bool cuda = false;
    bool interframeIcp = true;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    int rows = 480, cols = 640;
    bool kinectv2 = false;
    if(kinectv2) {
        fx = 369.54291259088183;
        fy = 370.20970328011168;
        cx = 259.715809081801;
        cy = 205.65796886324938;

        rows = 424, cols = 512;
    }
    int icpLevels = 3;
    gpu::matrix33 cameraCalibrationMatrix(fx, 0, cx, 0, fy, cy,0 ,0, 1);
    gpu::matrix33 inverseCameraCalibrationMatrix(1.f/fx, 0, -cx/fx, 0, 1.f/fy, -cy/fy,0 ,0, 1);

    gpu::matrix33 rotationMatrix;
    rotationMatrix.SetIdentity();
    gpu::float3 translation(0, 0, 0);
    Slam::KinectFusion kinectFusion;


    kinectFusion.SetupCuda(cameraCalibrationMatrix.row0.x, cameraCalibrationMatrix.row1.y, cameraCalibrationMatrix.row0.z, cameraCalibrationMatrix.row1.z,
                           rows, cols, icpLevels, rotationMatrix, translation, 1);
//    std::string baseDir = "/home/jonny91289/", dataSet = "projectiveTest";
//    std::string baseDir = "/home/jonny91289/", dataSet = "CameraMovingBackwards";
//    std::string baseDir = "/home/jonny91289/", dataSet = "MovingBackwards";
    std::string baseDir = "/home/jonny91289/", dataSet = "MovingForward";
//    std::string baseDir = "/home/jonny91289/", dataSet = "MovingRight";
//    std::string baseDir = "/home/jonny91289/", dataSet = "MovingRightDirect";
    std::string dataSetDir = baseDir + dataSet + "/", rgbFileName("rgb.txt"), depthFileName("depth.txt"),
            groundTruthFileName("groundtruth.txt");
    IO::FileParser parser(dataSetDir, rgbFileName, depthFileName, 5.e-2);

    double depthCurrentTime, rgbCurrentTime, groundTruthCurrentTime, depthPreviousTime, rgbPreviousTime, groundTruthPreviousTime;
    std::string depthFile, rgbFile;
    Eigen::Matrix4d groundTruth;
    gpu::matrix33 groundTruthRotation, groundTruthStartRotation;
    gpu::float3 groundTruthTranslation, groundTruthStartTranslation;


    std::vector<std::string> fileNames;
    int i;
//    while(parser.ReadData(fileNames) == 1 && i < 100) {
//        i++;
//    }
    if (parser.ReadData(fileNames) != 1) {
        std::cerr << "Couldn't read data for first frame pairs" << std::endl;
    }
    parser.GetImageTimeAndLocation(fileNames[IO::RGB], rgbPreviousTime, rgbFile);
    parser.GetImageTimeAndLocation(fileNames[IO::Depth], depthPreviousTime, depthFile);
//    parser.GetGroundTruthTimeAndRotationMatrix(fileNames[IO::GrouthTruth], groundTruthPreviousTime, groundTruth);
//    groundTruthRotation = ExtractRotation(groundTruth);
//    groundTruthTranslation = ExtractTranslation(groundTruth);
//    groundTruthStartRotation = ExtractRotation(groundTruth);
//    groundTruthStartTranslation = ExtractTranslation(groundTruth);


    cv::Mat_<double> prevDepthImage, depthImage;
    prevDepthImage = cv::imread(depthFile, -1);
    prevDepthImage = prevDepthImage * (1. / 5.);
    cv::Mat colourImage;
    cv::Mat prevColourImage = cv::imread(rgbFile);

    gpu::DepthMap currentDepth(prevDepthImage.rows, prevDepthImage.cols, prevDepthImage.data);
    gpu::Image currentColourImage(prevColourImage.rows, prevColourImage.cols, prevColourImage.channels(), prevColourImage.datastart);
    kinectFusion.CudaKinectV2(currentDepth, currentColourImage, prevDepthImage, prevColourImage, true);

    gpu::matrix33 newRotation;
    newRotation.SetIdentity();
    gpu::float3 newTranslation(0, 0, 0);

    while(parser.ReadData(fileNames) == 1) {

        std::cout << "\x1B[35m" << "Starting Iteration: " << i << "\x1B[0m" << std::endl;
        parser.GetImageTimeAndLocation(fileNames[IO::RGB], rgbCurrentTime, rgbFile);
        parser.GetImageTimeAndLocation(fileNames[IO::Depth], depthCurrentTime, depthFile);
//        parser.GetGroundTruthTimeAndRotationMatrix(fileNames[IO::GrouthTruth], groundTruthCurrentTime, groundTruth);

        depthImage = cv::imread(depthFile, -1);
        depthImage = depthImage * (1. / 5.);
        currentDepth.reset(depthImage.rows, depthImage.cols, depthImage.data);

        colourImage = cv::imread(rgbFile);
        currentColourImage.reset(colourImage.rows, colourImage.cols, colourImage.channels(), colourImage.data);
        //Use current depth, current colour

        if(cuda) {
//            ICP(imgA, rotationMatrix, translation, imgB, rotationResult, translationResult, cameraCalibrationMatrix, inverseCameraCalibrationMatrix);
            ICPCuda(prevDepthImage, rotationMatrix, translation, depthImage, newRotation, newTranslation, cameraCalibrationMatrix, inverseCameraCalibrationMatrix);
            //TODO Download raycasted image
            //TODO
        } else {
//            ICPCuda(imgA, rotationMatrix, translation, imgB, rotationResult, translationResult, cameraCalibrationMatrix, inverseCameraCalibrationMatrix);
//            ShowImages(prevDepthImage, depthImage);
//            cv::waitKey(10);
            ICP(prevDepthImage, rotationMatrix, translation, depthImage, newRotation, newTranslation, cameraCalibrationMatrix, inverseCameraCalibrationMatrix);
//            ICP(prevDepthImage, rotationMatrix, translation, prevDepthImage, newRotation, newTranslation, cameraCalibrationMatrix, inverseCameraCalibrationMatrix);
//            cv::waitKey(0);
//            exit(0);
//            viewTransform(prevDepthImage, cameraCalibrationMatrix, rotationMatrix, translation, newRotation, newTranslation);

        }



        rotationMatrix = newRotation;
        translation = newTranslation;

        prevColourImage = colourImage;
//        prevDepthImage = depthImage;
        depthImage.copyTo(prevDepthImage);
//        break;
        i++;
//        int cycle = 0;
//        while(parser.ReadData(fileNames) == 1 && cycle < 2) {
//            cycle++;
//        }
    }
    std::cout << "End Transform" << std::endl;
    std::cout << rotationMatrix.row0.x << "\t" << rotationMatrix.row0.y << "\t" << rotationMatrix.row0.z << "\t" << translation.x << std::endl;
    std::cout << rotationMatrix.row1.x << "\t" << rotationMatrix.row1.y << "\t" << rotationMatrix.row1.z << "\t" << translation.y << std::endl;
    std::cout << rotationMatrix.row2.x << "\t" << rotationMatrix.row2.y << "\t" << rotationMatrix.row2.z << "\t" << translation.z << std::endl;
}
