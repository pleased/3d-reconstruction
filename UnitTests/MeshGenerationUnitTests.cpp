/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2017/03/06.
//

#include "MeshGenerationUnitTests.hpp"

void BuildVertexMapAndNormalMap(gpu::float3 *vertexMap, gpu::float3 *normalMap, int rows, int cols) {
    int i, j;
    gpu::float3 nan(NAN, NAN, NAN);
    float depth = 1;
    float cx = 320;
    float cy = 240;
    float fx = 525;
    float fy = 525;
    for(i = 0; i < cols; i++) {
        for(j = 0; j < rows; j++) {
            vertexMap[gpu::device::Index2D(i, j , cols)] = nan;
            normalMap[gpu::device::Index2D(i, j, cols)] = nan;



            vertexMap[gpu::device::Index2D(i, j , cols)] = gpu::float3((i - cx) * depth / fx, (j - cy) * depth / fy, depth);
//            normalMap[gpu::device::Index2D(i, j, cols)] = nan;
        }
    }
}
void keyboardEventOccurred(const pcl::visualization::KeyboardEvent &event, void* viewer_void) {
//    boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer = *static_cast<boost::shared_ptr<pcl::visualization::PCLVisualizer> *>(viewer_void);
    pcl::visualization::PCLVisualizer* viewer = (pcl::visualization::PCLVisualizer*) viewer_void;
    //    std::cout << "Key pressed " << event.getKeySym() << " " << (unsigned char) event.getKeyCode() << std::endl;
    double dist = 0.01;
    Eigen::Vector4d fbud; // up down forward backward movement here
    Eigen::Vector3d strafing;
    fbud << 0, 0, 0, 1;
    strafing << 0, 0, 0;

    if (event.getKeySym() == "Up"  && event.keyDown()) {
        fbud(2) -= dist;
    } if (event.getKeySym() == "Down" && event.keyDown()) {
        fbud(2) += dist;
    } if (event.getKeySym() == "Left" && event.keyDown()) {
        strafing(0) -= dist;
    } if (event.getKeySym() == "Right" && event.keyDown()) {
        strafing(0) += dist;
    }

    //view matrix is the inverse of the transformation matrix
    pcl::visualization::Camera cam;
    viewer->getCameraParameters(cam);
    Eigen::Matrix4d viewMatrix;
    viewMatrix.setIdentity();
    cam.computeViewMatrix(viewMatrix);
    viewMatrix.block(0, 0, 3, 3).transposeInPlace();
    viewMatrix.block(0, 3, 3, 1) = -viewMatrix.block(0, 0, 3, 3) * viewMatrix.block(0, 3, 3, 1);
    fbud = viewMatrix * fbud;

    strafing = viewMatrix.block(0, 0, 3, 3) * strafing;
    std::cout  << strafing  << std::endl;
    cam.pos[0] = fbud(0) + strafing(0) , cam.pos[1] = fbud(1) + strafing(1), cam.pos[2] = fbud(2) + strafing(2);
    cam.focal[0] += strafing(0), cam.focal[1] += strafing(1), cam.focal[2] += strafing(2);
    viewer->setCameraParameters(cam);
}

void BuildMesh(gpu::float3* vertexMap, gpu::float3* normalMap, int rows, int cols) {

    //Get into format of "A Fast and efficient projection based approach for surface reconstruction"
    pcl::PointCloud<pcl::PointXYZ>::Ptr ptr(new pcl::PointCloud<pcl::PointXYZ>);

    ptr.get()->points.resize(rows * cols);
    ptr.get()->width = rows * cols;
    ptr.get()->height = 1;
    int i, j;
    for(i = 0; i < cols; i++) {
        for(j = 0; j < rows; j++) {
            ptr.get()->points[gpu::device::Index2D(i, j, cols)].x = vertexMap[gpu::device::Index2D(i, j , cols)].x;
            ptr.get()->points[gpu::device::Index2D(i, j, cols)].y = vertexMap[gpu::device::Index2D(i, j , cols)].y;
            ptr.get()->points[gpu::device::Index2D(i, j, cols)].z = vertexMap[gpu::device::Index2D(i, j , cols)].z;
        }
    }

    // Normal estimation*
    pcl::NormalEstimation<pcl::PointXYZ, pcl::Normal> n;
    pcl::PointCloud<pcl::Normal>::Ptr normals (new pcl::PointCloud<pcl::Normal>);
    pcl::search::KdTree<pcl::PointXYZ>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZ>);

//    tree->setInputCloud (ptr.get());
    tree->setInputCloud (ptr);
    n.setInputCloud (ptr);
    n.setSearchMethod (tree);
    n.setKSearch (20);
    n.compute (*normals);


    // Concatenate the XYZ and normal fields*
    pcl::PointCloud<pcl::PointNormal>::Ptr cloud_with_normals (new pcl::PointCloud<pcl::PointNormal>);
    pcl::concatenateFields (*ptr, *normals, *cloud_with_normals);
    //* cloud_with_normals = cloud + normals


    // Create search tree*
    pcl::search::KdTree<pcl::PointNormal>::Ptr tree2 (new pcl::search::KdTree<pcl::PointNormal>);
    tree2->setInputCloud (cloud_with_normals);

    // Initialize objects
    pcl::GreedyProjectionTriangulation<pcl::PointNormal> gp3;
    pcl::PolygonMesh triangles;

    // Set the maximum distance between connected points (maximum edge length)
    gp3.setSearchRadius (0.025);

    // Set typical values for the parameters
    gp3.setMu (2.5);
    gp3.setMaximumNearestNeighbors (100);
    gp3.setMaximumSurfaceAngle(M_PI/4); // 45 degrees
    gp3.setMinimumAngle(M_PI/18); // 10 degrees
    gp3.setMaximumAngle(2*M_PI/3); // 120 degrees
    gp3.setNormalConsistency(false);

    // Get result
    gp3.setInputCloud (cloud_with_normals);
    gp3.setSearchMethod (tree2);
    gp3.reconstruct (triangles);

    // Additional vertex information
    std::vector<int> parts = gp3.getPartIDs();
    std::vector<int> states = gp3.getPointStates();
    std::cout << "Viewing Mesh" << std::endl;
    pcl::io::saveVTKFile("mesh.vtk", triangles);
    pcl::visualization::PCLVisualizer viewer;
    viewer.setBackgroundColor (0, 0, 0);
    viewer.addCoordinateSystem (1.0);
    viewer.addPolygonMesh(triangles, "polygon");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "polygon");
    viewer.initCameraParameters ();


    viewer.registerKeyboardCallback(keyboardEventOccurred, (void*)&viewer);
//    viewer->registerMouseCallback(mouseEventOccurred, (void*)&viewer);
    std::cout << "Stopped ? " << viewer.wasStopped() << std::endl;
    while (!viewer.wasStopped ()) {
        viewer.spinOnce (100);
//        pcl::visualization::Camera cam;
//        viewer.getCameraParameters(cam);
//        std::cout << "Cam Params success" << std::endl;
//        std::cout << cam.pos[0] << " " << cam.pos[1] << " " << cam.pos[2] << std::endl;
//        cam.pos
//        viewer.setCameraPosition();
    }
}
//A Fast and Efficient Projection-Based Approach for Surface Reconstruction TODO PAPER
void LInfty(gpu::float3* vertexMap, int rows, int cols, gpu::float3& max, gpu::float3& min) {
    int i, j;
    max = gpu::float3(-INFINITY, -INFINITY, -INFINITY), min = gpu::float3(INFINITY, INFINITY, INFINITY);
    for(i = 0; i < cols; i++) {
        for(j = 0; j < rows; j++) {
            if(vertexMap[gpu::device::Index2D(i, j, cols)].x > max.x) {
                max.x = vertexMap[gpu::device::Index2D(i, j, cols)].x;
            } if(vertexMap[gpu::device::Index2D(i, j, cols)].x < min.x) {
                min.x = vertexMap[gpu::device::Index2D(i, j, cols)].x;
            }

            if(vertexMap[gpu::device::Index2D(i, j, cols)].y > max.y) {
                max.y = vertexMap[gpu::device::Index2D(i, j, cols)].y;
            } if(vertexMap[gpu::device::Index2D(i, j, cols)].y < min.y) {
                min.y = vertexMap[gpu::device::Index2D(i, j, cols)].y;
            }

            if(vertexMap[gpu::device::Index2D(i, j, cols)].z > max.z) {
                max.z = vertexMap[gpu::device::Index2D(i, j, cols)].z;
            } if(vertexMap[gpu::device::Index2D(i, j, cols)].z < min.z) {
                min.z = vertexMap[gpu::device::Index2D(i, j, cols)].z;
            }
        }
    }
}
void L2(gpu::float3* vertexMap, int rows, int cols, int x, int y, int xOther, int yOther, float& metric) {
    float xDist = vertexMap[gpu::device::Index2D(x, y, cols)].x - vertexMap[gpu::device::Index2D(xOther, yOther, cols)].x;
    float yDist = vertexMap[gpu::device::Index2D(x, y, cols)].y - vertexMap[gpu::device::Index2D(xOther, yOther, cols)].y;
    float zDist = vertexMap[gpu::device::Index2D(x, y, cols)].z - vertexMap[gpu::device::Index2D(xOther, yOther, cols)].z;
    metric = sqrtf(xDist * xDist + yDist * yDist + zDist * zDist);
}


//static int INVALID = -1;
//static int FREE = 0;
//static int FRINGE = 1;
//static int BOUNDARY = 2;
void CustomMeshBuilder(gpu::float3* vertexMap, gpu::float3* normalMap, int rows, int cols) {

//    //INIT
//    int* Mesh = new int[rows * cols];
//    int i, j;
//    //TODO THREAD
//    for(i = 0; i < cols; i++) {
//        for(j = 0; j < rows; j++) {
//            if(isnanf(vertexMap[gpu::device::Index2D(i, j, cols)].x)) {
//                Mesh[gpu::device::Index2D(i, j, cols)] = INVALID;
//            }
//        }
//    }

    //




}

void ViewFronts(std::vector<std::vector<gpu::float3>>& fronts) {


    pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloud(new pcl::PointCloud<pcl::PointXYZRGB>());

    int i, j;
    int numberOfPoints = 0;
    for(i = 0; i < fronts.size(); i++) {
        //pick colour
        numberOfPoints += fronts[i].size();
    }
    pointCloud->points.resize(numberOfPoints);

    pcl::visualization::PCLVisualizer viewer;
    int index = 0;
    srand(0);
    for(i = 0; i < fronts.size(); i++) {
        //pick colour
        std::cout << "Front " << i << std::endl;

        int r = rand()%255; int g = rand()%255; int b = rand()%255;
        int start = index;
        for(j = 0; j < fronts[i].size(); j++) {
            assert(index < numberOfPoints);
            pointCloud->points[index].x = fronts[i][j].x;
            pointCloud->points[index].y = fronts[i][j].y;
            pointCloud->points[index].z = fronts[i][j].z;
            pointCloud->points[index].r = r;
            pointCloud->points[index].g = g;
            pointCloud->points[index].b = b;
            std::stringstream ss;
            ss << "Front_" << i << "_Line_" << j;
            if(j > 0 ) {
                viewer.addLine<pcl::PointXYZRGB>(pointCloud->points[index - 1], pointCloud->points[index], r/255., g/255., b/255., ss.str());
            }
            index++;
            std::cout << fronts[i][j].x << " " << fronts[i][j].y << " " << fronts[i][j].z << std::endl;
        }
        if(fronts[i].size() > 0) {
            std::stringstream s2;
            s2 << "Front_" << i << "_Line_0";
            viewer.addLine<pcl::PointXYZRGB>(pointCloud->points[start], pointCloud->points[index - 1], r/255., g/255., b/255., s2.str());
        }
    }

    for(i = 0; i < 5;i++) {
        viewer.addLine<pcl::PointXYZRGB>(pointCloud->points[i], pointCloud->points[i + 1], "line" + i);
    }
    viewer.setBackgroundColor (0, 0, 0);
    viewer.addPointCloud<pcl::PointXYZRGB> (pointCloud, "sample cloud");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample cloud");
//    viewer.addCoordinateSystem (1.0);
    viewer.initCameraParameters ();
    while (!viewer.wasStopped ()) {
        viewer.spinOnce (100);
    }

}

void _TestMeshGeneration() {
    std::cout << "Starting Test Mesh Generation" << std::endl;


    //TODO RUN BPA
    std::cout << "\033[32mStartin BP Algorithm\033[39m" << std::endl;
    float radiusSphere = 0.1;
    pcl::PointCloud<pcl::PointXYZRGBNormal> pointCloud2;
//    pcl::io::loadPCDFile("../../GenerateSyntheticData/StanfordDataSets/bunny.pcd", pointCloud2);

    std::string filename = "Monkey";
    pcl::io::loadPCDFile("/home/jonny91289/masters/GenerateSyntheticData/SurfaceReconstructionTestModels/PCD/" + filename + ".pcd", pointCloud2);
    radiusSphere = 0.5;
    std::cout << "Loading completed" << std::endl;

    BallPivotAlgorithm bpa(pointCloud2.makeShared());
    long size = 3;
    float* radii = new float[size];
    radii[0] = 0.05;
    radii[1] = 0.1;
    radii[2] = 0.2;
    radii[3] = 1.;
    radii[4] = 1.3;
    radii[5] = 1.5;
    radii[6] = 2;
//    radii[1] = 1.5;
//    radii[0] = 0.5;
//    radii[1] = 1.5;
    radii[2] = 1 ;
    radii[2] = 2 ;
    radiusSphere = 0.1;
    radiusSphere = 0.2;

    radiusSphere = 0.5;
    std::cout << "Ball Size: " << radiusSphere << std::endl;
//    bpa(radiusSphere);
    bpa(radii, size);
//    bpa.SaveAsMesh("../../GenerateSyntheticData/StanfordDataSets/bunny.data");
    bpa.SaveAsMesh("/home/jonny91289/masters/BPAResults/" + filename + ".data");
//
    exit(0);
}
void GenerateConsistantModelGrid(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud) {
    int i, j, k;
    int xMax = 10, yMax = 10;
//    int xMax = 3, yMax = 3;
    k = 0;
    for(i = 0; i < xMax; i++) {
        for(j = 0; j < yMax; j++) {
            pcl::PointXYZRGBNormal point;
            point.x = i;    point.y = j;    point.z = k;
            point.r =255;   point.g =255;   point.b =255;
            point.normal_x = 0;     point.normal_y = 0;     point.normal_z = 1;
            pointCloud.get()->points.push_back(point);
        }
    }

//    k = 6;
//    for(i = 0; i < xMax; i++) {
//        for(j = 0; j < yMax; j++) {
//            pcl::PointXYZRGBNormal point;
//            point.x = i;    point.y = j;    point.z = k;
//            point.r =255;   point.g =255;   point.b =255;
//            point.normal_x = 0;     point.normal_y = 0;     point.normal_z = 1;
//            pointCloud.get()->points.push_back(point);
//        }
//    }

//    j = 2;
//    for(i = 4; i < xMax - 4; i++) {
//        for(k = 1; k < 5; k++) {
//            pcl::PointXYZRGBNormal point;
//            point.x = i;    point.y = j;    point.z = k;
//            point.r =255;   point.g =255;   point.b =255;
//            point.normal_x = 1;     point.normal_y = 0;     point.normal_z = 0;
//            pointCloud.get()->points.push_back(point);
//        }
//    }


    pointCloud.get()->height = 1;
    pointCloud.get()->width = (uint32_t ) (2 * xMax * yMax) + (6 * 4);
}
void GenerateInconsistantModelGrid(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud) {
    int i, j, k;
    int xMax = 10, yMax = 10;
    k = 0;
//    for(i = 0; i < xMax; i++) {
//        for(j = 0; j < yMax; j++) {
//            pcl::PointXYZRGBNormal point;
//            point.x = i;    point.y = j;    point.z = k;
//            point.r =255;   point.g =255;   point.b =255;
//            point.normal_x = 0;     point.normal_y = 0;     point.normal_z = 1;
//            pointCloud.get()->points.push_back(point);
//        }
//    }

//    k = 6;
//    for(i = 0; i < xMax; i++) {
//        for(j = 0; j < yMax; j++) {
//            pcl::PointXYZRGBNormal point;
//            point.x = i;    point.y = j;    point.z = k;
//            point.r =255;   point.g =255;   point.b =255;
//            point.normal_x = 0;     point.normal_y = 0;     point.normal_z = 1;
//            pointCloud.get()->points.push_back(point);
//        }
//    }

//    j = 5;
//    for(i = 4; i < xMax - 4; i++) {
//        for(k = 1; k < 5; k++) {
//            pcl::PointXYZRGBNormal point;
//            point.x = i;    point.y = j;    point.z = k;
//            point.r =255;   point.g =255;   point.b =255;
//            point.normal_x = 1;     point.normal_y = 0;     point.normal_z = 0;
//            pointCloud.get()->points.push_back(point);
//        }
//    }

    j = 0;
    for(i = 0; i < xMax; i++) {
        for(k = 0; k < yMax; k++) {
            pcl::PointXYZRGBNormal point;
            point.x = i;    point.y = j;    point.z = k;
            point.r =255;   point.g =255;   point.b =255;
            point.normal_x = 0;     point.normal_y = 1;     point.normal_z = 0;
            pointCloud.get()->points.push_back(point);
        }
    }

    pointCloud.get()->height = 1;
//    pointCloud.get()->width = (uint32_t )  (6 * 4);
    pointCloud.get()->width = (uint32_t )  (xMax * yMax);
//    pointCloud.get()->width = (uint32_t ) (xMax * yMax) + (6 * 4);
}

void _TestConsistentMeshGrid() {

    //Start visualizer thread
//    viewer->initCameraParameters();
//    viewer->setBackgroundColor(0, 0, 0);
//    viewer->addCoordinateSystem(1);
    std::cout << "Starting Test Consistent Mesh" << std::endl;

    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>());
    GenerateConsistantModelGrid(pointCloud);
    std::cout << "Size of point cloud: " << pointCloud.get()->size() << std::endl;
    BallPivotAlgorithm bpa(pointCloud);
    int i;
    for(i = 0; i < pointCloud.get()->points.size(); i++) {
        std::cout << "Index: " << i << " Point: " << pointCloud.get()->points[i].x << " " << pointCloud.get()->points[i].y << " " << pointCloud.get()->points[i].z << std::endl;
    }

    long size = 4;
    float* radii = new float[size];
    radii[0] = 0.9;
    radii[1] = 1.2;
    radii[2] = 1.5;
    radii[3] = 2;
    bpa(radii, size);
//    bpa(radii[0]);
    bpa.SaveAsMesh("/home/jonny91289/RelatedWork/MeshTests/_TestConsistentMeshGrid.data");
//    bpa.PclFrontViewerSetup();
//    std::vector<std::vector<gpu::float3>> listOfFront;
//    bpa.GetFrontListsAsGPUFloat(listOfFront);
//    ViewFronts(listOfFront);

    std::cout << "Ending Test Consistent Mesh" << std::endl;
}
void _TestInconsistentMeshGrid() {
    std::cout << "Starting Test Inconsistent Mesh" << std::endl;

    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>());
    GenerateInconsistantModelGrid(pointCloud);
    std::cout << "Size of point cloud: " << pointCloud.get()->size() << std::endl;
    BallPivotAlgorithm bpa(pointCloud);
    int i;
    for(i = 0; i < pointCloud.get()->points.size(); i++) {
        std::cout << "Index: " << i << " Point: " << pointCloud.get()->points[i].x << " " << pointCloud.get()->points[i].y << " " << pointCloud.get()->points[i].z << std::endl;
    }

    long size = 4;
    float* radii = new float[size];
    radii[0] = 0.9;
    radii[1] = 1.2;
    radii[2] = 1.5;
    radii[3] = 2;
//    bpa._viewerEnable = true;
//    bpa._viewerDelayTime = 50;
//    bpa._viewerStopIteration = 141;
//    bpa._viewerStopIteration = -1;
    bpa(radii, size);
//    bpa(radii[0]);
    bpa.SaveAsMesh("/home/jonny91289/RelatedWork/MeshTests/_TestInconsistentMeshGrid.data");
//    bpa.PclFrontViewerSetup();
//    std::vector<std::vector<gpu::float3>> listOfFront;
//    bpa.GetFrontListsAsGPUFloat(listOfFront);
//    ViewFronts(listOfFront);

    std::cout << "Ending Test Inconsistent Mesh" << std::endl;
}
void ComputeNormals(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr dst) {
    std::cout << "PCL normal estimation" << std::endl;
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr temp(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
    pcl::NormalEstimation<pcl::PointXYZRGBNormal, pcl::Normal> ne;
    ne.setInputCloud (cloud);

    // Create an empty kdtree representation, and pass it to the normal estimation object.
    // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
    pcl::search::KdTree<pcl::PointXYZRGBNormal>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBNormal> ());
    ne.setSearchMethod (tree);

    // Output datasets
    pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

    // Use all neighbors in a sphere of radius 3cm
    ne.setRadiusSearch (0.07);

    // Compute the features
    ne.compute (*cloud_normals);
    pcl::concatenateFields(*cloud,  *cloud_normals, *dst);
}
void _TestRGBDMeshGrid() {
    std::cout << "Starting Test Inconsistent Mesh" << std::endl;

    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>());
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud2(new pcl::PointCloud<pcl::PointXYZRGBNormal>());
//    GenerateInconsistantModelGrid(pointCloud);
    pcl::io::loadPCDFile("/home/jonny91289/RelatedWork/testMesh.pcd", *pointCloud.get());
    int i;
//    for(i = 0; i < pointCloud.get()->points.size();i++) {
//        if(pointCloud.get()->points[i].x > 0.1 && pointCloud.get()->points[i].y > 0.1) {
//            pointCloud2.get()->points.push_back(pointCloud.get()->points[i]);
//        }
//    }
//    pointCloud.get()->height = 0;
//    pointCloud.get()->width = 0;
//    pointCloud.get()->points.clear();
//    pointCloud2.get()->height = 1;
//    pointCloud2.get()->width = pointCloud2.get()->points.size();

    ComputeNormals(pointCloud, pointCloud2);

    std::cout << "Size of point cloud: " << pointCloud2.get()->size() << std::endl;
    BallPivotAlgorithm bpa(pointCloud2);
//    for(i = 0; i < pointCloud.get()->points.size(); i++) {
//        std::cout << "Index: " << i << " Point: " << pointCloud.get()->points[i].x << " " << pointCloud.get()->points[i].y << " " << pointCloud.get()->points[i].z <<
//                " Normal: " << pointCloud.get()->points[i].normal_x << " " << pointCloud.get()->points[i].normal_y << " " << pointCloud.get()->points[i].normal_z << std::endl;
//    }

    long size = 4;
    float* radii = new float[size];
//    radii[0] = 0.9;
//    radii[1] = 1.2;
//    radii[2] = 1.5;
//    radii[3] = 2;
    radii[0] = 0.05;
    radii[1] = 0.1;
    radii[2] = 0.15;
    radii[3] = 0.2;

    bpa._viewerEnable = true;
    bpa._viewerDelayTime = 50;
//    bpa._viewerStopIteration = 141;
    bpa._viewerStopIteration = -1;
    bpa(radii, size);
//    bpa(radii[0]);
    bpa.SaveAsMesh("/home/jonny91289/RelatedWork/MeshTests/_TestRGBMeshGrid.data");
//    bpa.PclFrontViewerSetup();
//    std::vector<std::vector<gpu::float3>> listOfFront;
//    bpa.GetFrontListsAsGPUFloat(listOfFront);
//    ViewFronts(listOfFront);

    std::cout << "Ending Test Inconsistent Mesh" << std::endl;
    exit(0);
}
void _TestMeshReconstruction() {

}