/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/06/13.
//


#include <pcl/filters/voxel_grid.h>
#include "KinectFusionUnitTests.hpp"

std::vector<cv::Mat_<double>> GenerateSquares() {
    std::vector<cv::Mat_<double>> images;
    int size = 1;
    images.resize(size);
    int i =0, x, y ;
    double depth = 900;
    double v = 0;
    for( i = 0 ; i < size; i++) {
        images[i] = cv::Mat_<double>(480, 640, v);
        int startX = 0;
        int endX = 640;
        int startY = 0;
        int endY = 480;
        for(x = startX; x < endX; x++) {
            for( y = startY; y < endY; y++) {
                images[i](y, x) = depth;
            }
            if(x % 5 == 0){
//                depth += 100;
            }
        }
    }
    return images;
}
std::vector<cv::Mat_<double>> GenerateSquareSpecialImage() {
    std::vector<cv::Mat_<double>> images;
    int size = 1;
    images.resize(size);
    int i =0, x, y ;
    double depth = 900;
    double v = 0;
    for( i = 0 ; i < size; i++) {
        images[i] = cv::Mat_<double>(480, 640, v);

        //Left block
        for(x = 0; x < 200; x++) {
            for( y = 0; y < 480; y++) {
                images[i](y, x) = 600;
            }
        }
        //right block
        for(x = 440; x < 640; x++) {
            for( y = 0; y < 480; y++) {
                images[i](y, x) = 600;
            }
        }
        //Top block
        for(x = 0; x < 640; x++) {
            for( y = 0; y < 150; y++) {
                images[i](y, x) = 600;
            }
        }
        //Bottom block
        for(x = 0; x < 640; x++) {
            for( y = 330; y < 480; y++) {
                images[i](y, x) = 600;
            }
        }
        //Middle block
        for(x = 200; x < 440; x++) {
            for( y = 150; y < 330; y++) {
                images[i](y, x) = 1000;
            }
        }
    }
    return images;
}
cv::Mat_<double> GenerateSquare() {


    int i =0, x, y ;
    int depth = 1500;
    double v = 0;
    cv::Mat_<double> images = cv::Mat_<double>(480, 640, v);
    int startX = 0;
    int endX = 640;
    int startY = 0;
    int endY = 480;
    for(x = startX; x < endX; x++) {
        for( y = startY; y < endY; y++) {
            images(y, x) = depth;
        }
        if(x % 5 == 0){
//                depth += 100;
        }
    }

    return images;
}




void _TestKinectFusionCudaBilateralFilter() {
    std::cout << "Starting _TestKinectFusionCudaBilateralFilter" << std::endl;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    Utility::SetCameraParameters(fx, fy, cx, cy);

    cv::Mat_<double> readImage = cv::imread("1D.png", -1);
    readImage = readImage * (1./5.);
    cv::imshow("TEST", readImage);
    cv::waitKey(0);

    gpu::DepthMap depthMap(readImage.rows, readImage.cols, readImage.data);
    Slam::KinectFusion kinectFusionCuda;
    int icpLevels = 3;
    gpu::matrix33 rotationMatrix(0, 0, 1, 0, 1, 0, -1, 0, 0);
    gpu::float3 translation(2.56, 2.56, 0.1);
    kinectFusionCuda.SetupCuda(fx, fy, cx, cy, 480, 640, icpLevels, rotationMatrix, translation, 1);
    kinectFusionCuda.CudaUploadImageData(depthMap);
    kinectFusionCuda.CudaBilateralFilterAndResize();
    kinectFusionCuda.CudaDownloadBilateralFilterAndResize();



    int x, y, imgNum;
    int r = 480, c = 640;
    int skip = 0;
    for(imgNum = 0; imgNum < 3; imgNum++) {
        cv::Mat_<ushort> img(r, c);
        for(x = 0; x < c; x++) {
            for(y = 0; y < r; y++) {
                img(y, x) = (ushort)((kinectFusionCuda.bilateralFilterTargetPtr[skip + y * c + x]) * 5);
                assert(y * c + x < r * c);
            }
        }
        skip += r * c;
        r /= 2.;
        c /= 2.;
        std::ostringstream oss;
        oss << "BilateralFilter";
        oss << imgNum ;
        oss << ".png";
        std::cout << oss.str() << std::endl;
        cv::imwrite(oss.str(), img);
//        cv::imwrite("BilateralFilter.png", img);
        cv::imshow("Cuda BilateralFilter", img);
    }
    std::cout << "Ending _TestKinectFusionCudaBilateralFilter" << std::endl;
}
void _TestKinectFusionCudaIterativeClosestPoint() {
    std::cout << "Starting _TestKinectFusionCudaIterativeClosestPoint" << std::endl;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    Utility::SetCameraParameters(fx, fy, cx, cy);


    cv::Mat_<double> readImage = cv::imread("1D.png", -1);
    readImage = readImage * (1. / 5.);
    cv::imshow("TEST", readImage);
    cv::waitKey(0);

    gpu::DepthMap depthMap(readImage.rows, readImage.cols, readImage.data);
    int icpLevels = 3;
    Slam::KinectFusion kinectFusionCuda;
    gpu::matrix33 rotationMatrix(0, 0, 1, 0, 1, 0, -1, 0, 0);
    gpu::float3 translation(2.56, 2.56, 2.56);
    kinectFusionCuda.SetupCuda(fx, fy, cx, cy, 480, 640, icpLevels, rotationMatrix, translation, 1);
    kinectFusionCuda.CudaUploadImageData(depthMap);
    kinectFusionCuda.CudaBilateralFilterAndResize();
    kinectFusionCuda.CudaDownloadBilateralFilterAndResize();
    bool integrationCheckPassed, pointcloudExtractionCheck;
    kinectFusionCuda.CudaIterativeClosestPoint(integrationCheckPassed, pointcloudExtractionCheck);
    kinectFusionCuda.CudaDownloadIterativeClosestPointVertexAndNormalMap();

    int x, y, imgNum, rows = readImage.rows, cols = readImage.cols;
    int skip = 0;
    for (imgNum = 0; imgNum < icpLevels; imgNum++) {
        std::cout << "CHECKING LEVEL " << imgNum << std::endl;
        gpu::matrix33 rotationMatrix(0, 0, 1, 0, 1, 0, -1, 0, 0), inverseRotation = rotationMatrix.transpose();
        gpu::float3 translation(2.56, 2.56, 2.56);

        float scale = powf(2, -imgNum);
        gpu::float3 f;
        gpu::float3 c;
        f.x = fx * scale;
        f.y = fy * scale;
        c.x = cx * scale;
        c.y = cy * scale;

        gpu::DepthMap bilateralFilteredMap;
        bilateralFilteredMap.rows = rows;
        bilateralFilteredMap.cols = cols;
        bilateralFilteredMap.data = &(kinectFusionCuda.bilateralFilterTargetPtr[skip]);
        int blkX = cols;
        int blkY = rows;
        int startX = 0;
        int startY = 0;
        for (x = startX; x < startX + blkX; x++) {
            for (y = startY; y < startY + blkY; y++) {
                int index = y * cols + x;
                gpu::float3 v3 = kinectFusionCuda._icpLocalTargetVertexMap[index  + skip];
                gpu::float3 ptr;
                float depthV = bilateralFilteredMap(y, x);

                if (depthV != 0) {
                    float d = 1.f/1000.f;
                    depthV = depthV * d;
                    ptr.x = (depthV * x - c.x) / f.x;
                    ptr.y = (depthV * y - c.y) / f.y;
                    ptr.z = depthV;
                    if(!fequal(ptr, v3)) {
                        std::cout << "Assertion failed at img " << imgNum <<" pos" << x << " " << y << std::endl;
                        printf("%f %f %f\n", ptr.x ,ptr.y, ptr.z);
                        printf("%f %f %f\n", v3.x ,v3.y, v3.z);
                        printf("%d %d %d %d\n", fequal(ptr.x, v3.x), fequal(ptr.y, v3.y), fequal(ptr.z, v3.z), fequal(ptr, v3));

                    }
                } else {
                    ptr.x = 0.f;
                    ptr.y = 0.f;
                    ptr.z = 0.f;
                }
                if(!fequal(ptr, v3)) {
                    std::cout << x << " " << y << std::endl;
                    std::cout << ptr.x << " " << ptr.y << " " << ptr.z << std::endl;
                    std::cout << v3.x << " " << v3.y << " " << v3.z << std::endl;
                }
                assert(fequal(ptr, v3));

            }
        }
        for (x = startX; x < startX + blkX - 1; x++) {
            for (y = startY; y < startY + blkY - 1; y++) {

                gpu::float3 y1, x1, xy;
                y1.x = kinectFusionCuda._icpLocalTargetVertexMap[(y + 1) * cols + x + skip].x; y1.y = kinectFusionCuda._icpLocalTargetVertexMap[(y + 1) * cols + x + skip].y; y1.z = kinectFusionCuda._icpLocalTargetVertexMap[(y + 1) * cols + x + skip].z;
                x1.x = kinectFusionCuda._icpLocalTargetVertexMap[y * cols + x + 1 + skip].x; x1.y = kinectFusionCuda._icpLocalTargetVertexMap[y * cols + x + 1 + skip].y; x1.z = kinectFusionCuda._icpLocalTargetVertexMap[y * cols + x + 1 + skip].z;
                xy.x = kinectFusionCuda._icpLocalTargetVertexMap[y * cols + x + skip].x; xy.y = kinectFusionCuda._icpLocalTargetVertexMap[y * cols + x + skip].y; xy.z = kinectFusionCuda._icpLocalTargetVertexMap[y * cols + x + skip].z;
                gpu::float3 normal = (x1 - xy).cross((y1 - xy));
                float normalTerm = normal.norm();
                if (normalTerm == 0.f)  {
                    normal.x = 0.f;
                    normal.y = 0.f;
                    normal.z = 0.f;
                    if(!fequal(kinectFusionCuda._icpLocalTargetNormalMap[y * cols + x + skip], normal)) {
                        assert(fequal(kinectFusionCuda._icpLocalTargetNormalMap[y * cols + x + skip], normal));
                    }
                    continue;

                }

                gpu::float3 v = normal.normalized();
                if(!finite(v.x) || !finite(v.y) || !finite(v.z)) {
                    v.x = 0;
                    v.y = 0;
                    v.z = 0;
                }
                if(!fequal(kinectFusionCuda._icpLocalTargetNormalMap[y * cols + x + skip], v)) {
                    std::cout << "Not equal Normalized" << std::endl;
                    std::cout << x << " " << y << " skip value " << skip << std::endl;
                    std::cout << v.x << " " << v.y << " "<< v.z << std::endl;
                    std::cout << kinectFusionCuda._icpLocalTargetNormalMap[y * cols + x + skip].x << " " << kinectFusionCuda._icpLocalTargetNormalMap[y * cols + x + skip].y << " "<< kinectFusionCuda._icpLocalTargetNormalMap[y * cols + x + skip].z << std::endl;
                }
                assert(fequal(kinectFusionCuda._icpLocalTargetNormalMap[y * cols + x + skip], v));
                if (!std::isfinite(normal.x) || !std::isfinite(normal.y) || !std::isfinite(normal.z)) {
                    printf("IS NOT FINITE %d %d %f %f %f\n",x,y, normal.x, normal.y, normal.z);
                }
            }
        }

        for (x = startX; x < startX + blkX - 1; x++) {
            for (y = startY; y < startY + blkY - 1; y++) {
                int index = y * cols + x;
                gpu::float3 vertex = kinectFusionCuda._icpLocalTargetVertexMap[index + skip];
                gpu::float3 globalVertex = rotationMatrix * vertex + translation;
                gpu::float3 solution = kinectFusionCuda._icpGlobalTargetVertexMap[index + skip];
                assert(globalVertex == solution);
            }
        }
        for (x = startX; x < startX + blkX - 1; x++) {
            for (y = startY; y < startY + blkY - 1; y++) {
                int index = y * cols + x;
                gpu::float3 normal = kinectFusionCuda._icpLocalTargetNormalMap[index + skip];
                gpu::float3 globalNormal = rotationMatrix * normal;
                gpu::float3 solution = kinectFusionCuda._icpGlobalTargetNormalMap[index + skip];
                if(!(globalNormal == solution)) {
                    std::cout << normal.x  << " " << normal.y << " " << normal.z << std::endl;
                    std::cout << globalNormal.x  << " " << globalNormal.y << " " << globalNormal.z << std::endl;
                    std::cout << solution.x  << " " << solution.y << " " << solution.z << std::endl;
                }
                assert(globalNormal == solution);
            }
        }
        skip += rows * cols;
        rows /= 2;
        cols /= 2;
//        return;
    }
    std::cout << "Ending _TestKinectFusionCudaIterativeClosestPoint" << std::endl;
}

void _TestKinectFusionCudaIterativeClosestPointSwapTest() {
    std::cout << "Starting _TestKinectFusionCudaIterativeClosestPoint" << std::endl;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    Utility::SetCameraParameters(fx, fy, cx, cy);


    cv::Mat_<double> readImage = GenerateSquare();
    readImage = readImage * (1. / 5.);
//    cv::imshow("TEST", readImage);
//    cv::waitKey(0);

    gpu::DepthMap depthMap(readImage.rows, readImage.cols, readImage.data);
    Slam::KinectFusion kinectFusionCuda;
    int icpLevels = 3;
    gpu::matrix33 rotationMatrix(0, 0, 1, 0, 1, 0, -1, 0, 0);
    gpu::float3 translation(2.56, 2.56, 0.1);
    kinectFusionCuda.SetupCuda(fx, fy, cx, cy, 480, 640, icpLevels, rotationMatrix, translation, 1);
    kinectFusionCuda.CudaUploadImageData(depthMap);
    kinectFusionCuda.CudaBilateralFilterAndResize();
    kinectFusionCuda.CudaDownloadBilateralFilterAndResize();
    bool integrationCheckPassed, pointcloudExtractionCheck;
    kinectFusionCuda.CudaIterativeClosestPoint(integrationCheckPassed, pointcloudExtractionCheck);
    kinectFusionCuda.CudaDownloadIterativeClosestPointVertexAndNormalMap();

    int x, y, imgNum, rows = readImage.rows, cols = readImage.cols;
    int skip = 0;
    for (imgNum = 0; imgNum < icpLevels; imgNum++) {
        std::cout << "CHECKING LEVEL " << imgNum << std::endl;
        gpu::matrix33 rotationMatrix(0, 0, 1, 0, 1, 0, -1, 0, 0), inverseRotation = rotationMatrix.transpose();
        gpu::float3 translation(2.56, 2.56, 2.56);

        float scale = powf(2, -imgNum);
        gpu::float3 f;
        gpu::float3 c;
        f.x = fx * scale;
        f.y = fy * scale;
        c.x = cx * scale;
        c.y = cy * scale;

        gpu::DepthMap bilateralFilteredMap;
        bilateralFilteredMap.rows = rows;
        bilateralFilteredMap.cols = cols;
        bilateralFilteredMap.data = &(kinectFusionCuda.bilateralFilterSourcePtr[skip]);
        int blkX = cols;
        int blkY = rows;
        int startX = 0;
        int startY = 0;
        for (x = startX; x < startX + blkX; x++) {
            for (y = startY; y < startY + blkY; y++) {
                int index = y * cols + x;
                gpu::float3 v3 = kinectFusionCuda._icpLocalTargetVertexMap[index  + skip];
                gpu::float3 ptr;
                float depthV = bilateralFilteredMap(y, x);

                if (depthV != 0) {
                    float d = 1.f/1000.f;
                    depthV = depthV * d;
                    ptr.x = (depthV * x - c.x) / f.x;
                    ptr.y = (depthV * y - c.y) / f.y;
                    ptr.z = depthV;
                    if(!fequal(ptr, v3)) {
                        std::cout << "Assertion failed at img " << imgNum <<" pos" << x << " " << y << std::endl;
                        printf("%f %f %f\n", ptr.x ,ptr.y, ptr.z);
                        printf("%f %f %f\n", v3.x ,v3.y, v3.z);
                        printf("%d %d %d %d\n", fequal(ptr.x, v3.x), fequal(ptr.y, v3.y), fequal(ptr.z, v3.z), fequal(ptr, v3));

                    }
                } else {
                    ptr.x = 0.f;
                    ptr.y = 0.f;
                    ptr.z = 0.f;
                }
                if(!fequal(ptr, v3)) {
                    std::cout << x << " " << y << std::endl;
                    std::cout << ptr.x << " " << ptr.y << " " << ptr.z << std::endl;
                    std::cout << v3.x << " " << v3.y << " " << v3.z << std::endl;
                }
                assert(fequal(ptr, v3));

            }
        }
//        for (x = startX; x < startX + blkX - 1; x++) {
//            for (y = startY; y < startY + blkY - 1; y++) {
//
//                gpu::float3 y1, x1, xy;
//                y1.x = kinectFusionCuda._icpLocalTargetVertexMap[(y + 1) * cols + x + skip].x; y1.y = kinectFusionCuda._icpLocalTargetVertexMap[(y + 1) * cols + x + skip].y; y1.z = kinectFusionCuda._icpLocalTargetVertexMap[(y + 1) * cols + x + skip].z;
//                x1.x = kinectFusionCuda._icpLocalTargetVertexMap[y * cols + x + 1 + skip].x; x1.y = kinectFusionCuda._icpLocalTargetVertexMap[y * cols + x + 1 + skip].y; x1.z = kinectFusionCuda._icpLocalTargetVertexMap[y * cols + x + 1 + skip].z;
//                xy.x = kinectFusionCuda._icpLocalTargetVertexMap[y * cols + x + skip].x; xy.y = kinectFusionCuda._icpLocalTargetVertexMap[y * cols + x + skip].y; xy.z = kinectFusionCuda._icpLocalTargetVertexMap[y * cols + x + skip].z;
//                gpu::float3 normal = (x1 - xy).cross((y1 - xy));
//                float normalTerm = normal.norm();
//                if (normalTerm == 0.f)  {
//                    normal.x = 0.f;
//                    normal.y = 0.f;
//                    normal.z = 0.f;
//                    if(!fequal(kinectFusionCuda._icpLocalTargetNormalMap[y * cols + x + skip], normal)) {
//                        assert(fequal(kinectFusionCuda._icpLocalTargetNormalMap[y * cols + x + skip], normal));
//                    }
//                    continue;
//
//                }
//
//                gpu::float3 v = normal.normalized();
//                if(!finite(v.x) || !finite(v.y) || !finite(v.z)) {
//                    v.x = 0;
//                    v.y = 0;
//                    v.z = 0;
//                }
//                if(!fequal(kinectFusionCuda._icpLocalTargetNormalMap[y * cols + x + skip], v)) {
//                    std::cout << "Not equal Normalized" << std::endl;
//                    std::cout << x << " " << y << " skip value " << skip << std::endl;
//                    std::cout << v.x << " " << v.y << " "<< v.z << std::endl;
//                    std::cout << kinectFusionCuda._icpLocalTargetNormalMap[y * cols + x + skip].x << " " << kinectFusionCuda._icpLocalTargetNormalMap[y * cols + x + skip].y << " "<< kinectFusionCuda._icpLocalTargetNormalMap[y * cols + x + skip].z << std::endl;
//                }
//                assert(fequal(kinectFusionCuda._icpLocalTargetNormalMap[y * cols + x + skip], v));
//                if (!std::isfinite(normal.x) || !std::isfinite(normal.y) || !std::isfinite(normal.z)) {
//                    printf("IS NOT FINITE %d %d %f %f %f\n",x,y, normal.x, normal.y, normal.z);
//                }
//            }
//        }

        for (x = startX; x < startX + blkX - 1; x++) {
            for (y = startY; y < startY + blkY - 1; y++) {
                int index = y * cols + x;
                gpu::float3 vertex = kinectFusionCuda._icpLocalTargetVertexMap[index + skip];
                gpu::float3 globalVertex = rotationMatrix * vertex + translation;
                gpu::float3 solution = kinectFusionCuda._icpSourceVertexMap[index + skip];
                assert(globalVertex == solution);
            }
        }
        for (x = startX; x < startX + blkX - 1; x++) {
            for (y = startY; y < startY + blkY - 1; y++) {
                int index = y * cols + x;
                gpu::float3 normal = kinectFusionCuda._icpLocalTargetNormalMap[index + skip];
                gpu::float3 globalNormal = rotationMatrix * normal;
                gpu::float3 solution = kinectFusionCuda._icpSourceNormalMap[index + skip];
                if(!(globalNormal == solution)) {
                    std::cout << normal.x  << " " << normal.y << " " << normal.z << std::endl;
                    std::cout << globalNormal.x  << " " << globalNormal.y << " " << globalNormal.z << std::endl;
                    std::cout << solution.x  << " " << solution.y << " " << solution.z << std::endl;
                }
                assert(globalNormal == solution);
            }
        }
        skip += rows * cols;
        rows /= 2;
        cols /= 2;
//        return;
    }
    std::cout << "Ending _TestKinectFusionCudaIterativeClosestPoint" << std::endl;
}


void _TestKinectFusionCudaIterativeClosestPointMatrixReductions4() {
    int i, size = 37000;
    Slam::KinectFusion kf;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    int icpLevels = 3;
    gpu::matrix33 rotationMatrix;
    rotationMatrix.SetIdentity();
    gpu::float3 translation;
    kf.SetupCuda(fx, fy, cx, cy, 480, 640, icpLevels, rotationMatrix, translation, 1);


    gpu::float7* numbers = new gpu::float7[size];
    Eigen::Matrix<float, Eigen::Dynamic, 6> A;
    A.resize(size, Eigen::NoChange);
    Eigen::Matrix<float, Eigen::Dynamic, 1> B;
    B.resize(size, Eigen::NoChange);
    float val = 0;
    int shift = 0;
    std::cout << "Shift value is " << shift << std::endl;
    for(i = 0; i < size; i++) {
        gpu::float7 row;
        // Powers of 2 are always numirically stable
        row(0) = (int) powf(2, (int)((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) * 6 - 3));
        row(1) = (int) powf(2, (int)((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) * 6 - 3));
        row(2) = (int) powf(2, (int)((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) * 6 - 3));
        row(3) = (int) powf(2, (int)((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) * 6 - 3));
        row(4) = (int) powf(2, (int)((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) * 6 - 3));
        row(5) = (int) powf(2, (int)((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) * 6 - 3));
        row(6) = (int) powf(2, (int)((static_cast <float> (rand()) / static_cast <float> (RAND_MAX)) * 6 - 3));
        if(i % 7500 == 0) {
            shift++;
        }
        row.err = 1<<shift;
        A(i, 0) = row(0);
        A(i, 1) = row(1);
        A(i, 2) = row(2);
        A(i, 3) = row(3);
        A(i, 4) = row(4);
        A(i, 5) = row(5);
        B(i, 0) = row(6);
        numbers[i] = row;
    }
    cudaMemset(gpu::host::GetGPUICPEquations(), 0, sizeof(gpu::float7) * 480 * 640);
    cudaMemcpy(gpu::host::GetGPUICPEquations(), numbers, sizeof(gpu::float7) * size, cudaMemcpyHostToDevice);

    float* AstarA = new float[36];
    float* AstarB = new float[6];
    float* gpuAstarA, gpuAstarB;
    float error;

    //Eigen
    Eigen::Matrix<float, 6, 6> eigenAstarA = A.transpose() * A;
    Eigen::Matrix<float, 6, 1> eigenAstarB = A.transpose() * B;
    std::cout << eigenAstarA << std::endl;
    std::cout << eigenAstarB << std::endl;

    float* res = new float[42] ;

    gpu::host::IterativeClosestPoint::ReduceEquations(0);
    cudaMemcpy(res, gpu::host::GetGPUICPReductionMatrix(), sizeof(float) * 42, cudaMemcpyDeviceToHost);
    cudaMemcpy(&error, gpu::host::GetGPUICPError(), sizeof(float), cudaMemcpyDeviceToHost);

    // Not summed over blocks
    std::cout << res[0] << "\t" << res[1] << "\t" << res[2] << "\t" << res[3] << "\t" << res[4] << "\t" << res[5]  << "\t" << res[6] <<  std::endl;
    std::cout << res[7] << "\t" << res[8] << "\t" << res[9] << "\t" << res[10] << "\t" << res[11] << "\t" << res[12]  << "\t" << res[13]  <<  std::endl;
    std::cout << res[14] << "\t" << res[15] << "\t" << res[16] << "\t" << res[17] << "\t" << res[18] << "\t" << res[19]  << "\t" << res[20]  << std::endl;
    std::cout << res[21] << "\t" << res[22] << "\t" << res[23] << "\t" << res[24] << "\t" << res[25] << "\t" << res[26]  << "\t" << res[27]  <<  std::endl;
    std::cout << res[28] << "\t" << res[29] << "\t" << res[30] << "\t" << res[31] << "\t" << res[32] << "\t" << res[33]  << "\t" << res[34]  << std::endl;
    std::cout << res[35] << "\t" << res[36] << "\t" << res[37] << "\t" << res[38] << "\t" << res[39] << "\t" << res[40]  << "\t" << res[41]  << std::endl;


    std::cout << "Error: " << error << std::endl;
    // Assertions

    std::cout << "Asserting" << std::endl;
    assert(fequal(eigenAstarA(0, 0), res[0]));
    assert(fequal(eigenAstarA(0, 1), res[1]));
    assert(fequal(eigenAstarA(0, 2), res[2]));
    assert(fequal(eigenAstarA(0, 3), res[3]));
    assert(fequal(eigenAstarA(0, 4), res[4]));
    assert(fequal(eigenAstarA(0, 5), res[5]));
    assert(fequal(eigenAstarB(0, 0), res[6]));

//    assert(fequal(eigenAstarA(1, 0), res[7]));
    assert(fequal(eigenAstarA(1, 1), res[8]));
    assert(fequal(eigenAstarA(1, 2), res[9]));
    assert(fequal(eigenAstarA(1, 3), res[10]));
    assert(fequal(eigenAstarA(1, 4), res[11]));
    assert(fequal(eigenAstarA(1, 5), res[12]));
    assert(fequal(eigenAstarB(1, 0), res[13]));

//    assert(fequal(eigenAstarA(2, 0), res[14]));
//    assert(fequal(eigenAstarA(2, 1), res[15]));
    assert(fequal(eigenAstarA(2, 2), res[16]));
    assert(fequal(eigenAstarA(2, 3), res[17]));
    assert(fequal(eigenAstarA(2, 4), res[18]));
    assert(fequal(eigenAstarA(2, 5), res[19]));
    assert(fequal(eigenAstarB(2, 0), res[20]));

//    assert(fequal(eigenAstarA(3, 0), res[21]));
//    assert(fequal(eigenAstarA(3, 1), res[22]));
//    assert(fequal(eigenAstarA(3, 2), res[23]));
    assert(fequal(eigenAstarA(3, 3), res[24]));
    assert(fequal(eigenAstarA(3, 4), res[25]));
    assert(fequal(eigenAstarA(3, 5), res[26]));
    assert(fequal(eigenAstarB(3, 0), res[27]));

//    assert(fequal(eigenAstarA(4, 0), res[28]));
//    assert(fequal(eigenAstarA(4, 1), res[29]));
//    assert(fequal(eigenAstarA(4, 2), res[30]));
//    assert(fequal(eigenAstarA(4, 3), res[31]));
    assert(fequal(eigenAstarA(4, 4), res[32]));
    assert(fequal(eigenAstarA(4, 5), res[33]));
    assert(fequal(eigenAstarB(4, 0), res[34]));

//    assert(fequal(eigenAstarA(5, 0), res[35]));
//    assert(fequal(eigenAstarA(5, 1), res[36]));
//    assert(fequal(eigenAstarA(5, 2), res[37]));
//    assert(fequal(eigenAstarA(5, 3), res[38]));
//    assert(fequal(eigenAstarA(5, 4), res[39]));
    assert(fequal(eigenAstarA(5, 5), res[40]));
    assert(fequal(eigenAstarB(5, 0), res[41]));
}
void _TestKinectFusionCudaIterativeClosestPointTransformComparison() {
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    Utility::SetCameraParameters(fx, fy, cx, cy);

    gpu::matrix33
            rotationMatrix(1, 0, 0, 0, 1, 0, 0, 0, 1), inverseRotation = rotationMatrix.transpose();
    gpu::float3 translation(2.5, 2.5, 0);



    cv::Mat_<double> imgA = cv::imread("A1D.png", -1);
    cv::Mat_<double> imgB = cv::imread("A2D.png", -1);
    imgA = imgA * (1./5.);
    imgB = imgB * (1./5.);

    cv::Mat imgAcolor = cv::imread("A1.png");
    cv::Mat imgBcolor = cv::imread("A2.png");



    gpu::DepthMap depthMapA(imgA.rows, imgA.cols, imgA.data);
    gpu::DepthMap depthMapB(imgB.rows, imgB.cols, imgB.data);
    int icpLevels = 3;
    Slam::KinectFusion kinectFusionCuda;

    kinectFusionCuda.SetupCuda(fx, fy, cx, cy, 480, 640, icpLevels, rotationMatrix, translation, 1);
    std::cout << "Starting Kinect cuda" << std::endl;
    kinectFusionCuda.CudaKinect(depthMapA, true);
    kinectFusionCuda.CudaDownloadRayCaster();
    kinectFusionCuda.CudaSaveRayCaster(0);
    kinectFusionCuda.CudaKinect(depthMapB);
    kinectFusionCuda.CudaDownloadRayCaster();
    kinectFusionCuda.CudaSaveRayCaster(1);

    gpu::matrix33 estimatedRotation = kinectFusionCuda.GetLastRotationMatrix();
    gpu::float3 estimatedTranslation = kinectFusionCuda.GetLastTranslationVector();
    std::cout << "Estimated Rotation Matrix " << std::endl
    << estimatedRotation.row0.x << " " << estimatedRotation.row0.y << " " << estimatedRotation.row0.z << " " << estimatedTranslation.x << std::endl
    << estimatedRotation.row1.x << " " << estimatedRotation.row1.y << " " << estimatedRotation.row1.z << " " << estimatedTranslation.y <<std::endl
    << estimatedRotation.row2.x << " " << estimatedRotation.row2.y << " " << estimatedRotation.row2.z << " " << estimatedTranslation.z <<std::endl;


    std::cout << "Opencv" << std::endl;

    cv::Mat prevRgb = cv::imread("A1.png");
    cv::Mat currRgb = cv::imread("A2.png");
    cv::Mat prevDepth = cv::imread("A1D.png", -1);
    cv::Mat currDepth = cv::imread("A2D.png", -1);
    prevDepth = prevDepth * (1./5.);
    currDepth = currDepth * (1./5.);
    kinectFusionCuda.SetCameraMatrix(Utility::GetCameraParameters());
    Eigen::Matrix4d res = kinectFusionCuda.OpenCVRgbd(prevRgb, prevDepth, currRgb, currDepth);
    std::cout << res << std::endl;
    exit(0);

}
void _TestKinectFusionCudaIterativeClosestPointSyntheticTransform() {
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    Utility::SetCameraParameters(fx, fy, cx, cy);

    gpu::matrix33 cameraMatrix(fx, 0, cx, 0, fy, cy, 0, 0, 1),
            rotationMatrix(1, 0, 0, 0, 1, 0, 0, 0, 1), inverseRotation = rotationMatrix.transpose();

    gpu::float3 translation(2.56, 2.56, 0);


    //Generate synthetic image
    gpu::matrix33 incRotation = Utility::GenerateRotationMatrix33(0.01, 0.02, 0.00);
    gpu::float3 incTranslation(0.002f, -0.009f, 0.002f);

    incRotation = rotationMatrix * incRotation;
    incTranslation = incTranslation + translation;
//    incRotation =  rotationMatrix * incRotation;
//    incTranslation = incTranslation + translation;

    cv::Mat_<double> imgA = cv::imread("1D.png", -1);
    cv::Mat imgAcolor = cv::imread("1.png");
    cv::Mat imgBcolor(imgA.rows, imgA.cols, imgAcolor.type());
    imgA = imgA * (1./5.);
    cv::Mat_<double> imgB(imgA.rows, imgA.cols);

//    cv::Mat_<double> imgA = cv::imread("A1D.png", -1);
//    imgA = imgA * (1./5.);
//    cv::Mat_<double> imgB = cv::imread("A2D.png", -1);
//    imgB = imgB * (1./5.);

    int x, y;
//    for( x = 0 ;x < imgA.cols; x++) {
//        double v = 300;
//        for(y = 0 ;y < imgA.rows; y++) {
//            imgA(y, x) = v;
//            v += 50;
//        }
//    }


    for( x = 0 ;x < imgA.cols; x++) {
        for(y = 0 ;y < imgA.rows; y++) {

            double d = imgA(y, x) / 1000.;
            gpu::float3 cameraSpace((float)((x - cx) * d /fx), (float)((y - cy) * d /fy), (float)(d));
            gpu::float3 worldSpace = rotationMatrix * cameraSpace + translation;
            gpu::float3 newCameraSpace = incRotation.transpose() * (worldSpace - incTranslation);
            int newX = (int)((newCameraSpace.x * fx / newCameraSpace.z) + cx);
            int newY = (int)((newCameraSpace.y * fy / newCameraSpace.z) + cy);
            if(newX < imgB.cols && newX >= 0 && newY < imgB.rows && newY >= 0) {
                imgB(newY, newX) = imgA(y, x);
                imgBcolor.at<cv::Vec3b>(newY, newX) = imgAcolor.at<cv::Vec3b>(y, x);
            }
        }
    }
    cv::Mat initial, result;
    imgA.convertTo(initial, CV_16U);
    initial = initial * 5;

//    imgB = cv::imread("2D.png", -1);
//    imgB = imgB * (1./5.);
    imgB.convertTo(result, CV_16U);
    result = result * 5;
    cv::imshow("Image", initial);
    cv::imshow("Warped Image", result);
    cv::waitKey(0);
    std::cout << "starting ICP " << std::endl;

    gpu::DepthMap depthMapA(imgA.rows, imgA.cols, imgA.data);
    gpu::DepthMap depthMapB(imgB.rows, imgB.cols, imgB.data);
    int icpLevels = 3;
    Slam::KinectFusion kinectFusionCuda;

    kinectFusionCuda.SetupCuda(fx, fy, cx, cy, 480, 640, icpLevels, rotationMatrix, translation, 1);
    std::cout << "Starting Kinect cuda" << std::endl;
//    kinectFusionCuda.CudaKinect(depthMapA, true);
//    kinectFusionCuda.CudaDownloadRayCaster();
//    kinectFusionCuda.CudaSaveRayCaster(0);
//    kinectFusionCuda.CudaKinect(depthMapB);
//    kinectFusionCuda.CudaDownloadRayCaster();
//    kinectFusionCuda.CudaSaveRayCaster(1);



    gpu::matrix33 estimatedRotation = kinectFusionCuda.GetLastRotationMatrix();
    gpu::float3 estimatedTranslation = kinectFusionCuda.GetLastTranslationVector();
    std::cout << "Estimated Rotation Matrix " << std::endl
    << estimatedRotation.row0.x << " " << estimatedRotation.row0.y << " " << estimatedRotation.row0.z << " " << estimatedTranslation.x << std::endl
    << estimatedRotation.row1.x << " " << estimatedRotation.row1.y << " " << estimatedRotation.row1.z << " " << estimatedTranslation.y <<std::endl
    << estimatedRotation.row2.x << " " << estimatedRotation.row2.y << " " << estimatedRotation.row2.z << " " << estimatedTranslation.z <<std::endl;

    std::cout << "Actual Rotation Matrix " << std::endl
    << incRotation.row0.x << " " << incRotation.row0.y << " " << incRotation.row0.z << " " << incTranslation.x << std::endl
    << incRotation.row1.x << " " << incRotation.row1.y << " " << incRotation.row1.z << " " << incTranslation.y <<std::endl
    << incRotation.row2.x << " " << incRotation.row2.y << " " << incRotation.row2.z << " " << incTranslation.z <<std::endl;


    std::cout << "Opencv" << std::endl;

    cv::Mat prevRgb = cv::imread("1.png");
    cv::Mat currRgb = cv::imread("2.png");
    cv::Mat prevDepth = cv::imread("1D.png", -1);
    cv::Mat currDepth = cv::imread("2D.png", -1);
    prevDepth = prevDepth * (1./5.);
    currDepth = currDepth * (1./5.);
    kinectFusionCuda.SetCameraMatrix(Utility::GetCameraParameters());
    Eigen::Matrix4d res = kinectFusionCuda.OpenCVRgbd(prevRgb, prevDepth, currRgb, currDepth);
    std::cout << res << std::endl;
    exit(0);

}
void _TestKinectFusionCudaIterativeClosestPointFull() {
    std::cout << "Starting _TestKinectFusionCudaIterativeClosestPointFull" << std::endl;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    Utility::SetCameraParameters(fx, fy, cx, cy);

    std::string dImg1("A1D.png");
    std::string img1("A1.png");
    std::string dImg2("A2D.png");
    std::string img2("A2.png");
    cv::Mat_<double> readImageA = cv::imread("A1D.png", -1);
//    cv::Mat_<double> readImageB = cv::imread("A2D.png", -1);
    cv::Mat_<double> readImageB = cv::imread("A2D.png", -1);
    readImageA = readImageA * (1. / 5.);
    readImageB = readImageB * (1. / 5.);
//    cv::imshow("TEST", readImageA);
//    cv::waitKey(0);

    gpu::DepthMap depthMapA(readImageA.rows, readImageA.cols, readImageA.data);
    gpu::DepthMap depthMapB(readImageB.rows, readImageB.cols, readImageB.data);
    int icpLevels = 3;
    Slam::KinectFusion kinectFusionCuda;
    gpu::matrix33 cameraMatrix(fx, 0, cx, 0, fy, cy, 0, 0, 1),
            rotationMatrix;
    rotationMatrix.SetIdentity();
    gpu::matrix33 inverseRotation = rotationMatrix.transpose();
    gpu::float3 translation(0, 0, 0);

    kinectFusionCuda.SetupCuda(fx, fy, cx, cy, 480, 640, icpLevels, rotationMatrix, translation, 1);
    std::cout << "Starting Kinect cuda" << std::endl;
    kinectFusionCuda.CudaKinect(depthMapA, true);
    kinectFusionCuda.CudaDownloadRayCaster();
    kinectFusionCuda.CudaSaveRayCaster(0);

////    std::cout << "Pass complete" << std::endl;
////    kinectFusionCuda.CudaKinect(depthMapB);
////    std::cout << "Ending Kinect cuda" << std::endl;
////    kinectFusionCuda.CudaDownloadIterativeClosestPointVertexAndNormalMap();
//    int i, x, y;
//    int rows = 480, cols = 640;
//    int skip = 0;
//    float scale = powf(2, 0);
//    std::cout << "Starting image save" << std::endl;
//    for(i = 0; i < 3; i++) {
//        scale = powf(2, -i);
//        cv::Mat_<ushort> generatedImage(rows, cols, (ushort)0);
//        for( x = 0 ; x < cols; x++) {
//            for ( y= 0 ; y < rows; y++) {
//
//
//                gpu::float3 r = kinectFusionCuda._icpSourceVertexMap[y * cols + x + skip];
//                gpu::float3 localPoint = inverseRotation * (r - translation);
////                std::cout << r.z << std::endl;
//
//                int2 p;
//                p.x = (int)((localPoint.x * (fx * scale))/localPoint.z + (cx * scale));
//                p.y = (int)((localPoint.y * (fy * scale))/localPoint.z + (cy * scale));
//                if (p.x == 347 && p.y == 394 && i ==0) {
//                    std::cout << "\t\t saddddddddddddddddddddddddd" << std::endl;
//                    std::cout << localPoint.x << " " << localPoint.y << " " << localPoint.z << std::endl;
//                }
//                if (x == 347 && y == 394 && i ==0) {
//                    std::cout << "\t\t OTHER" << std::endl;
//                    std::cout << r.x << " " << r.y << " " << r.z << std::endl;
//                    std::cout << localPoint.x << " " << localPoint.y << " " << localPoint.z << std::endl;
//                }
//                if(localPoint.z < 0 && p.y < 0 || p.y >= rows || p.x < 0 || p.x >= cols) {
//                    continue;
//                }
//
//                generatedImage(p.y, p.x) = (ushort)(5 * ((int)(localPoint.z * 1000.)));
//                //            std::cout << generatedImage((int)r.y, (int)r.x) << std::endl;
//                //            exit(0);
//                //347 394
//            }
//        }
//        skip += rows * cols;
//        rows /= 2;
//        cols /= 2;
//
//        //    std::cout << counter << std::endl;
//        std::ostringstream oss;
//        oss << "RaycastImageLevel";
//        oss << i;
//        oss << ".png";
//        cv::imwrite(oss.str(), generatedImage);
//    }
    kinectFusionCuda.CudaKinect(depthMapB);
    kinectFusionCuda.CudaDownloadRayCaster();
    kinectFusionCuda.CudaSaveRayCaster(1);
//    cv::Ptr<cv::rgbd::OdometryFrame> frame_prev = cv::Ptr<cv::rgbd::OdometryFrame>(new cv::rgbd::OdometryFrame()),
//            frame_curr = cv::Ptr<cv::rgbd::OdometryFrame>(new cv::rgbd::OdometryFrame());
//    cv::Ptr<cv::rgbd::Odometry> odometry = cv::rgbd::Odometry::create( "RgbdOdometry");
//    cv::Mat cameraMatrixs = cv::Mat::eye(3,3,CV_32FC1);
//    cameraMatrixs.at<float>(0,0) = fx;
//    cameraMatrixs.at<float>(1,1) = fy;
//    cameraMatrixs.at<float>(0,2) = cx;
//    cameraMatrixs.at<float>(1,2) = cy;
//    odometry->setCameraMatrix(cameraMatrixs);
//
//    cv::Mat prevRgb, currentRgb, currentDepth, prevDepth;
//    prevRgb = cv::imread(img1);
//    prevDepth = cv::imread(dImg1, -1);
//    currentRgb = cv::imread(img2);
//    currentDepth = cv::imread(dImg2, -1);
//
//
//    cv::Mat depth_flt;
//    currentDepth.convertTo(depth_flt, CV_32FC1, 1.f/5000.f);
////        currentDepth.release();
//    depth_flt.setTo(std::numeric_limits<float>::quiet_NaN(), currentDepth == 0);
//    currentDepth = depth_flt;
//    depth_flt.release();
//
//    prevDepth.convertTo(depth_flt, CV_32FC1, 1.f/5000.f);
////        prevDepth.release();
//    depth_flt.setTo(std::numeric_limits<float>::quiet_NaN(), prevDepth == 0);
//    prevDepth = depth_flt;
//    depth_flt.release();
//
//    cv::Mat currentGray;
//    cv::cvtColor(currentRgb, currentGray, cv::COLOR_BGR2GRAY);
//    frame_curr->image = currentGray;
//    frame_curr->depth = currentDepth;
//    cv::Mat prevGray;
//    cv::cvtColor(prevRgb, prevGray, cv::COLOR_BGR2GRAY);
//    frame_prev->image = prevGray;
//    frame_prev->depth = prevDepth;
//
//
//
//    cv::Mat Rt;
//    bool res = odometry->compute(frame_curr, frame_prev, Rt);
//    std::cout << "Opencv " << std::endl << Rt << std::endl;



    std::cout << "Ending _TestKinectFusionCudaIterativeClosestPointFull" << std::endl;
}

void _TestKinectFusionCudaVolumeProperties() {
    std::cout << "Startin _TestKinectFusionCudaVolumeProperties" << std::endl;
    Slam::KinectFusion kinectFusionCuda;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    int icpLevels = 3;
    gpu::matrix33 rotationMatrix(0, 0, 1, 0, 1, 0, -1, 0, 0);
    gpu::float3 translation(2.56, 2.56, 0.1);
    kinectFusionCuda.SetupCuda(fx, fy, cx, cy, 480, 640, icpLevels, rotationMatrix, translation, 1);

    assert(kinectFusionCuda._gpuVoxelCellSize.x == 0.01);
    assert(kinectFusionCuda._gpuVoxelCellSize.y == 0.01);
    assert(kinectFusionCuda._gpuVoxelCellSize.z == 0.01);

    assert(kinectFusionCuda._gpuVoxelCenter.x == 0);
    assert(kinectFusionCuda._gpuVoxelCenter.y == 0);
    assert(kinectFusionCuda._gpuVoxelCenter.z == 0);

    assert(kinectFusionCuda._gpuVoxelGridSize.x == 512);
    assert(kinectFusionCuda._gpuVoxelGridSize.y == 512);
    assert(kinectFusionCuda._gpuVoxelGridSize.z == 512);
    std::cout << "Ending _TestKinectFusionCudaVolumeProperties" << std::endl;
}
void _TestKinectFusionCudaPackUnpackTsdf() {
//    std::cout << "Startin _TestKinectFusionCudaPackUnpackTsdf" << std::endl;
//    Slam::KinectFusion kinectFusionCuda;
//    float fx = 525.0f, // default
//            fy = 525.0f,
//            cx = 319.5f,
//            cy = 239.5f;
//    int icpLevels = 3;
//    gpu::matrix33 rotationMatrix(0, 0, 1, 0, 1, 0, -1, 0, 0);
//    gpu::float3 translation(2.56, 2.56, 0.1);
//    kinectFusionCuda.SetupCuda(fx, fy, cx, cy, 480, 640, icpLevels, rotationMatrix, translation);
//    kinectFusionCuda.CudaPackTsdf(kinectFusionCuda.GetTsdfPointer(5,7,200), 0.3, 0.4);
//    float read = kinectFusionCuda.CudaReadTsdf(5,7,200);
//    std::cout << read << " " << 0.4 << std::endl;
//    assert(std::abs(0.4 - read) < 1.e-3);
//    int i, j, k;
//    float v = 0;
//    for(i = 0; i < 512; i++) {
//        for(j = 0; j < 512;j++) {
//            for(k = 0; k < 512; k++) {
//                kinectFusionCuda.CudaPackTsdf(kinectFusionCuda.GetTsdfPointer(i, j, k), 0.3, v);
//                v += 0.1;
//                if (v > 0.9) {
//                    v = 0;
//                }
//            }
//        }
//    }
//    v = 0;
//    for(i = 0; i < 512; i++) {
//        for(j = 0; j < 512;j++) {
//            for(k = 0; k < 512; k++) {
//                assert((kinectFusionCuda.CudaReadTsdf(i,j,k) - v) < 1.e-3 );
//                v += 0.1;
//                if (v > 0.9) {
//                    v = 0;
//                }
//            }
//        }
//    }
//    std::cout << "Ending _TestKinectFusionCudaPackUnpackTsdf" << std::endl;
}
void _TestKinectFusionCudaDepthMapUploadAndDownload() {
    Slam::KinectFusion kinectFusionCuda;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
//    kinectFusionCuda.SetupCuda(fx, fy, cx, cy, 480,640, 3);

    std::vector<cv::Mat_<double>> images =  GenerateSquare();

}
void CudaTsdfVisualizer(gpu::int3 voxelCenter, gpu::int3 voxelGridSize, int* _gpuVolumePtr, std::string dir) {
    std::cout << "Visualizer" << std::endl;
    int i, j, z;
    std::cout << voxelGridSize.z << " " << voxelGridSize.y << " " << voxelGridSize.x << std::endl;
    for ( z = 0; z < voxelGridSize.z; z++) {
        cv::Mat img2(voxelGridSize.x, voxelGridSize.y, CV_8UC1,cv::Scalar(0));
        for (i = 0; i < voxelGridSize.x; i++) {
            for (j = 0; j <  voxelGridSize.y; j++) {
                int index = gpu::device::Index3D(i, j, z, &voxelGridSize, & voxelGridSize);
                float v = 0, w = 0;
                short* ptr = (short*)(_gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, v);
                if (v < 0) {
                    v = 0;
                } else {
                    v = 255;
                }
                img2.at<uchar>(i, j) = (uchar) v;

            }
        }
        std::ostringstream oss2;
        oss2 << dir;
        oss2 << "/";
        oss2 << "TsdfAZ";
        oss2 << z;
        oss2 << ".png";
        cv::imwrite(oss2.str(), img2);
    }
}
void CudaTsdfVisualizer(gpu::int3 voxelCenter, gpu::int3 voxelGridSize, int* _gpuVolumePtr) {
    std::cout << "Visualizer" << std::endl;
    int i, j, z;
    std::cout << voxelGridSize.z << " " << voxelGridSize.y << " " << voxelGridSize.x << std::endl;
    for ( z = 0; z < voxelGridSize.z; z++) {
        cv::Mat img2(voxelGridSize.x, voxelGridSize.y, CV_8UC1,cv::Scalar(0));
        for (i = 0; i < voxelGridSize.x; i++) {
            for (j = 0; j <  voxelGridSize.y; j++) {
                int index = gpu::device::Index3DBasic(i, j, z, &voxelGridSize);
                float v = 0, w = 0;
                short* ptr = (short*)(_gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, v);
                if (v < 0) {
                    v = 0;
                } else {
                    v = 255;
                }
                img2.at<uchar>(i, j) = (uchar) v;

            }
        }
        std::ostringstream oss2;
        oss2 << "TsdfAZ";
        oss2 << z;
        oss2 << ".png";
        cv::imwrite(oss2.str(), img2);
    }
}
void _TestKinectFusionCudaTsdfVolumeVisualizer() {
    std::cout << "Starting _TestKinectFusionTsdfVolumeVisualizerCuda" << std::endl;
//    gpu::host::Tsdf::ProjectiveTSDF(ptr);

    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    Utility::SetCameraParameters(fx, fy, cx, cy);


    Slam::KinectFusion kinectFusionCuda;
    int icpLevels = 3;
//    gpu::matrix33 rotationMatrix(0, 0, 1, 0, 1, 0, -1, 0, 0);
    gpu::matrix33 rotationMatrix;
    rotationMatrix.SetIdentity();
//    gpu::float3 translation(2.56, 2.56, 1);
    gpu::float3 translation(0, 0, 0);
    kinectFusionCuda.SetupCuda(fx, fy, cx, cy, 480, 640, icpLevels, rotationMatrix, translation, 1);
//    kinectFusionCuda._transformEstimate = Utility::GenerateRotationMatrix(0,  (2 * M_PI)/4., 0, 2.56, 2.56, 2.56);
    std::vector<cv::Mat_<double>> imagesCV = GenerateSquares();
    //convert cv::Mat to depthMap
    std::vector<gpu::DepthMap> images;
    images.resize(imagesCV.size());
    int i;
    std::cout << "Reassigned" << std::endl;
    for (i = 0; i < images.size(); i++) {
        images[i].Init(imagesCV[i].rows, imagesCV[i].cols, imagesCV[i].datastart, imagesCV[i].dataend);
//        exit(0);
    }
    std::cout << "Finished" << std::endl;
    for(i = 0; i < images.size(); i++){
//        std::cout << "TSDF start" << std::endl;
        kinectFusionCuda.CudaUploadImageData(images[i]);
        kinectFusionCuda.CudaProjectiveTsdf();
//        std::cout << "TSDF end" << std::endl;
    }
    kinectFusionCuda.CudaDownloadTsdf();
    CudaTsdfVisualizer(kinectFusionCuda._voxelCenter, kinectFusionCuda._voxelGridSize, kinectFusionCuda._gpuVolumePtr);
//    kinectFusionCuda.CudaTsdfVisualizer();
//    kinectFusionCuda.TsdfVisualizer();

    std::cout << "Ending _TestKinectFusionTsdfVolumeVisualizerCuda" << std::endl;
}

void _TestKinectFusionCudaTsdfVolumeAndRayCaster() {
    std::cout << "Starting _TestKinectFusionCudaTsdfVolumeAndRayCaster" << std::endl;
//    gpu::host::Tsdf::ProjectiveTSDF(ptr);

    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    Utility::SetCameraParameters(fx, fy, cx, cy);


    Slam::KinectFusion kinectFusionCuda;
    int icpLevels = 3;
    gpu::matrix33 rotationMatrix, inverseRotation;
    rotationMatrix.SetIdentity();
    inverseRotation = rotationMatrix.transpose();
//    gpu::float3 translation(2.56, 2.56, 2.56);
    gpu::float3 translation(0, 0, 0);
    kinectFusionCuda.SetupCuda(fx, fy, cx, cy, 480, 640, icpLevels, rotationMatrix, translation, 1);
//    kinectFusionCuda._transformEstimate = Utility::GenerateRotationMatrix(0,  (2 * M_PI)/4., 0, 2.56, 2.56, 2.56);
//    std::vector<cv::Mat_<double>> imagesCV = GenerateSquares();
    std::vector<cv::Mat_<double>> imagesCV = GenerateSquares();;
    //convert cv::Mat to depthMap
    std::vector<gpu::DepthMap> images;
    images.resize(imagesCV.size());
    int i;
    std::cout << "Reassigned" << std::endl;
    for (i = 0; i < images.size(); i++) {
        images[i].Init(imagesCV[i].rows, imagesCV[i].cols, imagesCV[i].datastart, imagesCV[i].dataend);
    }
    std::cout << "Finished" << std::endl;
    for(i = 0; i < images.size(); i++){
        kinectFusionCuda.CudaUploadImageData(images[i]);
        kinectFusionCuda.CudaProjectiveTsdf();
    }
    std::cout << "Raycast 1" << std::endl;
    kinectFusionCuda.CudaRayCaster();
    kinectFusionCuda.CudaDownloadRayCaster();
    kinectFusionCuda.CudaSaveRayCaster(10);

    //Compare Raycaster data with raycaster data when vc is moved

    gpu::float3* beforeVertexs = new gpu::float3[640 * 480];
    gpu::float3* beforeNormals = new gpu::float3[640 * 480];
    std::memcpy(beforeVertexs, kinectFusionCuda._icpSourceVertexMap, sizeof(gpu::float3) * 640 * 480);
    std::memcpy(beforeNormals, kinectFusionCuda._icpSourceNormalMap, sizeof(gpu::float3) * 640 * 480);

    kinectFusionCuda._voxelCenter = gpu::int3(30, 0, 0);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &(kinectFusionCuda._voxelCenter), sizeof(gpu::int3), cudaMemcpyHostToDevice);

    std::cout << "Raycast 2" << std::endl;
    kinectFusionCuda.CudaRayCaster();
    kinectFusionCuda.CudaDownloadRayCaster();
    int j;
    for(i = 0; i < 640; i++ ) {
        for(j = 0; j < 480; j++) {
            gpu::float3 beforeVertex = beforeVertexs[gpu::device::Index2D(i, j, 640)];
            gpu::float3 beforeNormal = beforeNormals[gpu::device::Index2D(i, j, 640)];
            gpu::float3 afterVertex = kinectFusionCuda._icpSourceVertexMap[gpu::device::Index2D(i, j, 640)];
            gpu::float3 afterNormal = kinectFusionCuda._icpSourceNormalMap[gpu::device::Index2D(i, j, 640)];

            if(std::isnan(beforeVertex.x) && std::isnan(afterVertex.x)) {
                continue;
            }
            if(std::isnan(beforeVertex.x) || std::isnan(afterVertex.x)) {
                //fail
                std::cout << "Pos: " << i << ", " << j << std::endl;
                std::cout << "Before Vertex: " << beforeVertex.x << "\t" << beforeVertex.y << "\t" << beforeVertex.z << std::endl;
                std::cout << "After  Vertex: " << afterVertex.x << "\t" << afterVertex.y << "\t" << afterVertex.z << std::endl;
                exit(-1);
            }
            if(beforeVertex != afterVertex) {
                //fail
                std::cout << "Pos: " << i << ", " << j << std::endl;
                std::cout << "Before Vertex: " << beforeVertex.x << "\t" << beforeVertex.y << "\t" << beforeVertex.z << std::endl;
                std::cout << "After  Vertex: " << afterVertex.x << "\t" << afterVertex.y << "\t" << afterVertex.z << std::endl;
                exit(-1);
            }

            if(std::isnan(beforeNormal.x) && std::isnan(afterNormal.x)) {
                continue;
            }
            if(std::isnan(beforeNormal.x) || std::isnan(afterNormal.x)) {
                //fail
                std::cout << "Pos: " << i << ", " << j << std::endl;
                std::cout << "Before Vertex: " << beforeNormal.x << "\t" << beforeNormal.y << "\t" << beforeNormal.z << std::endl;
                std::cout << "After  Vertex: " << afterNormal.x << "\t" << afterNormal.y << "\t" << afterNormal.z << std::endl;
                exit(-1);
            }
            if(beforeNormal != afterNormal) {
                //fail
                std::cout << "Pos: " << i << ", " << j << std::endl;
                std::cout << "Before Vertex: " << beforeNormal.x << "\t" << beforeNormal.y << "\t" << beforeNormal.z << std::endl;
                std::cout << "After  Vertex: " << afterNormal.x << "\t" << afterNormal.y << "\t" << afterNormal.z << std::endl;
                exit(-1);
            }

        }
    }













    std::cout << "Generating Image" << std::endl;

    int x=0, y=0;
    int r = 480, c = 640;
    int skip = 0;
    float scale = 1;
    for(i = 0; i < 1; i++) {
        cv::Mat_<ushort> generatedImage(r, c, (ushort)0);
        scale = powf(2, -i);
        for( x = 0 ; x < c; x++) {
            for ( y= 0 ; y < r; y++) {

    //            std::cout << (y * 640 + x) << std::endl;
                gpu::float3 point = kinectFusionCuda._icpSourceVertexMap[gpu::device::Index2D(x, y, c) + skip];
//                if(!std::isnan(point.x) || !std::isnan(point.y) || !std::isnan(point.z)) {
//                    std::cout << "point " << x << " " << y << " is not nan" << std::endl;
//                }
                if(x == 293 && y == 176) {
                    std::cout << point.x << " " << point.y << " " << point.z << std::endl;
                    std::cout << "At Position " << x << " " << y << std::endl;
                }
                gpu::float3 localPoint = inverseRotation * (point - translation);

                if (localPoint.z <= 0 ) {
                    continue;
                }
//                int2 p;
//                p.x = (int)((localPoint.x * (fx * scale))/localPoint.z + (cx * scale));
//                p.y = (int)((localPoint.y * (fy * scale))/localPoint.z + (cy * scale));
//                if (p.x < 0 || p.x >= c || p.y < 0 || p.y >= r) {
//                    continue;
//                }

                ushort d = (ushort)( ((int)(localPoint.z * 1000.)));
                generatedImage(y, x) = d;
            }
        }
        skip += r * c;
        r /= 2;
        c /= 2;

    //    std::cout << counter << std::endl;
        std::ostringstream oss;
        oss << "TestRaycastImageLevel";
        oss << i;
        oss << ".png";
        generatedImage = generatedImage * 5;
        cv::imwrite(oss.str(), generatedImage);
    }
    kinectFusionCuda.CudaDownloadTsdf();
    CudaTsdfVisualizer(kinectFusionCuda._voxelCenter, kinectFusionCuda._voxelGridSize, kinectFusionCuda._gpuVolumePtr);
//    kinectFusionCuda.TsdfVisualizer();

    std::cout << "Ending _TestKinectFusionCudaTsdfVolumeAndRayCaster" << std::endl;
}
void _TestKinectFusionCudaTsdfVolumeAndRayCasterTogether() {
    std::cout << "Starting _TestKinectFusionTsdfVolumeVisualizerCuda" << std::endl;
//    gpu::host::Tsdf::ProjectiveTSDF(ptr);

    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    Utility::SetCameraParameters(fx, fy, cx, cy);


    Slam::KinectFusion kinectFusionCuda;
    int icpLevels = 3;
//    gpu::matrix33 rotationMatrix(0, 0, 1, 0, 1, 0, -1, 0, 0);
    gpu::matrix33 rotationMatrix;
    rotationMatrix.SetIdentity();
//    gpu::float3 translation(2.56, 2.56, 2.56);
    gpu::float3 translation(0, 0, 0);
    kinectFusionCuda.SetupCuda(fx, fy, cx, cy, 480, 640, icpLevels, rotationMatrix, translation, 1);
//    std::cout << "Cuda setup" << std::endl;
//    kinectFusionCuda._transformEstimate = Utility::GenerateRotationMatrix(0,  (2 * M_PI)/4., 0, 2.56, 2.56, 2.56);
    std::vector<cv::Mat_<double>> imagesCV = GenerateSquares();
    //convert cv::Mat to depthMap
    std::vector<gpu::DepthMap> images;
    images.resize(imagesCV.size());
    int i;
    std::cout << "Reassigned" << std::endl;
    for (i = 0; i < images.size(); i++) {
        images[i].Init(imagesCV[i].rows, imagesCV[i].cols, imagesCV[i].datastart, imagesCV[i].dataend);
//        exit(0);
    }
    std::cout << "Finished" << std::endl;
    return;
    for(i = 0; i < images.size(); i++){
//        std::cout << "TSDF start" << std::endl;
        kinectFusionCuda.CudaUploadImageData(images[i]);
        kinectFusionCuda.CudaProjectiveTsdf();
//        std::cout << "TSDF end" << std::endl;
    }
    kinectFusionCuda.CudaDownloadTsdf();
    CudaTsdfVisualizer(kinectFusionCuda._voxelCenter, kinectFusionCuda._voxelGridSize, kinectFusionCuda._gpuVolumePtr);
    kinectFusionCuda.CudaRayCaster();
    kinectFusionCuda.CudaDownloadRayCaster(0);
    kinectFusionCuda.CudaSaveRayCaster(0);

//    kinectFusionCuda.GeneratePointCloudUsingRayCaster(pc);
//    kinectFusionCuda.viewPointCloud(pc);
    std::cout << "Ending _TestKinectFusionTsdfVolumeVisualizerCuda" << std::endl;
}
void _TestKinectFusionCudaTsdfVolumeAndRayCasterToModel() {
    std::cout << "Starting _TestKinectFusionCudaTsdfVolumeAndRayCaster" << std::endl;
//    gpu::host::Tsdf::ProjectiveTSDF(ptr);

    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    Utility::SetCameraParameters(fx, fy, cx, cy);


    Slam::KinectFusion kinectFusionCuda;
    int icpLevels = 3;
    gpu::matrix33 rotationMatrix, inverseRotation;
    rotationMatrix.SetIdentity();
    inverseRotation = rotationMatrix.transpose();
//    gpu::float3 translation(2.56, 2.56, 2.56);
    gpu::float3 translation(0, 0, 0);
    kinectFusionCuda.SetupCuda(fx, fy, cx, cy, 480, 640, icpLevels, rotationMatrix, translation, 1);
//    kinectFusionCuda._transformEstimate = Utility::GenerateRotationMatrix(0,  (2 * M_PI)/4., 0, 2.56, 2.56, 2.56);
//    std::vector<cv::Mat_<double>> imagesCV = GenerateSquares();
    std::vector<cv::Mat_<double>> imagesCV;
    std::vector<cv::Mat> imagesCVRGB;
    cv::Mat_<double> testImage = cv::imread("test/1D.png", -1);
    cv::Mat testImageRGB = cv::imread("test/1.png", cv::IMREAD_COLOR);
    testImage = testImage * (1.f/5.f);

    std::cout << "Test: " << testImage(320, 25) << std::endl;
    imagesCV.push_back(testImage);
    imagesCVRGB.push_back(testImageRGB);
    //convert cv::Mat to depthMap
    std::vector<gpu::DepthMap> images;
    std::vector<gpu::Image> imagesRGB;
    images.resize(imagesCV.size());
    imagesRGB.resize(imagesCVRGB.size());
    int i;
    std::cout << "Reassigned" << std::endl;
    for (i = 0; i < images.size(); i++) {
        images[i].Init(imagesCV[i].rows, imagesCV[i].cols, imagesCV[i].datastart, imagesCV[i].dataend);
        imagesRGB[i] = gpu::Image(imagesCVRGB[i].rows, imagesCVRGB[i].cols, imagesCVRGB[i].channels(), imagesCVRGB[i].data);
    }
    std::cout << "Finished" << std::endl;
    for(i = 0; i < images.size(); i++){
        kinectFusionCuda.CudaUploadImageData(images[i]);
        kinectFusionCuda.CudaUploadColourImageData(imagesRGB[i]);
        kinectFusionCuda.CudaProjectiveTsdf();
    }
    std::cout << "Raycast 1" << std::endl;
    kinectFusionCuda.CudaRayCaster();
    kinectFusionCuda.CudaDownloadRayCaster();
    kinectFusionCuda.CudaSaveRayCaster(10);
//    exit(0);

    //Compare Raycaster data with raycaster data when vc is moved

    gpu::float3* beforeVertexs = new gpu::float3[640 * 480];
    gpu::float3* beforeNormals = new gpu::float3[640 * 480];
    std::memcpy(beforeVertexs, kinectFusionCuda._icpSourceVertexMap, sizeof(gpu::float3) * 640 * 480);
    std::memcpy(beforeNormals, kinectFusionCuda._icpSourceNormalMap, sizeof(gpu::float3) * 640 * 480);

    kinectFusionCuda._voxelCenter = gpu::int3(30, 0, 0);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &(kinectFusionCuda._voxelCenter), sizeof(gpu::int3), cudaMemcpyHostToDevice);

    std::cout << "Raycast 2" << std::endl;
    kinectFusionCuda.CudaRayCaster();
    kinectFusionCuda.CudaDownloadRayCaster();
//    exit(0);
    int j;
    for(i = 0; i < 640; i++ ) {
        for(j = 0; j < 480; j++) {
            gpu::float3 beforeVertex = beforeVertexs[gpu::device::Index2D(i, j, 640)];
            gpu::float3 beforeNormal = beforeNormals[gpu::device::Index2D(i, j, 640)];
            gpu::float3 afterVertex = kinectFusionCuda._icpSourceVertexMap[gpu::device::Index2D(i, j, 640)];
            gpu::float3 afterNormal = kinectFusionCuda._icpSourceNormalMap[gpu::device::Index2D(i, j, 640)];

//            if(std::isnan(beforeVertex.x) && std::isnan(afterVertex.x)) {
//                continue;
//            }
//            if(std::isnan(beforeVertex.x) || std::isnan(afterVertex.x)) {
//                //fail
//                std::cout << "Pos: " << i << ", " << j << std::endl;
//                std::cout << "Before Vertex: " << beforeVertex.x << "\t" << beforeVertex.y << "\t" << beforeVertex.z << std::endl;
//                std::cout << "After  Vertex: " << afterVertex.x << "\t" << afterVertex.y << "\t" << afterVertex.z << std::endl;
//                exit(-1);
//            }
//            if(beforeVertex != afterVertex) {
//                //fail
//                std::cout << "Pos: " << i << ", " << j << std::endl;
//                std::cout << "Before Vertex: " << beforeVertex.x << "\t" << beforeVertex.y << "\t" << beforeVertex.z << std::endl;
//                std::cout << "After  Vertex: " << afterVertex.x << "\t" << afterVertex.y << "\t" << afterVertex.z << std::endl;
//                exit(-1);
//            }
//
//            if(std::isnan(beforeNormal.x) && std::isnan(afterNormal.x)) {
//                continue;
//            }
//            if(std::isnan(beforeNormal.x) || std::isnan(afterNormal.x)) {
//                //fail
//                std::cout << "Pos: " << i << ", " << j << std::endl;
//                std::cout << "Before Vertex: " << beforeNormal.x << "\t" << beforeNormal.y << "\t" << beforeNormal.z << std::endl;
//                std::cout << "After  Vertex: " << afterNormal.x << "\t" << afterNormal.y << "\t" << afterNormal.z << std::endl;
//                exit(-1);
//            }
//            if(beforeNormal != afterNormal) {
//                //fail
//                std::cout << "Pos: " << i << ", " << j << std::endl;
//                std::cout << "Before Vertex: " << beforeNormal.x << "\t" << beforeNormal.y << "\t" << beforeNormal.z << std::endl;
//                std::cout << "After  Vertex: " << afterNormal.x << "\t" << afterNormal.y << "\t" << afterNormal.z << std::endl;
//                exit(-1);
//            }

        }
    }
    for(i = 0; i < 640; i++ ) {
        for(j = 0; j < 480; j++) {
            gpu::float3 afterVertex = kinectFusionCuda._icpSourceVertexMap[gpu::device::Index2D(i, j, 640)];
            if(afterVertex.z < 0) {
                std::cout << "Negative Point: " << i << " " << j << std::endl;
                std::cout << "Point Details : " << afterVertex.x << " " << afterVertex.y << " " << afterVertex.z << std::endl;
            }
        }
    }

    std::cout << "Building PointCloud" << std::endl;
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
    kinectFusionCuda.GeneratePointCloudUsingRayCaster(pointCloud);
    kinectFusionCuda.viewPointCloud(pointCloud);










    std::cout << "Generating Image" << std::endl;

    int x=0, y=0;
    int r = 480, c = 640;
    int skip = 0;
    float scale = 1;
    for(i = 0; i < 1; i++) {
        cv::Mat_<ushort> generatedImage(r, c, (ushort)0);
        scale = powf(2, -i);
        for( x = 0 ; x < c; x++) {
            for ( y= 0 ; y < r; y++) {

                //            std::cout << (y * 640 + x) << std::endl;
                gpu::float3 point = kinectFusionCuda._icpSourceVertexMap[gpu::device::Index2D(x, y, c) + skip];
//                if(!std::isnan(point.x) || !std::isnan(point.y) || !std::isnan(point.z)) {
//                    std::cout << "point " << x << " " << y << " is not nan" << std::endl;
//                }
                if(x == 293 && y == 176) {
                    std::cout << point.x << " " << point.y << " " << point.z << std::endl;
                    std::cout << "At Position " << x << " " << y << std::endl;
                }
                gpu::float3 localPoint = inverseRotation * (point - translation);

                if (localPoint.z <= 0 ) {
                    continue;
                }
//                int2 p;
//                p.x = (int)((localPoint.x * (fx * scale))/localPoint.z + (cx * scale));
//                p.y = (int)((localPoint.y * (fy * scale))/localPoint.z + (cy * scale));
//                if (p.x < 0 || p.x >= c || p.y < 0 || p.y >= r) {
//                    continue;
//                }

                ushort d = (ushort)( ((int)(localPoint.z * 1000.)));
                generatedImage(y, x) = d;
            }
        }
        skip += r * c;
        r /= 2;
        c /= 2;

        //    std::cout << counter << std::endl;
        std::ostringstream oss;
        oss << "TestRaycastImageLevel";
        oss << i;
        oss << ".png";
        generatedImage = generatedImage * 5;
        cv::imwrite(oss.str(), generatedImage);
    }
    kinectFusionCuda.CudaDownloadTsdf();
    CudaTsdfVisualizer(kinectFusionCuda._voxelCenter, kinectFusionCuda._voxelGridSize, kinectFusionCuda._gpuVolumePtr);
//    kinectFusionCuda.TsdfVisualizer();

    std::cout << "Ending _TestKinectFusionCudaTsdfVolumeAndRayCaster" << std::endl;
}
//
// center
// 1d array center at 5 size 10
// size - center + index


int _TestBasicIndexing(int x, int y, int z, gpu::int3& volumeSize) {
    return x + y * volumeSize.z + z * volumeSize.x * volumeSize.y;
}
int _TestIndexing(int x, int y, int z, gpu::int3& volumeCenter, gpu::int3& volumeSize) {
    int index = _TestBasicIndexing((x + volumeCenter.x) % volumeSize.x, (y + volumeCenter.y) % volumeSize.y, (z + volumeCenter.z) % volumeSize.z, volumeSize);
    if(index < 0) {
        index = volumeSize.x * volumeSize.y * volumeSize.z + index;
    }
    return index;
}

void _TestResetVolume(int* ptr, gpu::int3 u, gpu::int3 volumeCenter, gpu::int3 volumeGridSize) {


    //reset values local cube x = 0 -> 10 for all y, z; but index 512-524 for all y,z must be reset;

    gpu::int3 oldCenter = volumeCenter;
    gpu::int3 half(256,256,256);
    gpu::int3 start;
    gpu::int3 end = u;
    if(u.x < 0) {
        start.x = volumeGridSize.x + u.x;
        end.x = volumeGridSize.x;
    } if (u.y < 0) {
        start.y = volumeGridSize.y + u.y;
        end.y = volumeGridSize.y;
    } if (u.z < 0) {
        start.z = volumeGridSize.z + u.z;
        end.z = volumeGridSize.z;
    }
    std::cout << "Start and End" << std::endl;
    std::cout << start.x << " " << start.y << " " << start.z << std::endl;
    std::cout << end.x << " " << end.y << " " << end.z << std::endl;
//    gpu::int3 start =  (oldCenter + half) , end = oldCenter + half + u;
//    if(start.x > end.x) {
//        int temp = start.x;
//        start.x = end.x;
//        end.x = temp;
//    }
//    if(start.y > end.y) {
//        int temp = start.y;
//        start.y = end.y;
//        end.y = temp;
//    }
//    if(start.z > end.z) {
//        int temp = start.z;
//        start.z = end.z;
//        end.z = temp;
//    }
    int i, j, k;
    std::cout << "I started" << std::endl;
    for(i = start.x; i < end.x; i++) {
        for( j = 0; j< volumeGridSize.y; j++) {
            for(k =0;k < volumeGridSize.z; k++) {
                ptr[_TestIndexing(i, j, k, volumeCenter, volumeGridSize)] = 1;
            }
        }
    }
    std::cout << "I completed" << std::endl;
    for(j = start.y; j < end.y; j++) {
        for( i =0; i< volumeGridSize.x; i++) {
            for(k =0;k < volumeGridSize.z; k++) {
                ptr[_TestIndexing(i, j, k, volumeCenter, volumeGridSize)] = 1;
            }
        }
    }
    for(k = start.z; k < end.z; k++) {
        for( j =0; j< volumeGridSize.y; j++) {
            for(i =0;i < volumeGridSize.x; i++) {
                ptr[_TestIndexing(i, j, k, volumeCenter, volumeGridSize)] = 1;
            }
        }
    }
}
gpu::int3 move(int* ptr, gpu::float3 pos, gpu::float3 motion, gpu::int3& voxelThreshold, gpu::int3& volumeGridSize, gpu::int3& volumeCenter, gpu::float3& voxelCellSize) {
    gpu::float3 cameraPos2 = pos;
    cameraPos2 += motion;
    gpu::float3 tempF = cameraPos2/voxelCellSize - pos/ voxelCellSize;
    gpu::int3 movedVoxels((int) tempF.x, (int) tempF.y, (int) tempF.z);
    gpu::int3 u;
    if(std::abs(movedVoxels.x) >= voxelThreshold.x ||
       std::abs(movedVoxels.y) >= voxelThreshold.y ||
       std::abs(movedVoxels.z) >= voxelThreshold.z) {
        //'Move' volume
        std::cout << "TRIGGERED" << std::endl;
        gpu::float3 tempu = cameraPos2/voxelCellSize;
        u.x =(int) tempu.x; u.y = (int) tempu.y; u.z = (int) tempu.z;
        std::cout << "Moved Voxels: " << u.x << " " << u.y << " " << u.z << std::endl;
        gpu::float3 tPrime = cameraPos2 - voxelCellSize * u;
        std::cout << "New of Camera: " << tPrime.x << " " << tPrime.y << " " << tPrime.z << std::endl;
        // Perform Volume Reset
        _TestResetVolume(ptr, u, volumeCenter, volumeGridSize);
        volumeCenter = volumeCenter + u;
        std::cout << "Volume Center: " << volumeCenter.x << " " << volumeCenter.y << " " << volumeCenter.z << std::endl;
    }
    return u;
}
void _TestVoxelToWorld() {
    gpu::float3 cameraPos1(0.f, 0.0f, 0.0f);
    float size = 5.12f;
    gpu::int3 volumeCenter(1024, 1024, 1024);
    gpu::int3 volumeSize(512, 512, 512);
    gpu::float3 voxelCellSize(size / volumeSize.x, size/volumeSize.y, size/volumeSize.z);
    std::cout << "Index: " << _TestIndexing(0, 0, 0, volumeCenter, volumeSize) << std::endl;
    gpu::float3 w = gpu::device::VoxelToWorld(0, 0, 0, &volumeSize, &volumeCenter, &voxelCellSize);
    std::cout << "World is " << w.x << " " << w.y << " " << w.z << std::endl;

}
void _TestKinectFusionMovingIndex() {
    gpu::float3 cameraPos1(0.f, 0.0f, 0.0f);
    gpu::int3 volumeCenter(0, 0, 0);
    gpu::int3 volumeSize(512, 512, 512);
    float size = 5.12f;
    gpu::float3 voxelCellSize(size / volumeSize.x, size/volumeSize.y, size/volumeSize.z);
    gpu::int3 voxelThreshold(10, 10, 10);

    std::cout << "Indexing " << _TestIndexing(0, 0, 0, volumeCenter, volumeSize) << std::endl;
    gpu::float3 firstCo, secondCo;
    firstCo = gpu::device::VoxelToWorld(0, 0, 0, &volumeSize, &volumeCenter, &voxelCellSize);
    secondCo = gpu::device::VoxelToWorld(511, 511, 511, &volumeSize, &volumeCenter, &voxelCellSize);
    std::cout << "Coordinate " << firstCo.x << " " << firstCo.y << " " << firstCo.z << std::endl;
    std::cout << "Indexing " << _TestIndexing(511, 511, 511, volumeCenter, volumeSize) << std::endl;
    std::cout << "Coordinate " << secondCo.x << " " << secondCo.y << " " << secondCo.z << std::endl;
//    return;
//    _TestVoxelToWorld();
//    return;


    int* volume = new int[512 * 512 * 512];
    int* endVolume = volume + 512 * 512 * 512;
    int* first = volume;
    for(;first != endVolume;first++) {
        *first = -1;
    }
//    CudaTsdfVisualizer(volumeSize, volume);
    std::cout << "filled" << std::endl;
    int i;
    for(i = 0; i < 512 * 512 * 512; i++) {
        assert(volume[i] == -1);
    }

    //Move camera 50 voxels
    gpu::float3 motion(-0.101f, 0.f, 0.f);
    gpu::int3 u = move(volume, cameraPos1, motion, voxelThreshold, volumeSize, volumeCenter, voxelCellSize);
    std::cout << "Indexing " << _TestIndexing(0, 0, 0, volumeCenter, volumeSize) << std::endl;
    std::cout << "Indexing " << _TestIndexing(511, 511, 511, volumeCenter, volumeSize) << std::endl;
    //Indexing test

    std::cout << "Center " << volumeCenter.x << " " << volumeCenter.y << " " << volumeCenter.z << std::endl;
    //Execute linear raycast on voxels that need to be zeroed

    //Push vertexs into triangluation algorithm

    //Reset the voxels
//    _TestResetVolume(volume, u, volumeCenter, volumeSize);
    first = volume;
    for(;first != endVolume;first++) {
        *first = -1;
    }
//    //move
    motion.x = -0.201f;
    motion.y = 0;
    motion.z = -0.5f;
    u = move(volume, cameraPos1, motion, voxelThreshold, volumeSize, volumeCenter, voxelCellSize);
    std::cout << "Center " << volumeCenter.x << " " << volumeCenter.y << " " << volumeCenter.z << std::endl;
//

    first = volume;
    for(;first != endVolume;first++) {
        *first = -1;
    }
    motion.x = -0.301f;
    motion.y = 0;
    motion.z = -0.501f;
    u = move(volume, cameraPos1, motion, voxelThreshold, volumeSize, volumeCenter, voxelCellSize);
    CudaTsdfVisualizer(volumeCenter, volumeSize, volume, "after");
    std::cout << "Center " << volumeCenter.x << " " << volumeCenter.y << " " << volumeCenter.z << std::endl;

//    _TestResetVolume(volume, u, volumeCenter, volumeSize);

//    std::cout << "NEGA "  << _TestIndexing(0, 0, 0, volumeCenter, volumeSize) << std::endl;

}

void _TestKinectFusionCudaMovingVolume() {
    std::cout << "Starting KinectFusionCudaMovingVolume Tests" << std::endl;
    Slam::KinectFusion kinectFusion;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    gpu::float3 translation;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotation, translation, 1);
    //Basic test No movement
    bool check = kinectFusion.CudaCheckVolumeBounds();
    assert(!check);
    //Movement without trigger
    kinectFusion._translation.x = 0.06f;
    check = kinectFusion.CudaCheckVolumeBounds();
    assert(!check);
    assert(kinectFusion._voxelCenter.x == 0);
    assert(kinectFusion._voxelCenter.y == 0);
    assert(kinectFusion._voxelCenter.z == 0);
    //Movement triggered in 1 dim
    kinectFusion._translation.x = 0.1f;
    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == 10);
    assert(kinectFusion._voxelCenter.y == 0);
    assert(kinectFusion._voxelCenter.z == 0);
    //Movement triggered in 1 dim
    kinectFusion._translation.x = 0.0f;
    check = kinectFusion.CudaCheckVolumeBounds();
//    std::cout << "Cehck" << std::endl;
    assert(check);
    assert(kinectFusion._voxelCenter.x == 0);
    assert(kinectFusion._voxelCenter.y == 0);
    assert(kinectFusion._voxelCenter.z == 0);

//    //Movement back in dir
    kinectFusion._translation.x = 0.1f;
    check = kinectFusion.CudaCheckVolumeBounds();
//    std::cout << "Cehck" << std::endl;
    assert(check);
    assert(kinectFusion._voxelCenter.x == 10);
    assert(kinectFusion._voxelCenter.y == 0);
    assert(kinectFusion._voxelCenter.z == 0);
//    // Movement in nega dir
    kinectFusion._translation.y = -0.2f;
    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == 10);
    assert(kinectFusion._voxelCenter.y == -20);
    assert(kinectFusion._voxelCenter.z == 0);

    std::cout << "Ending KinectFusionCudaMovingVolume Tests" << std::endl;
}

void _TestKinectFusionCudaMovingVolumeWithRayCaster() {
    std::cout << "Starting _TestKinectFusionCudaResetTsdfVolumeSection Tests" << std::endl;
    Slam::KinectFusion kinectFusion;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    rotation.SetIdentity();
    gpu::float3 translation;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotation, translation, 1);

    gpu::int3 cellPos = gpu::device::WorldToVoxel(0, 0, 0, &kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
    std::cout << "Global Pos " << cellPos.x << " " << cellPos.y << " " << cellPos.z << std::endl;
    gpu::device::GlobalVoxelToLocalVoxel(cellPos.x, cellPos.y, cellPos.z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize, &kinectFusion._voxelCellSize, cellPos.x, cellPos.y, cellPos.z);
    std::cout << "Local  Pos " << cellPos.x << " " << cellPos.y << " " << cellPos.z << std::endl;


    cellPos = gpu::device::WorldToVoxel(0, 0, 1.5, &kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
    std::cout << "Global Pos " << cellPos.x << " " << cellPos.y << " " << cellPos.z << std::endl;
    gpu::device::GlobalVoxelToLocalVoxel(cellPos.x, cellPos.y, cellPos.z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize, &kinectFusion._voxelCellSize, cellPos.x, cellPos.y, cellPos.z);
    std::cout << "Local  Pos " << cellPos.x << " " << cellPos.y << " " << cellPos.z << std::endl;

//    return;
    int x, y, z;
    int* data = new int[kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z];
    #pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                short *ptr = (short *) (data +
//                                        gpu::device::Index3DBasic(x, y, z, &kinectFusion._voxelGridSize));
                                        gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));

                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }

//    #pragma omp parallel for private(y, z)
//    for (x = 0; x < 512; x++) {
//        for (y = 0; y < 512; y++) {
//            for (z = 300; z < 512; z++) {
//
//                short *ptr = (short *) (data +
////                                        gpu::device::Index3DBasic(x, y, z, &kinectFusion._voxelGridSize));
//                                            gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//
//                gpu::device::PackTsdf(ptr, 1, -1);
//            }
//        }
//    }

    //TODO OKAY Camera must be at pos 256, 256, 0 ? or we just brighten up the ray caster cause from [256]^3 must be very dark
#pragma omp parallel for private(y, z)
    for (x = 0; x < 512; x++) {
        for (y = 0; y < 512; y++) {
            for (z = 20; z < 250; z++) {

                short *ptr = (short *) (data +
                                            gpu::device::Index3DBasic(x, y, z, &kinectFusion._voxelGridSize));
//                                        gpu::device::Index3DBasic(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));

                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
//    kinectFusion.CudaDownloadTsdf();
//    CudaTsdfVisualizer(kinectFusion._voxelCenter, kinectFusion._voxelGridSize, kinectFusion._gpuVolumePtr);
    std::cout << "Rendering" << std::endl;
    kinectFusion.CudaRayCaster();
    kinectFusion.CudaDownloadRayCaster();
    kinectFusion.CudaSaveRayCaster(10000);
    KinectFusionViewer viewer(4, 640, 480);
    viewer.MapCuda();
    //Write to Cuda
    gpu::host::KinectFusionViewerMemCopy::CopyToViewers(viewer.GetGPUPointers());
    viewer.UnMapCuda();
    viewer.CreateTextureFromCuda();
    while(viewer.CloseWindow()) {
//         View
        viewer.Render();
    }




}

void _TestKinectFusionCudaMovingCenterWithRayCaster() {
    std::cout << "Starting _TestKinectFusionCudaResetTsdfVolumeSection Tests" << std::endl;
    Slam::KinectFusion kinectFusion;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    rotation.SetIdentity();
    gpu::float3 translation;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotation, translation, 1);

//    return;
    int x, y, z;
    int* data = new int[kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z];
#pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                short *ptr = (short *) (data +
                                        gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));

                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }

    gpu::int3 voxelPos = gpu::device::WorldToVoxel(kinectFusion._translation, &kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
    short *ptr = (short *) (data +
                            gpu::device::Index3D(voxelPos.x, voxelPos.y, voxelPos.z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));

    gpu::device::PackTsdf(ptr, 1, -1);
    std::cout << "Packed -1 in voxel: " << voxelPos.x << " " << voxelPos.y << " " << voxelPos.z << " Indexes to " << gpu::device::Index3D(voxelPos.x, voxelPos.y, voxelPos.z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize) << std::endl;
    float w, tsdf;

    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 10;
    voxelPos = gpu::device::WorldToVoxel(kinectFusion._translation, &kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
    voxelPos = voxelPos - kinectFusion._voxelCenter;
    ptr = (short *) (data +
                            gpu::device::Index3D(voxelPos.x, voxelPos.y, voxelPos.z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));

//    gpu::device::PackTsdf(ptr, 1, -1);
    std::cout << "Packed -1 in voxel: " << voxelPos.x << " " << voxelPos.y << " " << voxelPos.z << " Indexes to " << gpu::device::Index3D(voxelPos.x, voxelPos.y, voxelPos.z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize) << std::endl;

}


/**
 * Full tests for resetting the volume section
 */
void _TestKinectFusionCudaResetTsdfVolumeSection() {
    std::cout << "Starting _TestKinectFusionCudaResetTsdfVolumeSection Tests" << std::endl;
    Slam::KinectFusion kinectFusion;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    gpu::float3 translation;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotation, translation, 1);

    int x, y, z;
    int* data = new int[kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z];
    #pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                short *ptr = (short *) (data +
                                        gpu::device::Index3DBasic(x, y, z, &kinectFusion._voxelGridSize));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }
    gpu::int3 start(0, 0, 0), end(0, 0, 0);
//    CudaTsdfVisualizer(kinectFusion._voxelCenter, kinectFusion._voxelGridSize, kinectFusion._gpuVolumePtr, "before");
    //Starting tests

    //Test 1 x, 0 -> 10
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
    start.x = 0; start.y = 0; start.z = 0;
    end.x = 10; end.y = 0; end.z = 0;
    kinectFusion.CudaResetTsdfSection(start, end);
    //assertion
    kinectFusion.CudaDownloadTsdf();
    start = start + kinectFusion._voxelGridSize/2;
    end = end + kinectFusion._voxelGridSize/2;
    #pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                float w, tsdf;
                int index = gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize);
                short* ptr = (short*) (kinectFusion._gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                if(start.x <= x && x < end.x) {
                    assert(tsdf == 0.f);
                } else {
                    assert(tsdf == -1.f);
                }
            }
        }
    }
    //Test 1 y, 0 -> 10
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
    start.x = 0; start.y = 0; start.z = 0;
    end.x = 0; end.y = 10; end.z = 0;
    kinectFusion.CudaResetTsdfSection(start, end);
    start = start + kinectFusion._voxelGridSize/2;
    end = end + kinectFusion._voxelGridSize/2;
    //assertion
    kinectFusion.CudaDownloadTsdf();
#pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                float w, tsdf;
                int index = gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize);
                short* ptr = (short*) (kinectFusion._gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                if(start.y <= y && y < end.y) {
                    if(tsdf != 0.f) {
                        std::cout << "A " << x << " " << y << " " << z << std::endl;
                    }
                    assert(tsdf == 0.f);
                } else {
                    if(tsdf != -1.f) {
                        std::cout << "B " << x << " " << y << " " << z << std::endl;
                    }
                    assert(tsdf == -1.f);
                }
            }
        }
    }
    //Test 1 z, 0 -> 10
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
    start.x = 0; start.y = 0; start.z = 0;
    end.x = 0; end.y = 0; end.z = 10;
    kinectFusion.CudaResetTsdfSection(start, end);
    start = start + kinectFusion._voxelGridSize/2;
    end = end + kinectFusion._voxelGridSize/2;
    //assertion
    kinectFusion.CudaDownloadTsdf();
#pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                float w, tsdf;
                int index = gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize);
                short* ptr = (short*) (kinectFusion._gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                if(start.z <= z && z < end.z) {
                    assert(tsdf == 0.f);
                } else {
                    assert(tsdf == -1.f);
                }
            }
        }
    }


    //Test 1 x and y, 0 -> 10
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
    start.x = 0; start.y = 0; start.z = 0;
    end.x = 10; end.y = 10; end.z = 0;
    kinectFusion.CudaResetTsdfSection(start, end);
    start = start + kinectFusion._voxelGridSize/2;
    end = end + kinectFusion._voxelGridSize/2;
    //assertion
    kinectFusion.CudaDownloadTsdf();
#pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                float w, tsdf;
                int index = gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize);
                short* ptr = (short*) (kinectFusion._gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                if((start.x <= x && x < end.x) || (start.y <= y && y < end.y)) {
                    assert(tsdf == 0.f);
                } else {
                    assert(tsdf == -1.f);
                }
            }
        }
    }

    //Test 1 x and z, 0 -> 10
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
    start.x = 0; start.y = 0; start.z = 0;
    end.x = 10; end.y = 0; end.z = 10;
    kinectFusion.CudaResetTsdfSection(start, end);
    start = start + kinectFusion._voxelGridSize/2;
    end = end + kinectFusion._voxelGridSize/2;
    //assertion
    kinectFusion.CudaDownloadTsdf();
#pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                float w, tsdf;
                int index = gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize);
                short* ptr = (short*) (kinectFusion._gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                if((start.x <= x && x < end.x) || (start.z <= z && z < end.z)) {
                    assert(tsdf == 0.f);
                } else {
                    assert(tsdf == -1.f);
                }
            }
        }
    }

    //Test 1 y and z, 0 -> 10
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
    start.x = 0; start.y = 0; start.z = 0;
    end.x = 0; end.y = 10; end.z = 10;
    kinectFusion.CudaResetTsdfSection(start, end);
    start = start + kinectFusion._voxelGridSize/2;
    end = end + kinectFusion._voxelGridSize/2;
    //assertion
    kinectFusion.CudaDownloadTsdf();
#pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                float w, tsdf;
                int index = gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize);
                short* ptr = (short*) (kinectFusion._gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                if((start.y <= y && y < end.y) || (start.z <= z && z < end.z)) {
                    assert(tsdf == 0.f);
                } else {
                    assert(tsdf == -1.f);
                }
            }
        }
    }

    //Test 1 x, y and z, 0 -> 10
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
    start.x = 0; start.y = 0; start.z = 0;
    end.x = 10; end.y = 10; end.z = 10;
    kinectFusion.CudaResetTsdfSection(start, end);
    start = start + kinectFusion._voxelGridSize/2;
    end = end + kinectFusion._voxelGridSize/2;
    //assertion
    kinectFusion.CudaDownloadTsdf();
    #pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                float w, tsdf;
                int index = gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize);
                short* ptr = (short*) (kinectFusion._gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                if((start.x <= x && x < end.x) || (start.y <= y && y < end.y) || (start.z <= z && z < end.z)) {
                    assert(tsdf == 0.f);
                } else {
                    assert(tsdf == -1.f);
                }
            }
        }
    }

    // tests from var to 512

    //Test 1 x, 450 -> 512
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
    start.x = 450; start.y = 0; start.z = 0;
    end.x = 512; end.y = 0; end.z = 0;
    kinectFusion.CudaResetTsdfSection(start, end);
    start = start + kinectFusion._voxelGridSize/2;
    end = end + kinectFusion._voxelGridSize/2;
    //assertion
    kinectFusion.CudaDownloadTsdf();
#pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                float w, tsdf;
                int index = gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize);
                short* ptr = (short*) (kinectFusion._gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                if((start.x % 512 <= x && x < end.x%512)) {
                    assert(tsdf == 0.f);
                } else {
                    assert(tsdf == -1.f);
                }
            }
        }
    }

    //Test 1 y, 450 -> 512
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
    start.x = 0; start.y = 450; start.z = 0;
    end.x = 0; end.y = 512; end.z = 0;
    kinectFusion.CudaResetTsdfSection(start, end);
    start = start + kinectFusion._voxelGridSize/2;
    end = end + kinectFusion._voxelGridSize/2;
    //assertion
    kinectFusion.CudaDownloadTsdf();
#pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                float w, tsdf;
                int index = gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize);
                short* ptr = (short*) (kinectFusion._gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                if((start.y % 512 <= y && y < end.y % 512 )) {
                    assert(tsdf == 0.f);
                } else {
                    assert(tsdf == -1.f);
                }
            }
        }
    }

    //Test 1 z, 450 -> 512
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
    start.x = 0; start.y = 0; start.z = 450;
    end.x = 0; end.y = 0; end.z = 512;
    kinectFusion.CudaResetTsdfSection(start, end);
    start = start + kinectFusion._voxelGridSize/2;
    end = end + kinectFusion._voxelGridSize/2;
    //assertion
    kinectFusion.CudaDownloadTsdf();
#pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                float w, tsdf;
                int index = gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize);
                short* ptr = (short*) (kinectFusion._gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                if((start.z % 512 <= z && z < end.z % 512)) {
                    assert(tsdf == 0.f);
                } else {
                    assert(tsdf == -1.f);
                }
            }
        }
    }

    //Test 1 x and y, 450 -> 512
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
    start.x = 450; start.y = 450; start.z = 0;
    end.x = 512; end.y = 512; end.z = 0;
    kinectFusion.CudaResetTsdfSection(start, end);
    start = start + kinectFusion._voxelGridSize/2;
    end = end + kinectFusion._voxelGridSize/2;
    //assertion
    kinectFusion.CudaDownloadTsdf();
#pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                float w, tsdf;
                int index = gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize);
                short* ptr = (short*) (kinectFusion._gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                if((start.x % 512 <= x && x < end.x % 512) || (start.y % 512 <= y && y < end.y % 512)) {
                    assert(tsdf == 0.f);
                } else {
                    assert(tsdf == -1.f);
                }
            }
        }
    }

    //Test 1 x and z, 450 -> 512
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
    start.x = 450; start.y = 0; start.z = 450;
    end.x = 512; end.y = 0; end.z = 512;
    kinectFusion.CudaResetTsdfSection(start, end);
    start = start + kinectFusion._voxelGridSize/2;
    end = end + kinectFusion._voxelGridSize/2;
    //assertion
    kinectFusion.CudaDownloadTsdf();
#pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                float w, tsdf;
                int index = gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize);
                short* ptr = (short*) (kinectFusion._gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                if((start.x % 512 <= x && x < end.x % 512) || (start.z % 512 <= z && z < end.z % 512)) {
                    assert(tsdf == 0.f);
                } else {
                    assert(tsdf == -1.f);
                }
            }
        }
    }

    //Test 1 y and z, 450 -> 512
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
    start.x = 0; start.y = 450; start.z = 450;
    end.x = 0; end.y = 512; end.z = 512;
    kinectFusion.CudaResetTsdfSection(start, end);
    start = start + kinectFusion._voxelGridSize/2;
    end = end + kinectFusion._voxelGridSize/2;
    //assertion
    kinectFusion.CudaDownloadTsdf();
#pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                float w, tsdf;
                int index = gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize);
                short* ptr = (short*) (kinectFusion._gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                if((start.y % 512 <= y && y < end.y % 512) || (start.z % 512 <= z && z < end.z % 512)) {
                    assert(tsdf == 0.f);
                } else {
                    assert(tsdf == -1.f);
                }
            }
        }
    }

    //Test 1 x, y and z, 0 -> 10
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z * sizeof(int),
               cudaMemcpyHostToDevice);
    start.x = 450; start.y = 450; start.z = 450;
    end.x = 512; end.y = 512; end.z = 512;
    kinectFusion.CudaResetTsdfSection(start, end);
    start = start + kinectFusion._voxelGridSize/2;
    end = end + kinectFusion._voxelGridSize/2;
    //assertion
    kinectFusion.CudaDownloadTsdf();
#pragma omp parallel for private(y, z)
    for (x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for (y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for (z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                float w, tsdf;
                int index = gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize);
                short* ptr = (short*) (kinectFusion._gpuVolumePtr + index);
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                if((start.x % 512 <= x && x < end.x % 512) || (start.y % 512 <= y && y < end.y % 512) || (start.z % 512 <= z && z < end.z % 512)) {
                    assert(tsdf == 0.f);
                } else {
                    assert(tsdf == -1.f);
                }
            }
        }
    }
    free(data);
    exit(0);
    std::cout << "Ending _TestKinectFusionCudaResetTsdfVolumeSection Tests" << std::endl;
}
void TestTsdf(gpu::int3 start, gpu::int3 end, int* gpuVolumePtr, float testValue, gpu::int3 voxelCenter, gpu::int3 voxelGridSize) {
    int x, y, z;
    #pragma omp parallel for private(y, z)
    for(x = start.x; x < end.x; x++) {
        for(y = start.y; y < end.y; y++) {
            for(z = start.z; z < end.z; z++) {
                float w, tsdf;
                short* ptr = (short*) (gpuVolumePtr + gpu::device::Index3D(x, y, z, &voxelCenter, &voxelGridSize));
                gpu::device::UnpackTsdf(ptr, w, tsdf);
                assert(tsdf == testValue);
            }
        }
    }
}
void _TestKinectFusionCudaMovingVolumeResetTest() {
    std::cout << "Starting _TestKinectFusionCudaMovingVolumeResetTest Tests" << std::endl;
    Slam::KinectFusion kinectFusion;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    gpu::float3 translation;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotation, translation, 1);
    int x, y, z;
    int size = kinectFusion._voxelGridSize.x * kinectFusion._voxelGridSize.y * kinectFusion._voxelGridSize.z;
    int* data = new int[size];
    #pragma omp parallel for private(y, z)
    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for(y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                short* ptr = (short*) (data + gpu::device::Index3DBasic(x, y, z, &kinectFusion._voxelGridSize));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
//    //Basic test No movement
    bool check = kinectFusion.CudaCheckVolumeBounds();
    assert(!check);
    //Movement without trigger
    kinectFusion._translation.z = 0.06f;
    check = kinectFusion.CudaCheckVolumeBounds();
    assert(!check);
    assert(kinectFusion._voxelCenter.x == 0);
    assert(kinectFusion._voxelCenter.y == 0);
    assert(kinectFusion._voxelCenter.z == 0);
    //Movement triggered in 1 dim Z
    kinectFusion._translation.z = 0.1f;
    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == 0);
    assert(kinectFusion._voxelCenter.y == 0);
    assert(kinectFusion._voxelCenter.z == 10);
//    cudaMemcpy(kinectFusion._gpuVolumePtr, gpu::host::GetGPUTsdfVolume(), 512 * 512 * 512 * sizeof(int), cudaMemcpyDeviceToHost);
    kinectFusion.CudaDownloadTsdf();
//    CudaTsdfVisualizer(kinectFusion._voxelCenter, kinectFusion._voxelGridSize, kinectFusion._gpuVolumePtr, "after");
    //check

    gpu::int3 testStart, testEnd;
    testStart.x = 0; testStart.y = 0; testStart.z = kinectFusion._voxelGridSize.z - 10;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z - 10;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
//    #pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y; y++) {
//            for(z = kinectFusion._voxelGridSize.z - 10; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//    #pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z - 10; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == -1.f);
//            }
//        }
//    }
    // Reset
    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 0;
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &kinectFusion._voxelCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
    // Y dim only
    kinectFusion._translation.x = 0;
    kinectFusion._translation.y = 0.1f;
    kinectFusion._translation.z = 0;

    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == 0);
    assert(kinectFusion._voxelCenter.y == 10);
    assert(kinectFusion._voxelCenter.z == 0);
    kinectFusion.CudaDownloadTsdf();
    //check

    testStart.x = 0; testStart.y = kinectFusion._voxelGridSize.z - 10; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y - 10; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = kinectFusion._voxelGridSize.y - 10; y < kinectFusion._voxelGridSize.y ; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y - 10; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                if(tsdf != -1.f) {
//                    std::cout << tsdf << " " << x << " " << y << " " << z << std::endl;
//                }
//                assert(tsdf == -1.f);
//            }
//        }
//    }

    // Reset
    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 0;
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &kinectFusion._voxelCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
    // X dim only
    kinectFusion._translation.x = 0.1f;
    kinectFusion._translation.y = 0;
    kinectFusion._translation.z = 0;

    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == 10);
    assert(kinectFusion._voxelCenter.y == 0);
    assert(kinectFusion._voxelCenter.z == 0);
    kinectFusion.CudaDownloadTsdf();
    //check
    testStart.x = kinectFusion._voxelGridSize.x - 10; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);


    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x - 10; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
//#pragma omp parallel for private(y, z)
//    for(x = kinectFusion._voxelGridSize.x - 10; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y ; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x - 10; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == -1.f);
//            }
//        }
//    }

    // Reset
    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 0;
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &kinectFusion._voxelCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
    // X and Y dim only
    kinectFusion._translation.x = 0.1f;
    kinectFusion._translation.y = 0.1f;
    kinectFusion._translation.z = 0;

    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == 10);
    assert(kinectFusion._voxelCenter.y == 10);
    assert(kinectFusion._voxelCenter.z == 0);
    kinectFusion.CudaDownloadTsdf();
    //check
    //Test X dim
    testStart.x = kinectFusion._voxelGridSize.x - 10; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
    //Test Y dim
    testStart.x = 0; testStart.y = kinectFusion._voxelGridSize.y - 10; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x - 10; testEnd.y = kinectFusion._voxelGridSize.y - 10; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

//    #pragma omp parallel for private(y, z)
//    for(x = kinectFusion._voxelGridSize.x - 10; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y ; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//    #pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = kinectFusion._voxelGridSize.y - 10; y < kinectFusion._voxelGridSize.y ; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x - 10; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y - 10; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == -1.f);
//            }
//        }
//    }

    // Reset
    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 0;
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &kinectFusion._voxelCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
    // X and Z dim only
    kinectFusion._translation.x = 0.1f;
    kinectFusion._translation.y = 0;
    kinectFusion._translation.z = 0.1f;

    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == 10);
    assert(kinectFusion._voxelCenter.y == 0);
    assert(kinectFusion._voxelCenter.z == 10);
    kinectFusion.CudaDownloadTsdf();
    //check
    //Test X dim
    testStart.x = kinectFusion._voxelGridSize.x - 10; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
    //Test Z dim
    testStart.x = 0; testStart.y = 0; testStart.z = kinectFusion._voxelGridSize.y - 10;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x - 10; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z - 10;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
//#pragma omp parallel for private(y, z)
//    for(x = kinectFusion._voxelGridSize.x - 10; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y ; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y ; y++) {
//            for(z = kinectFusion._voxelGridSize.z - 10; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x - 10; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z- 10; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == -1.f);
//            }
//        }
//    }

    // Reset
    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 0;
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &kinectFusion._voxelCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
    // Y and Z dim only
    kinectFusion._translation.x = 0;
    kinectFusion._translation.y = 0.1f;
    kinectFusion._translation.z = 0.1f;

    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == 0);
    assert(kinectFusion._voxelCenter.y == 10);
    assert(kinectFusion._voxelCenter.z == 10);
    kinectFusion.CudaDownloadTsdf();
    //check
    //Test Y dim
    testStart.x = 0; testStart.y = kinectFusion._voxelGridSize.y - 10; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
    //Test Z dim
    testStart.x = 0; testStart.y = 0; testStart.z = kinectFusion._voxelGridSize.y - 10;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y - 10; testEnd.z = kinectFusion._voxelGridSize.z - 10;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = kinectFusion._voxelGridSize.y - 10; y < kinectFusion._voxelGridSize.y ; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y ; y++) {
//            for(z = kinectFusion._voxelGridSize.z - 10; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y - 10; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z- 10; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == -1.f);
//            }
//        }
//    }
    // Reset
    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 0;
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &kinectFusion._voxelCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
    // X, Y and Z dim only
    kinectFusion._translation.x = 0.1f;
    kinectFusion._translation.y = 0.1f;
    kinectFusion._translation.z = 0.1f;

    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == 10);
    assert(kinectFusion._voxelCenter.y == 10);
    assert(kinectFusion._voxelCenter.z == 10);
    kinectFusion.CudaDownloadTsdf();
    //check
    //Test X dim
    testStart.x = kinectFusion._voxelGridSize.x - 10; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
    //Test Y dim
    testStart.x = 0; testStart.y = kinectFusion._voxelGridSize.x - 10; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
    //Test Z dim
    testStart.x = 0; testStart.y = 0; testStart.z = kinectFusion._voxelGridSize.y - 10;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x - 10; testEnd.y = kinectFusion._voxelGridSize.y - 10; testEnd.z = kinectFusion._voxelGridSize.z - 10;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

//#pragma omp parallel for private(y, z)
//    for(x = kinectFusion._voxelGridSize.y - 10; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y ; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = kinectFusion._voxelGridSize.y - 10; y < kinectFusion._voxelGridSize.y ; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y ; y++) {
//            for(z = kinectFusion._voxelGridSize.z - 10; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x - 10; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y - 10; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z- 10; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == -1.f);
//            }
//        }
//    }
    // negative dir
    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 0;
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &kinectFusion._voxelCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
    // Z dim only
    kinectFusion._translation.x = 0;
    kinectFusion._translation.y = 0;
    kinectFusion._translation.z = -0.1f;

    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == 0);
    assert(kinectFusion._voxelCenter.y == 0);
    assert(kinectFusion._voxelCenter.z == -10);
    kinectFusion.CudaDownloadTsdf();
    //check
    //Test Z dim
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = 10;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

    testStart.x = 0; testStart.y = 0; testStart.z = 10;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y ; y++) {
//            for(z = 0; z < 10; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                if(tsdf != 0.f) {
//                    std::cout << x << " " << y << " " << z << std::endl;
//                }
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y; y++) {
//            for(z = 10; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == -1.f);
//            }
//        }
//    }

    // Reset
    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 0;
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &kinectFusion._voxelCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
    // Y dim only
    kinectFusion._translation.x = 0;
    kinectFusion._translation.y = -0.1f;
    kinectFusion._translation.z = 0;

    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == 0);
    assert(kinectFusion._voxelCenter.y == -10);
    assert(kinectFusion._voxelCenter.z == 0);
    kinectFusion.CudaDownloadTsdf();
    //check
    //Test Y dim
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = 10; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

    testStart.x = 0; testStart.y = 10; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < 10 ; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 10; y < kinectFusion._voxelGridSize.y; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                if(tsdf != -1.f) {
//                    std::cout << tsdf << " " << x << " " << y << " " << z << std::endl;
//                }
//                assert(tsdf == -1.f);
//            }
//        }
//    }

    // Reset
    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 0;
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &kinectFusion._voxelCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
    // X dim only
    kinectFusion._translation.x = -0.1f;
    kinectFusion._translation.y = 0;
    kinectFusion._translation.z = 0;

    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == -10);
    assert(kinectFusion._voxelCenter.y == 0);
    assert(kinectFusion._voxelCenter.z == 0);
    kinectFusion.CudaDownloadTsdf();
    //check
    //Test X dim
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = 10; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

    testStart.x = 10; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < 10; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y ; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 10; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == -1.f);
//            }
//        }
//    }

    // Reset
    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 0;
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &kinectFusion._voxelCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
    // X and Y dim only
    kinectFusion._translation.x = -0.1f;
    kinectFusion._translation.y = -0.1f;
    kinectFusion._translation.z = 0;

    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == -10);
    assert(kinectFusion._voxelCenter.y == -10);
    assert(kinectFusion._voxelCenter.z == 0);
    kinectFusion.CudaDownloadTsdf();
    //check
    //Test X dim
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = 10; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
    //Test Y dim
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = 10; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

    testStart.x = 10; testStart.y = 10; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < 10; x++) {
//        for(y = 0; y < 10; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 10; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 10; y < kinectFusion._voxelGridSize.y; y++) {
//            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == -1.f);
//            }
//        }
//    }

    // Reset
    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 0;
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &kinectFusion._voxelCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
    // X and Z dim only
    kinectFusion._translation.x = -0.1f;
    kinectFusion._translation.y = 0;
    kinectFusion._translation.z = -0.1f;

    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == -10);
    assert(kinectFusion._voxelCenter.y == 0);
    assert(kinectFusion._voxelCenter.z == -10);
    kinectFusion.CudaDownloadTsdf();
    //check
    //Test X dim
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = 10; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
    //Test Z dim
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = 10;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

    testStart.x = 10; testStart.y = 0; testStart.z = 10;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < 10; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y ; y++) {
//            for(z = 0; z < 10; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 10; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < kinectFusion._voxelGridSize.y; y++) {
//            for(z = 10; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == -1.f);
//            }
//        }
//    }

    // Reset
    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 0;
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &kinectFusion._voxelCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
    // Y and Z dim only
    kinectFusion._translation.x = 0;
    kinectFusion._translation.y = -0.1f;
    kinectFusion._translation.z = -0.1f;

    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == 0);
    assert(kinectFusion._voxelCenter.y == -10);
    assert(kinectFusion._voxelCenter.z == -10);
    kinectFusion.CudaDownloadTsdf();
    //check
    //Test Y dim
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = 10; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
    //Test Z dim
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = 10;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

    testStart.x = 0; testStart.y = 10; testStart.z = 10;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 0; y < 10; y++) {
//            for(z = 0; z < 10; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 10; y < kinectFusion._voxelGridSize.y; y++) {
//            for(z = 10; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == -1.f);
//            }
//        }
//    }

    // Reset
    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 0;
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &kinectFusion._voxelCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
    // X, Y and Z dim only
    kinectFusion._translation.x = -0.1f;
    kinectFusion._translation.y = -0.1f;
    kinectFusion._translation.z = -0.1f;

    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == -10);
    assert(kinectFusion._voxelCenter.y == -10);
    assert(kinectFusion._voxelCenter.z == -10);
    kinectFusion.CudaDownloadTsdf();
    //check
    //Test X dim
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = 10; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
    //Test Y dim
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = 10; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
    //Test Z dim
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = 10;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

    testStart.x = 10; testStart.y = 10; testStart.z = 10;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
//#pragma omp parallel for private(y, z)
//    for(x = 0; x < 10; x++) {
//        for(y = 0; y < 10; y++) {
//            for(z = 0; z < 10; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == 0.f);
//            }
//        }
//    }
//#pragma omp parallel for private(y, z)
//    for(x = 10; x < kinectFusion._voxelGridSize.x; x++) {
//        for(y = 10; y < kinectFusion._voxelGridSize.y; y++) {
//            for(z = 10; z < kinectFusion._voxelGridSize.z; z++) {
//                float w, tsdf;
//                short* ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3D(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize));
//                gpu::device::UnpackTsdf(ptr, w, tsdf);
//                assert(tsdf == -1.f);
//            }
//        }
//    }

    //Mixed Resets
    // Reset
    kinectFusion._voxelCenter.x = 0;
    kinectFusion._voxelCenter.y = 0;
    kinectFusion._voxelCenter.z = 0;
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), data, size * sizeof(int), cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &kinectFusion._voxelCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
    // X, Y and Z dim only
    kinectFusion._translation.x = -0.1f;
    kinectFusion._translation.y = 0.1f;
    kinectFusion._translation.z = -0.1f;

    check = kinectFusion.CudaCheckVolumeBounds();
    assert(check);
    assert(kinectFusion._voxelCenter.x == -10);
    assert(kinectFusion._voxelCenter.y == 10);
    assert(kinectFusion._voxelCenter.z == -10);
    kinectFusion.CudaDownloadTsdf();
    //check
    //Test X dim
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = 10; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
    //Test Y dim
    testStart.x = 0; testStart.y = kinectFusion._voxelGridSize.y - 10; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);
    //Test Z dim
    testStart.x = 0; testStart.y = 0; testStart.z = 0;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y; testEnd.z = 10;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, 0.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);

    testStart.x = 10; testStart.y = 0; testStart.z = 10;
    testEnd.x = kinectFusion._voxelGridSize.x; testEnd.y = kinectFusion._voxelGridSize.y - 10; testEnd.z = kinectFusion._voxelGridSize.z;
    TestTsdf(testStart, testEnd, kinectFusion._gpuVolumePtr, -1.f, kinectFusion._voxelCenter, kinectFusion._voxelGridSize);




    CudaTsdfVisualizer(kinectFusion._voxelCenter, kinectFusion._voxelGridSize, kinectFusion._gpuVolumePtr, "after");




    std::cout << "Ending KinectFusionCudaMovingVolume Tests" << std::endl;
}


void viewPointCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr ptr) {
    pcl::visualization::PCLVisualizer viewer;
    viewer.setBackgroundColor (0, 0, 0);
    viewer.addPointCloud<pcl::PointXYZRGBNormal> (ptr, "sample cloud");
    viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample cloud");
    viewer.addCoordinateSystem (1.0);
    viewer.initCameraParameters ();
    std::cout << "Stopped ? " << viewer.wasStopped() << std::endl;
    while (!viewer.wasStopped ()) {
        viewer.spinOnce (100);
        boost::this_thread::sleep (boost::posix_time::microseconds (100000));
    }
}

void _TestKinectFusionCudaMovingVolumeRayCastSectionMultipleAxisMultipleSquaresGeneratePointCloud() {
    std::cout << "Starting _TestKinectFusionCudaMovingVolumeRayCastSectionMultipleAxisMultipleSquaresGeneratePointCloud Tests" << std::endl;
    Slam::KinectFusion kinectFusion;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    gpu::float3 translation;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotation, translation, 1);
    gpu::int3 start(0, 0, 0);
    gpu::int3 end(10, 10, 10);
    int x, y, z;
    short* ptr;
    std::cout << "Configuring Synthetic Volume Data" << std::endl;

    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for(y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3DBasic(x, y, z, &kinectFusion._voxelGridSize));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }
    // 1 square
    for(x = 2; x < 6; x++) {
        for(y = 2; y < 6; y++) {
            for(z = 2; z < 6; z++) {
                ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3DBasic(x, y, z, &kinectFusion._voxelGridSize));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    for(x = 2; x < 6; x++) {
        for(y = 2; y < 6; y++) {
            for(z = 248; z < 252; z++) {
                ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3DBasic(x, y, z, &kinectFusion._voxelGridSize));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    for(x = 2; x < 6; x++) {
        for(y = 248; y < 252; y++) {
            for(z = 2; z < 6; z++) {
                ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3DBasic(x, y, z, &kinectFusion._voxelGridSize));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    for(x = 248; x < 252; x++) {
        for(y = 2; y < 6; y++) {
            for(z = 2; z < 6; z++) {
                ptr = (short*) (kinectFusion._gpuVolumePtr + gpu::device::Index3DBasic(x, y, z, &kinectFusion._voxelGridSize));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }

    std::cout << "Ending _TestKinectFusionCudaMovingVolumeRayCastSectionMultipleAxisMultipleSquaresGeneratePointCloud Tests" << std::endl;
}

void _TestKinectFusionCPUSectionRayCastingExperiment() {
    gpu::int3 start(500, 500, 500);
    gpu::int3 end(511, 511, 511);
    gpu::int3 volumeGrid(512, 512, 512);
    gpu::int3 volumeCenter(0, 0, 0);
    gpu::float3 voxelCellSize(0.01, 0.01, 0.01);
    //main GPU volume;
    int size = 512 * 512 * 512;
    int* volumeStart = new int[size];

    //Raycaster results;
    gpu::int3 otherGridSize(512, 512, 64);
    size = 512 * 512 * 64;
    int* raycastedPointer = new int[size];

//    memset(raycastedPointer, 0, size * sizeof(int));
    int x, y, z;


    short* ptr;
    std::cout << "Configuring Synthetic data" << std::endl;
    #pragma omp parallel for private(y, z, ptr)
    for(x = 0; x < volumeGrid.x; x++) {
        for(y = 0; y < volumeGrid.y; y++) {
            for(z = 0; z < volumeGrid.z; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    // 1 square
    for(x = 2; x < 6; x++) {
        for(y = 2; y < 6; y++) {
            for(z = 2; z < 6; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    for(x = 2; x < 6; x++) {
        for(y = 2; y < 6; y++) {
            for(z = 248; z < 252; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    for(x = 2; x < 6; x++) {
        for(y = 248; y < 252; y++) {
            for(z = 2; z < 6; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    for(x = 248; x < 252; x++) {
        for(y = 2; y < 6; y++) {
            for(z = 2; z < 6; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    //should only show this square
    for(x = 501; x < 506; x++) {
        for(y = 501; y < 506; y++) {
            for(z = 501; z < 506; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    std::cout << "Synthetic data Configured" << std::endl;

    std::cout << "RayCasting" << std::endl;
    int counter = 0;
    #pragma omp parallel for private(y, z)
    for(x = start.x; x < end.x - 1; x++) {
        for(y = 0; y < volumeGrid.y - 1; y++) {
            //New rayCaster
            //for CUDA
            int nextIndex = 0;
            // now do ray cast
            for(z = 0; z < volumeGrid.z - 1; z++) {
                float myTsdf, x1, y1, z1, w;
                gpu::device::UnpackTsdf(gpu::device::GetTsdfPointer(volumeStart, x, y, z, &volumeCenter, &volumeGrid), w, myTsdf);
                gpu::device::UnpackTsdf(gpu::device::GetTsdfPointer(volumeStart, x + 1, y, z, &volumeCenter, &volumeGrid), w, x1);
                gpu::device::UnpackTsdf(gpu::device::GetTsdfPointer(volumeStart, x, y + 1, z, &volumeCenter, &volumeGrid), w, y1);
                gpu::device::UnpackTsdf(gpu::device::GetTsdfPointer(volumeStart, x, y, z + 1, &volumeCenter, &volumeGrid), w, z1);
                bool point = (myTsdf > 0 && ((x1 < 0) || y1 < 0 || z1 < 0)) || (myTsdf < 0 && (x1 > 0 || y1 > 0 || z1 > 0));
                if(point) {
                    raycastedPointer[gpu::device::Index3DBasic(x, y, nextIndex, &otherGridSize)] = z;
                    nextIndex++;
                }
            }

        }
    }
#pragma omp parallel for private(y, z)
    for(x = 0; x < volumeGrid.x - 1; x++) {
        for(y = start.y; y < end.y - 1; y++) {
            //New rayCaster
            //for CUDA
            int nextIndex = 0;
            // now do ray cast
            for(z = 0; z < volumeGrid.z - 1; z++) {
                float myTsdf, x1, y1, z1, w;
                gpu::device::UnpackTsdf(gpu::device::GetTsdfPointer(volumeStart, x, y, z, &volumeCenter, &volumeGrid), w, myTsdf);
                gpu::device::UnpackTsdf(gpu::device::GetTsdfPointer(volumeStart, x + 1, y, z, &volumeCenter, &volumeGrid), w, x1);
                gpu::device::UnpackTsdf(gpu::device::GetTsdfPointer(volumeStart, x, y + 1, z, &volumeCenter, &volumeGrid), w, y1);
                gpu::device::UnpackTsdf(gpu::device::GetTsdfPointer(volumeStart, x, y, z + 1, &volumeCenter, &volumeGrid), w, z1);
                bool point = (myTsdf > 0 && ((x1 < 0) || y1 < 0 || z1 < 0)) || (myTsdf < 0 && (x1 > 0 || y1 > 0 || z1 > 0));
                if(point) {
                    raycastedPointer[gpu::device::Index3DBasic(x, y, nextIndex, &otherGridSize)] = z;
                    nextIndex++;
                }
            }

        }
    }
#pragma omp parallel for private(y, z)
    for(x = 0; x < volumeGrid.x - 1; x++) {
        for(y = 0; y < volumeGrid.y - 1; y++) {
            //New rayCaster
            //for CUDA
            int nextIndex = 0;
            // now do ray cast
            for(z = start.z; z < end.z - 1; z++) {
                float myTsdf, x1, y1, z1, w;
                gpu::device::UnpackTsdf(gpu::device::GetTsdfPointer(volumeStart, x, y, z, &volumeCenter, &volumeGrid), w, myTsdf);
                gpu::device::UnpackTsdf(gpu::device::GetTsdfPointer(volumeStart, x + 1, y, z, &volumeCenter, &volumeGrid), w, x1);
                gpu::device::UnpackTsdf(gpu::device::GetTsdfPointer(volumeStart, x, y + 1, z, &volumeCenter, &volumeGrid), w, y1);
                gpu::device::UnpackTsdf(gpu::device::GetTsdfPointer(volumeStart, x, y, z + 1, &volumeCenter, &volumeGrid), w, z1);
                bool point = (myTsdf > 0 && ((x1 < 0) || y1 < 0 || z1 < 0)) || (myTsdf < 0 && (x1 > 0 || y1 > 0 || z1 > 0));
                if(point) {
                    raycastedPointer[gpu::device::Index3DBasic(x, y, nextIndex, &otherGridSize)] = z;
                    nextIndex++;
                }
            }

        }
    }

    std::cout << "Generating points" << std::endl;
    //Point Generation
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
    clock_t startTime = clock();
    int nextIndex;

    for(x = 0; x < otherGridSize.x; x++) {
        for(y = 0; y < otherGridSize.y; y++) {

            for(nextIndex = 0; nextIndex < otherGridSize.z; nextIndex++) {
                int index = gpu::device::Index3DBasic(x, y, nextIndex, &otherGridSize);
//                if(index >= maxIndex) {
//                    std::cout << "ERROR" << std::endl;
//                }
                z = raycastedPointer[index];
                if( z == -1) {
                    break;
                } else {
                    //Generate point at x, y, z[]
                    gpu::float3 p = gpu::device::VoxelToWorld(x, y, z, &volumeGrid, &volumeCenter, &voxelCellSize);
                    pcl::PointXYZRGBNormal pointXYZRGB;
                    pointXYZRGB.x = p.x;
                    pointXYZRGB.y = p.y;
                    pointXYZRGB.z = p.z;
                    pointXYZRGB.r = 255;
                    pointXYZRGB.g = 0;
                    pointXYZRGB.b = 0;
                    pointCloud.get()->points.push_back(pointXYZRGB);
                }
            }
        }
    }
    pointCloud.get()->width = (unsigned int) pointCloud.get()->points.size();
    pointCloud.get()->height = 1;

    viewPointCloud(pointCloud);
}
void AssertPoints(int* ptr) {

}
void _TestKinectFusionCudaSectionRayCastingStartingFromZeroAllAxes() {
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    gpu::float3 translation;
    rotation.SetIdentity();
    gpu::int3 start(256, 0, 0); // local index
    gpu::int3 end(266, 0, 0);
    gpu::int3 volumeGrid(512, 512, 512);
    gpu::int3 volumeCenter(0, 0, 0);
//    gpu::int3 volumeCenter(256, 256, 256);

    Slam::KinectFusion kinectFusion;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotation, translation, 1);
    gpu::float3 voxelCellSize = kinectFusion._voxelCellSize;


    //main GPU volume;
    int size = 512 * 512 * 512;
    int* volumeStart = new int[size];

    //Raycaster results;
    gpu::int3 otherGridSize(512, 512, 64);
    size = 512 * 512 * 64;
    int* raycastedPointer = new int[size];

//    memset(raycastedPointer, 0, size * sizeof(int));
    //ResetRaycasterVolume
    int x, y, z;

    kinectFusion.CudaResetPointIndexVolume();
    cudaMemcpy(raycastedPointer, gpu::host::GetGPUPointIndex(), sizeof(int) * 512 * 512 * 64, cudaMemcpyDeviceToHost);
    #pragma omp parallel for private(y, z)
    for(x = 0; x < otherGridSize.x; x++ ) {
        for(y = 0; y < otherGridSize.y; y++) {
            for(z = 0; z < otherGridSize.z; z++) {
                assert(raycastedPointer[gpu::device::Index3DBasic(x, y, z, &otherGridSize)] == -2147483648);
            }
        }
    }

    std::cout << "Assertion Complete" << std::endl;
    short* ptr;
    std::cout << "Configuring Synthetic data" << std::endl;
    for(x = 0; x < volumeGrid.x; x++) {
        for(y = 0; y < volumeGrid.y; y++) {
            for(z = 0; z < volumeGrid.z; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3D(x, y, z, &volumeCenter, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }

    for(x = 2; x < 6; x++) {
        for(y = 2; y < 6; y++) {
            for(z = 2; z < 6; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }

    for(x = 2; x < 6; x++) {
        for(y = 2; y < 6; y++) {
            for(z = 248; z < 252; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    for(x = 2; x < 6; x++) {
        for(y = 248; y < 252; y++) {
            for(z = 248; z < 252; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    for(x = 2; x < 6; x++) {
        for(y = 248; y < 252; y++) {
            for(z = 2; z < 6; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    for(x = 248; x < 252; x++) {
        for(y = 248; y < 252; y++) {
            for(z = 2; z < 6; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    for(x = 248; x < 252; x++) {
        for(y = 2; y < 6; y++) {
            for(z = 2; z < 6; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    for(x = 248; x < 252; x++) {
        for(y = 2; y < 6; y++) {
            for(z = 248; z < 252; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }

    for(x = 248; x < 252; x++) {
        for(y = 248; y < 252; y++) {
            for(z = 248; z < 252; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }
    std::cout << "Synthetic data Configured" << std::endl;
    //TODO copy to gpu
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), volumeStart, sizeof(int) * 512 * 512 * 512, cudaMemcpyHostToDevice);

    std::cout << "RayCasting" << std::endl;

    //Raycasting Test 1
    //Checking raycasting along yz plane x -> x + delta


    kinectFusion.CudaRayCastTsdfSection(start, end);
    kinectFusion.CudaDownloadRayCastTsdfSection();
    kinectFusion.CudaDownloadTsdf();
    kinectFusion.CudaTsdfVisualizer("kfunitTestRaycaster/test");
//    kinectFusion.CudaKinectV2Finish();
    std::cout << "Generating points" << std::endl;
    //Point Generation
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = kinectFusion.GeneratePointCloud(true);
    int i;
    std::cout << "Number of points: " << pointCloud.get()->points.size() << std::endl;
    bool* testPool = new bool[pointCloud.get()->points.size()];
    std::memset(testPool, 0, sizeof(bool) * pointCloud.get()->points.size());
    //Assert x -> x + delta
    //First block yz plane

    x = 1;
    for(y = 1; y < 6 ;y++) {
        for(z = 1; z < 6; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    x = 5;
    for(y = 1; y < 6 ;y++) {
        for(z = 1; z < 6; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    //xy plane
    z = 1;
    for(x = 2; x < 5 ;x++) {
        for(y = 1; y < 6; y++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    z = 5;
    for(x = 2; x < 5 ;x++) {
        for(y = 1; y < 6; y++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    //xz
    y = 1;
    for(x = 2; x < 5 ;x++) {
        for(z = 2; z < 5; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    y = 5;
    for(x = 2; x < 5 ;x++) {
        for(z = 2; z < 5; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }

    //2nd

    x = 1;
    for(y = 1; y < 6 ;y++) {
        for(z = 247; z < 252; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    x = 5;
    for(y = 1; y < 6 ;y++) {
        for(z = 247; z < 252; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    //xy plane
    z = 247;
    for(x = 2; x < 5 ;x++) {
        for(y = 1; y < 6; y++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    z = 251;
    for(x = 2; x < 5 ;x++) {
        for(y = 1; y < 6; y++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    //xz
    y = 1;
    for(x = 2; x < 5 ;x++) {
        for(z = 248; z < 251; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    y = 5;
    for(x = 2; x < 5 ;x++) {
        for(z = 248; z < 251; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }

    // 3rd
    x = 1;
    for(y = 247; y < 252 ;y++) {
        for(z = 1; z < 6; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    x = 5;
    for(y = 247; y < 252 ;y++) {
        for(z = 1; z < 6; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    //xy plane
    z = 1;
    for(x = 2; x < 5 ;x++) {
        for(y = 247; y < 252; y++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    z = 5;
    for(x = 2; x < 5 ;x++) {
        for(y = 247; y < 252; y++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    //xz
    y = 247;
    for(x = 2; x < 5 ;x++) {
        for(z = 2; z < 5; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            std::cout << i << " " << testPool[i] << std::endl;
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    y = 251;
    for(x = 2; x < 5 ;x++) {
        for(z = 2; z < 5; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }


    // 4th
    x = 1;
    for(y = 247; y < 252 ;y++) {
        for(z = 247; z < 252; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    x = 5;
    for(y = 247; y < 252 ;y++) {
        for(z = 247; z < 252; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    //xy plane
    z = 247;
    for(x = 2; x < 5 ;x++) {
        for(y = 247; y < 252 ;y++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    z = 251;
    for(x = 2; x < 5 ;x++) {
        for(y = 247; y < 252 ;y++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    //xz
    y = 247;
    for(x = 2; x < 5 ;x++) {
        for(z = 248; z < 251; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }
    y = 251;
    for(x = 2; x < 5 ;x++) {
        for(z = 248; z < 251; z++) {
            bool testPoint = false;
            gpu::float3 point = gpu::device::VoxelToWorld(x, y, z,&kinectFusion._voxelGridSize, &kinectFusion._voxelCenter, &kinectFusion._voxelCellSize);
            std::cout << "Fake point Generated: " << point.x << " " << point.y << " " << point.z << std::endl;
            for(i = 0; i < pointCloud.get()->points.size(); i++) {
                if(pointCloud.get()->points[i].x == point.x &&pointCloud.get()->points[i].y == point.y &&pointCloud.get()->points[i].z == point.z ) {
                    if(testPool[i]) {
                        assert(false);
                    } else {
                        testPool[i] = true;
                        break;
                    }
                }
            }
            assert(i < pointCloud.get()->points.size() && testPool[i]);
        }
    }

    for(i = 0; i < pointCloud.get()->points.size(); i++) {
        assert(testPool[i]);
    }
    delete[] testPool;
    viewPointCloud(pointCloud);

    //Raycasting Test 2
    //Checking raycasting along xz plane y -> y  delta
    start = gpu::int3(0, 256, 0); // local index
    end = gpu::int3 (0, 266, 0);




    kinectFusion.CudaResetPointIndexVolume();
    kinectFusion.CudaRayCastTsdfSection(start, end);
    kinectFusion.CudaDownloadRayCastTsdfSection();
    kinectFusion.CudaDownloadTsdf();
    kinectFusion.CudaTsdfVisualizer("kfunitTestRaycaster/test");
//    kinectFusion.CudaKinectV2Finish();
    std::cout << "Generating points" << std::endl;
    //Point Generation
    pointCloud = kinectFusion.GeneratePointCloud(true);
    //Assert x -> x + delta
    //First block yz plane

    std::cout << "Number of points: " << pointCloud.get()->points.size() << std::endl;
    testPool = new bool[pointCloud.get()->points.size()];
    std::memset(testPool, 0, sizeof(bool) * pointCloud.get()->points.size());


    viewPointCloud(pointCloud);

    //Raycasting Test 2
    //Checking raycasting along xy plane z -> z + delta
    start = gpu::int3(0, 0, 256); // local index
    end = gpu::int3 (0, 0, 266);




    kinectFusion.CudaResetPointIndexVolume();
    kinectFusion.CudaRayCastTsdfSection(start, end);
    kinectFusion.CudaDownloadRayCastTsdfSection();
    kinectFusion.CudaDownloadTsdf();
    kinectFusion.CudaTsdfVisualizer("kfunitTestRaycaster/test");
//    kinectFusion.CudaKinectV2Finish();
    std::cout << "Generating points" << std::endl;
    //Point Generation
    pointCloud = kinectFusion.GeneratePointCloud(true);
    std::cout << "Number of points: " << pointCloud.get()->points.size() << std::endl;
    testPool = new bool[pointCloud.get()->points.size()];
    std::memset(testPool, 0, sizeof(bool) * pointCloud.get()->points.size());


    viewPointCloud(pointCloud);


    exit(0);
}

void _TestKinectFusionCudaSectionRayCastingFromEndGridAllAxes() {
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    gpu::float3 translation;
    rotation.SetIdentity();
    gpu::int3 start(500, 500, 500);
    gpu::int3 end(511, 511, 511);
    gpu::int3 volumeGrid(512, 512, 512);
    gpu::int3 volumeCenter(0, 0, 0);
    gpu::float3 voxelCellSize(0.01, 0.01, 0.01);

    Slam::KinectFusion kinectFusion;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotation, translation, 1);


    //main GPU volume;
    int size = 512 * 512 * 512;
    int* volumeStart = new int[size];

    //Raycaster results;
    gpu::int3 otherGridSize(512, 512, 64);
    size = 512 * 512 * 64;
    int* raycastedPointer = new int[size];

//    memset(raycastedPointer, 0, size * sizeof(int));
    //ResetRaycasterVolume
    int x, y, z;

    kinectFusion.CudaResetPointIndexVolume();
    cudaMemcpy(raycastedPointer, gpu::host::GetGPUPointIndex(), sizeof(int) * 512 * 512 * 64, cudaMemcpyDeviceToHost);
    short* ptr;


    std::cout << "Assertion Complete" << std::endl;
    std::cout << "Configuring Synthetic data" << std::endl;
#pragma omp parallel for private(y, z, ptr)
    for(x = 0; x < volumeGrid.x; x++) {
        for(y = 0; y < volumeGrid.y; y++) {
            for(z = 0; z < volumeGrid.z; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }

#pragma omp parallel for private(y, z, ptr)
    for(x = 502; x < 510; x++) {
        for(y = 502; y < 510; y++) {
            for(z = 502; z < 510; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }


#pragma omp parallel for private(y, z, ptr)
    for(x = 0; x < 10; x++) {
        for(y = 502; y < 510; y++) {
            for(z = 502; z < 510; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }

#pragma omp parallel for private(y, z, ptr)
    for(x = 502; x < 510; x++) {
        for(y = 0; y < 10; y++) {
            for(z = 502; z < 510; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }

#pragma omp parallel for private(y, z, ptr)
    for(x = 502; x < 510; x++) {
        for(y = 502; y < 510; y++) {
            for(z = 0; z < 10; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }

    std::cout << "Synthetic data Configured" << std::endl;
    //TODO copy to gpu
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), volumeStart, sizeof(int) * 512 * 512 * 512, cudaMemcpyHostToDevice);

    std::cout << "RayCasting" << std::endl;
    //TODO Raycast
    kinectFusion.CudaRayCastTsdfSection(start, end);
    kinectFusion.CudaDownloadRayCastTsdfSection();
    std::cout << "Generating points" << std::endl;
    //Point Generation
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = kinectFusion.GeneratePointCloud();
//    viewPointCloud(pointCloud);
}

void _TestKinectFusionCudaSectionRayCastingXAxisTest() {
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    gpu::float3 translation;
    rotation.SetIdentity();
    gpu::int3 start(0, 0, 0);
    gpu::int3 end(64, 0, 0);
    gpu::int3 volumeGrid(512, 512, 512);
    gpu::int3 volumeCenter(0, 0, 0);
    gpu::float3 voxelCellSize(0.01, 0.01, 0.01);

    Slam::KinectFusion kinectFusion;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotation, translation, 1);


    //main GPU volume;
    int size = 512 * 512 * 512;
    int* volumeStart = new int[size];

    //Raycaster results;
    gpu::int3 otherGridSize(512, 512, 64);
    size = 512 * 512 * 64;
    int* raycastedPointer = new int[size];

//    memset(raycastedPointer, 0, size * sizeof(int));
    //ResetRaycasterVolume
    int x, y, z;

    kinectFusion.CudaResetPointIndexVolume();
    cudaMemcpy(raycastedPointer, gpu::host::GetGPUPointIndex(), sizeof(int) * 512 * 512 * 64, cudaMemcpyDeviceToHost);
#pragma omp parallel for private(y, z)
    for(x = 0; x < otherGridSize.x; x++ ) {
        for(y = 0; y < otherGridSize.y; y++) {
            for(z = 0; z < otherGridSize.z; z++) {
                assert(raycastedPointer[gpu::device::Index3DBasic(x, y, z, &otherGridSize)] == INT32_MIN);
            }
        }
    }

    std::cout << "Assertion Complete" << std::endl;
    short* ptr;
    std::cout << "Configuring Synthetic data" << std::endl;
    for(x = 0; x < volumeGrid.x; x++) {
        for(y = 0; y < volumeGrid.y; y++) {
            for(z = 0; z < volumeGrid.z; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }
    // 1 square z start and end
    for(x = 20; x < 40; x++) {
        for(y = 0; y < 512; y++) {
            for(z = 0; z < 512; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3DBasic(x, y, z, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
            }
        }
    }


    std::cout << "Synthetic data Configured" << std::endl;
    //TODO copy to gpu
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), volumeStart, sizeof(int) * 512 * 512 * 512, cudaMemcpyHostToDevice);

    std::cout << "RayCasting" << std::endl;
    //TODO Raycast
    int i;
    for(i = 0 ;i < 8; i++) {
        std::cout << start.x << " " << start.y << " " << start.z << std::endl;
        std::cout << end.x << " " << end.y << " " << end.z << std::endl;
        kinectFusion.CudaRayCastTsdfSection(start, end);
        kinectFusion.CudaDownloadRayCastTsdfSection(true);
        std::cout << "Generating points" << std::endl;
        //Assert
        pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointcloud = kinectFusion.GeneratePointCloud();
        kinectFusion.viewPointCloud(pointcloud);
        printf("%d %d\n", 'a', 'A');
        start.x = end.x;
        end.x += 64;
    }
    //Point Generation
//    pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloud = kinectFusion.GeneratePointCloud();
//    viewPointCloud(pointCloud);
}

void _TestKinectFusionCudaSectionRayCastingFullVolume() {
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    gpu::float3 translation;
    rotation.SetIdentity();
    gpu::int3 start(0, 0, 480);
    gpu::int3 end(0, 0, 511);
    gpu::int3 volumeGrid(512, 512, 512);
    gpu::int3 volumeCenter(-100, 0, 0);
    gpu::float3 voxelCellSize(0.01, 0.01, 0.01);

    Slam::KinectFusion kinectFusion;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotation, translation, 1);
    kinectFusion._voxelCenter = volumeCenter;
    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &volumeCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);

    //main GPU volume;
    int size = 512 * 512 * 512;
    int* volumeStart = new int[size];
    int* volumeStartColour = new int[size];

    //Raycaster results;
    gpu::int3 otherGridSize(512, 512, 64);
    size = 512 * 512 * 64;
    int* raycastedPointer = new int[size];

//    memset(raycastedPointer, 0, size * sizeof(int));
    //ResetRaycasterVolume
    int x, y, z;

    kinectFusion.CudaResetPointIndexVolume();
    short* ptr;
    #pragma omp parallel for private(y, z, ptr)
    for(x = 0; x < volumeGrid.x; x++) {
        for(y = 0; y < volumeGrid.y; y++) {
            for(z = 0; z < volumeGrid.z; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3D(x, y, z, &volumeCenter, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, 1);
                gpu::device::PackColourValue(volumeStartColour + gpu::device::Index3D(x, y, z, &volumeCenter, &volumeGrid), 0xFF, 0x00, 0x00);
            }
        }
    }
    gpu::int3 global;
#pragma omp parallel for private(y, z, ptr)
    for(x = 0; x < volumeGrid.x; x++) {
        for(y = 0; y < volumeGrid.y; y++) {
            for(z = 508; z < volumeGrid.z; z++) {
                global.x = x; global.y = y; global.z = z;
                global = gpu::device::LocalVoxelToGlobalVoxel(global, &volumeCenter, &volumeGrid, &voxelCellSize);
                ptr = (short*) (volumeStart + gpu::device::Index3D(global.x, global.y, global.z, &volumeGrid, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }

#pragma omp parallel for private(y, z, ptr)
    for(x = 0; x < volumeGrid.x; x++) {
        for(y = 0; y < volumeGrid.y; y++) {
            for(z = 505; z < 507; z++) {
                global.x = x; global.y = y; global.z = z;
                global = gpu::device::LocalVoxelToGlobalVoxel(global, &volumeCenter, &volumeGrid, &voxelCellSize);
                ptr = (short*) (volumeStart + gpu::device::Index3D(global.x, global.y, global.z, &volumeGrid, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }
#pragma omp parallel for private(y, z, ptr)
    for(x = 0; x < volumeGrid.x; x++) {
        for(y = 0; y < volumeGrid.y; y++) {
            for(z = 495; z < 500; z++) {
                global.x = x; global.y = y; global.z = z;
                global = gpu::device::LocalVoxelToGlobalVoxel(global, &volumeCenter, &volumeGrid, &voxelCellSize);
                ptr = (short*) (volumeStart + gpu::device::Index3D(global.x, global.y, global.z, &volumeGrid, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }

#pragma omp parallel for private(y, z, ptr)
    for(x = 0; x < volumeGrid.x; x++) {
        for(y = 0; y < volumeGrid.y; y++) {
            for(z = 490; z < 492; z++) {
                global.x = x; global.y = y; global.z = z;
                global = gpu::device::LocalVoxelToGlobalVoxel(global, &volumeCenter, &volumeGrid, &voxelCellSize);
                ptr = (short*) (volumeStart + gpu::device::Index3D(global.x, global.y, global.z, &volumeGrid, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }

#pragma omp parallel for private(y, z, ptr)
    for(x = 0; x < volumeGrid.x; x++) {
        for(y = 0; y < volumeGrid.y; y++) {
            for(z = 482; z < 486; z++) {
                global.x = x; global.y = y; global.z = z;
                global = gpu::device::LocalVoxelToGlobalVoxel(global, &volumeCenter, &volumeGrid, &voxelCellSize);
                ptr = (short*) (volumeStart + gpu::device::Index3D(global.x, global.y, global.z, &volumeGrid, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }

#pragma omp parallel for private(y, z, ptr)
    for(x = 0; x < volumeGrid.x; x++) {
        for(y = 0; y < volumeGrid.y; y++) {
            for(z = 0; z < 5; z++) {
                global.x = x; global.y = y; global.z = z;
                global = gpu::device::LocalVoxelToGlobalVoxel(global, &volumeCenter, &volumeGrid, &voxelCellSize);
                ptr = (short*) (volumeStart + gpu::device::Index3D(global.x, global.y, global.z, &volumeGrid, &volumeGrid));
                gpu::device::PackTsdf(ptr, 1, -1);
            }
        }
    }

    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), volumeStart, sizeof(int) * 512 * 512 * 512, cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUTsdfColourVolume(), volumeStartColour, sizeof(int) * 512 * 512 * 512, cudaMemcpyHostToDevice);
    std::cout << "RayCasting" << std::endl;
    //TODO Raycast
    kinectFusion.CudaRayCastTsdfSection(start, end);
    kinectFusion.CudaDownloadRayCastTsdfSection(false);


    kinectFusion.viewPointCloud(kinectFusion.GeneratePointCloud());
}

void _TestKinectFusionCudaSectionRayCastingFullVolumeHoleFilling() {
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    gpu::float3 translation;
    rotation.SetIdentity();
//    rotation = Utility::GenerateRotationMatrix33((M_PI_2) * (3./4), 0, 0);
//    gpu::int3 start(0, 0, 480);
//    gpu::int3 end(0, 0, 511);
//    gpu::int3 volumeGrid(512, 512, 512);
//    gpu::int3 volumeCenter(256, 256, 256);
//    gpu::float3 voxelCellSize(0.01, 0.01, 0.01);

    Slam::KinectFusion kinectFusion;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotation, translation, 1);

    std::vector<cv::Mat_<double>> images = GenerateSquareSpecialImage();

    cv::Scalar s(254, 0, 0);
    cv::Mat rgb = cv::Mat(480, 640, CV_8UC3, s);

    cv::Scalar area(254, 254, 254);
    int i, j;
    for (i = 200 ; i < 440;i++) {
        for(j = 150; j < 330; j++) {
            int index = (j * rgb.cols + i) * 3;
            rgb.data[index + 0] = (uchar)255;
            rgb.data[index + 1] = (uchar)255;
            rgb.data[index + 2] = (uchar)255;
        }
    }
//    for (i = 110 ; i < 160;i++) {
//        for(j = 150; j < 330; j++) {
//            int index = (j * rgb.cols + i) * 3;
//            rgb.data[index + 0] = (uchar)0;
//            rgb.data[index + 1] = (uchar)255;
//            rgb.data[index + 2] = (uchar)0;
//        }
//    }

    gpu::DepthMap depthMap(images[0].rows, images[0].cols, images[0].data);
    gpu::Image rgbImage(rgb.rows, rgb.cols, rgb.channels(), rgb.datastart);

    kinectFusion.CudaKinectV2(depthMap, rgbImage, images[0], rgb, true);


//    kinectFusion._useRayCasterAsPointCloudGenerator = true;
    kinectFusion.CudaKinectV2Finish();
    kinectFusion.viewPointCloud(kinectFusion._globalPointCloud);

//    kinectFusion.CudaDownloadTsdf();

//    kinectFusion.CudaTsdfVisualizer("TsdfVolume/SectionRaycastTest");

//    kinectFusion.CudaRayCastTsdfSection(start, end);
//    kinectFusion.CudaDownloadRayCastTsdfSection(false);


//    kinectFusion.viewPointCloud(kinectFusion.GeneratePointCloud());
//    exit(0);
}
void _TestKinectFusionCudaSectionRayCastingFullVolumeHoleFillingFromTwoAngle() {
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    gpu::float3 translation;
    rotation.SetIdentity();
//    rotation = Utility::GenerateRotationMatrix33((M_PI_2) * (3./4), 0, 0);
//    gpu::int3 start(0, 0, 480);
//    gpu::int3 end(0, 0, 511);
//    gpu::int3 volumeGrid(512, 512, 512);
//    gpu::int3 volumeCenter(256, 256, 256);
//    gpu::float3 voxelCellSize(0.01, 0.01, 0.01);

    Slam::KinectFusion kinectFusion;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 5.12, rotation, translation, 1);

    std::vector<cv::Mat_<double>> images = GenerateSquareSpecialImage();

    cv::Scalar s(254, 0, 0);
    cv::Mat rgb = cv::Mat(480, 640, CV_8UC3, s);

    cv::Scalar area(254, 254, 254);
    int i, j;
    for (i = 200 ; i < 440;i++) {
        for(j = 150; j < 330; j++) {
            int index = (j * rgb.cols + i) * 3;
            rgb.data[index + 0] = (uchar)255;
            rgb.data[index + 1] = (uchar)255;
            rgb.data[index + 2] = (uchar)255;
        }
    }

    cv::imwrite("ExperimentRGB.png", rgb);
    cv::Mat iii;
    images[0].convertTo(iii, CV_16UC1);
    iii = iii * 20;
    cv::imwrite("ExperimentDepth.png", iii);
//    for (i = 110 ; i < 160;i++) {
//        for(j = 150; j < 330; j++) {
//            int index = (j * rgb.cols + i) * 3;
//            rgb.data[index + 0] = (uchar)0;
//            rgb.data[index + 1] = (uchar)255;
//            rgb.data[index + 2] = (uchar)0;
//        }
//    }

    gpu::DepthMap depthMap(images[0].rows, images[0].cols, images[0].data);
    gpu::Image rgbImage(rgb.rows, rgb.cols, rgb.channels(), rgb.datastart);

    //Position update
    kinectFusion._translation.x = 0;
    kinectFusion._translation.y = 0;
    kinectFusion._translation.z = -1.1;
    gpu::matrix33 inv = kinectFusion._rotationMatrix.transpose();
    gpu::host::UploadTransformation(kinectFusion._rotationMatrix,inv, kinectFusion._translation,
                                    gpu::host::GetGPURotation(), gpu::host::GetGPUInverseRotation(), gpu::host::GetGPUTranslation());

    kinectFusion.CudaKinectV2(depthMap, rgbImage, images[0], rgb, true);
//    kinectFusion.CudaKinectV2(depthMap, rgbImage, images[0], rgb, true);
//    kinectFusion.CudaKinectV2(depthMap, rgbImage, images[0], rgb, true);
//    kinectFusion.CudaKinectV2(depthMap, rgbImage, images[0], rgb, true);

    kinectFusion._translation.x = 0;
    kinectFusion._translation.y = 0;
    kinectFusion._translation.z = 1.1;
    kinectFusion._rotationMatrix = kinectFusion._rotationMatrix * Utility::GenerateRotationMatrix33(M_PI, 0, 0);

    inv = kinectFusion._rotationMatrix.transpose();
    gpu::host::UploadTransformation(kinectFusion._rotationMatrix,inv, kinectFusion._translation,
                                    gpu::host::GetGPURotation(), gpu::host::GetGPUInverseRotation(), gpu::host::GetGPUTranslation());


    kinectFusion.CudaKinectV2(depthMap, rgbImage, images[0], rgb, true);
    kinectFusion.CudaKinectV2(depthMap, rgbImage, images[0], rgb, true);
    kinectFusion.CudaKinectV2(depthMap, rgbImage, images[0], rgb, true);
//    kinectFusion.CudaKinectV2(depthMap, rgbImage, images[0], rgb, true);
//    kinectFusion.CudaKinectV2(depthMap, rgbImage, images[0], rgb, true);



    kinectFusion.CudaKinectV2Finish();
    kinectFusion.viewPointCloud(kinectFusion._globalPointCloud);

//    kinectFusion.CudaDownloadTsdf();

//    kinectFusion.CudaTsdfVisualizer("TsdfVolume/SectionRaycastTest");

//    kinectFusion.CudaRayCastTsdfSection(start, end);
//    kinectFusion.CudaDownloadRayCastTsdfSection(false);


//    kinectFusion.viewPointCloud(kinectFusion.GeneratePointCloud());
//    exit(0);
}
void _TestKinectFusionCudaSectionRayCastingNormalsWithCircle() {
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    gpu::float3 translation;
    rotation.SetIdentity();
//    gpu::int3 start(0, 0, 480);
//    gpu::int3 end(0, 0, 511);
    gpu::int3 volumeCenter(0, 0, 0);
    gpu::float3 voxelCellSize(0.01, 0.01, 0.01);

    Slam::KinectFusion kinectFusion;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotation, translation, 1);
//    kinectFusion._voxelCenter = volumeCenter;
//    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &volumeCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);

    //main GPU volume;
    int size = 512 * 512 * 512;
    int* volumeStart = new int[size];
    int* volumeStartColour = new int[size];

    //Raycaster results;
    gpu::int3 otherGridSize(512, 512, 64);
    size = 512 * 512 * 64;
    int* raycastedPointer = new int[size];

//    memset(raycastedPointer, 0, size * sizeof(int));
    //ResetRaycasterVolume
    int x, y, z;

//    kinectFusion.CudaResetPointIndexVolume();
    short* ptr;
#pragma omp parallel for private(y, z, ptr)
    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for(y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3D(x, y, z, &volumeCenter, &kinectFusion._voxelGridSize));
                gpu::device::PackTsdf(ptr, 1, 1);
                gpu::device::PackColourValue(volumeStartColour + gpu::device::Index3D(x, y, z, &volumeCenter, &kinectFusion._voxelGridSize), 0xFF, 0x00, 0x00);
            }
        }
    }
    gpu::int3 global;
    gpu::float3 circleCenter(0, 0, 0);

    #pragma omp parallel for private(y, z, ptr)
    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for(y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                int gx, gy, gz;
                gpu::device::LocalVoxelToGlobalVoxel(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize, &kinectFusion._voxelCellSize, gx, gy, gz);
                gpu::float3 pos = gpu::device::VoxelToWorldShiftedToCenter(gx, gy, gz, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize, &kinectFusion._voxelCellSize);

                if(std::abs((circleCenter - pos).norm()) > 0.1) {
                    continue;
                }
                ptr = (short*) (volumeStart + gpu::device::Index3D(gx-kinectFusion._voxelCenter.x, gy-kinectFusion._voxelCenter.y, gz-kinectFusion._voxelCenter.z, &volumeCenter, &kinectFusion._voxelGridSize));
                gpu::device::PackTsdf(ptr, 1, -1);
                gpu::device::PackColourValue(volumeStartColour + gpu::device::Index3D(gx-kinectFusion._voxelCenter.x, gy-kinectFusion._voxelCenter.y, gz-kinectFusion._voxelCenter.z, &volumeCenter, &kinectFusion._voxelGridSize), 0xFF, 0x00, 0x00);
            }
        }
    }
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), volumeStart, sizeof(int) * 512 * 512 * 512, cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUTsdfColourVolume(), volumeStartColour, sizeof(int) * 512 * 512 * 512, cudaMemcpyHostToDevice);
    kinectFusion.CudaKinectV2Finish();
    x = 0;
    for(x = 0; x <kinectFusion._globalPointCloud.get()->points.size(); x++) {
//        std::cout << "Normal: " << kinectFusion._globalPointCloud.get()->points[x].normal_x << " " << kinectFusion._globalPointCloud.get()->points[x].normal_y << " " << kinectFusion._globalPointCloud.get()->points[x].normal_z << std::endl;
    }
    kinectFusion.viewPointCloud(kinectFusion._globalPointCloud);
    pcl::io::savePCDFile("SphereWithNormals.pcl", *kinectFusion._globalPointCloud.get());
}
void _TestKinectFusionCudaSectionRayCastingNormalsCheckAllAxisWithCircle() {
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    gpu::float3 translation;
    rotation.SetIdentity();
//    gpu::int3 start(0, 0, 480);
//    gpu::int3 end(0, 0, 511);
    gpu::int3 volumeCenter(0, 0, 0);
    gpu::float3 voxelCellSize(0.01, 0.01, 0.01);

    Slam::KinectFusion kinectFusion;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotation, translation, 1);
//    kinectFusion._voxelCenter = volumeCenter;
//    cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &volumeCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);

    //main GPU volume;
    int size = 512 * 512 * 512;
    int* volumeStart = new int[size];
    int* volumeStartColour = new int[size];

    //Raycaster results;
    gpu::int3 otherGridSize(512, 512, 64);
    size = 512 * 512 * 64;
    int* raycastedPointer = new int[size];

//    memset(raycastedPointer, 0, size * sizeof(int));
    //ResetRaycasterVolume
    int x, y, z;

//    kinectFusion.CudaResetPointIndexVolume();
    short* ptr;
#pragma omp parallel for private(y, z, ptr)
    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for(y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                ptr = (short*) (volumeStart + gpu::device::Index3D(x, y, z, &volumeCenter, &kinectFusion._voxelGridSize));
                gpu::device::PackTsdf(ptr, 1, 1);
                gpu::device::PackColourValue(volumeStartColour + gpu::device::Index3D(x, y, z, &volumeCenter, &kinectFusion._voxelGridSize), 0xFF, 0x00, 0x00);
            }
        }
    }
    gpu::int3 global;
    gpu::float3 circleCenter(0, 0, 0);

#pragma omp parallel for private(y, z, ptr)
    for(x = 0; x < kinectFusion._voxelGridSize.x; x++) {
        for(y = 0; y < kinectFusion._voxelGridSize.y; y++) {
            for(z = 0; z < kinectFusion._voxelGridSize.z; z++) {
                int gx, gy, gz;
                gpu::device::LocalVoxelToGlobalVoxel(x, y, z, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize, &kinectFusion._voxelCellSize, gx, gy, gz);
                gpu::float3 pos = gpu::device::VoxelToWorldShiftedToCenter(gx, gy, gz, &kinectFusion._voxelCenter, &kinectFusion._voxelGridSize, &kinectFusion._voxelCellSize);

                if(std::abs((circleCenter - pos).norm()) > 0.1) {
                    continue;
                }
                ptr = (short*) (volumeStart + gpu::device::Index3D(gx-kinectFusion._voxelCenter.x, gy-kinectFusion._voxelCenter.y, gz-kinectFusion._voxelCenter.z, &volumeCenter, &kinectFusion._voxelGridSize));
                gpu::device::PackTsdf(ptr, 1, -1);
                gpu::device::PackColourValue(volumeStartColour + gpu::device::Index3D(gx-kinectFusion._voxelCenter.x, gy-kinectFusion._voxelCenter.y, gz-kinectFusion._voxelCenter.z, &volumeCenter, &kinectFusion._voxelGridSize), 0xFF, 0x00, 0x00);
            }
        }
    }
    cudaMemcpy(gpu::host::GetGPUTsdfVolume(), volumeStart, sizeof(int) * 512 * 512 * 512, cudaMemcpyHostToDevice);
    cudaMemcpy(gpu::host::GetGPUTsdfColourVolume(), volumeStartColour, sizeof(int) * 512 * 512 * 512, cudaMemcpyHostToDevice);

    //Check X axis
    int i, iterations, stepSize;
    gpu::int3 start, end;
    gpu::int3 volumeSize = kinectFusion._voxelGridSize;
    stepSize = (int) (kinectFusion._pointIndexGridSize.z * 0.7);
    iterations = (volumeSize.x / stepSize) + 1;
//            iterations = 1;
    std::cout << iterations << std::endl;
    std::cout << (iterations * kinectFusion._pointIndexGridSize.z) << std::endl;
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloudXAxis(new pcl::PointCloud<pcl::PointXYZRGBNormal>);

    //temp
//            start.z = 484; end.z = 484;
    for(i = 0 ; i < iterations; i++) {
        end.x += stepSize;
        end.x = std::min(end.x, kinectFusion._voxelGridSize.x);
        std::cout << "X: " << start.x << " " << end.x << std::endl;
        std::cout << "Y: " << start.y << " " << end.y << std::endl;
        std::cout << "Z: " << start.z << " " << end.z << std::endl;
        kinectFusion.CudaRayCastTsdfSection(start, end);
        kinectFusion.CudaDownloadRayCastTsdfSection(true);
        (*pointCloudXAxis) += (*kinectFusion.GeneratePointCloud());
        start = end;
    }
    std::cout << "Point Cloud Size: " << pointCloudXAxis->size() << " " << pointCloudXAxis->points.size() << std::endl;
    kinectFusion.viewPointCloud(pointCloudXAxis);
    pcl::io::savePCDFile("SphereWithNormalsXAxisTest.pcl", *pointCloudXAxis);

    //Check Y axis
    start.x = 0; start.y = 0; start.z = 0;
    end.x = 0; end.y = 0; end.z = 0;
    stepSize = (int) (kinectFusion._pointIndexGridSize.z * 0.7);
    iterations = (volumeSize.x / stepSize) + 1;
//            iterations = 1;
    std::cout << iterations << std::endl;
    std::cout << (iterations * kinectFusion._pointIndexGridSize.z) << std::endl;
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloudYAxis(new pcl::PointCloud<pcl::PointXYZRGBNormal>);

    //temp
    for(i = 0 ; i < iterations; i++) {
        end.x += stepSize;
        end.x = std::min(end.x, kinectFusion._voxelGridSize.x);
        std::cout << "X: " << start.x << " " << end.x << std::endl;
        std::cout << "Y: " << start.y << " " << end.y << std::endl;
        std::cout << "Z: " << start.z << " " << end.z << std::endl;
        kinectFusion.CudaRayCastTsdfSection(start, end);
        kinectFusion.CudaDownloadRayCastTsdfSection(true);
        (*pointCloudYAxis) += (*kinectFusion.GeneratePointCloud());
        start = end;
    }
    std::cout << "Point Cloud Size: " << pointCloudYAxis->size() << " " << pointCloudYAxis->points.size() << std::endl;
    kinectFusion.viewPointCloud(pointCloudYAxis);
    pcl::io::savePCDFile("SphereWithNormalsYAxisTest.pcl", *pointCloudYAxis);

    //Check Z axis
    kinectFusion.CudaKinectV2Finish();
    kinectFusion.viewPointCloud(kinectFusion._globalPointCloud);

    pcl::io::savePCDFile("SphereWithNormalsZAxisTest.pcl", *kinectFusion._globalPointCloud);
}
void _TestRestRaycastingStorageVolume() {
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    gpu::float3 translation;
    rotation.SetIdentity();
//    gpu::int3 start(0, 0, 480);
//    gpu::int3 end(0, 0, 511);
//    gpu::int3 volumeGrid(512, 512, 512);
//    gpu::int3 volumeCenter(256, 256, 256);
//    gpu::float3 voxelCellSize(0.01, 0.01, 0.01);

    Slam::KinectFusion kinectFusion;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 3, rotation, translation, 1);

    std::vector<cv::Mat_<double>> images = GenerateSquareSpecialImage();

    cv::Scalar s(254, 0, 0);
    cv::Mat rgb = cv::Mat(480, 640, CV_8UC3, s);

    cv::Scalar area(254, 254, 254);
    int i, j;
    for (i = 200 ; i < 440;i++) {
        for(j = 150; j < 330; j++) {
            int index = (j * rgb.cols + i) * 3;
            rgb.data[index + 0] = (uchar)255;
            rgb.data[index + 1] = (uchar)255;
            rgb.data[index + 2] = (uchar)255;
        }
    }
//    for (i = 110 ; i < 160;i++) {
//        for(j = 150; j < 330; j++) {
//            int index = (j * rgb.cols + i) * 3;
//            rgb.data[index + 0] = (uchar)0;
//            rgb.data[index + 1] = (uchar)255;
//            rgb.data[index + 2] = (uchar)0;
//        }
//    }

    gpu::DepthMap depthMap(images[0].rows, images[0].cols, images[0].data);
    gpu::Image rgbImage(rgb.rows, rgb.cols, rgb.channels(), rgb.datastart);

    kinectFusion.CudaKinectV2(depthMap, rgbImage, images[0], rgb, true);



    kinectFusion.CudaKinectV2Finish();
//    kinectFusion.CudaResetPointIndexVolume();

//    kinectFusion.CudaDownloadRayCastTsdfSection(false);

    int max = kinectFusion._pointIndexGridSize.x * kinectFusion._pointIndexGridSize.y * kinectFusion._pointIndexGridSize.z;
    for(i = 0; i < max; i++) {
        assert(kinectFusion._gpuPointIndexVolume[i] == -2147483648);
    }

//    kinectFusion.CudaDownloadTsdf();

//    kinectFusion.CudaTsdfVisualizer("TsdfVolume/SectionRaycastTest");

//    kinectFusion.viewPointCloud(kinectFusion._globalPointCloud);
//    kinectFusion.CudaRayCastTsdfSection(start, end);
//    kinectFusion.CudaDownloadRayCastTsdfSection(false);


//    kinectFusion.viewPointCloud(kinectFusion.GeneratePointCloud());
}
void _TestKinectFusionCudaSectionRayCastingFullVolumeIgnorePlaneExtraction() {
    std::cout << "Starting  _TestKinectFusionCudaSectionRayCastingFullVolumeIgnorePlaneExtraction" << std::endl;
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    gpu::matrix33 rotation;
    gpu::float3 translation;
    rotation.SetIdentity();
//    rotation = Utility::GenerateRotationMatrix33((M_PI_2) * (3./4), 0, 0);
//    gpu::int3 start(0, 0, 480);
//    gpu::int3 end(0, 0, 511);
//    gpu::int3 volumeGrid(512, 512, 512);
//    gpu::int3 volumeCenter(256, 256, 256);
//    gpu::float3 voxelCellSize(0.01, 0.01, 0.01);

    Slam::KinectFusion kinectFusion;
    kinectFusion.SetupCuda(fx, fy, cx, cy, 480, 640, 5.12, rotation, translation, 1);

    std::vector<cv::Mat_<double>> images = GenerateSquareSpecialImage();

    cv::Scalar s(254, 0, 0);
    cv::Mat rgb = cv::Mat(480, 640, CV_8UC3, s);

    cv::Scalar area(254, 254, 254);
    int i, j;
    for (i = 200 ; i < 440;i++) {
        for(j = 150; j < 330; j++) {
            int index = (j * rgb.cols + i) * 3;
            rgb.data[index + 0] = (uchar)255;
            rgb.data[index + 1] = (uchar)255;
            rgb.data[index + 2] = (uchar)255;
        }
    }
//    for (i = 110 ; i < 160;i++) {
//        for(j = 150; j < 330; j++) {
//            int index = (j * rgb.cols + i) * 3;
//            rgb.data[index + 0] = (uchar)0;
//            rgb.data[index + 1] = (uchar)255;
//            rgb.data[index + 2] = (uchar)0;
//        }
//    }

    gpu::DepthMap depthMap(images[0].rows, images[0].cols, images[0].data);
    gpu::Image rgbImage(rgb.rows, rgb.cols, rgb.channels(), rgb.datastart);

    //Integrate image data into tsdf volume
    kinectFusion.CudaKinectV2(depthMap, rgbImage, images[0], rgb, true);

    //Move the camera
    kinectFusion._translation.x = 0.8;
    kinectFusion._translation.y = 0;
    kinectFusion._translation.z = 0;
    gpu::matrix33 inv = kinectFusion._rotationMatrix.transpose();
    gpu::host::UploadTransformation(kinectFusion._rotationMatrix,inv, kinectFusion._translation,
                                    gpu::host::GetGPURotation(), gpu::host::GetGPUInverseRotation(), gpu::host::GetGPUTranslation());
    //Trigger volume center Check
    bool moved = kinectFusion.CudaCheckVolumeBounds();
    //Generate PointCloud from extracted area
//    kinectFusion._poseGraph.AddNode(kinectFusion._rotationMatrix, kinectFusion._translation, kinectFusion._voxelCenter, kinectFusion.GeneratePointCloud(),
//                                    kinectFusion._icpSourceVertexMap, kinectFusion._icpSourceNormalMap, kinectFusion._colourMap, gpu::host::GetImageRows(), gpu::host::GetImageCols());
    //Move camera

    kinectFusion._translation.x = 1.;
    kinectFusion._translation.y = 0;
    kinectFusion._translation.z = 0;
    inv = kinectFusion._rotationMatrix.transpose();
    gpu::host::UploadTransformation(kinectFusion._rotationMatrix,inv, kinectFusion._translation,
                                    gpu::host::GetGPURotation(), gpu::host::GetGPUInverseRotation(), gpu::host::GetGPUTranslation());

//    kinectFusion.CudaKinectV2Finish();
    moved = kinectFusion.CudaCheckVolumeBounds();
    std::list<Node>::iterator it = kinectFusion._poseGraph._nodes.begin();
    std::cout << kinectFusion._poseGraph._nodes.size() << " Nodes in the graph" << std::endl;
    for(; it != kinectFusion._poseGraph._nodes.end();it++) {
        if((*it).GetPointCloud().get()->points.size() == 0) {
            continue;
        }
        pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr filteredPointCloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
        pcl::VoxelGrid<pcl::PointXYZRGBNormal> sor;
        sor.setInputCloud ((*it).GetPointCloud());
        sor.setLeafSize (0.01, 0.01, 0.01);
        sor.filter (*filteredPointCloud.get());

        kinectFusion.viewPointCloud(filteredPointCloud);
    }
    std::cout << "Ending  _TestKinectFusionCudaSectionRayCastingFullVolumeIgnorePlaneExtraction" << std::endl;
    exit(0);
}