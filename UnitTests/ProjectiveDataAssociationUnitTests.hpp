/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/08/23.
//

#ifndef VSLAM_PROJECTIVEDATAASSOCIATIONUNITTESTS_HPP
#define VSLAM_PROJECTIVEDATAASSOCIATIONUNITTESTS_HPP

#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <eigen3/Eigen/Dense>
#include <cmath>
#include "../KinectFusion.hpp"
#include "../FileParser.hpp"
#include <opencv2/rgbd.hpp>


#include <opencv2/highgui.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/core/eigen.hpp>

#include <cassert>
#include "../UtilityFunctions.hpp"
#include "../KinectFusion/DeviceTypes.hpp"
void _TestProjectiveDataAssociation();
void _TestProjectiveDataAssociationWithImageSequence();
void _TestProjectiveDataAssociationCustomImages();
void _TestProjectiveDataAssociationWithTsdfAndRaycaster();
#endif //VSLAM_PROJECTIVEDATAASSOCIATIONUNITTESTS_HPP
