/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2017/04/20.
//

#include "BallPivotAlgorithmUnitTests.hpp"
pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr ConvertVertexMapNormalMapToPCL(gpu::float3* vertexMap, gpu::float3* normalMap, unsigned int size) {
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
    pointCloud.get()->points.resize(size);
    pointCloud.get()->width = 1;
    pointCloud.get()->height = size;

    pcl::PointCloud<pcl::PointXYZRGBNormal>* ptr = pointCloud.get();
    long i;
    for(i = 0l; i < size; i++) {
        ptr->points[i].x = vertexMap[i].x;
        ptr->points[i].y = vertexMap[i].y;
        ptr->points[i].z = vertexMap[i].z;

        ptr->points[i].normal_x = normalMap[i].x;
        ptr->points[i].normal_y = normalMap[i].y;
        ptr->points[i].normal_z = normalMap[i].z;
    }
    return pointCloud;
}
void _TestMarkAsBoundary() {
    std::cout << "Begin _TestMarkAsBoundary" << std::endl;
    gpu::float3* points = new gpu::float3[4];
    points[0] = gpu::float3(0, 0, 0);
    points[1] = gpu::float3(0, 1, 0);
    points[2] = gpu::float3(1, 0, 0);
    points[3] = gpu::float3(1, 0, 0);

    gpu::float3* normals = new gpu::float3[4];
    normals[0] = gpu::float3(0, 0, 1);
    normals[1] = gpu::float3(0, 0, 1);
    normals[2] = gpu::float3(0, 0, 1);
    normals[3] = gpu::float3(0, 0, 1);
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = ConvertVertexMapNormalMapToPCL(points, normals, 4);
    pcl::PointXYZRGBNormal center;
    center.x = 0;
    center.y = 0;
    center.z = 0;
    BallPivotAlgorithm BPA(pointCloud);

    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._currentFrontSet = 0;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(0, 1), 2, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 2), 0, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(2, 0), 1, center, BallPivotAlgorithm::ACTIVE));
    BPA._currentFrontSet = 1;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 3), 2, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(3, 2), 1, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(2, 1), 3, center, BallPivotAlgorithm::ACTIVE));

    BallPivotAlgorithm::FrontEdge* activeEdge = NULL;

    while(BPA.GetActiveEdge(&activeEdge)) {
        BPA.MarkAsBoundary(activeEdge);
        assert(std::get<3>(*activeEdge) == BallPivotAlgorithm::BOUNDARY);
    }


    delete[] points;
    delete[] normals;
    std::cout << "End _TestMarkAsBoundary" << std::endl;
}
void _TestMarkAsUsed() {
    std::cout << "Begin _TestMarkAsUsed" << std::endl;
    gpu::float3* points = new gpu::float3[3];
    gpu::float3* normals = new gpu::float3[3];

    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = ConvertVertexMapNormalMapToPCL(points, normals, 3);
    pcl::PointXYZRGBNormal center;
    center.x = 0;
    center.y = 0;
    center.z = 0;

    BallPivotAlgorithm BPA(pointCloud);
    std::memset(BPA._mask, 0, sizeof(bool) * 3);

    std::cout << BPA._mask[0] << " " << BPA._mask[1] << " " << BPA._mask[2] << std::endl;
    assert(!BPA._mask[0]);
    assert(!BPA._mask[1]);
    assert(!BPA._mask[2]);
    BPA.MarkAsUsed(2);
    assert(BPA._mask[2]);
    delete[] points;
    delete[] normals;
    std::cout << "End _TestMarkAsUsed" << std::endl;
}
//
void _TestFindEdge() {
    std::cout << "Begin _TestFindEdge" << std::endl;
    gpu::float3* points = new gpu::float3[4];
    points[0] = gpu::float3(0, 0, 0);
    points[1] = gpu::float3(0, 1, 0);
    points[2] = gpu::float3(1, 0, 0);
    points[3] = gpu::float3(1, 0, 0);

    gpu::float3* normals = new gpu::float3[4];
    normals[0] = gpu::float3(0, 0, 1);
    normals[1] = gpu::float3(0, 0, 1);
    normals[2] = gpu::float3(0, 0, 1);
    normals[3] = gpu::float3(0, 0, 1);
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = ConvertVertexMapNormalMapToPCL(points, normals, 4);
    pcl::PointXYZRGBNormal center;
    center.x = 0;
    center.y = 0;
    center.z = 0;

    BallPivotAlgorithm BPA(pointCloud);

    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._currentFrontSet = 0;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(0, 1), 2, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 2), 0, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(2, 0), 1, center, BallPivotAlgorithm::ACTIVE));
    BPA._currentFrontSet = 1;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 3), 2, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(3, 2), 1, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(2, 1), 3, center, BallPivotAlgorithm::ACTIVE));

    std::list<BallPivotAlgorithm::FrontEdge>::iterator it = BPA._front[0].begin();
    it++;
    it++;
    BallPivotAlgorithm::FrontEdge* searchPointer = NULL;
    long frontIndex = -1;
    BPA.FindEdge(2, 0, &searchPointer, frontIndex);
    assert(frontIndex == 0);
    assert(searchPointer == &(*it));
    searchPointer = NULL;
    BPA.FindEdge(3, 2, &searchPointer, frontIndex);
    it = BPA._front[1].begin();
    it++;
    assert(frontIndex == 1);
    assert(searchPointer == &(*it));







    delete[] points;
    delete[] normals;
    std::cout << "End _TestFindEdge" << std::endl;
}

void _TestGetActiveEdge() {
    std::cout << "Begin _TestGetActiveEdge" << std::endl;
    gpu::float3* points = new gpu::float3[4];
    points[0] = gpu::float3(0, 0, 0);
    points[1] = gpu::float3(0, 1, 0);
    points[2] = gpu::float3(1, 0, 0);
    points[3] = gpu::float3(1, 0, 0);

    gpu::float3* normals = new gpu::float3[4];
    normals[0] = gpu::float3(0, 0, 1);
    normals[1] = gpu::float3(0, 0, 1);
    normals[2] = gpu::float3(0, 0, 1);
    normals[3] = gpu::float3(0, 0, 1);
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = ConvertVertexMapNormalMapToPCL(points, normals, 4);
    pcl::PointXYZRGBNormal center;
    center.x = 0;
    center.y = 0;
    center.z = 0;

    BallPivotAlgorithm BPA(pointCloud);
    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._currentFrontSet = 0;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(0, 1), 2, center, BallPivotAlgorithm::BOUNDARY));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 2), 0, center, BallPivotAlgorithm::BOUNDARY));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(2, 0), 1, center, BallPivotAlgorithm::ACTIVE));
    BPA._currentFrontSet = 1;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 3), 2, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(3, 2), 1, center, BallPivotAlgorithm::BOUNDARY));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(2, 1), 3, center, BallPivotAlgorithm::ACTIVE));

    BallPivotAlgorithm::FrontEdge* activeEdge = NULL;

    BPA.GetActiveEdge(&activeEdge);
    assert(BPA._currentFrontSet == 0);
    std::list<BallPivotAlgorithm::FrontEdge>::iterator it = BPA._front[0].begin();
    it++;
    it++;
    assert(activeEdge == &(*it));
    BPA.MarkAsBoundary(activeEdge);
    activeEdge = NULL;
    BPA.GetActiveEdge(&activeEdge);
    it = BPA._front[1].begin();
    assert(BPA._currentFrontSet == 1);
    assert(activeEdge == &(*it));
    BPA.MarkAsBoundary(activeEdge);


    activeEdge = NULL;
    BPA.GetActiveEdge(&activeEdge);
    it = BPA._front[1].begin();
    it++; it++;
    assert(BPA._currentFrontSet == 1);
    assert(activeEdge == &(*it));
    BPA.MarkAsBoundary(activeEdge);

    BPA.GetActiveEdge(&activeEdge);
    assert(activeEdge == NULL);
    assert(BPA._currentFrontSet == -1);

    delete[] points;
    delete[] normals;
    std::cout << "End _TestGetActiveEdge" << std::endl;
}
void _TestJoin1() {
    std::cout << "Begin _TestJoin1" << std::endl;
    gpu::float3* points = new gpu::float3[4];
    gpu::float3* normals = new gpu::float3[4];
    points[0] = gpu::float3(0, 0, 0);
    points[1] = gpu::float3(1, 0, 0);
    points[2] = gpu::float3(0, 1, 0);
    points[3] = gpu::float3(1, 1, 0);
    normals[0] = gpu::float3(0, 0, 1);
    normals[1] = gpu::float3(0, 0, 1);
    normals[2] = gpu::float3(0, 0, 1);
    normals[3] = gpu::float3(0, 0, 1);
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = ConvertVertexMapNormalMapToPCL(points, normals, 4);
    pcl::PointXYZRGBNormal center;
    center.x = 0;
    center.y = 0;
    center.z = 0;

    BallPivotAlgorithm BPA(pointCloud);
    std::memset(BPA._mask, 0, sizeof(bool) * 4);
    BPA._mask[0] = true;
    BPA._mask[1] = true;
    BPA._mask[2] = true;

    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._currentFrontSet = 1;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(0, 1), 2, center, BallPivotAlgorithm::BOUNDARY));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 2), 0, center, BallPivotAlgorithm::BOUNDARY));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(2, 0), 1, center, BallPivotAlgorithm::ACTIVE));
    std::list<BallPivotAlgorithm::FrontEdge>::iterator it = BPA._front[BPA._currentFrontSet].begin();
    it++;
    BallPivotAlgorithm::FrontEdge* edgeIK = NULL, *edgeKJ = NULL;
    BPA.Join(&(*it), 3, gpu::float3(4, 0, 0), &edgeIK, &edgeKJ);
    it = BPA._front[BPA._currentFrontSet].begin();


//    std::cout << std::get<0>(*it).first << " " << std::get<0>(*it).second << " " << std::get<1>(*it) << " center: (" <<
//                 std::get<2>(*it).x << ", " << std::get<2>(*it).y << ", " << std::get<2>(*it).z << ")" << std::endl;
    BallPivotAlgorithm::FrontEdge* search;
    long index;
    BPA.FindEdge(0, 1, &search, index);
    assert(index == 1);
    assert(search == &(*it));
    it++;

    BPA.FindEdge(1, 3, &search, index);
    assert(index == 1);
    assert(search == &(*it));
    it++;

    BPA.FindEdge(3, 2, &search, index);
    assert(index == 1);
    assert(search == &(*it));
    it++;

    BPA.FindEdge(2, 0, &search, index);
    assert(index == 1);
    assert(search == &(*it));
    it++;

    delete[] points;
    delete[] normals;

    std::cout << "End _TestJoin1" << std::endl;
}
void _TestJoin2() {
    std::cout << "Begin _TestJoin2" << std::endl;
    gpu::float3* points = new gpu::float3[6];
    gpu::float3* normals = new gpu::float3[6];
    points[0] = gpu::float3(0, 0, 0);
    points[1] = gpu::float3(1, 0, 0);
    points[2] = gpu::float3(0, 1, 0);
    points[3] = gpu::float3(1, 1, 0);
    points[4] = gpu::float3(2, 1, 0);
    points[5] = gpu::float3(1, 2, 0);
    normals[0] = gpu::float3(0, 0, 1);
    normals[1] = gpu::float3(0, 0, 1);
    normals[2] = gpu::float3(0, 0, 1);
    normals[3] = gpu::float3(0, 0, 1);
    normals[4] = gpu::float3(0, 0, 1);
    normals[5] = gpu::float3(0, 0, 1);
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = ConvertVertexMapNormalMapToPCL(points, normals, 6);
    pcl::PointXYZRGBNormal center;
    center.x = 0;
    center.y = 0;
    center.z = 0;

    BallPivotAlgorithm BPA(pointCloud);
    std::memset(BPA._mask, 0, sizeof(bool) * 6);
    BPA._mask[0] = true;
    BPA._mask[1] = true;
    BPA._mask[2] = true;
    BPA._mask[3] = true;
    BPA._mask[4] = true;
    BPA._mask[5] = true;

    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._currentFrontSet = 0;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(0, 1), 2, center, BallPivotAlgorithm::BOUNDARY));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 2), 0, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(2, 0), 1, center, BallPivotAlgorithm::BOUNDARY));
    BPA._currentFrontSet = 1;

    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(3, 4), 5, center, BallPivotAlgorithm::BOUNDARY));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(4, 5), 3, center, BallPivotAlgorithm::BOUNDARY));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(5, 3), 4, center, BallPivotAlgorithm::BOUNDARY));



    BPA._currentFrontSet = 0;
    std::list<BallPivotAlgorithm::FrontEdge>::iterator it = BPA._front[BPA._currentFrontSet].begin();
    it++;
    BallPivotAlgorithm::FrontEdge* edgeIK = NULL, *edgeKJ = NULL;
    BPA.Join(&(*it), 3, gpu::float3(4, 0, 0), &edgeIK, &edgeKJ);

    it = BPA._front[BPA._currentFrontSet].begin();
    BallPivotAlgorithm::FrontEdge* search;
    long index;
    BPA.FindEdge(0, 1, &search, index);
    assert(index == 0);
    std::cout << "CMP: " << search << " " << &(*it) << std::endl;
    assert(search == &(*it));
    it++;

    BPA.FindEdge(1, 3, &search, index);
    assert(index == 0);
    assert(search == &(*it));
    it++;

    BPA.FindEdge(3, 2, &search, index);
    assert(index == 0);
    assert(search == &(*it));
    it++;

    BPA.FindEdge(2, 0, &search, index);
    assert(index == 0);
    assert(search == &(*it));
    it++;


    delete[] points;
    delete[] normals;
    std::cout << "End _TestJoin2" << std::endl;
}
void _TestJoin3() {
    std::cout << "Begin _TestJoin3" << std::endl;
    gpu::float3* points = new gpu::float3[5];
    gpu::float3* normals = new gpu::float3[5];
    points[0] = gpu::float3(0, 0, 0);
    points[1] = gpu::float3(1, 0, 0);
    points[2] = gpu::float3(0, 1, 0);
    points[3] = gpu::float3(1, 1, 0);
    points[4] = gpu::float3(0.5, 0.5, 0.5);
    normals[0] = gpu::float3(0, 0, 1);
    normals[1] = gpu::float3(0, 0, 1);
    normals[2] = gpu::float3(0, 0, 1);
    normals[3] = gpu::float3(0, 0, 1);
    normals[4] = gpu::float3(0, 0, 1);
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = ConvertVertexMapNormalMapToPCL(points, normals, 5);
    pcl::PointXYZRGBNormal center;
    center.x = 0;
    center.y = 0;
    center.z = 0;

    BallPivotAlgorithm BPA(pointCloud);

    std::memset(BPA._mask, 0, sizeof(bool) * 5);
    BPA._mask[0] = true;
    BPA._mask[1] = true;
    BPA._mask[2] = true;
    BPA._mask[3] = true;
    BPA._mask[4] = true;

    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._currentFrontSet = 1;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(0, 1), 2, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 3), 2, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(3, 2), 1, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(2, 0), 1, center, BallPivotAlgorithm::ACTIVE));
    std::list<BallPivotAlgorithm::FrontEdge>::iterator it = BPA._front[BPA._currentFrontSet].begin();
    it++;
    BallPivotAlgorithm::FrontEdge* edgeIK = NULL, *edgeKJ = NULL;
    BPA.Join(&(*it), 4, gpu::float3(4, 0, 0), &edgeIK, &edgeKJ);
    it = BPA._front[BPA._currentFrontSet].begin();


//    std::cout << std::get<0>(*it).first << " " << std::get<0>(*it).second << " " << std::get<1>(*it) << " center: (" <<
//                 std::get<2>(*it).x << ", " << std::get<2>(*it).y << ", " << std::get<2>(*it).z << ")" << std::endl;
    BallPivotAlgorithm::FrontEdge* search;
    long index;
    BPA.FindEdge(0, 1, &search, index);
    assert(index == 1);
    assert(search == &(*it));
    it++;

    BPA.FindEdge(1, 3, &search, index);
    assert(index == 1);
    assert(search == &(*it));
    it++;

    BPA.FindEdge(3, 2, &search, index);
    assert(index == 1);
    assert(search == &(*it));
    it++;

    BPA.FindEdge(2, 0, &search, index);
    assert(index == 1);
    assert(search == &(*it));
    it++;
    assert(BPA._front[BPA._currentFrontSet].size() == 4);

    delete[] points;
    delete[] normals;
    std::cout << "End _TestJoin3" << std::endl;
}
void _TestGlueClosedLoop() {
    std::cout << "Begin _TestGlueClosedLoop" << std::endl;
    gpu::float3* points = new gpu::float3[2];
    gpu::float3* normals = new gpu::float3[2];
    points[0] = gpu::float3(0, 0, 0);
    points[1] = gpu::float3(1, 0, 0);
    normals[0] = gpu::float3(0, 0, 1);
    normals[1] = gpu::float3(0, 0, 1);
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = ConvertVertexMapNormalMapToPCL(points, normals, 2);
    pcl::PointXYZRGBNormal center;
    center.x = 0;
    center.y = 0;
    center.z = 0;

    BallPivotAlgorithm BPA(pointCloud);
    std::memset(BPA._mask, 0, sizeof(bool) * 2);
    BPA._mask[0] = true;
    BPA._mask[1] = true;

    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._currentFrontSet = 0;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(0, 1), 0, center, BallPivotAlgorithm::BOUNDARY));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 0), 0, center, BallPivotAlgorithm::ACTIVE));
    std::list<BallPivotAlgorithm::FrontEdge>::iterator it = BPA._front[BPA._currentFrontSet].begin();
    BallPivotAlgorithm::FrontEdge* activeEdge = &(*it), *other = NULL;


    long index = -1;
    bool rt = BPA.FindEdge(1, 0, &other, index);
    assert(rt);
    assert(index == 0);
    BPA.Glue(activeEdge, other, index);
    assert(BPA._front[BPA._currentFrontSet].size() == 0);


    delete[] points;
    delete[] normals;

    std::cout << "End _TestGlueClosedLoop" << std::endl;
}

void _TestGlueClosedLoopMultipleFronts() {
    std::cout << "Begin _TestGlueClosedLoopMultipleFronts" << std::endl;
    gpu::float3* points = new gpu::float3[6];
    gpu::float3* normals = new gpu::float3[6];
    points[0] = gpu::float3(0, 0, 0);
    points[1] = gpu::float3(1, 0, 0);
    points[2] = gpu::float3(0, 1, 0);
    points[3] = gpu::float3(1, 1, 0);
    points[4] = gpu::float3(2, 1, 0);
    points[5] = gpu::float3(1, 2, 0);
    normals[0] = gpu::float3(0, 0, 1);
    normals[1] = gpu::float3(0, 0, 1);
    normals[2] = gpu::float3(0, 0, 1);
    normals[3] = gpu::float3(0, 0, 1);
    normals[4] = gpu::float3(0, 0, 1);
    normals[5] = gpu::float3(0, 0, 1);
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = ConvertVertexMapNormalMapToPCL(points, normals, 6);
    pcl::PointXYZRGBNormal center;
    center.x = 0;
    center.y = 0;
    center.z = 0;

    BallPivotAlgorithm BPA(pointCloud);
    std::memset(BPA._mask, 0, sizeof(bool) * 6);
    BPA._mask[0] = true;
    BPA._mask[1] = true;
    BPA._mask[2] = true;
    BPA._mask[3] = true;
    BPA._mask[4] = true;
    BPA._mask[5] = true;

    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._currentFrontSet = 0;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(0, 1), 2, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 0), 0, center, BallPivotAlgorithm::ACTIVE));
    BPA._currentFrontSet = 1;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(2, 3), 1, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(3, 2), 1, center, BallPivotAlgorithm::ACTIVE));
    BPA._currentFrontSet = 2;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(4, 5), 1, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(5, 4), 1, center, BallPivotAlgorithm::ACTIVE));



    BallPivotAlgorithm::FrontEdge* activeAB = NULL, *otherBA = NULL;
    long index = -1;
    assert(BPA.GetActiveEdge(&activeAB));

    assert(BPA.FindEdge(1, 0, &otherBA, index));
    assert(BPA._currentFrontSet == index);
    BPA.Glue(activeAB, otherBA, index);
    assert(BPA._front[BPA._currentFrontSet].size() == 0);
    assert(BPA._front[1].size() == 2);
    assert(BPA._front[2].size() == 2);


    index = -1;
    assert(BPA.GetActiveEdge(&activeAB));
    assert(BPA._currentFrontSet == 1);

    assert(BPA.FindEdge(3, 2, &otherBA, index));
    assert(BPA._currentFrontSet == index);
    BPA.Glue(activeAB, otherBA, index);
    assert(BPA._front[BPA._currentFrontSet].size() == 0);
    assert(BPA._front[1].size() == 0);
    assert(BPA._front[2].size() == 2);

    index = -1;
    assert(BPA.GetActiveEdge(&activeAB));
    assert(BPA._currentFrontSet == 2);

    assert(BPA.FindEdge(5, 4, &otherBA, index));
    assert(BPA._currentFrontSet == index);
    BPA.Glue(activeAB, otherBA, index);
    assert(BPA._front[BPA._currentFrontSet].size() == 0);
    assert(BPA._front[1].size() == 0);
    assert(BPA._front[2].size() == 0);



    delete[] points;
    delete[] normals;

    std::cout << "End _TestGlueClosedLoopMultipleFronts" << std::endl;

}
void _TestGlueConsecutiveEdges() {
    std::cout << "Begin _TestGlueConsecutiveEdges" << std::endl;
    gpu::float3* points = new gpu::float3[4];
    gpu::float3* normals = new gpu::float3[4];
    points[0] = gpu::float3(0, 0, 0);
    points[1] = gpu::float3(1, 0, 0);
    points[2] = gpu::float3(0, 1, 0);
    points[3] = gpu::float3(1, 1, 0);
    normals[0] = gpu::float3(0, 0, 1);
    normals[1] = gpu::float3(0, 0, 1);
    normals[2] = gpu::float3(0, 0, 1);
    normals[3] = gpu::float3(0, 0, 1);
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = ConvertVertexMapNormalMapToPCL(points, normals, 4);
    pcl::PointXYZRGBNormal center;
    center.x = 0;
    center.y = 0;
    center.z = 0;

    BallPivotAlgorithm BPA(pointCloud);
    std::memset(BPA._mask, 0, sizeof(bool) * 4);
    BPA._mask[0] = true;
    BPA._mask[1] = true;
    BPA._mask[2] = true;

    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._currentFrontSet = 1;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(0, 1), 2, center, BallPivotAlgorithm::BOUNDARY));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 2), 0, center, BallPivotAlgorithm::BOUNDARY));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(2, 1), 1, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 3), 1, center, BallPivotAlgorithm::ACTIVE));
    BallPivotAlgorithm::FrontEdge* edgeJI = NULL, *edgeIJ = NULL;
    long indexIJ, indexJI;
    BPA.FindEdge(2, 1, &edgeJI, indexJI);
    BPA.FindEdge(1, 2, &edgeIJ, indexIJ);
    BPA.Glue(edgeIJ, edgeJI, indexJI);


//    std::cout << std::get<0>(*it).first << " " << std::get<0>(*it).second << " " << std::get<1>(*it) << " center: (" <<
//                 std::get<2>(*it).x << ", " << std::get<2>(*it).y << ", " << std::get<2>(*it).z << ")" << std::endl;
    BallPivotAlgorithm::FrontEdge* search;
    long index;
    std::list<BallPivotAlgorithm::FrontEdge>::iterator it = BPA._front[1].begin();
    BPA.FindEdge(0, 1, &search, index);
    assert(index == 1);
    assert(search == &(*it));
    it++;

    BPA.FindEdge(1, 3, &search, index);
    assert(index == 1);
    assert(search == &(*it));
    it++;
    assert(it == BPA._front[1].end());

    assert(BPA._front[1].size() == 2);


    BPA.FindEdge(2, 1, &search, index);
    assert(index == -1);
    assert(search == NULL);
    BPA.FindEdge(1, 2, &search, index);
    assert(index == -1);
    assert(search == NULL);

    delete[] points;
    delete[] normals;
    std::cout << "End _TestGlueConsecutiveEdges" << std::endl;
}
void _TestGlueSplit() {
    std::cout << "Begin _TestGlueSplit" << std::endl;
    gpu::float3* points = new gpu::float3[6];
    gpu::float3* normals = new gpu::float3[6];
    points[0] = gpu::float3(0, 0, 0);
    points[1] = gpu::float3(1, 0, 0);
    points[2] = gpu::float3(0, 1, 0);
    points[3] = gpu::float3(1, 1, 0);
    points[4] = gpu::float3(2, 1, 0);
    points[5] = gpu::float3(1, 2, 0);
    normals[0] = gpu::float3(0, 0, 1);
    normals[1] = gpu::float3(0, 0, 1);
    normals[2] = gpu::float3(0, 0, 1);
    normals[3] = gpu::float3(0, 0, 1);
    normals[4] = gpu::float3(0, 0, 1);
    normals[5] = gpu::float3(0, 0, 1);
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = ConvertVertexMapNormalMapToPCL(points, normals, 6);
    pcl::PointXYZRGBNormal center;
    center.x = 0;
    center.y = 0;
    center.z = 0;

    BallPivotAlgorithm BPA(pointCloud);
    BPA._mask[0] = true;
    BPA._mask[1] = true;
    BPA._mask[2] = true;
    BPA._mask[3] = true;
    BPA._mask[4] = true;
    BPA._mask[5] = true;

    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._currentFrontSet = 0;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(2, 0), 2, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(0, 1), 0, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 2), 1, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(2, 3), 5, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(3, 4), 3, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(4, 5), 4, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(5, 3), 4, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(3, 2), 4, center, BallPivotAlgorithm::ACTIVE));

    BallPivotAlgorithm::FrontEdge* Edge23 = NULL, *Edge32 = NULL;
    long index23 = -1l, index32 = -1l;
    BPA.FindEdge(3, 2, &Edge32, index32);
    BPA.FindEdge(2, 3, &Edge23, index23);
    std::cout << index32 << " " << index23 << std::endl;
    std::cout << Edge32 << std::endl;
    std::cout << Edge23 << std::endl;
    std::cout << "Before glue sizes: " << BPA._front[0].size() << " " << BPA._front.size() << std::endl;
    BPA.Glue(Edge23, Edge32, index32);
    assert(index23 == index32);
    assert(BPA._front.size() == 2);
    std::cout << BPA._front[0].size() << " " << BPA._front[1].size() << std::endl;
    assert(BPA._front[0].size() == 3);
    assert(BPA._front[1].size() == 3);

//    std::list<BallPivotAlgorithm::FrontEdge>::iterator iterator1 = std::find(BPA._front[BPA._currentFrontSet].begin(), BPA._front[BPA._currentFrontSet].end(), Edge23);
//    assert(iterator1 == BPA._front[BPA._currentFrontSet].end());
//    iterator1 = std::find(BPA._front[BPA._front.size() - 1].begin(), BPA._front[BPA._front.size() - 1].end(), Edge23);
//    assert(iterator1 == BPA._front[BPA._front.size() - 1].end());
//
//    iterator1 = std::find(BPA._front[BPA._currentFrontSet].begin(), BPA._front[BPA._currentFrontSet].end(), Edge32);
//    assert(iterator1 == BPA._front[BPA._currentFrontSet].end());
//    iterator1 = std::find(BPA._front[BPA._front.size() - 1].begin(), BPA._front[BPA._front.size() - 1].end(), Edge32);
//    assert(iterator1 == BPA._front[BPA._front.size() - 1].end());
    std::list<BallPivotAlgorithm::FrontEdge>::iterator search;
    for(search = BPA._front[0].begin(); search != BPA._front[0].end(); search++) {
        assert(&(*search) != Edge23);
        assert(&(*search) != Edge32);
    }
    for(search = BPA._front[1].begin(); search != BPA._front[1].end(); search++) {
        assert(&(*search) != Edge23);
        assert(&(*search) != Edge32);
    }

    search = BPA._front[0].begin();
    assert(!(std::get<0>(*search).first == 2 && std::get<0>(*search).second == 3));
    search++;
    assert(!(std::get<0>(*search).first == 2 && std::get<0>(*search).second == 3));
    search++;
    assert(!(std::get<0>(*search).first == 2 && std::get<0>(*search).second == 3));
    search++;

    search = BPA._front[1].begin();
    assert(!(std::get<0>(*search).first == 3 && std::get<0>(*search).second == 2));
    search++;
    assert(!(std::get<0>(*search).first == 3 && std::get<0>(*search).second == 2));
    search++;
    assert(!(std::get<0>(*search).first == 3 && std::get<0>(*search).second == 2));
    search++;

    delete[] points;
    delete[] normals;
    std::cout << "Ending _TestGlueSplit" << std::endl;
}
void _TestGlueMerge() {
    std::cout << "Begin _TestGlueMerge" << std::endl;
    gpu::float3* points = new gpu::float3[6];
    gpu::float3* normals = new gpu::float3[6];
    points[0] = gpu::float3(0, 0, 0);
    points[1] = gpu::float3(1, 0, 0);
    points[2] = gpu::float3(0, 1, 0);
    points[3] = gpu::float3(1, 1, 0);

    points[4] = gpu::float3(2, 0, 0);
    points[5] = gpu::float3(2, 1, 0);
    normals[0] = gpu::float3(0, 0, 1);
    normals[1] = gpu::float3(0, 0, 1);
    normals[2] = gpu::float3(0, 0, 1);
    normals[3] = gpu::float3(0, 0, 1);
    normals[4] = gpu::float3(0, 0, 1);
    normals[5] = gpu::float3(0, 0, 1);
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = ConvertVertexMapNormalMapToPCL(points, normals, 6);
    pcl::PointXYZRGBNormal center;
    center.x = 0;
    center.y = 0;
    center.z = 0;

    BallPivotAlgorithm BPA(pointCloud);
    BPA._mask[0] = true;
    BPA._mask[1] = true;
    BPA._mask[2] = true;
    BPA._mask[3] = true;
    BPA._mask[4] = true;
    BPA._mask[5] = true;

    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._front.push_back(std::list<BallPivotAlgorithm::FrontEdge>());
    BPA._currentFrontSet = 0;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(0, 1), 2, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 3), 0, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(3, 2), 0, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(2, 0), 0, center, BallPivotAlgorithm::ACTIVE));
    BPA._currentFrontSet = 1;
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(5, 3), 1, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(3, 1), 1, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(1, 4), 1, center, BallPivotAlgorithm::ACTIVE));
    BPA._front[BPA._currentFrontSet].push_back(BallPivotAlgorithm::FrontEdge(BallPivotAlgorithm::Edge(4, 5), 1, center, BallPivotAlgorithm::ACTIVE));


    BallPivotAlgorithm::FrontEdge* edge13 = NULL, *edge31 = NULL;
    long index31 = -1;
    long index13 = -1;
    BPA.FindEdge(3, 1, &edge31, index31);
    BPA.FindEdge(1, 3, &edge13, index13);
    BPA._currentFrontSet = index13;
    std::cout << "index: " << BPA._currentFrontSet << " " << index31 << " " << index13 << std::endl;
    assert(BPA._currentFrontSet != index31);
    assert(index31 != index13);
    assert(index31 != BPA._currentFrontSet);
    BPA.Glue(edge13, edge31, index31);
    std::cout << BPA._front[0].size() << std::endl;
    std::cout << BPA._front[1].size() << std::endl;
    assert(BPA._front[BPA._currentFrontSet].size() == 6);
    assert(BPA._front[index31].size() == 0);
    std::list<BallPivotAlgorithm::FrontEdge>::iterator iterator1 = BPA._front[BPA._currentFrontSet].begin();
    for(; iterator1 != BPA._front[BPA._currentFrontSet].end(); iterator1++) {
//        std::cout << std::get<0>(*iterator1).first << " " << std::get<0>(*iterator1).first->y << " " << std::get<0>(*iterator1).first->z << " -> " << std::get<0>(*iterator1).second->x << " " << std::get<0>(*iterator1).second->y << " " << std::get<0>(*iterator1).second->z << std::endl;
    }
    delete[] points;
    delete[] normals;
    std::cout << "End _TestGlueMerge" << std::endl;
}

void _TestInfinitLoop() {
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);

//    BallPivotAlgorithm bpa(cloud);


}
void _TestGetBallCenter() {
    gpu::float3* points = new gpu::float3[6];
    gpu::float3* normals = new gpu::float3[6];
    points[0] = gpu::float3(0, 0, 0);
    points[1] = gpu::float3(1, 0, 0);
    points[2] = gpu::float3(0, 1, 0);
    points[3] = gpu::float3(1, 1, 0);

    points[4] = gpu::float3(2, 0, 0);
    points[5] = gpu::float3(2, 1, 0);
    normals[0] = gpu::float3(0, 0, 1);
    normals[1] = gpu::float3(0, 0, 1);
    normals[2] = gpu::float3(0, 0, 1);
    normals[3] = gpu::float3(0, 0, 1);
    normals[4] = gpu::float3(0, 0, 1);
    normals[5] = gpu::float3(0, 0, 1);
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud = ConvertVertexMapNormalMapToPCL(points, normals, 6);
    pcl::PointXYZRGBNormal center;
    center.x = 0;
    center.y = 0;
    center.z = 0;

    BallPivotAlgorithm BPA(pointCloud);

    gpu::float3 p0(0, 0, 1), n0(0, 0, 1);
    gpu::float3 p1(0, -1, 0), n1(0, -1, 0);
    gpu::float3 p2(0, 0, -1), n2(0, 0, -1);
    gpu::float3 bcenter;
    bool swap = false;
    bool valid = BPA.GetBallCenter(p0, n0, p1, n1, p2, n2, 1, bcenter, swap);
    std::cout << valid << std::endl;
    std::cout << bcenter.x << " " << bcenter.y << " " << bcenter.z << std::endl;
//    exit(0);
}

void _TestBallPivotAlgorithm() {
    std::cout << "Running BPA Test cases" << std::endl;
    _TestMarkAsBoundary();
    _TestMarkAsUsed();
    _TestFindEdge();
    _TestGetActiveEdge();

    _TestJoin1();
    _TestJoin2();
    _TestJoin3();

    _TestGlueClosedLoop();
    _TestGlueClosedLoopMultipleFronts();

    _TestGlueConsecutiveEdges();
    _TestGlueSplit();
    _TestGlueMerge();

    _TestInfinitLoop();
    _TestGetBallCenter();
//    exit(0);
}