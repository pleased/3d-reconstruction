/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/07/21.
//

#include "GlobalDeviceUnitTests.hpp"
void _TestVoxelInteractions() {
    gpu::int3 voxelCenter, voxelGridSize;
    gpu::float3 voxelCellSize;
//    voxelCenter.x = 0;
//    voxelCenter.y = 0;
//    voxelCenter.z = 0;
//
//    voxelGridSize.x = 500;
//    voxelGridSize.y = 500;
//    voxelGridSize.z = 500;
//
//    voxelCellSize.x = 0.01;
//    voxelCellSize.y = 0.01;
//    voxelCellSize.z = 0.01;
//
//    gpu::float3 worldCo = gpu::device::VoxelToWorld(0, 0, 0, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    gpu::int3 voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    assert(worldCo.x == -2.5f && worldCo.y == -2.5f && worldCo.z == -2.5f);
//    assert(voxelCo.x == 0 && voxelCo.y == 0 && voxelCo.z == 0);
//
//    worldCo = gpu::device::VoxelToWorld(0, 500, 0, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    assert(worldCo.x == -2.5f && worldCo.y == 2.5f && worldCo.z == -2.5f);
//    assert(voxelCo.x == 0 && voxelCo.y == 500 && voxelCo.z == 0);
//
//    worldCo = gpu::device::VoxelToWorld(500, 0, 0, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    assert(worldCo.x == 2.5f && worldCo.y == -2.5f && worldCo.z == -2.5f);
//    assert(voxelCo.x == 500 && voxelCo.y == 0 && voxelCo.z == 0);
//
//    worldCo = gpu::device::VoxelToWorld(0, 0, 500, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    assert(worldCo.x == -2.5 && worldCo.y == -2.5 && worldCo.z == 2.5);
//    assert(voxelCo.x == 0 && voxelCo.y == 0 && voxelCo.z == 500);
//
//    worldCo = gpu::device::VoxelToWorld(500, 500, 500, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    assert(worldCo.x == 2.5 && worldCo.y == 2.5 && worldCo.z == 2.5);
//    assert(voxelCo.x == 500 && voxelCo.y == 500 && voxelCo.z == 500);


//    voxelCenter.x = 250;
//    voxelCenter.y = 250;
//    voxelCenter.z = 250;
//    worldCo = gpu::device::VoxelToWorld(0, 0, 0, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    assert(worldCo.x == 0 && worldCo.y == 0 && worldCo.z == 0);
//    assert(voxelCo.x == 0 && voxelCo.y == 0 && voxelCo.z == 0);
//
//    worldCo = gpu::device::VoxelToWorld(0, 500, 0, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    assert(worldCo.x == 0 && worldCo.y == 5 && worldCo.z == 0);
//    assert(voxelCo.x == 0 && voxelCo.y == 500 && voxelCo.z == 0);
//
//    worldCo = gpu::device::VoxelToWorld(500, 0, 0, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    assert(worldCo.x == 5 && worldCo.y == 0 && worldCo.z == 0);
//    assert(voxelCo.x == 500 && voxelCo.y == 0 && voxelCo.z == 0);
//
//    worldCo = gpu::device::VoxelToWorld(0, 0, 500, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    assert(worldCo.x == 0 && worldCo.y == 0 && worldCo.z == 5);
//    assert(voxelCo.x == 0 && voxelCo.y == 0 && voxelCo.z == 500);
//
//    worldCo = gpu::device::VoxelToWorld(500, 500, 500, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    assert(worldCo.x == 5 && worldCo.y == 5 && worldCo.z == 5);
//    assert(voxelCo.x == 500 && voxelCo.y == 500 && voxelCo.z == 500);


//    //Moving to random pos
//    voxelCenter.x = 453;
//    voxelCenter.y = 500;
//    voxelCenter.z = 621;
//
//    worldCo = gpu::device::VoxelToWorld(15, 0, 5, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    assert(fequal(worldCo.x, 2.18f) && fequal(worldCo.y, 2.5f) && fequal(worldCo.z, 3.76f));
//    assert(voxelCo.x == 15 && voxelCo.y == 0 && voxelCo.z == 5);
//
//    worldCo = gpu::device::VoxelToWorld(432, 23, 90, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    //6.35,  2.73,  4.61
//    assert(fequal(worldCo.x, 6.35f) && fequal(worldCo.y, 2.73f) && fequal(worldCo.z, 4.61f));
//    assert(voxelCo.x == 432 && voxelCo.y == 23 && voxelCo.z == 90);
//
//    worldCo = gpu::device::VoxelToWorld(250, 250, 250, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    //4.53,  5.  ,  6.21
//    assert(fequal(worldCo.x, 4.53f) && fequal(worldCo.y, 5) && fequal(worldCo.z, 6.21f));
//    worldCo.x = 4.53f;
//    voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    assert(voxelCo.x == 250 && voxelCo.y == 250 && voxelCo.z == 250);
//
//    worldCo = gpu::device::VoxelToWorld(23, 45, 66, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    //2.26,  2.95,  4.37
//    assert(fequal(worldCo.x, 2.26f) && fequal(worldCo.y, 2.95f) && fequal(worldCo.z, 4.37f));
//    assert(voxelCo.x == 23 && voxelCo.y == 45 && voxelCo.z == 66);
//
//    worldCo = gpu::device::VoxelToWorld(345, 432, 299, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    voxelCo = gpu::device::WorldToVoxel(worldCo, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    //5.48,  6.82,  6.7
//    assert(fequal(worldCo.x, 5.48f) && fequal(worldCo.y, 6.82) && fequal(worldCo.z, 6.7f));
//    assert(voxelCo.x == 345 && voxelCo.y == 432 && voxelCo.z == 299);


//Moving to random pos
    voxelCenter.x = 0;
    voxelCenter.y = 0;
    voxelCenter.z = 0;
    voxelGridSize.x = 500;
    voxelGridSize.y = 500;
    voxelGridSize.z = 500;

//    int x, y, z;
//    for(x = 0; x < 10; x++) {
//        for( y = 0; y < 10; y++) {
//            for(z = 0; z < 10; z++) {
//                int index = gpu::device::Index3D(x, y, z, &voxelCenter, &voxelGridSize);
//                std::cout << index << std::endl;
//            }
//        }
//    }
    int index = gpu::device::Index3D(0, 0, 0, &voxelCenter, &voxelGridSize);
    std::cout << index << std::endl;

    index = gpu::device::Index3D(499, 499, 499, &voxelCenter, &voxelGridSize);
    std::cout << index << std::endl;


    gpu::float3 test;
    test = gpu::device::VoxelToWorld(0, 0, 0, &voxelGridSize, &voxelCenter, &voxelCellSize);
    std::cout << test.x << " " << test.y << " " << test.z << std::endl;
    test = gpu::device::VoxelToWorld(500, 500, 500, &voxelGridSize, &voxelCenter, &voxelCellSize);
    std::cout << test.x << " " << test.y << " " << test.z << std::endl;
//    test = gpu::device::VoxelToWorld(500, 500, 500, &voxelGridSize, &voxelCenter, &voxelCellSize);
//    std::cout << test.x << " " << test.y << " " << test.z << std::endl;
//    index = gpu::device::Index3D(x, y, z, &voxelCenter, &voxelGridSize);
//    std::cout << index << std::endl;
//    index = gpu::device::Index3D(x, y, z, &voxelCenter, &voxelGridSize);
//    std::cout << index << std::endl;

    // Pos 2.5 2.5 0.1
}

void _TestUploads() {
    std::cout << "Starting _TestUploads" << std::endl;

    gpu::float3 floats(1.333, 2.333, 4.333);
    gpu::float3* gpuFloat3 = gpu::host::AllocateFloat3();
    cudaMemcpy((void*)gpuFloat3, (void*)&floats, sizeof(gpu::float3), cudaMemcpyHostToDevice);


    gpu::float3 recvFloat3;

    cudaMemcpy((void*) &recvFloat3, gpuFloat3, sizeof(gpu::float3), cudaMemcpyDeviceToHost);
    cudaFree(gpuFloat3);
    float eps = 1.e-4;
    assert(std::abs(recvFloat3.x - floats.x) < eps);
    assert(std::abs(recvFloat3.y - floats.y) < eps);
    assert(std::abs(recvFloat3.z - floats.z) < eps);


    gpu::matrix33 matrix(1, 2, 3, 4, 5, 6, 7, 8, 9);
    gpu::matrix33* gpuMatrix33 = gpu::host::AllocateMatrix33();
    cudaMemcpy((void*) gpuMatrix33, (void*) &matrix, sizeof(gpu::matrix33), cudaMemcpyHostToDevice);
    gpu::matrix33 recvMatrix;
    cudaMemcpy((void*)&recvMatrix, gpuMatrix33, sizeof(gpu::matrix33),cudaMemcpyDeviceToHost);
    cudaFree(gpuMatrix33);
    assert(recvMatrix.row0.x == 1);
    assert(recvMatrix.row0.y == 2);
    assert(recvMatrix.row0.z == 3);
    assert(recvMatrix.row1.x == 4);
    assert(recvMatrix.row1.y == 5);
    assert(recvMatrix.row1.z == 6);
    assert(recvMatrix.row2.x == 7);
    assert(recvMatrix.row2.y == 8);
    assert(recvMatrix.row2.z == 9);

    std::cout << "\tSuccessful" << std::endl << "Ending _TestUploads" << std::endl;
}

void _TestTsdfUploads() {
    std::cout << "Starting _TestTsdfUploads" << std::endl;
    double fx = 525;
    double fy = 525;
    double cx = 320;
    double cy = 240;
    gpu::matrix33 cameraMatrix(fx, 0, cx, 0, fy, cy, 0, 0, 1), rotationMatrix, inverseRotation;
    rotationMatrix.SetIdentity(); inverseRotation.SetIdentity();
    gpu::float3 translation(22), voxelCellSize(0.01);
    gpu::int3 voxelCenter(256), voxelGridSize(512), pointIndexGridSize(512, 512, 64);
    int rows = 480;
    int cols = 640;

    gpu::host::init(cameraMatrix, rotationMatrix, inverseRotation, translation, voxelCenter, voxelGridSize, voxelCellSize, pointIndexGridSize, rows, cols, 3, 3);

    assert(gpu::host::GetGPUCameraMatrix() != 0);
    assert(gpu::host::GetGPUDepthMap() != 0);
    assert(gpu::host::GetGPUDepthMapContainer() != 0);
    assert(gpu::host::GetGPURotation() != 0);
    assert(gpu::host::GetGPUInverseRotation() != 0);
    assert(gpu::host::GetGPUTranslation() != 0);
    assert(gpu::host::GetGPUVoxelCellSize() != 0);
    assert(gpu::host::GetGPUVoxelGridSize() != 0);
    assert(gpu::host::GetGPUVoxelCenter() != 0);

    gpu::matrix33 downCameraMatrix, downRotationMatrix, downInverseRotation;
    gpu::float3 downTranslation, downVoxelCellSize;
    gpu::int3 downVoxelCenter, downVoxelGridSize;

    gpu::host::Download(gpu::host::GetGPUCameraMatrix(), &downCameraMatrix, sizeof(gpu::matrix33));
    gpu::host::Download(gpu::host::GetGPURotation(), &downRotationMatrix, sizeof(gpu::matrix33));
    gpu::host::Download(gpu::host::GetGPUInverseRotation(), &downInverseRotation, sizeof(gpu::matrix33));

    gpu::host::Download(gpu::host::GetGPUTranslation(), &downTranslation, sizeof(gpu::float3));
    gpu::host::Download(gpu::host::GetGPUVoxelCenter(), &downVoxelCenter, sizeof(gpu::int3));
    gpu::host::Download(gpu::host::GetGPUVoxelGridSize(), &downVoxelGridSize, sizeof(gpu::int3));
    gpu::host::Download(gpu::host::GetGPUVoxelCellSize(), &downVoxelCellSize, sizeof(gpu::float3));

    gpu::host::DeallocateAll();
    assert( downCameraMatrix == cameraMatrix);
    assert( downInverseRotation == inverseRotation);
    assert( downRotationMatrix == rotationMatrix);
    assert( downTranslation == translation);
    assert( downVoxelCenter == voxelCenter);
    assert( downVoxelCellSize == voxelCellSize);
    assert( downVoxelGridSize == voxelGridSize);


    std::cout << "\tSuccessful" << std::endl << "Ending _TestTsdfUploads" << std::endl;
}