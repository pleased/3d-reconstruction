/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2017/02/27.
//

#include "FeatureBasedICPUnitTests.hpp"
static std::default_random_engine generator(0);
static std::normal_distribution<float> translationDistribution(0, 0.02f);
static std::normal_distribution<float> rotationDistribution(0, 0.01f);
// format is <source, destination>
std::vector<std::pair<gpu::float3, gpu::float3>> BuildPointCorrospondences(gpu::matrix33 rotationMatrix, gpu::float3 translation, bool noise=false) {
    std::vector<std::pair<gpu::float3, gpu::float3>> points;


    gpu::float3 basePoint(-1, -1, 1);

    gpu::matrix33 noiseRotation;
    noiseRotation.SetIdentity();
    gpu::float3 noiseTranslation(0, 0, 0);
    int i, j;
    int numbPoints = 40;
    float size = 2.f/numbPoints;

    for(i = 0; i < numbPoints; i++) {
        for(j = 0; j < numbPoints;j++) {
            if(noise) {
                noiseTranslation.x = translationDistribution(generator);
                noiseTranslation.y = translationDistribution(generator);
                noiseTranslation.z = translationDistribution(generator);
                noiseRotation = Utility::GenerateRotationMatrix33(rotationDistribution(generator), rotationDistribution(generator), rotationDistribution(generator));
            }

            gpu::float3 sourcePoint, destinationPoint;
            sourcePoint.x = basePoint.x + i * size;
            sourcePoint.y = basePoint.y + j * size;
            sourcePoint.z = basePoint.z;
            destinationPoint = noiseRotation * rotationMatrix * sourcePoint + (translation + noiseTranslation);
            points.push_back(std::pair<gpu::float3, gpu::float3>(sourcePoint, destinationPoint));
        }
    }
    return points;
}
void WarpImage(cv::Mat rgb, cv::Mat_<double> depthImage, cv::Mat& newRgb, cv::Mat_<double>& newDepth,
               gpu::matrix33 rotation, gpu::float3 translation, float fx, float fy, float cx, float cy) {

    std::cout << "Warp Translation " << translation.x << " " << translation.y << " " << translation.z << std::endl;
    int x, y;
    for(x = 0; x < newRgb.cols; x++) {
        for(y = 0; y < newRgb.rows; y++) {

            float depth = depthImage(y, x);
            if(depth < 1.e-4) {
                continue;
            }
            gpu::float3 point;
            //Project to world space
            point.x = (x - cx) * depth * (1./fx);
            point.y = (y - cy) * depth * (1./fy);
            point.z = depth;
            //Move point
            point = rotation * point + translation;

            //project to new image
            point.x = ((point.x * fx)/point.z) + cx;
            point.y = ((point.y * fy)/point.z) + cy;
            if(point.x < 0 && point.x >= newRgb.cols && point.y < 0 && point.y >= newRgb.cols) {
                continue;
            }
            newDepth((int) point.y, (int) point.x) = point.z;

            newRgb.at<cv::Vec3b>((int) point.y, (int) point.x) = rgb.at<cv::Vec3b>(y, x);
        }
    }


}
void ApplyInverseTransformationMulTransformation(gpu::matrix33 r1, gpu::float3 t1, gpu::matrix33 r2, gpu::float3 t2, gpu::matrix33& resultRotation, gpu::float3& resultTranslation) {

    gpu::matrix33 newR1 = r1.transpose();
    gpu::float3 newT1 = - (newR1 * t1);


    resultRotation = newR1 * r2;
    resultTranslation = newR1 * t2 + newT1;
}

void _TestFeatureBasedICP() {
    std::cout << "Starting _TestFeatureBasedRegistration" << std::endl;
    AssociatedICP featureBasedICP;
    gpu::matrix33 rotationMatrix, estimatedRotationMatrix, rotationDifference;
    gpu::float3 translation, estimatedTranslation, translationDifference;
    std::vector<std::pair<gpu::float3, gpu::float3>> points;
    clock_t begin, end;
    double elapsed_secs = double(end - begin) / CLOCKS_PER_SEC;

    //Basic Test
    std::cout << "\033[1;33mBasic Test\033[0m" << std::endl;
    rotationMatrix = Utility::GenerateRotationMatrix33(0.5, 0.1, 0);
    translation = gpu::float3(0.1, 0.2, 0.05);
    points = BuildPointCorrospondences(rotationMatrix, translation, false);
    begin = clock();
    featureBasedICP.ComputeICP(points, estimatedRotationMatrix, estimatedTranslation, true);
    end = clock();
    ApplyInverseTransformationMulTransformation(estimatedRotationMatrix, estimatedTranslation, rotationMatrix, translation, rotationDifference, translationDifference);
    std::cout << "Transformation Difference" << std::endl;
    std::cout << rotationDifference.row0.x << "\t" << rotationDifference.row0.y << "\t" << rotationDifference.row0.z << "\t" << translationDifference.x << std::endl;
    std::cout << rotationDifference.row1.x << "\t" << rotationDifference.row1.y << "\t" << rotationDifference.row1.z << "\t" << translationDifference.y << std::endl;
    std::cout << rotationDifference.row2.x << "\t" << rotationDifference.row2.y << "\t" << rotationDifference.row2.z << "\t" << translationDifference.z << std::endl;
    std::cout << "\033[1;32mTime: " << double(end - begin) / CLOCKS_PER_SEC << " with " << points.size() << " Points" "\033[0m" << std::endl;
    //With noise
    std::cout << "\033[1;33mBasic Test with noise\033[0m" << std::endl;
    rotationMatrix = Utility::GenerateRotationMatrix33(0.5, 0.1, 0);
    translation = gpu::float3(0.1, 0.2, 0.05);
    points = BuildPointCorrospondences(rotationMatrix, translation, true);
    begin = clock();
    featureBasedICP.ComputeICP(points, estimatedRotationMatrix, estimatedTranslation);
    end = clock();
    ApplyInverseTransformationMulTransformation(estimatedRotationMatrix, estimatedTranslation, rotationMatrix, translation, rotationDifference, translationDifference);
    std::cout << "Transformation Difference" << std::endl;
    std::cout << rotationDifference.row0.x << "\t" << rotationDifference.row0.y << "\t" << rotationDifference.row0.z << "\t" << translationDifference.x << std::endl;
    std::cout << rotationDifference.row1.x << "\t" << rotationDifference.row1.y << "\t" << rotationDifference.row1.z << "\t" << translationDifference.y << std::endl;
    std::cout << rotationDifference.row2.x << "\t" << rotationDifference.row2.y << "\t" << rotationDifference.row2.z << "\t" << translationDifference.z << std::endl;
    std::cout << "\033[1;32mTime: " << double(end - begin) / CLOCKS_PER_SEC << " with " << points.size() << " Points" "\033[0m" << std::endl;

    std::cout << "\033[1;33mHarder Test\033[0m" << std::endl;
    rotationMatrix = Utility::GenerateRotationMatrix33(M_PI, M_PI, M_PI);
    translation = gpu::float3(2, 5, 0.4);
    points = BuildPointCorrospondences(rotationMatrix, translation, false);
    begin = clock();
    featureBasedICP.ComputeICP(points, estimatedRotationMatrix, estimatedTranslation);
    end = clock();
    ApplyInverseTransformationMulTransformation(estimatedRotationMatrix, estimatedTranslation, rotationMatrix, translation, rotationDifference, translationDifference);
    std::cout << "Transformation Difference" << std::endl;
    std::cout << rotationDifference.row0.x << "\t" << rotationDifference.row0.y << "\t" << rotationDifference.row0.z << "\t" << translationDifference.x << std::endl;
    std::cout << rotationDifference.row1.x << "\t" << rotationDifference.row1.y << "\t" << rotationDifference.row1.z << "\t" << translationDifference.y << std::endl;
    std::cout << rotationDifference.row2.x << "\t" << rotationDifference.row2.y << "\t" << rotationDifference.row2.z << "\t" << translationDifference.z << std::endl;
    std::cout << "\033[1;32mTime: " << double(end - begin) / CLOCKS_PER_SEC << " with " << points.size() << " Points" "\033[0m" << std::endl;
//
//
//    rotationMatrix = Utility::GenerateRotationMatrix33(0.5, 0.1, 0);
//    translation = gpu::float3(0.1, 0.2, 0.05);
//    points = BuildPointCorrospondences(rotationMatrix, translation, false);
//    featureBasedICP.ComputeICP(points, estimatedRotationMatrix, estimatedTranslation);
//
//    ApplyInverseTransformationMulTransformation(estimatedRotationMatrix, estimatedTranslation, rotationMatrix, translation, rotationDifference, translationDifference);
//    std::cout << "Transformation Difference" << std::endl;
//    std::cout << rotationDifference.row0.x << "\t" << rotationDifference.row0.y << "\t" << rotationDifference.row0.z << "\t" << translationDifference.x << std::endl;
//    std::cout << rotationDifference.row1.x << "\t" << rotationDifference.row1.y << "\t" << rotationDifference.row1.z << "\t" << translationDifference.y << std::endl;
//    std::cout << rotationDifference.row2.x << "\t" << rotationDifference.row2.y << "\t" << rotationDifference.row2.z << "\t" << translationDifference.z << std::endl;






//    std::cout << rotationMatrix.row0.x << " " << rotationMatrix.row0.y << " " << rotationMatrix.row0.z << " " << translation.x << std::endl;
//    std::cout << rotationMatrix.row1.x << " " << rotationMatrix.row1.y << " " << rotationMatrix.row1.z << " " << translation.y << std::endl;
//    std::cout << rotationMatrix.row2.x << " " << rotationMatrix.row2.y << " " << rotationMatrix.row2.z << " " << translation.z << std::endl;



    std::cout << "Ending _TestFeatureBasedRegistration" << std::endl;
}
void _TestFeatureBasedICPSystemModifiedImages() {
    std::cout << "_TestFeatureBasedICPSystemModifiedImages" << std::endl;
    cv::Mat prevRgb = cv::imread("test/1.png");
    cv::Mat_<double> prevDepth = cv::imread("test/1D.png", CV_LOAD_IMAGE_ANYDEPTH);
    prevDepth = prevDepth * (1./5000);
    float zero = 0;
    cv::Mat newRgb(prevRgb.rows, prevRgb.cols,prevRgb.type(), cv::Scalar(0));
    cv::Mat_<double> newDepth(prevDepth.rows, prevDepth.cols, zero);
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;

    gpu::matrix33 rotation;
    rotation = Utility::GenerateRotationMatrix33(0, 0, 0.1);
    gpu::float3 translation(0, 0, 0.3);
    WarpImage(prevRgb, prevDepth, newRgb, newDepth, rotation, translation, fx, fy, cx, cy);
//    prevDepth = prevDepth * 10000 / 65535.;
//    newDepth = newDepth * 15000 / 65535.;
//    cv::imshow("PrevDepth", prevDepth);
//    cv::imshow("newDepth", newDepth);
//    cv::imshow("Prevrgb", prevRgb);
//    cv::imshow("newrgb", newRgb);
//    cv::waitKey(0);
//    exit(0);
    FBICP fbicp(10, 5, 100, 10, 0.3, 5, fx, fy, cx, cy);
    clock_t begin, end;
    std::cout << "\033[1;33mBasic Test with noise\033[0m" << std::endl;
    fbicp.SetRaycasterImage(prevRgb, prevDepth);
    gpu::matrix33 estimatedRotation;
    gpu::float3 estimatedTranslation;
    begin = clock();
    fbicp.EstimateMotion(newRgb, newDepth, estimatedRotation, estimatedTranslation);
    end = clock();
    std::cout << "\033[1;33m" << double(end - begin)/CLOCKS_PER_SEC << "\033[0m" << std::endl;


    std::cout << rotation.row0.x << " " << rotation.row0.y << " " << rotation.row0.z << " " << translation.x << std::endl;
    std::cout << rotation.row1.x << " " << rotation.row1.y << " " << rotation.row1.z << " " << translation.y << std::endl;
    std::cout << rotation.row2.x << " " << rotation.row2.y << " " << rotation.row2.z << " " << translation.z << std::endl;
    std::cout << std::endl;
    std::cout << estimatedRotation.row0.x << " " << estimatedRotation.row0.y << " " << estimatedRotation.row0.z << " " << estimatedTranslation.x << std::endl;
    std::cout << estimatedRotation.row1.x << " " << estimatedRotation.row1.y << " " << estimatedRotation.row1.z << " " << estimatedTranslation.y << std::endl;
    std::cout << estimatedRotation.row2.x << " " << estimatedRotation.row2.y << " " << estimatedRotation.row2.z << " " << estimatedTranslation.z << std::endl;

}
void _TestFeatureBasedICPSystem() {
    cv::Mat prevRgb = cv::imread("test/1.png");
    cv::Mat_<double> prevDepth = cv::imread("test/1D.png", CV_LOAD_IMAGE_ANYDEPTH);
    prevDepth = prevDepth * (1./5000);
//    prevDepth = prevDepth / 65535.;
    cv::Mat rgb = cv::imread("test/2.png");
    cv::Mat_<double> depth = cv::imread("test/2D.png", CV_LOAD_IMAGE_ANYDEPTH);
//    depth = depth/65535.;
    depth = depth * (1./5000);
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    FBICP fbicp(10, 5, 100, 10, 0.3, 5, fx, fy, cx, cy);
    clock_t begin, end;
    std::cout << "\033[1;33mBasic Test with noise\033[0m" << std::endl;
    fbicp.SetRaycasterImage(prevRgb, prevDepth);
    gpu::matrix33 rotation;
    gpu::float3 translation;
    begin = clock();
    fbicp.EstimateMotion(rgb, depth, rotation, translation);
    end = clock();
    std::cout << "\033[1;33m" << double(end - begin)/CLOCKS_PER_SEC << "\033[0m" << std::endl;
    std::cout << rotation.row0.x << " " << rotation.row0.y << " " << rotation.row0.z << " " << translation.x << std::endl;
    std::cout << rotation.row1.x << " " << rotation.row1.y << " " << rotation.row1.z << " " << translation.y << std::endl;
    std::cout << rotation.row2.x << " " << rotation.row2.y << " " << rotation.row2.z << " " << translation.z << std::endl;
    std::cout << std::endl;

}
void _TestFeatureBasedICPSystem2() {
    cv::Mat prevRgb = cv::imread("TESTA.png");
    cv::Mat_<double> prevDepth = cv::imread("test/1D.png", CV_LOAD_IMAGE_ANYDEPTH);
    prevDepth = prevDepth * (1./5000);
//    prevDepth = prevDepth / 65535.;
    cv::Mat rgb = cv::imread("TESTB.png");
    cv::Mat_<double> depth = cv::imread("test/2D.png", CV_LOAD_IMAGE_ANYDEPTH);
//    depth = depth/65535.;
    depth = depth * (1./5000);
    float fx = 525.0f, // default
            fy = 525.0f,
            cx = 319.5f,
            cy = 239.5f;
    FBICP fbicp(10, 5, 100, 10, 0.3, 5, fx, fy, cx, cy);
    clock_t begin, end;
    std::cout << "\033[1;33mBasic Test with noise\033[0m" << std::endl;
    fbicp.SetRaycasterImage(prevRgb, prevDepth);
    gpu::matrix33 rotation;
    gpu::float3 translation;
    begin = clock();
    fbicp.EstimateMotion(rgb, depth, rotation, translation);
    end = clock();
    std::cout << "\033[1;33m" << double(end - begin)/CLOCKS_PER_SEC << "\033[0m" << std::endl;
    std::cout << rotation.row0.x << " " << rotation.row0.y << " " << rotation.row0.z << " " << translation.x << std::endl;
    std::cout << rotation.row1.x << " " << rotation.row1.y << " " << rotation.row1.z << " " << translation.y << std::endl;
    std::cout << rotation.row2.x << " " << rotation.row2.y << " " << rotation.row2.z << " " << translation.z << std::endl;
    std::cout << std::endl;

}
