/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/06/13.
//

#ifndef VSLAM_KINECTFUSIONUNITTESTS_HPP
#define VSLAM_KINECTFUSIONUNITTESTS_HPP
#include <iostream>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <eigen3/Eigen/Dense>
#include <cuda.h>
#include <cuda_runtime.h>
#include "../KinectFusion.hpp"
#include "../KinectFusion/TsdfVolume.hpp"
#include "../KinectFusion/RayCaster.hpp"
#include "../KinectFusion/DeviceTypes.hpp"
#include "../KinectFusion/IterativeClosestPoint.hpp"
#include "../KinectFusion/GlobalDevice.hpp"
#include "../KinectFusionViewer.hpp"
#include "../KinectFusion/KinectFusionViewerMemCopy.hpp"
#include "GlobalTestFunctions.hpp"
#include <math.h>

#include <opencv2/rgbd.hpp>
#include <pcl-1.8/pcl/point_types.h>
#include <pcl-1.8/pcl/point_cloud.h>
#include <pcl-1.8/pcl/io/pcd_io.h>
#include <pcl-1.8/pcl/visualization/cloud_viewer.h>
#include <pcl/io/pcd_io.h>
#include <opencv2/highgui.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/core/eigen.hpp>

#include <cassert>


void _TestKinectFusionCudaBilateralFilter();
void _TestKinectFusionCudaIterativeClosestPoint();
void _TestKinectFusionCudaIterativeClosestPointSwapTest();
void _TestKinectFusionCudaIterativeClosestPointMatrixReductions2();
void _TestKinectFusionCudaIterativeClosestPointMatrixReductions3();
void _TestKinectFusionCudaIterativeClosestPointMatrixReductions4();
void _TestKinectFusionCudaIterativeClosestPointTransformComparison();
void _TestKinectFusionCudaIterativeClosestPointSyntheticTransform();
void _TestKinectFusionCudaIterativeClosestPointFull();
void _TestKinectFusionCudaPackUnpackTsdf();
void _TestKinectFusionCudaVolumeProperties();
void _TestKinectFusionCudaTsdfVolumeVisualizer();
void _TestKinectFusionCudaTsdfVolumeAndRayCaster();
void _TestKinectFusionCudaTsdfVolumeAndRayCasterTogether();
void _TestKinectFusionCudaTsdfVolumeAndRayCasterToModel();
void _TestKinectFusionFull();
void _TestKinectFusionMovingIndex();
void _TestKinectFusionCudaMovingVolume();
void _TestKinectFusionCudaMovingVolumeWithRayCaster();
void _TestKinectFusionCudaMovingCenterWithRayCaster();
void _TestKinectFusionCudaResetTsdfVolumeSection();
void _TestKinectFusionCudaMovingVolumeResetTest();
void _TestKinectFusionCPUSectionRayCastingExperiment();
void _TestKinectFusionCudaSectionRayCastingStartingFromZeroAllAxes();
void _TestKinectFusionCudaSectionRayCastingFromEndGridAllAxes();
void _TestKinectFusionCudaSectionRayCastingXAxisTest();
void _TestKinectFusionCudaSectionRayCastingFullVolume();
void _TestKinectFusionCudaSectionRayCastingFullVolumeHoleFilling();
void _TestKinectFusionCudaSectionRayCastingFullVolumeHoleFillingFromTwoAngle();
void _TestKinectFusionCudaSectionRayCastingFullVolumeIgnorePlaneExtraction();
void _TestKinectFusionCudaSectionRayCastingNormalsWithCircle();
void _TestKinectFusionCudaSectionRayCastingNormalsCheckAllAxisWithCircle();
void _TestKinectFusionCudaSectionRayCastingNormalsWithCircle();
void _TestRestRaycastingStorageVolume();
void _TestSystem();
void _TestSystem2();

#endif //VSLAM_KINECTFUSIONUNITTESTS_HPP
