/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/05/30.
//

#include "FileParserUnitTests.hpp"

void _TestReadSynchronisedRGBDImages() {
    std::string baseDir, rgbFile, depthFile, groundTruthFile, imuFile;
    baseDir = "/home/jonny91289/rgbd_dataset_freiburg1_xyz/", rgbFile = "rgb.txt", depthFile = "depth.txt", groundTruthFile = "groundtruth.txt";
    IO::FileParser fileParser(baseDir, rgbFile, depthFile, 5.e-3);
    fileParser.EnableGroundTruth(groundTruthFile);
    std::vector<std::string> data;
    fileParser.ReadData(data);
    std::cout << data[IO::RGB] << std::endl << data[IO::Depth] << std::endl << data[IO::GrouthTruth] << std::endl;


    data.clear();
    IO::FileParser fileParser2(baseDir, rgbFile, depthFile, 5.e-2);
    fileParser2.EnableGroundTruth(groundTruthFile);
    while(fileParser2.ReadData(data) == 1) {
        std::cout << data[IO::RGB] << std::endl << data[IO::Depth] << std::endl << data[IO::GrouthTruth] << std::endl;
        exit(0);
    }

}