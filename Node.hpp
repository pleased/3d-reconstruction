/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2017/02/17.
//
#ifndef VSLAM_NODE_HPP
#define VSLAM_NODE_HPP
#include <vector>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include "KinectFusion/DeviceTypes.hpp"
#include <pcl-1.8/pcl/point_types.h>
#include <pcl-1.8/pcl/point_cloud.h>
/**
 * Note because of the way c++ compiles template classes the full implementation needs to be placed here
 */
//Node<gpu::matrix33, gpu::float3, pcl::PointCloud<pcl::PointXYZRGB>::Ptr>
class Node {
public:
    Node(gpu::matrix33 rotation, gpu::float3 translation, gpu::int3 voxelCenter, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud, gpu::float3* vertexMap, gpu::float3* normalMap, uchar* image, int rows, int cols) {
        _rotation = rotation;
        _translation = translation;
        _pointCloud = pointCloud;
        _voxelCenter = voxelCenter;
        _image = cv::Mat(rows, cols, CV_8UC3);
        _vertexMap = new gpu::float3[rows * cols];
        _normalMap = new gpu::float3[rows * cols];
        int i;
        for(i = 0; i < rows * cols; i++) {
            //stored as rgb, cv needs bgr format
            _image.data[i * 3 + 0] = image[i * 3 + 0];
            _image.data[i * 3 + 1] = image[i * 3 + 1];
            _image.data[i * 3 + 2] = image[i * 3 + 2];

            _vertexMap[i] = vertexMap[i];
            _normalMap[i] = normalMap[i];
        }
    }
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr GetPointCloud() {
        return _pointCloud;
    }
    gpu::matrix33 GetRotation() {
        return _rotation;
    }
    gpu::float3 GetTranslation() {
        return _translation;
    }
    cv::Mat GetImage() {
        return _image;
    }
//private:
    gpu::int3 _voxelCenter;
    gpu::matrix33 _rotation;
    gpu::float3 _translation;
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr _pointCloud;
    cv::Mat _image;
    gpu::float3* _vertexMap;
    gpu::float3* _normalMap;
};
#endif //VSLAM_NODE_HPP