/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2017/04/18.
//

#include "BallPivotAlgorithm.hpp"
struct FrontEdgeFinder {
    FrontEdgeFinder(BallPivotAlgorithm::FrontEdge *frontEdge): f(frontEdge) {

    }
    const BallPivotAlgorithm::FrontEdge* f;
    bool operator ()(BallPivotAlgorithm::FrontEdge a) {
        return std::get<0>(a).first == std::get<0>(*f).first && std::get<0>(a).second == std::get<0>(*f).second;
    }
};

bool operator ==(pcl::PointXYZRGBNormal a, pcl::PointXYZRGBNormal b) {
    return a.x == b.x && a.y == b.y && a.z == b.z && a.normal_x == b.normal_x && a.normal_y == b.normal_y && a.normal_z == b.normal_z;
}
pcl::PointXYZRGBNormal operator +(pcl::PointXYZRGBNormal a, pcl::PointXYZRGBNormal b) {
    pcl::PointXYZRGBNormal p;
    p.x = a.x + b.x;
    p.y = a.y + b.y;
    p.z = a.z + b.z;
    return p;
}
pcl::PointXYZRGBNormal operator -(pcl::PointXYZRGBNormal a, pcl::PointXYZRGBNormal b) {
    pcl::PointXYZRGBNormal p;
    p.x = a.x - b.x;
    p.y = a.y - b.y;
    p.z = a.z - b.z;
    return p;
}
pcl::PointXYZRGBNormal operator *(pcl::PointXYZRGBNormal a, float b) {
    pcl::PointXYZRGBNormal p;
    p.x = a.x * b;
    p.y = a.y * b;
    p.z = a.z * b;
    return p;
}
pcl::PointXYZRGBNormal operator *(float b, pcl::PointXYZRGBNormal a) {
    pcl::PointXYZRGBNormal p;
    p.x = a.x * b;
    p.y = a.y * b;
    p.z = a.z * b;
    return p;
}
gpu::float3 makeXYZGPUFloat(pcl::PointXYZRGBNormal p) {
    return gpu::float3(p.x, p.y, p.z);
}
gpu::float3 makeNormalGPUFloat(pcl::PointXYZRGBNormal p) {
    return gpu::float3(p.normal_x, p.normal_y, p.normal_z);
}
pcl::PointXYZRGBNormal makePCLXYZ(gpu::float3 p) {
    pcl::PointXYZRGBNormal r;
    r.x = p.x;
    r.y = p.y;
    r.z = p.z;
    return r;
}

pcl::PointXYZRGBNormal makePCLNormal(gpu::float3 p) {
    pcl::PointXYZRGBNormal r;
    r.normal_x = p.x;
    r.normal_y = p.y;
    r.normal_z = p.z;
    return r;
}
pcl::PointXYZRGBNormal makePCLXYZRGBNormal(gpu::float3 xyz, gpu::float3 normal) {
    pcl::PointXYZRGBNormal r;
    r.x = xyz.x;
    r.y = xyz.y;
    r.z = xyz.z;
    r.normal_x = normal.x;
    r.normal_y = normal.y;
    r.normal_z = normal.z;
    return r;
}

//Sorting
static float EuclideanDistance(const gpu::float3* lhs, const gpu::float3* rhs) {
    float x = lhs->x - rhs->x;
    float y = lhs->y - rhs->y;
    float z = lhs->z - rhs->z;
    return sqrtf(x * x + y * y + z * z);
}
struct DistanceFunc
{
    DistanceFunc(const gpu::float3& _p) : p(_p) {}
    bool operator()(const std::pair<gpu::float3*, gpu::float3*>& lhs, const std::pair<gpu::float3*, gpu::float3*>& rhs) const
    {
        return EuclideanDistance(&p, lhs.first) < EuclideanDistance(&p, rhs.first);
    }

private:
    gpu::float3 p;
};
static void sortByDistance(const gpu::float3& queryPoint, std::vector<std::pair<gpu::float3*, gpu::float3*>>& points) {
    std::sort(points.begin(), points.end(), DistanceFunc(queryPoint));
}





const short BallPivotAlgorithm::NONE = 0;
const short BallPivotAlgorithm::ACTIVE = 1;
const short BallPivotAlgorithm::BOUNDARY = 2;
const short BallPivotAlgorithm::FROZEN = 4;
std::ostream& operator<<(std::ostream& ostr, BallPivotAlgorithm::FrontEdge frontEdge)
{

    ostr << " " << std::get<0>(frontEdge).first << " " << std::get<0>(frontEdge).second << " " << std::get<1>(frontEdge) << " center: (" <<
    std::get<2>(frontEdge).x << ", " << std::get<2>(frontEdge).y << ", " << std::get<2>(frontEdge).z << ")";
    switch(std::get<3>(frontEdge)) {
        case BallPivotAlgorithm::BOUNDARY:
            ostr << " Boundary";
            break;
        case BallPivotAlgorithm::ACTIVE:
            ostr << " Active";
            break;
        case BallPivotAlgorithm::FROZEN:
            ostr << " Frozen";
            break;
        case BallPivotAlgorithm::NONE:
            ostr << " None";
            break;
    }
    return ostr;
}

BallPivotAlgorithm::BallPivotAlgorithm(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud) {
    _cloud = cloud;
    _pointSize = cloud.get()->points.size();
    _kdtree.setInputCloud(cloud);
    _mask = new bool[_pointSize];

//    float radius = 0.003;
//    pcl::PointXYZRGBNormal searchPoint;
//    searchPoint.x = -0.0096f;
//    searchPoint.y = 0.03856f;
//    searchPoint.z = 0.02657f;
//    std::vector<int> pointIdxRadiusSearch;
//    std::vector<float> pointRadiusSquaredDistance;
//    int search = _kdtree.radiusSearch (searchPoint, radius, pointIdxRadiusSearch, pointRadiusSquaredDistance);
//    int i = 0;
//    for( i = 0; i < search; i++) {
//        int index = pointIdxRadiusSearch[i];
//        std::cout << "distance: " << pointRadiusSquaredDistance[i] << std::endl;
//        std::cout << _cloud.get()->points[index].x << " " << _cloud.get()->points[index].y << " "<< _cloud.get()->points[index].z << std::endl;
//    }

//    std::cout << search << std::endl;
//    std::cout << "Constructor Exiting" << std::endl;
//    exit(0);
//    _pointSize = cloud.get()->points.size();
//    std::cout << "Cloud Size: " << _pointSize << std::endl;
//    _points = new gpu::float3[_pointSize];
//    _normals = new gpu::float3[_pointSize];
//    _mask = new bool[_pointSize];
//    int i;
//    for(i = 0; i < cloud.get()->points.size(); i++) {
//        pcl::PointXYZRGBNormal point = cloud.get()->points[i];
//        _points[i].x = point.x;
//        _points[i].y = point.y;
//        _points[i].z = point.z;
//        _normals[i].x = point.normal_x;
//        _normals[i].y = point.normal_y;
//        _normals[i].z = point.normal_z;
//    }
//    _allocatedPointsAndNormals = true;
}
BallPivotAlgorithm::BallPivotAlgorithm(gpu::float3* vertexMap, gpu::float3* normalMap, unsigned long size) {
    std::cerr << "Please use other constructor" << std::endl;
    exit(1);
    _points = vertexMap;
    _normals = normalMap;
    _pointSize = size;
    _mask = new bool[_pointSize];
}
BallPivotAlgorithm::~BallPivotAlgorithm() {
    if(_allocatedPointsAndNormals) {
        delete[] _points;
        delete[] _normals;
    }
    delete[] _mask;
}
std::vector<std::pair<gpu::float3*, gpu::float3*>> BallPivotAlgorithm::findClosestPoints(const gpu::float3& queryPoint) {
    std::vector<std::pair<gpu::float3*, gpu::float3*>> results;
    gpu::int3 pos = WorldToVoxel(queryPoint);
    int i, j, k;
    for(i = -1; i < 2; i++) {
        for(j = -1; j < 2; j++) {
            for(k = -1; k < 2; k++) {
                gpu::int3 tempPos = pos;
                tempPos.x += i;
                tempPos.y += j;
                tempPos.z += k;
                if( tempPos.x >= 0 && tempPos.x < _dimSize.x &&
                    tempPos.y >= 0 && tempPos.y < _dimSize.y &&
                    tempPos.z >= 0 && tempPos.z < _dimSize.z) {
                    results.insert(results.end(), _buckets[Index3D(tempPos)].begin(), _buckets[Index3D(tempPos)].end());
                }
            }
        }
    }
    //TODO remove query Point from the list
    return results;
}
//TODO Test
std::vector<std::pair<gpu::float3*, gpu::float3*>> BallPivotAlgorithm::findClosestPointsFromPoint(const gpu::float3* queryPoint) {
    std::vector<std::pair<gpu::float3*, gpu::float3*>> results = findClosestPoints(*queryPoint);
    int i;
    for(i = 0; i < results.size(); i++) {
        if(results[i].first == queryPoint) {
            results.erase(results.begin() + i);
            break;
        }
    }
    //TODO remove query Point from the list
    return results;
}

bool BallPivotAlgorithm::GetActiveEdge(FrontEdge** activeEdge) {
    std::cout << "Finding ActiveEdge" << std::endl;
    std::list<FrontEdge>::iterator iterator;
    long i;
    for(i = 0; i < _front.size(); i++) {
        for(iterator = _front[i].begin(); iterator != _front[i].end(); iterator++) {
            if(std::get<3>(*iterator) == ACTIVE) {
                *activeEdge = &(*iterator);
                _currentFrontSet = i;
                return true;
            }
        }
    }
    _currentFrontSet = -1;
    *activeEdge = NULL;
    return false;
}
bool BallPivotAlgorithm::NotUsed(int pointK) {
    assert(pointK < _pointSize);
    return !_mask[pointK];
}
bool BallPivotAlgorithm::OnFront(int pointK) {
    std::cout << "Starting OnFront" << std::endl;
    std::list<FrontEdge>::iterator iterator;
    bool onFront = false;
    long i;
    for(i = 0l; i < _front.size(); i++) {
        for(iterator = _front[i].begin(); iterator != _front[i].end(); iterator++) {
            if(std::get<0>(*iterator).first == pointK || std::get<0>(*iterator).second == pointK) {
                std::cout << "On front " << i << std::endl;
                onFront = true;
                break;
            }
        }
        if(onFront) {
            break;
        }
    }
    std::cout << "Ending OnFront" << std::endl;
    return onFront;
}
void BallPivotAlgorithm::BucketSort(float ballRadius) {
    std::cout << "Starting BucketSort" << std::endl;
    _voxelSize = 2 * ballRadius;
    std::cout << "Voxel Size: " << _voxelSize << std::endl;
    int i;
    gpu::float3 max = gpu::float3(-INFINITY, -INFINITY, -INFINITY); _min = gpu::float3(INFINITY, INFINITY, INFINITY);
    for(i = 0; i < _pointSize; i++) {
        if(_points[i].x > max.x) {
            max.x = _points[i].x;
        } if(_points[i].x < _min.x) {
            _min.x = _points[i].x;
        }

        if(_points[i].y > max.y) {
            max.y = _points[i].y;
        } if(_points[i].y < _min.y) {
            _min.y = _points[i].y;
        }

        if(_points[i].z > max.z) {
            max.z = _points[i].z;
        } if(_points[i].z < _min.z) {
            _min.z = _points[i].z;
        }
    }
    assert(max.x != -INFINITY);
    assert(max.y != -INFINITY);
    assert(max.z != -INFINITY);
    assert(_min.x != INFINITY);
    assert(_min.y != INFINITY);
    assert(_min.z != INFINITY);
    gpu::float3 dimSizeFloat = max - _min;
    dimSizeFloat = dimSizeFloat/_voxelSize;
    _dimSize = gpu::int3((int) std::ceil(dimSizeFloat.x) + 1, (int) std::ceil(dimSizeFloat.y) + 1, (int) std::ceil(dimSizeFloat.z) + 1);
    std::cout << "Min: " << _min.x << " " << _min.y << " " << _min.z << std::endl;
    std::cout << "Max: " << max.x << " " << max.y << " " << max.z << std::endl;
    std::cout << "Voxel Size: " << _voxelSize << std::endl;
    std::cout << "dimSizeFloat: " << dimSizeFloat.x << " " << dimSizeFloat.y << " " << dimSizeFloat.z << std::endl;
    std::cout << "voxelSize: " << _voxelSize << std::endl;
    std::cout << "dimSize: " << _dimSize.x << " " << _dimSize.y << " " << _dimSize.z << std::endl;
    _bucketSize = _dimSize.x * _dimSize.y * _dimSize.z;
    _buckets = new std::vector<std::pair<gpu::float3*, gpu::float3*>>[_bucketSize];

    for(i = 0; i < _pointSize; i++) {
//        std::cout << "Point: " << _points[i].x << " " << _points[i].y << " " << _points[i].z << std::endl;
//        std::cout << "index " << WorldToIndex3D(_points[i]) << std::endl;
//        if(WorldToIndex3D(_points[i]) != 0 ) {
//            std::cout << "index: " << WorldToIndex3D(_points[i]) << std::endl;
//        }
        _buckets[WorldToIndex3D(_points[i])].push_back(std::pair<gpu::float3*, gpu::float3*>(&(_points[i]), &(_normals[i])));
    }
//    std::cout << "testing" << std::endl;
//    gpu::float3 p(1, 1, 0);
//    std::cout << (p - _min).x << " " << (p - _min).y << " " << (p - _min).z << std::endl;
//    std::cout << (p - _min).x/_voxelSize << " " << (p - _min).y/_voxelSize << " " << (p - _min).z/_voxelSize << std::endl;
//    std::cout << WorldToIndex3D(p) << std::endl;
    std::cout << "Ending BucketSort" << std::endl;
}
bool BallPivotAlgorithm::CheckValidManifold(FrontEdge* edge, int newPoint) {
    std::cerr << "\033[31m Check If it will create a nonmanifold/nonorientation manifold\033[39m" << std::endl;
    FrontEdge* edgeKI;
    long frontIndex = -1;
    int pointI, pointJ, pointK;
    pointI = std::get<0>(*edge).first;
    pointK = newPoint;
    pointJ = std::get<0>(*edge).second;
    int newTriangle[3];
    newTriangle[0] = pointI;
    newTriangle[1] = pointK;
    newTriangle[2] = pointJ;
    int ik = 0, kj = 0, ji = 0;
    //Dirty search

    std::pair<int, int> newEdge1 = std::pair<int, int>(pointI, pointK);
    std::pair<int, int> newEdge2 = std::pair<int, int>(pointK, pointJ);
    std::pair<int, int> newEdge3 = std::pair<int, int>(pointJ, pointI);

    unsigned long i, j, k;
    for(i = 0l; i < _triangles.size(); i++) {
        int triangle[3];

        triangle[0] = std::get<0>(_triangles[i]);
        triangle[1] = std::get<1>(_triangles[i]);
        triangle[2] = std::get<2>(_triangles[i]);
        std::pair<int, int> edge1 = std::pair<int, int>(triangle[0], triangle[1]);
        std::pair<int, int> edge2 = std::pair<int, int>(triangle[1], triangle[2]);
        std::pair<int, int> edge3 = std::pair<int, int>(triangle[2], triangle[0]);

        std::pair<int, int> edge1R = std::pair<int, int>(triangle[1], triangle[0]);
        std::pair<int, int> edge2R = std::pair<int, int>(triangle[2], triangle[1]);
        std::pair<int, int> edge3R = std::pair<int, int>(triangle[0], triangle[2]);

        if((newEdge1 == edge1) || (newEdge1 == edge1R)) {
            ik++;
        } else if((newEdge1 == edge2) || (newEdge1 == edge2R)) {
            ik++;
        } else if((newEdge1 == edge3) || (newEdge1 == edge3R)) {
            ik++;
        }

        if((newEdge2 == edge1) || (newEdge2 == edge1R)) {
            kj++;
        } else if((newEdge2 == edge2) || (newEdge2 == edge2R)) {
            kj++;
        } else if((newEdge2 == edge3) || (newEdge2 == edge3R)) {
            kj++;
        }

        if((newEdge3 == edge1) || (newEdge3 == edge1R)) {
            ji++;
        } else if((newEdge3 == edge2) || (newEdge3 == edge2R)) {
            ji++;
        } else if((newEdge3 == edge3) || (newEdge3 == edge3R)) {
            ji++;
        }
    }
    bool invalid = (ik == 2) || (kj == 2) || (ji == 2);

    //Check if front has the edge already
    FrontEdge* frontEdge;
    invalid = invalid | FindEdge(pointI, pointK,&frontEdge, frontIndex);
    invalid = invalid | FindEdge(pointK, pointJ,&frontEdge, frontIndex);

//    std::cout << "Finding edge " << newPoint << " " << std::get<0>(*edge).first << std::endl;
//    FindEdge(newPoint, std::get<0>(*edge).first, &edgeKI, frontIndex);
//    std::cout << "Found Other edge at ? " << frontIndex << std::endl;
//    std::cout << "Finding edge " << std::get<0>(*edge).second << " " << newPoint << std::endl;
//    FindEdge(std::get<0>(*edge).second, newPoint, &edgeKI, frontIndex);
//    std::cout << "Found Other edge at ? " << frontIndex << std::endl;
//    bool invalid = (FindEdge(newPoint, std::get<0>(*edge).first, &edgeKI, frontIndex) && FindEdge(std::get<0>(*edge).second, newPoint, &edgeKI, frontIndex)) ;
    return !invalid;
}
void BallPivotAlgorithm::performJoin(FrontEdge* edge, int newPoint, gpu::float3 newCenter, FrontEdge** edgeIK, FrontEdge** edgeKJ) {



    std::list<FrontEdge>::iterator it = std::find_if(_front[_currentFrontSet].begin(), _front[_currentFrontSet].end(), FrontEdgeFinder(edge));
    if(it == _front[_currentFrontSet].end()) {

        std::cout << "Cant find it on current front" << std::endl;
    }
    std::cout << "Workding on front " << _currentFrontSet << std::endl;
    int pi = std::get<0>(*edge).first;
    int pj = std::get<0>(*edge).second;

    std::cout << "Join" << std::endl;
    std::cout << "Adding edge " << pi << " -> " << newPoint << std::endl;
    std::cout << "Adding edge " << newPoint << " -> " << pj << std::endl;
    std::cout << "Removing edge " << pi << " -> " << pj << std::endl;
    std::cout << "Join" << std::endl;

    it = _front[_currentFrontSet].insert(it, FrontEdge(Edge(pi, newPoint), pj, makePCLXYZRGBNormal(newCenter, gpu::float3(0, 0, 0)), ACTIVE));
    *edgeIK = &(*it);
    std::advance(it, 2);
    it = _front[_currentFrontSet].insert(it, FrontEdge(Edge(newPoint, pj), pi, makePCLXYZRGBNormal(newCenter, gpu::float3(0, 0, 0)), ACTIVE));
    *edgeKJ = &(*it);
    it--;
    _front[_currentFrontSet].erase(it);
//    gpu::float3* pi = std::get<0>(*edge).first;
//    gpu::float3* pj = std::get<0>(*edge).second;
//
//    _front.push_back(FrontEdge(Edge(pi, newPoint), pj, newCenter, NULL, NULL, ACTIVE));
//    *edgeIK = &(_front.back());
//    _front.push_back(FrontEdge(Edge(newPoint, pj), pi, newCenter, NULL, NULL, ACTIVE));
//    *edgeKJ = &(_front.back());
//
//    std::get<3>(**edgeIK) = std::get<3>(*edge);
//    std::get<4>(**edgeIK) = *edgeKJ;
//    std::get<3>(**edgeKJ) = *edgeIK;
//    std::get<4>(**edgeKJ) = std::get<4>(*edge);
//
//    //Get Prev node and set its next
//    FrontEdge* prev = (FrontEdge*) std::get<3>(*edge);
//    std::get<4>(*prev) = *edgeIK;
//
//    //Get Next node and set its prev
//    FrontEdge* next = (FrontEdge*) std::get<4>(*edge);
//    std::get<3>(*next) = *edgeKJ;
//
//    _front.remove(*edge);
}
void BallPivotAlgorithm::MarkAsUsed(int point) {
//    unsigned long index = point - _points;
    assert(point >=0);
    assert(point < _pointSize);
    assert(!_mask[point]);
    _mask[point] = true;
}
//TODO Test
bool BallPivotAlgorithm::Join(FrontEdge* edge, int newPoint, gpu::float3 newCenter,
          FrontEdge** edgeIK, FrontEdge** edgeKJ) {
//    std::cout << "Join starting" << std::endl;
//    unsigned long index = newPoint - _points;
    assert(newPoint < _pointSize);
    int colour = 0;
    int pointI, pointJ, pointK;
    pointI = std::get<0>(*edge).first;
    pointJ = std::get<0>(*edge).second;
    pointK = newPoint;
    if(!_mask[newPoint]) {
//        std::cout << "Join 1" << std::endl;
        performJoin(edge, newPoint, newCenter, edgeIK, edgeKJ);
        MarkAsUsed(newPoint);
        GetColour(colour, pointI, pointK, pointJ);
        _triangles.push_back(std::tuple<int, int, int, int>(pointI, pointK, pointJ, colour));
        return true;
    } else {

        bool belongsToFront = OnFront(newPoint);
        if(belongsToFront) {


            bool validManifold = CheckValidManifold(edge, newPoint);
            if(validManifold) {
                performJoin(edge, newPoint, newCenter, edgeIK, edgeKJ);
                GetColour(colour, pointI, pointK, pointJ);
                _triangles.push_back(std::tuple<int, int, int, int>(pointI, pointK, pointJ, colour));
                return true;
            } else {
//                std::cerr << "\033[31m Invaild Triangle will produce a nonmanifold/nonorientation surface\033[39m" << std::endl;
//                std::cout << "Marking As Boundary join test" << std::endl;
                MarkAsBoundary(edge);
            }
        } else {
            std::cout << "Marking As Boundary Join 3" << std::endl;
            MarkAsBoundary(edge);
            GetColour(colour, pointI, pointK, pointJ);
            _triangles.push_back(std::tuple<int, int, int, int>(pointI, pointK, pointJ, colour));
//            std::cout << "Join ending" << std::endl;
            return false;
        }

    }
//    std::cout << "Join ending" << std::endl;
    return false;
}
long mod(long i, long n) {
    return ((i % n) + n ) % n;
}
//Testing complete
void BallPivotAlgorithm::Glue(FrontEdge* edgeAB, long& ABIndex, FrontEdge* edgeBA, long& BAIndex) {
//    std::cout << "Starting Glue" << std::endl;
//    std::cout << "Looking to remove " << std::endl << std::get<0>(*edgeAB).first << " -> " << std::get<0>(*edgeAB).second <<  " Loop index: " << ABIndex << std::endl;
//    std::cout << std::get<0>(*edgeBA).first << " -> " << std::get<0>(*edgeBA).second <<  " Loop index: " << BAIndex << std::endl;
    //TODO CHECK
    //edgeAB is on _currentFrontSet | edgeBA is on frontIndex
    if(BAIndex == ABIndex) {
//        std::cout << "In Same Loop" << std::endl;
        std::list<FrontEdge>::iterator PrevAB;
        std::list<FrontEdge>::iterator AB;
        std::list<FrontEdge>::iterator NextAB;
        std::list<FrontEdge>::iterator PrevBA ;
        std::list<FrontEdge>::iterator BA;
        std::list<FrontEdge>::iterator NextBA;
        PrevAB = NextAB = AB = std::find_if(_front[ABIndex].begin(), _front[ABIndex].end(), FrontEdgeFinder(edgeAB));
        PrevBA = BA = NextBA =  std::find_if(_front[ABIndex].begin(), _front[ABIndex].end(), FrontEdgeFinder(edgeBA));

        if(&(*_front[ABIndex].begin()) == &(*AB)) {
            PrevAB = --_front[ABIndex].end();
        } else {
            PrevAB--;
        }
        if(&(*(--_front[ABIndex].end())) == &(*AB)) {
            NextAB = _front[ABIndex].begin();
        } else {
            NextAB++;
        }

        if(&(*_front[ABIndex].begin()) == &(*BA)) {
            PrevBA = --_front[ABIndex].end();
        } else {
            PrevBA--;
        }
        if(&(*(--_front[ABIndex].end())) == &(*BA)) {
            NextBA = _front[ABIndex].begin();
        } else {
            NextBA++;
        }
//        std::cout << "iterators: " << &(*PrevAB) << " " << &(*AB) << " " << &(*NextAB) << std::endl;
//        std::cout << "iterators: " << &(*PrevBA) << " " << &(*BA) << " " << &(*NextBA) << std::endl;
        //Condition A
        if(&(*NextAB) == &(*BA) && &(*PrevAB) == &(*BA) &&
           &(*NextBA) == &(*AB) && &(*PrevBA) == &(*AB)) {
//            std::cout << "Condition ClosedLoop" << std::endl;
            //TODO remove AB and BA
            assert(ABIndex == BAIndex);
            _front[ABIndex].erase(std::find_if(_front[ABIndex].begin(), _front[ABIndex].end(), FrontEdgeFinder(edgeAB)));
            _front[ABIndex].erase(std::find_if(_front[ABIndex].begin(), _front[ABIndex].end(), FrontEdgeFinder(edgeBA)));
            return;
        }
        //ConditionB
        //condition B edgeAB -> edgeBA
        if(&(*NextAB) == edgeBA) {
//            std::cout << "Condition Consecutive Edges" << std::endl;
            //remove both edges
            assert(ABIndex == BAIndex);
            _front[ABIndex].erase(std::find_if(_front[ABIndex].begin(), _front[ABIndex].end(), FrontEdgeFinder(edgeAB)));
            _front[ABIndex].erase(std::find_if(_front[ABIndex].begin(), _front[ABIndex].end(), FrontEdgeFinder(edgeBA)));
            //        std::cout << "Ending Glue" << std::endl;
            return;
        } else if(&(*NextBA) == edgeAB) { //condition B edgeBA -> edgeAB
//            std::cout << "Condition Consecutive Edges" << std::endl;
            assert(ABIndex == BAIndex);
            _front[ABIndex].erase(std::find_if(_front[ABIndex].begin(), _front[ABIndex].end(), FrontEdgeFinder(edgeAB)));
            _front[ABIndex].erase(std::find_if(_front[ABIndex].begin(), _front[ABIndex].end(), FrontEdgeFinder(edgeBA)));
            //        std::cout << "Ending Glue" << std::endl;
            return;
        }
        // Same loop
        // Condition C
//        std::cout << "Condition Split loops" << std::endl;
        assert(ABIndex == BAIndex);
//        _front.push_back(std::list<FrontEdge());
        //TODO Check
        bool found = false;
        std::list<FrontEdge>::iterator it;
        for(it = _front[ABIndex].begin(); it != _front[ABIndex].end(); it++) {
            if(edgeAB == &(*it) || edgeBA == &(*it)) {
                found = true;
                break;
            }
        }
        it++;
        assert(found);
        found = false;
        std::list<FrontEdge>::iterator it2 = it;
        for(; it2 != _front[ABIndex].end(); it2++) {
            std::cout << &(*it2) << std::endl;
            if(edgeAB == &(*it2) || edgeBA == &(*it2)) {
//                std::cout << "Found " << std::endl;
                found = true;
                break;
            }
        }
        assert(found);
//        std::cout << &(*it) << " " << &(*it2) << std::endl;
        assert(&(*it) != &(*it2));
        assert(it != _front[ABIndex].end());
        assert(it2 != _front[ABIndex].end());
        _front.push_back(std::list<FrontEdge>());
        _front[_front.size() - 1].splice(_front[_front.size() - 1].begin(), _front[ABIndex], it, it2);

        _front[ABIndex].erase(std::find_if(_front[ABIndex].begin(), _front[ABIndex].end(), FrontEdgeFinder(edgeAB)));
        _front[ABIndex].erase(std::find_if(_front[ABIndex].begin(), _front[ABIndex].end(), FrontEdgeFinder(edgeBA)));
        return;
    } else {
//        std::cout << "Condition Merge Loops" << std::endl;
        //In different loop
        //Condition D
        assert(BAIndex != ABIndex);
        //Merge Loop TODO
        std::list<FrontEdge>::iterator M1 = std::find_if(_front[BAIndex].begin(), _front[BAIndex].end(), FrontEdgeFinder(edgeBA));
        std::list<FrontEdge>::iterator M2 = std::find_if(_front[ABIndex].begin(), _front[ABIndex].end(), FrontEdgeFinder(edgeAB));
//        std::cout << "Looking for " << std::get<0>(*edgeAB).first->x << " " << std::get<0>(*edgeAB).first->y << " " << std::get<0>(*edgeAB).first->z << " -> " << std::get<0>(*edgeAB).second->x << " " << std::get<0>(*edgeAB).second->y << " " << std::get<0>(*edgeAB).second->z << std::endl;
//        std::cout << "Looking for " << std::get<0>(*edgeBA).first->x << " " << std::get<0>(*edgeBA).first->y << " " << std::get<0>(*edgeBA).first->z << " | " << std::get<0>(*edgeBA).second->x << " " << std::get<0>(*edgeBA).second->y << " " << std::get<0>(*edgeBA).second->z << std::endl;

//        std::cout << "FrontIndex" << std::endl;
        std::list<BallPivotAlgorithm::FrontEdge>::iterator iterator1 = _front[BAIndex].begin();
//        for(; iterator1 != _front[BAIndex].end(); iterator1++) {
//            std::cout << std::get<0>(*iterator1).first->x << " " << std::get<0>(*iterator1).first->y << " " << std::get<0>(*iterator1).first->z << " -> " << std::get<0>(*iterator1).second->x << " " << std::get<0>(*iterator1).second->y << " " << std::get<0>(*iterator1).second->z << std::endl;
//            std::cout << &(*iterator1) << std::endl;
//        }
//        std::cout << "Current" << std::endl;
        iterator1 = _front[ABIndex].begin();
//        for(; iterator1 != _front[ABIndex].end(); iterator1++) {
//            std::cout << std::get<0>(*iterator1).first->x << " " << std::get<0>(*iterator1).first->y << " " << std::get<0>(*iterator1).first->z << " -> " << std::get<0>(*iterator1).second->x << " " << std::get<0>(*iterator1).second->y << " " << std::get<0>(*iterator1).second->z << std::endl;
//            std::cout << &(*iterator1) << std::endl;
//        }
        FrontEdge* test = NULL;
        long testIndex = -1;
        FindEdge(std::get<0>(*edgeAB).first, std::get<0>(*edgeAB).first, &test,  testIndex);
        std::cout << test << " " << testIndex << std::endl;
        assert(M1 != _front[BAIndex].end());
        assert(M2 != _front[ABIndex].end());
        M1++;
        _front[ABIndex].splice(M2, _front[BAIndex], M1, _front[BAIndex].end());
        _front[ABIndex].splice(M2, _front[BAIndex], _front[BAIndex].begin(), _front[BAIndex].end());
//        std::cout << "After Splicing" << std::endl;
//        std::cout << "FrontIndex" << std::endl;
//        iterator1 = _front[frontIndex].begin();
//        for(; iterator1 != _front[frontIndex].end(); iterator1++) {
//            std::cout << std::get<0>(*iterator1).first->x << " " << std::get<0>(*iterator1).first->y << " " << std::get<0>(*iterator1).first->z << " -> " << std::get<0>(*iterator1).second->x << " " << std::get<0>(*iterator1).second->y << " " << std::get<0>(*iterator1).second->z << std::endl;
//        }
//        std::cout << "Current" << std::endl;
//        iterator1 = _front[_currentFrontSet].begin();
//        for(; iterator1 != _front[_currentFrontSet].end(); iterator1++) {
//            std::cout << std::get<0>(*iterator1).first->x << " " << std::get<0>(*iterator1).first->y << " " << std::get<0>(*iterator1).first->z << " -> " << std::get<0>(*iterator1).second->x << " " << std::get<0>(*iterator1).second->y << " " << std::get<0>(*iterator1).second->z << std::endl;
//        }
//        std::cout << "Spliced" << std::endl;
//        iterator1 = _front[_currentFrontSet].begin();
//        for(; iterator1 != _front[_currentFrontSet].end(); iterator1++) {
//            std::cout << std::get<0>(*iterator1).first->x << " " << std::get<0>(*iterator1).first->y << " " << std::get<0>(*iterator1).first->z << " -> " << std::get<0>(*iterator1).second->x << " " << std::get<0>(*iterator1).second->y << " " << std::get<0>(*iterator1).second->z << std::endl;
//        }
        _front[ABIndex].erase(std::find_if(_front[ABIndex].begin(), _front[ABIndex].end(), FrontEdgeFinder(edgeAB)));
        _front[ABIndex].erase(std::find_if(_front[ABIndex].begin(), _front[ABIndex].end(), FrontEdgeFinder(edgeBA)));
        return;
    }

}
void BallPivotAlgorithm::Glue(FrontEdge* edgeAB, FrontEdge* edgeBA, long& frontIndex) {
    Glue(edgeAB, _currentFrontSet, edgeBA, frontIndex);
}

void BallPivotAlgorithm::InsertEdges(int seedTrianglePointI, int seedTrianglePointJ, int seedTrianglePointK,
                 gpu::float3 center) {
    _front[_currentFrontSet].push_back(FrontEdge(Edge(seedTrianglePointI, seedTrianglePointJ), seedTrianglePointK, makePCLXYZRGBNormal(center, gpu::float3(0, 0, 0)), ACTIVE));
    _front[_currentFrontSet].push_back(FrontEdge(Edge(seedTrianglePointJ, seedTrianglePointK), seedTrianglePointI, makePCLXYZRGBNormal(center, gpu::float3(0, 0, 0)), ACTIVE));
    _front[_currentFrontSet].push_back(FrontEdge(Edge(seedTrianglePointK, seedTrianglePointI), seedTrianglePointJ, makePCLXYZRGBNormal(center, gpu::float3(0, 0, 0)), ACTIVE));
}
void BallPivotAlgorithm::MarkAsBoundary(FrontEdge* edge) {
    std::get<3> (*edge) = BOUNDARY;
}
void BallPivotAlgorithm::MarkAsActive(FrontEdge* edge) {
    std::get<3> (*edge) = ACTIVE;
}
bool BallPivotAlgorithm::FindEdge(int pointI, int pointJ, FrontEdge** pointer, long& frontIndex) {

    std::list<FrontEdge>::iterator iterator;
    int edgePointI, edgePointJ;
    long i;
    for(i = 0l; i < _front.size(); i++) {
        for(iterator = _front[i].begin(); iterator != _front[i].end(); iterator++) {
            edgePointI = std::get<0>(*iterator).first;
            edgePointJ = std::get<0>(*iterator).second;
            if(pointI == edgePointI && pointJ == edgePointJ) {
                *pointer = &(*iterator);
                frontIndex = i;
                return true;
            }
        }
    }
    frontIndex = -1;
    *pointer = NULL;
    return false;

}
bool BallPivotAlgorithm::FindEdgeOnFront(int pointI, int pointJ, long frontIndex, FrontEdge** pointer) {
    assert(frontIndex < _front.size());
    std::list<FrontEdge>::iterator iterator;
    int edgePointI, edgePointJ;
    for(iterator = _front[frontIndex].begin(); iterator != _front[frontIndex].end(); iterator++) {
        edgePointI = std::get<0>(*iterator).first;
        edgePointJ = std::get<0>(*iterator).second;
        if(pointI == edgePointI && pointJ == edgePointJ) {
            *pointer = &(*iterator);
            return true;
        }
    }
    return false;
}

bool BallPivotAlgorithm::SphereIsEmpty(gpu::float3& queryPoint, float radius, int p1, int p2, int p3, double scale) {


//    std::cout << "Checking sphere" << std::endl;
//    std::vector<std::pair<gpu::float3 *, gpu::float3 *>> closePoints = findClosestPoints(*queryPoint);
//    sortByDistance(*queryPoint, closePoints);
    std::vector<int> index;
    std::vector<float> distances;
    pcl::PointXYZRGBNormal qPoint = makePCLXYZRGBNormal(queryPoint, gpu::float3(0, 0, 0));
//    std::cout << "radius " << radius * scale << std::endl;
    int searchSize = _kdtree.radiusSearch(qPoint, radius * scale, index, distances);
    //
    bool pass = true;
//    std::cout << queryPoint->x << " " << queryPoint->y << " " << queryPoint->z << std::endl;
//    std::cout << closePoints.size() << std::endl;
//    float radiusThreshold = (radius - 1.e-3f) * (radius - 1.e-3f);
//    assert(searchSize >= 3);
    int i;
//    std::cout << "Search size: " << searchSize << std::endl;
//    assert(searchSize >= 3);
    for (i = 0; i < searchSize; i++) {
        if (index[i] == p1 || index[i] == p2 || index[i] == p3) {
            continue;
        } else {
//            if (distances[i] < radiusThreshold) {
//                pass = false;
//                break;
//            }
            gpu::float3 p = makeXYZGPUFloat(_cloud.get()->points[index[i]]) - queryPoint;
//            std::cout << std::fabs(p.norm() - radius) << std::endl;
            if(std::fabs(p.norm() - radius) > 1.e-3) {
                pass = false;
                break;
            }
        }
    }
    return pass;
}
void BallPivotAlgorithm::GenerateSphereCenter(gpu::float3& p1, gpu::float3& p2, gpu::float3& p3,
                          float sphereRadius, gpu::float3& center, float& circleRadius) {

    gpu::float3 p12 = p1 - p2;
    gpu::float3 p13 = p1 - p3;
    gpu::float3 p21 = p2 - p1;
    gpu::float3 p23 = p2 - p3;
    gpu::float3 p31 = p3 - p1;
    gpu::float3 p32 = p3 - p2;

    float p12Norm = p12.norm();
    float p23Norm = p23.norm();
    float p13Norm = p13.norm();
    float p12cp23Norm = p12.cross(p23).norm();
    circleRadius = (p12Norm * p23Norm * p31.norm())/(2 * p12cp23Norm);
    gpu::float3 e21 = p2 - p1;
    gpu::float3 e31 = p3 - p1;
    gpu::float3 normal = e21.cross(e31);

    float a = ((p23Norm*p23Norm) * p12.dot(p13)) / (2 * p12cp23Norm * p12cp23Norm);
    float b = ((p13Norm*p13Norm) * p21.dot(p23)) / (2 * p12cp23Norm * p12cp23Norm);
    float c = ((p12Norm*p12Norm) * p31.dot(p32)) / (2 * p12cp23Norm * p12cp23Norm);

    center = a * p1 + b * p2 + c * p3;
}
bool BallPivotAlgorithm::CheckNormalConsistency(gpu::float3& tempNormalA, gpu::float3& tempNormalB, gpu::float3& tempNormalC, gpu::float3& normal, const float normalThreshold) {
    bool consistent = (tempNormalA.dot(normal) > normalThreshold && tempNormalB.dot(normal) > normalThreshold && tempNormalC.dot(normal));
//    if(!consistent) {
//        (*normal) = *normal * -1;
//        normal->normalized();
//        consistent = (tempNormalA->dot(*normal) > normalThreshold && tempNormalB->dot(*normal) > normalThreshold && tempNormalC->dot(*normal) > normalThreshold);
//    }
    return consistent;
}
bool BallPivotAlgorithm::GetBallCenter(gpu::float3 p0, gpu::float3 n0, gpu::float3 p1, gpu::float3 n1, gpu::float3 p2, gpu::float3 n2,
                                       const float ballRadius, gpu::float3& center, bool& swap) {
    bool status = false;

    gpu::float3 e10 = p1 - p0;
    gpu::float3 e20 = p2 - p0;
//    std::cout << "E10: " << e10.x << " " << e10.y << " " << e10.z << std::endl;
//    std::cout << "E20: " << e20.x << " " << e20.y << " " << e20.z << std::endl;
    gpu::float3 normal = e10.normalized().cross(e20.normalized());

//    gpu::float3 otherNormal = e10.cross(e20);

//    std::cout << otherNormal.x << " " << otherNormal.y << " " << otherNormal.z << std::endl;
//    otherNormal = otherNormal.normalized();
//    std::cout << otherNormal.x << " " << otherNormal.y << " " << otherNormal.z << std::endl;
    // Calculate ball center only if points are not collinear
    if (normal.norm() > 1.e-3)
    {
        // Can only normalize after colinear check since A x B = 0 iff directions are the same or 180 degrees
        normal = normal.normalized();

        if(!CheckNormalConsistency(n0, n1, n2, normal, 0)) {
            swap = true;
            gpu::float3 temp = p0;
            p0 = p1;
            p1 = temp;

            temp = n0;
            n0 = n1;
            n1 = temp;


            e10 = p1 - p0;
            e20 = p2 - p0;
            normal = e10.cross(e20);
            normal = normal.normalized();
        }

        float radius;
        GenerateSphereCenter(p0, p1, p2, ballRadius, center, radius);
        double squaredDistance = ballRadius * ballRadius - radius * radius;
        if (squaredDistance > 0)
        {
            double distance = sqrt(fabs(squaredDistance));
            center = center + distance * normal;
//            std::cout << center.x << " " << center.y << " " << center.z << std::endl;
            status = true;
        } else {
//            std::cout << "SQRT dist too far " << std::endl;

        }
    } else {
//        std::cout << "Points are colinear" << std::endl;
    }
    return status;
}
bool BallPivotAlgorithm::FindSeedTriangle(int& pointA, int& pointB, int& pointC, const float ballRadius, gpu::float3& center) {
    std::cout << "Searching for new Seed Triangle" << std::endl;
    float normalThreshold = .5f;
    int i;
    for(i = 0; i < _pointSize; i++) {
        if(!_mask[i]) {

//            std::vector<std::pair<gpu::float3*, gpu::float3*>> closePoints = findClosestPointsFromPoint(&(_points[i]));
            std::vector<int> index;
            std::vector<float> distances;

            int searchSize = _kdtree.radiusSearch(_cloud.get()->points[i],  ballRadius * 1.3, index, distances);
            if(searchSize < 3) {
                continue;
            }
//            std::cout << "Search Size: " << searchSize << std::endl;
//            //build Potential Triangles
            gpu::float3 tempPointA = makeXYZGPUFloat(_cloud.get()->points[i]);
            gpu::float3 tempPointB;
            gpu::float3 tempPointC;
//
            gpu::float3 tempNormalA = makeNormalGPUFloat(_cloud.get()->points[i]);
            gpu::float3 tempNormalB;
            gpu::float3 tempNormalC;
            int j, k;
            for(j = 0; j < searchSize; j++) {
                if (i == index[j]) {
                    continue;
                }
//                tempPointB = makeXYZGPUFloat(_cloud.get()->points[index[j]]);
//                tempNormalB = makeNormalGPUFloat(_cloud.get()->points[index[j]]);
                for(k = 0; k < searchSize; k++) {
                    if( i == index[k] || index[j] == index[k] || _mask[index[j]] || _mask[index[k]]) {
                        continue;
                    }
                    //All point combinations here

//                    tempPointC = makeXYZGPUFloat(_cloud.get()->points[index[k]]);
//                    tempNormalC = makeNormalGPUFloat(_cloud.get()->points[index[k]]);

//                    std::cout << "Testing seed (" << i << ", " << index[j] << ", " << index[k] << std::endl;

                    std::vector<int> v;
                    v.push_back(i); v.push_back(index[j]); v.push_back(index[k]);
                    std::sort(v.begin(), v.end());
                    tempPointA = makeXYZGPUFloat(_cloud.get()->points[v[0]]);
                    tempNormalA = makeNormalGPUFloat(_cloud.get()->points[v[0]]);
                    tempPointB = makeXYZGPUFloat(_cloud.get()->points[v[1]]);
                    tempNormalB = makeNormalGPUFloat(_cloud.get()->points[v[1]]);
                    tempPointC = makeXYZGPUFloat(_cloud.get()->points[v[2]]);
                    tempNormalC = makeNormalGPUFloat(_cloud.get()->points[v[2]]);
                    gpu::float3 normal;
                    bool swap = false;
                    if(GetBallCenter(tempPointA, tempNormalA, tempPointB, tempNormalB, tempPointC, tempNormalC, ballRadius, center, swap)) {
                        if (SphereIsEmpty(center, ballRadius, i, index[j], index[k])) {

                            if(swap) {
                                pointA = v[1];
                                pointB = v[0];
                                pointC = v[2];
                            } else {
                                pointA = v[0];
                                pointB = v[1];
                                pointC = v[2];
                            }

//                            pointA = v[0];
//                            pointB = v[1];
//                            pointC = v[2];
//                            pointA = i;
//                            pointB = index[j];
//                            pointC = index[k];
                            return true;
                        } else {
//                            std::cout << "Sphere isnt empty " << std::endl;
                        }
                    } else {
//                        std::cout << "cant fit ball " << std::endl;
//                        std::cout << tempPointA.x << " " << tempPointA.y << " " << tempPointA.z << std::endl;
//                        std::cout << tempPointB.x << " " << tempPointB.y << " " << tempPointB.z << std::endl;
//                        std::cout << tempPointC.x << " " << tempPointC.y << " " << tempPointC.z << std::endl;
//                        exit(0);
                    }
                }
            }
        }
    }
    std::cout << "Cant find Seed" << std::endl;
//    std::cout << "Ending FindSeedTriangle" << " At end of methods" << std::endl;
    return false;
}
void BallPivotAlgorithm::ProjectPointOnPlane(gpu::float3 normal, gpu::float3 origin, gpu::float3 point, gpu::float3& projected_point) {
    float distance = (point - origin).dot(normal);
    projected_point = point - distance * normal;
}
bool BallPivotAlgorithm::BallPivot3(FrontEdge* EdgeAB, float ballRadius, int& pointC, gpu::float3& newCenter) {
//    std::cout << "Starting Ball Pivot Around edge" << std::endl;
    int v0 = std::get<0>(*EdgeAB).first;
    int v1 = std::get<0>(*EdgeAB).second;
//    std::cout << "Testing Edge (" << v0 << ", " << v1 << ")" << std::endl;
//    std::cout << v0 << " -> " << v1 << std::endl;
//    std::cout << v0 << " " << v1 << std::endl;
//    std::cout << _cloud.get()->points[v0].x << " " << _cloud.get()->points[v0].y << " " << _cloud.get()->points[v0].z
//    << " -> " << _cloud.get()->points[v1].x << " " << _cloud.get()->points[v1].y << " " << _cloud.get()->points[v1].z << std::endl;
//    exit(0);
    int n0 = v0;
    int n1 = v1;

    int oppositePoint = std::get<1>(*EdgeAB);
    int oppositePointNormal = oppositePoint;
    gpu::float3 oppositePointGPUFloat = makeXYZGPUFloat(_cloud.get()->points[oppositePoint]);
    gpu::float3 v0GPUFloat = makeXYZGPUFloat(_cloud.get()->points[v0]);
    gpu::float3 v1GPUFloat = makeXYZGPUFloat(_cloud.get()->points[v1]);
    gpu::float3 n0GPUFloat = makeNormalGPUFloat(_cloud.get()->points[v0]);
    gpu::float3 n1GPUFloat = makeNormalGPUFloat(_cloud.get()->points[v1]);

    gpu::float3 originalCenterPoint = makeXYZGPUFloat(std::get<2>(*EdgeAB));
    gpu::float3 midPoint = 0.5f * (v1GPUFloat + v0GPUFloat);

//    std::cout << "Mid Point: " << midPoint.x << " " << midPoint.y << " " << midPoint.z << std::endl;

    gpu::float3 v0mid = 100 * (makeXYZGPUFloat(_cloud.get()->points[v0]) - midPoint);
    gpu::float3 BallCenterTomid = 100 * (originalCenterPoint - midPoint);

//    y = v0mid.cross(ballcenter)
    //normal = ballcenter.cross(y)
    gpu::float3 normal = BallCenterTomid.cross(v0mid.cross(BallCenterTomid).normalized()).normalized();


    gpu::float3 zeroAngle;
    ProjectPointOnPlane(normal, midPoint, (oppositePointGPUFloat - midPoint).normalized(), zeroAngle);
    zeroAngle = (zeroAngle - midPoint).normalized();

    float circleRadius = sqrtf((oppositePointGPUFloat - midPoint).dot((oppositePointGPUFloat - midPoint)));

//    std::vector<std::pair<gpu::float3 *, gpu::float3 *>> closePoints = findClosestPoints(midPoint);
    std::vector<int> index;
    std::vector<float> squaredDistance;
    pcl::PointXYZRGBNormal midPointpcl = makePCLXYZ(midPoint);
    int searchSize = _kdtree.radiusSearch(midPointpcl, 2 * ballRadius, index, squaredDistance);
    int i;
    float smallestAngle = M_PI;
    pointC = -1;
    bool pass = false;
//    std::cout << circleRadius << " Search Size " << searchSize << std::endl;
    int consistencyFailures = 0;
    int isEmptyFailures = 0;
    int elseFailures = 0;
    int here = 0;
    int skipFailures = 0, elseBallFailure = 0;
    for (i = 0; i < searchSize; i++) {
//        std::cout << "I is " << i << std::endl;
        int newPointIndex = index[i];
        gpu::float3 newPoint = makeXYZGPUFloat(_cloud.get()->points[newPointIndex]);
        gpu::float3 newPointNormal = makeNormalGPUFloat(_cloud.get()->points[newPointIndex]);

//        std::cout << "New point: " << newPoint.x << " " << newPoint.y << " " << newPoint.z << std::endl;
//        std::cout << "Distance : " << squaredDistance[i] << std::endl;
//        gpu::float3 *newPoint = closePoints[i].first;
//        gpu::float3 *newPointNormal = &(_normals[newPointIndex - _points]);

        if (v0 == newPointIndex || v1 == newPointIndex || oppositePoint == newPointIndex) {
            skipFailures++;

            continue;
        }


//        std::cout << "Selected Point: " << newPoint->x << " " << newPoint->y << " " << newPoint->z << std::endl;
//        std::cout << "Selected Normal: " << newPointNormal->x << " " << newPointNormal->y << " " << newPointNormal->z <<
//        std::endl;
        if (std::sqrt((newPoint - midPoint).dot(newPoint - midPoint)) <= ballRadius) {
//        if ((newPoint - midPoint).dot(newPoint - midPoint) <= ballRadius) {
            gpu::float3 center;
            bool swap = false;
            if (GetBallCenter(v0GPUFloat, n0GPUFloat, v1GPUFloat, n1GPUFloat, newPoint, newPointNormal, ballRadius, center, swap)) {

                if (!SphereIsEmpty(center, ballRadius, v0, v1, newPointIndex)) {
                    isEmptyFailures++;
                    continue;
                }

//                if(swap) {
                    //TODO remove if this test doesnt work
//                    int temp = v0;
//                    v0 = v1;
//                    v1 = temp;
//                    temp = n0;
//                    n0 = n1;
//                    n1 = temp;
//                    gpu::float3 tempfloat = v0GPUFloat;
//                    v0GPUFloat = v1GPUFloat;
//                    v1GPUFloat = tempfloat;
//                    tempfloat = n0GPUFloat;
//                    n0GPUFloat = n1GPUFloat;
//                    n1GPUFloat = tempfloat;
//                }

                gpu::float3 e10 = v1GPUFloat - v0GPUFloat;
                gpu::float3 eNew0 = newPoint - v0GPUFloat;
                gpu::float3 faceNormal = eNew0.cross(e10).normalized();
                if (!CheckNormalConsistency(n0GPUFloat, n1GPUFloat, newPointNormal, faceNormal, 0)) {
                    consistencyFailures++;
                    continue;
                }

                gpu::float3 projectedCenter;
                ProjectPointOnPlane(normal, midPoint, center, projectedCenter);
                projectedCenter = projectedCenter - midPoint;
                float dotProduct = zeroAngle.dot(projectedCenter.normalized());
                float angle = acosf(dotProduct);
                here++;
                if (angle < smallestAngle) {
                    smallestAngle = angle;
                    pass = true;
                    pointC = newPointIndex;
                    newCenter = center;
//                    std::cout << "reported: " << pointC << " swap: " << swap << std::endl;
                }
            } else {
//                std::cout << "Cant fit a ball here" << std::endl;
                elseBallFailure++;
            }
        } else {
//            std::cout << "Too far" << std::endl;
            elseFailures++;
        }


    }
//    std::cout << "I value is " << i << std::endl;
//    std::cout << "Pass: " << pass << " Consistency Failures: " << consistencyFailures << " EmptyFailures " << isEmptyFailures <<
//            " Else failures " << elseFailures << " Else Ball Failure: " << elseBallFailure << " Here: " << here << " Skip failures " << skipFailures <<  std::endl;
//    exit(0);
    return pass;
}
bool BallPivotAlgorithm::CheckNextIteration(float radius) {
    int i;
//    std::string in;
//    std::cout << "Before NextIteration " << radius << std::endl;
//    std::cin >> in;
    bool atLeastOneMarked = false;
    for(i = 0; i < _front.size(); i++ ) {
        std::list<FrontEdge>::iterator it;
        for(it = _front[i].begin(); it != _front[i].end(); it++) {
            int p1, p2, p3;
            p1 = std::get<0>(*it).first;
            p2 = std::get<0>(*it).second;
            p3 = std::get<1>(*it);

            gpu::float3 center;
            bool swap = false;
            if(GetBallCenter(makeXYZGPUFloat(_cloud.get()->points[p1]), makeNormalGPUFloat(_cloud.get()->points[p1]),
                             makeXYZGPUFloat(_cloud.get()->points[p2]), makeNormalGPUFloat(_cloud.get()->points[p2]),
                             makeXYZGPUFloat(_cloud.get()->points[p3]), makeNormalGPUFloat(_cloud.get()->points[p3]),
                             radius, center, swap) &&
               SphereIsEmpty(center, radius, p1, p2, p3)
                    ) {
                MarkAsActive(&(*it));
                atLeastOneMarked = true;
            }
        }
    }
    if(atLeastOneMarked) {
//        if(_viewerEndEnabled) {
//            OpenViewer();
//            WaitForViewer();
//        if(_viewerEnable) {
//            UpdatePCLViewer(true);
//            CloseViewer();
//        std::this_thread::sleep_for(std::chrono::seconds(5));
//        }
//        std::cout << "Marked One edge" << std::endl;
    }
//    std::cin >> in;
    std::cout << "\033[32m Next Ball size " << radius << "\033[39m" << std::endl;
    return atLeastOneMarked;
}
void BallPivotAlgorithm::GetColour(int& colour, int p1, int p2, int p3) {
    uint8_t r1 = _cloud.get()->points[p1].r;
    uint8_t g1 = _cloud.get()->points[p1].g;
    uint8_t b1 = _cloud.get()->points[p1].b;

    uint8_t r2 = _cloud.get()->points[p2].r;
    uint8_t g2 = _cloud.get()->points[p2].g;
    uint8_t b2 = _cloud.get()->points[p2].b;

    uint8_t r3 = _cloud.get()->points[p3].r;
    uint8_t g3 = _cloud.get()->points[p3].g;
    uint8_t b3 = _cloud.get()->points[p3].b;

    int r = (r1) << 16;
    int g = (g1) << 8;
    int b = b1;
    colour = 0;
    colour = r | g | b;
}

void BallPivotAlgorithm::GetFrontListsAsGPUFloat(std::vector<std::vector<gpu::float3>>& listOfFronts) {
    int i, j;
    listOfFronts.resize(_front.size());
    for(i = 0; i < _front.size(); i++ ) {
        std::list<FrontEdge>::iterator it = _front[i].begin();
        int p1;
        while(it != _front[i].end()) {
            p1 = (std::get<0>(*it)).first;
            listOfFronts[i].push_back(makeXYZGPUFloat(_cloud.get()->points[p1]));
            it++;
        }
    }
}
void BallPivotAlgorithm::OpenViewer() {
    std::cout << "Test number: " << testNumber << std::endl;
    _viewerThread = new std::thread(&BallPivotAlgorithm::ViewFront, this);
}
void BallPivotAlgorithm::WaitForViewer() {
    while(true) {
        std::unique_lock<std::mutex> lock(_mutexViewer, std::try_to_lock);
        if(lock.owns_lock()) {
            if(_viewer != 0) {
                lock.unlock();
                break;
            }
            lock.unlock();
            std::this_thread::sleep_for(std::chrono::milliseconds(10));
        }
    }
}
void BallPivotAlgorithm::PclFrontViewerSetup() {
    while(true) {
        std::unique_lock<std::mutex> lock(_mutexViewer, std::try_to_lock);
        if(lock.owns_lock()) {
            pcl::PointCloud<pcl::PointXYZRGB>::Ptr t(new pcl::PointCloud<pcl::PointXYZRGB>());
            _viewerPointCloud = t;
            pcl::visualization::PCLVisualizer::Ptr viewer(new pcl::visualization::PCLVisualizer());
            _viewer = viewer;
            _viewer.get()->setBackgroundColor (0, 0, 0);
//            _viewer.get()->addPointCloud<pcl::PointXYZRGB> (_viewerPointCloud, "sample cloud");
            _viewer.get()->addPointCloud<pcl::PointXYZRGBNormal> (_cloud, "sample cloud");
            _viewer.get()->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 4, "sample cloud");
            _viewer.get()->addPointCloudNormals<pcl::PointXYZRGBNormal>(_cloud, 20, 0.04, "normals");
//            viewer.addPointCloudNormals<pcl::PointXYZRGBNormal>(filteredPointCloud, 10, 0.02, "normals");
            _viewer.get()->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 4, "normals");
            _viewer.get()->addCoordinateSystem (1.0);
            _viewer.get()->setCameraPosition(5, 5, 5, 1, -1, 1);
            _viewer.get()->updateCamera();
//            _viewer.get()->initCameraParameters();
            lock.unlock();
            break;
        }
    }

}
void BallPivotAlgorithm::ViewFront() {
    PclFrontViewerSetup();
    while(true) {
        std::unique_lock<std::mutex> lock(_mutexViewer, std::try_to_lock);
        if(lock.owns_lock()) {
            _viewer.get()->spinOnce ();
//            std::cout << "Spin" << std::endl;
            if(_viewer.get()->wasStopped()) {
                break;
            }
            lock.unlock();
        }
    }
}
void BallPivotAlgorithm::CloseViewer() {
    if(_viewerThread != 0) {
        _viewerThread->join();
        delete _viewerThread;
    }
}
void BallPivotAlgorithm::EnableViewer() {
    _viewerEnable = true;
}
void BallPivotAlgorithm::DisableViewer() {
    _viewerEnable = false;
}
void BallPivotAlgorithm::EnableEndViewer() {
    DisableViewer();
    _viewerEndEnabled = true;
}
void BallPivotAlgorithm::UpdatePCLViewer(bool showEdgeState) {
//    std::this_thread::sleep_for(std::chrono::seconds(20));
    std::unique_lock<std::mutex> lock(_mutexViewer, std::try_to_lock);
    while(!lock.owns_lock()) {
        if(lock.try_lock()) {
            break;
        }
    }
//    std::cout << "Updating Viewer" << std::endl;
//    _mutexViewer.unlock();
//    std::cout << "INIT VIEWER MEM: " << _viewer << " THREAD ID: " << std::this_thread::get_id() << std::endl;
//    std::cout << "INIT VIEWER MEM: " << _viewer << std::endl;
    _viewer.get()->removeAllShapes(0);
//    std::cout << "REmoved" << std::endl;
    int i, j, index, numberOfPoints;
    numberOfPoints = 0;
    j = 0;
    for(i = 0; i < _front.size(); i++) {
        //pick colour
        numberOfPoints += _front[i].size();
    }
    _viewerPointCloud->points.resize(numberOfPoints);
//    std::cout << "PointCloud Size : " << _viewerPointCloud->points.size() << std::endl;



    index = 0;
    srand(0);
    for(i = 0; i < _front.size(); i++) {
        //pick colour
//        std::cout << "Front " << i << " Size is: " << _front[i].size() <<  std::endl;

        int r = rand()%255; int g = rand()%255; int b = rand()%255;
        int start = index;
        std::list<FrontEdge>::iterator it;
        for(it = _front[i].begin(); it != _front[i].end(); it++) {

            assert(index < numberOfPoints);
            int pointIndex = std::get<0>(*it).first;
            _viewerPointCloud->points[index].x = _cloud->points[pointIndex].x;
            _viewerPointCloud->points[index].y = _cloud->points[pointIndex].y;
            _viewerPointCloud->points[index].z = _cloud->points[pointIndex].z;
            _viewerPointCloud->points[index].r = (uint8_t) r;
            _viewerPointCloud->points[index].g = (uint8_t) g;
            _viewerPointCloud->points[index].b = (uint8_t) b;
            std::stringstream ss;
            ss << "Front_" << i << "_Line_" << j;
            if(it != _front[i].begin() ) {
                if(showEdgeState) {
                    if(std::get<3>(*it) == BOUNDARY) {
                        r = 255;    g = 0;  b = 0;
                    } else if(std::get<3>(*it) == ACTIVE) {
                        r = 0;    g = 0;  b = 255;
                    } else if(std::get<3>(*it) == NONE) {
                        r = 0;    g = 255;  b = 0;
                    }
                }
                assert(index - 1 >= 0);
                _viewer->addLine<pcl::PointXYZRGB>(_viewerPointCloud->points[index - 1], _viewerPointCloud->points[index], r/255., g/255., b/255., ss.str());
            }
//            std::cout << (*it).x << " " << (*it).y << " " << (*it).z << std::endl;
//            std::cout << "Index: " << index << std::endl;
            index++;
            j++;
        }
        if(_front[i].size() > 0) {
            std::stringstream s2;
            s2 << "Front_" << i << "_Line_0";
            _viewer->addLine<pcl::PointXYZRGB>(_viewerPointCloud->points[start], _viewerPointCloud->points[index - 1], r/255., g/255., b/255., s2.str());
        }
    }
    lock.unlock();

//    std::this_thread::sleep_for(std::chrono::seconds(2));

    if(_viewerStopIteration != -1) {
        if(_viewerIteration >= _viewerStopIteration) {
            std::cout << "\033[1;31mPress any key to continue\033[0m" << std::endl;
            std::string in;
            std::cin >> in;
        }
    } else if(_viewerDelayTime == -1) {
        std::cout << "\033[1;31mPress any key to continue\033[0m" << std::endl;
        std::string in;
        std::cin >> in;
    } else {
        std::this_thread::sleep_for(std::chrono::milliseconds(_viewerDelayTime));
    }
    _viewerIteration++;


//    for(i = 0; i < 5;i++) {
//        viewer.addLine<pcl::PointXYZRGB>(pointCloud->points[i], pointCloud->points[i + 1], "line" + i);
//    }
}
void BallPivotAlgorithm::operator() (float* radii, int size) {
    std::cout << "Starting BallPivotAlgorithm" << std::endl;
    //INITI stuff
    if(_viewerEnable) {
        OpenViewer();
        WaitForViewer();
    }


    unsigned long maskSize = _pointSize;
    std::memset(_mask, 0, sizeof(bool) * maskSize);
    //Algorithm
    int i = 0;
    FrontEdge* activeEdge, *edgeIK, *edgeKJ;
    int pointI, pointJ, pointK;
    gpu::float3 newCenter;
    bool stop = false;


    int colour = 0;
    int repeat;
    int limit = 2;
    for(repeat = 0; repeat < limit; repeat++) { //TODO need to run untill no changes are present this fixes the small holes that appear in some cases due to front management
        std::cout << "\033[32m Repeating \033[39m" << std::endl;
        for(i = 0; i < size; i++) {
    //        BucketSort(radii[i]);
            CheckNextIteration(
                    radii[i]); // Resets the front edges to active state if a pball can fit in the original 3 points that define this edge
            if (_viewerEnable) {
                UpdatePCLViewer(true);
            }
            stop = false;
            while (!stop) {
                while (GetActiveEdge(&activeEdge)) {
                    if (BallPivot3(activeEdge, radii[i], pointK, newCenter)
                        && (NotUsed(pointK) || OnFront(pointK))) {
                        assert(_currentFrontSet > -1);
                        pointI = std::get<0>(*activeEdge).first;
                        pointJ = std::get<0>(*activeEdge).second;
    //                    GetColour(colour, pointI, pointK, pointJ);

    //                    _triangles.push_back(std::tuple<int, int, int, int>(pointI, pointK, pointJ, colour));



                        Join(activeEdge, pointK, newCenter, &edgeIK, &edgeKJ);

                        //PCL
                        if (_viewerEnable) {
//                            int test;
//                            std::cout << _front.size() << std::endl;
//                            for (test = 0; test < _front.size(); test++) {
//                                if (_front[test].size() == 0) {
//                                    continue;
//                                }
//                                std::list<FrontEdge>::iterator it;
//                                std::cout << _front[test].size() << std::endl;
//    //                        std::cout << (void*)_front[i].begin() << std::endl;
//    //                        std::cout << (void*)_front[i].end() << std::endl;
//                                for (it = _front[test].begin(); it != _front[test].end(); it++) {
//    //                            if(std::get<0>(*it).first == 17 && std::get<0>(*it).second == 19) {
//                                    std::cout << std::get<0>(*it).first << " - > " << std::get<0>(*it).second << " | ";
//    //                            }
//    //                            if(std::get<0>(*it).first == 19 && std::get<0>(*it).second == 17) {
//    //                                std::cout << std::get<0>(*it).first << " - > " << std::get<0>(*it).second << " | ";
//    //                            }
//                                }
//                                std::cout << std::endl;
//                            }
                            UpdatePCLViewer(true);
                        }
                        FrontEdge *edgeAB = NULL;
                        FrontEdge *edgeBA = NULL;
                        long frontABIndex = -1;
                        long frontBAIndex = -1;
                        // A == I | B == K
                        if (FindEdge(pointI, pointK, &edgeAB, frontABIndex) &&
                            FindEdge(pointK, pointI, &edgeBA, frontBAIndex)) {
    //                        std::cout << "Removing duplicates K, I" << std::endl;
    //                        std::cout << "in FindEdge" << std::endl;
    //                        std::cout << _front.size() << std::endl;
                            assert(frontABIndex != -1);
                            assert(frontBAIndex != -1);
                            Glue(edgeAB, frontABIndex, edgeBA, frontBAIndex);

                            if (_viewerEnable) {
                                UpdatePCLViewer(true);
                            }
                        }
                        edgeAB = NULL;
                        frontABIndex = -1;
                        //A == J | B == K
                        if (FindEdge(pointJ, pointK, &edgeAB, frontABIndex) &&
                            FindEdge(pointK, pointJ, &edgeBA, frontBAIndex)) {
    //                        std::cout << "In second find Edge" << std::endl;
                            assert(frontABIndex != -1);
                            assert(frontBAIndex != -1);
    //                    std::cout << "Removing duplicates K, J" << std::endl;
    //                    std::cout << _front.size() << std::endl;
                            Glue(edgeAB, frontABIndex, edgeBA, frontBAIndex);

                            if (_viewerEnable) {
                                UpdatePCLViewer(true);
                            }
                        }


                    } else {
                        std::cout << "Marking As Boundary" << std::endl;
                        MarkAsBoundary(activeEdge);
                    }
    //                std::cout << "Num of Triangles: " << _triangles.size() << std::endl;
                }
                if (FindSeedTriangle(pointI, pointJ, pointK, radii[i], newCenter)) {
                    assert(_currentFrontSet == -1);
    //                std::cout << "\tSeed Found (" << pointI << ", " << pointJ << ", " << pointK << ")" << std::endl;
    //            std::cout << pointI << " " << pointJ << " " << pointK << std::endl;

    //            std::cout << _cloud.get()->points[pointI].x << " " << _cloud.get()->points[pointI].y << " " << _cloud.get()->points[pointI].z << std::endl;
    //            std::cout << _cloud.get()->points[pointK].x << " " << _cloud.get()->points[pointK].y << " " << _cloud.get()->points[pointK].z << std::endl;
    //            std::cout << _cloud.get()->points[pointJ].x << " " << _cloud.get()->points[pointJ].y << " " << _cloud.get()->points[pointJ].z << std::endl;

                    GetColour(colour, pointI, pointJ, pointK);
                    _triangles.push_back(std::tuple<int, int, int, int>(pointI, pointJ, pointK, colour));
                    _front.push_back(std::list<FrontEdge>());
                    std::cout << "Previous Front index: " << _currentFrontSet << std::endl;
                    _currentFrontSet = _front.size() - 1;
                    std::cout << "adding to front index " << _currentFrontSet << std::endl;
                    MarkAsUsed(pointI);
                    MarkAsUsed(pointJ);
                    MarkAsUsed(pointK);
                    InsertEdges(pointI, pointJ, pointK, newCenter);

                    if (_viewerEnable) {
    //                    int j;
    //                    for(j = 0; j < _front.size(); j++) {
    //                        std::list<FrontEdge>::iterator it;
    //                        std::cout << "Front: " << j << std::endl;
    //                        for(it = _front[j].begin(); it != _front[j].end(); it++) {
    //                            std::cout << "Edge: " << std::get<0>(*it).first << " " << std::get<0>(*it).second << std::endl;
    //                        }
    //                    }
                        UpdatePCLViewer(true);
                    }
    //            std::cout << "Seed Triangle" << std::endl;
    //            std::cout << pointI->x << " " << pointI->y << " " << pointI->z << std::endl;
    //            std::cout << pointJ->x << " " << pointJ->y << " " << pointJ->z << std::endl;
    //            std::cout << pointK->x << " " << pointK->y << " " << pointK->z << std::endl;
    //            std::cout << "With center located at pos" << std::endl;
    //            std::cout << newCenter.x << " " << newCenter.y << " " << newCenter.z << std::endl;

    //            exit(0);
                } else {
    //            std::cout << "No seed Triangle found" << std::endl;
                    stop = true;
    //                stop = !CheckNextIteration(radii[i]);
    //                if(_viewerEnable) {
    //                    UpdatePCLViewer(true);
    //                    std::cout << "Sleepign" << std::endl;
    //                    std::this_thread::sleep_for(std::chrono::seconds(10));
    //                }
                }

            }
        }
        std::cout << "Iteration: " << i << std::endl;
        std::cout << "Triangles generated: " << _triangles.size() << std::endl;
    }


    std::cout << "Number of Triangles Generated: " << _triangles.size() << std::endl;
    //printing the Front

    std::cout << "Front Size: " << _front.size() << std::endl;

    Reformat();

    if(_viewerEnable) {
//        while(true) {
//            std::unique_lock<std::mutex> lock(_mutexViewer, std::try_to_lock);
//            if(lock.owns_lock()) {
//                _viewer->spinOnce ();
//                if(_viewer->wasStopped()) {
//                    break;
//                }
//                lock.unlock();
//            }
//        }
        UpdatePCLViewer(true);
        std::string tests;
        std::cin >> tests;
        CloseViewer();
    }
    if(_viewerEndEnabled) {
        OpenViewer();
        WaitForViewer();
        UpdatePCLViewer(true);
        CloseViewer();
    }
    std::cout << "Ending BallPivotAlgorithm" << std::endl;
}


void BallPivotAlgorithm::operator() (float ballRadius) {
    std::cerr << "Depricated: void BallPivotAlgorithm::operator() (float ballRadius)" << std::endl;
    exit(0);
    std::cout << "Starting BallPivotAlgorithm" << std::endl;
    //INITI stuff


//    BucketSort(ballRadius);
//    exit(0);
    unsigned long maskSize = _pointSize;
    std::memset(_mask, 0, sizeof(bool) * maskSize);
    //Algorithm
    int i = 0;
    FrontEdge* activeEdge, *edgeIK, *edgeKJ;
    int pointI, pointJ, pointK;
    gpu::float3 newCenter;
    bool stop = false;


    int colour = 0;
    while(!stop) {
        while(GetActiveEdge(&activeEdge)) {
            if(BallPivot3(activeEdge, ballRadius, pointK, newCenter)
               && (NotUsed(pointK) || OnFront(pointK))) {
                std::cout << "in accept" << std::endl;
                std::cout << pointK << std::endl;
//                exit(0);
                pointI = std::get<0>(*activeEdge).first;
                pointJ = std::get<0>(*activeEdge).second;
                GetColour(colour, pointI, pointK, pointJ);
                _triangles.push_back(std::tuple<int, int, int, int>(pointI, pointK, pointJ, colour));
                Join(activeEdge, pointK, newCenter, &edgeIK, &edgeKJ);


                FrontEdge* edgeAB = NULL;
                FrontEdge* edgeBA = NULL;
                long frontABIndex = -1;
                long frontBAIndex = -1;
                // A == I | B == K
                if(FindEdge(pointI, pointK, &edgeAB, frontABIndex) && FindEdge( pointK, pointI, &edgeBA, frontBAIndex)) {
                    std::cout << "Removing duplicates K, I" << std::endl;
//                        std::cout << "in FindEdge" << std::endl;
//                        std::cout << _front.size() << std::endl;
                    assert(frontABIndex != -1);
                    assert(frontBAIndex != -1);
                    Glue(edgeAB, frontABIndex, edgeBA, frontBAIndex);
                }
                edgeAB = NULL;
                frontABIndex = -1;
                //A == J | B == K
                if(FindEdge( pointJ, pointK, &edgeAB, frontABIndex) && FindEdge(pointK, pointJ, &edgeBA, frontBAIndex)) {
//                        std::cout << "In second find Edge" << std::endl;
                    assert(frontABIndex != -1);
                    assert(frontBAIndex != -1);
//                    std::cout << "Removing duplicates K, J" << std::endl;
//                    std::cout << _front.size() << std::endl;
                    Glue(edgeAB, frontABIndex, edgeBA, frontBAIndex);
                }

            } else {
                std::cout << "Marking As Boundary" << std::endl;
                MarkAsBoundary(activeEdge);
            }
            std::cout << "Num of Triangles: " << _triangles.size() << std::endl;
        }
        if (FindSeedTriangle(pointI, pointJ, pointK, ballRadius, newCenter)) {
            std::cout << "\tSeed Found (" << pointI << ", " << pointJ << ", " << pointK << ")" << std::endl;
//            std::cout << pointI << " " << pointJ << " " << pointK << std::endl;

//            std::cout << _cloud.get()->points[pointI].x << " " << _cloud.get()->points[pointI].y << " " << _cloud.get()->points[pointI].z << std::endl;
//            std::cout << _cloud.get()->points[pointK].x << " " << _cloud.get()->points[pointK].y << " " << _cloud.get()->points[pointK].z << std::endl;
//            std::cout << _cloud.get()->points[pointJ].x << " " << _cloud.get()->points[pointJ].y << " " << _cloud.get()->points[pointJ].z << std::endl;

            GetColour(colour, pointI, pointJ, pointK);
            _triangles.push_back(std::tuple<int, int, int, int>(pointI, pointJ, pointK, colour));
            _front.push_back(std::list<FrontEdge>());
            _currentFrontSet = _front.size() - 1;
//            std::cout << "Entered" << std::endl;
            MarkAsUsed(pointI);
            MarkAsUsed(pointJ);
            MarkAsUsed(pointK);
            InsertEdges(pointI, pointJ, pointK, newCenter);

//            std::cout << "Seed Triangle" << std::endl;
//            std::cout << pointI->x << " " << pointI->y << " " << pointI->z << std::endl;
//            std::cout << pointJ->x << " " << pointJ->y << " " << pointJ->z << std::endl;
//            std::cout << pointK->x << " " << pointK->y << " " << pointK->z << std::endl;
//            std::cout << "With center located at pos" << std::endl;
//            std::cout << newCenter.x << " " << newCenter.y << " " << newCenter.z << std::endl;

//            exit(0);
        } else {
//            std::cout << "No seed Triangle found" << std::endl;
            stop = true;
        }
        std::cout << "Iteration: " << i << std::endl;
        std::cout << "Triangles generated: " << _triangles.size() << std::endl;

        i++;
    }


    std::cout << "Number of Triangles Generated: " << _triangles.size() << std::endl;
    //printing the Front

    std::cout << "Front Size: " << _front.size() << std::endl;

    if(_front.size() > 0 ) {
//        FrontEdge* frontEdge1 = &(*_front.begin());

//        while(frontEdge1!= NULL) {
//            std::cout << "Edge" << std::endl;
//            std::cout << std::get<0>(*frontEdge1).first->x << " "  << std::get<0>(*frontEdge1).first->y << " " << std::get<0>(*frontEdge1).first->z << std::endl;
//            std::cout << std::get<0>(*frontEdge1).second->x << " "  << std::get<0>(*frontEdge1).second->y << " " << std::get<0>(*frontEdge1).second->z << std::endl;
//            frontEdge1 = (FrontEdge*) std::get<4>(*frontEdge1);
//        }
    }

//    std::cout << "[" << std::endl;
//    for(i =0 ;i < _triangles.size(); i++) {
//        std::cout << "(" << std::endl;
//        std::cout << "(" << std::get<0>(_triangles[i])->x << ", " << std::get<0>(_triangles[i])->y << ", " << std::get<0>(_triangles[i])->z << ")," << std::endl;
//        std::cout << "(" << std::get<1>(_triangles[i])->x << ", " << std::get<1>(_triangles[i])->y << ", " << std::get<1>(_triangles[i])->z << "),"<< std::endl;
//        std::cout << "(" << std::get<2>(_triangles[i])->x << ", " << std::get<2>(_triangles[i])->y << ", " << std::get<2>(_triangles[i])->z << ")";
//        std::cout << ")" << std::endl;
//        if(i < _triangles.size() - 1) {
//            std::cout << ", " << std::endl;
//        }
//    }
//    std::cout << "]" << std::endl;
    Reformat();

//    delete[] _buckets;
    std::cout << "Ending BallPivotAlgorithm" << std::endl;
}
void BallPivotAlgorithm::Reformat() {
    long i;
    std::vector<long> usedPoints;
    for(i = 0; i < _pointSize;i++) {
        if(_mask[i]) {
            usedPoints.push_back(i);
        }
    }
    for(i = 0; i < _pointSize;i++) {
        if(_mask[i]) {
            _resultingPoints.push_back(makeXYZGPUFloat(_cloud.get()->points[i]));
        }
    }

    std::vector<long>::iterator it;
    for(i = 0; i < _triangles.size(); i++) {
        int colour = std::get<3>(_triangles[i]);
        assert(std::get<0>(_triangles[i]) >= 0);
        it = std::find(usedPoints.begin(), usedPoints.end(), std::get<0>(_triangles[i]));
        long v0 = std::distance(usedPoints.begin(), it);
        assert(std::get<1>(_triangles[i]) >= 0);
        it = std::find(usedPoints.begin(), usedPoints.end(), std::get<1>(_triangles[i]));
        long v1 = std::distance(usedPoints.begin(), it);
        assert(std::get<2>(_triangles[i]) >= 0);
        it = std::find(usedPoints.begin(), usedPoints.end(), std::get<2>(_triangles[i]));
        long v2 = std::distance(usedPoints.begin(), it);
        _resultingFaces.push_back(std::tuple<long, long, long, int>(v0, v1, v2, colour));
    }
}
void BallPivotAlgorithm::SaveAsMesh(std::string meshName) {
    std::ofstream mesh;
    mesh.open(meshName, ios::out);
    unsigned long i;
    mesh << "Points " << _resultingPoints.size() << std::endl;
    for(i = 0; i < _resultingPoints.size(); i++) {
        mesh << _resultingPoints[i].x << " " << _resultingPoints[i].y << " " << _resultingPoints[i].z << std::endl;
    }
    mesh << "Faces " << _resultingFaces.size() << std::endl;
    for(i = 0; i < _resultingFaces.size(); i++) {
        mesh << std::get<0>(_resultingFaces[i]) << " " << std::get<1>(_resultingFaces[i]) << " " << std::get<2>(_resultingFaces[i]) << " " << std::get<3>(_resultingFaces[i]) << std::endl;
    }
    mesh.close();
//    int i;
//    std::vector<int> usedPoints;
//
//    for(i = 0; i < _pointSize;i++) {
//        if(_mask[i]) {
//            usedPoints.push_back(i);
//        }
//    }
//    mesh << "Points " << usedPoints.size() << std::endl;
//    for(i = 0; i < _pointSize;i++) {
//        if(_mask[i]) {
//            mesh << _points[i].x << " " << _points[i].y << " " << _points[i].z << std::endl;
//            _resultingPoints.push_back(_points[i]);
//        }
//    }
//
//    std::vector<int>::iterator it;
//    mesh << "Faces " << _triangles.size() << std::endl;
//    for(i = 0; i < _triangles.size(); i++) {
//        assert((std::get<0>(_triangles[i]) - _points) >= 0);
//        it = std::find(usedPoints.begin(), usedPoints.end(), (std::get<0>(_triangles[i]) - _points));
//        long v0 = std::distance(usedPoints.begin(), it);
//        assert((std::get<1>(_triangles[i]) - _points) >= 0);
//        it = std::find(usedPoints.begin(), usedPoints.end(), (std::get<1>(_triangles[i]) - _points));
//        long v1 = std::distance(usedPoints.begin(), it);
//        assert((std::get<2>(_triangles[i]) - _points) >= 0);
//        it = std::find(usedPoints.begin(), usedPoints.end(), (std::get<2>(_triangles[i]) - _points));
//        long v2 = std::distance(usedPoints.begin(), it);
//        mesh << v0 << " " << v1 << " " << v2 << std::endl;
//        _resultingFaces.push_back(std::tuple<long, long, long>(v0, v1, v2));
//    }
//    mesh.close();
}
void BallPivotAlgorithm::SaveAsPCD(std::string meshName) {
    std::cerr << "TODO Implement" << std::endl;
}