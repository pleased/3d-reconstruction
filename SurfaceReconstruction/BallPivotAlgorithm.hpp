/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2017/04/18.
//

#ifndef VSLAM_BALLPIVOTALGORITHM_HPP
#define VSLAM_BALLPIVOTALGORITHM_HPP
#include <pcl/point_cloud.h>
#include <pcl/point_types.h>
#include <pcl/kdtree/kdtree_flann.h>
#include <pcl/features/normal_3d.h>
#include <pcl/surface/gp3.h>
#include <pcl/io/vtk_io.h>
#include <pcl/io/pcd_io.h>
#include <pcl/visualization/cloud_viewer.h>
#include "../KinectFusion/DeviceTypes.hpp"
#include "../KinectFusion/GlobalDevice.hpp"
#include <list>
#include <mutex>
#include <thread>



class BallPivotAlgorithm {
public:
    typedef std::pair<int, int> Edge;
//Format Edge, opposite vertex, center vertex, edge Status
    typedef std::tuple<Edge, int, pcl::PointXYZRGBNormal, short> FrontEdge;



    std::vector<std::pair<gpu::float3*, gpu::float3*>> findClosestPoints(const gpu::float3& queryPoint);
    std::vector<std::pair<gpu::float3*, gpu::float3*>> findClosestPointsFromPoint(const gpu::float3* queryPoint);

    gpu::int3 WorldToVoxel(gpu::float3 point) {
        point = (point - _min)/_voxelSize;
        return gpu::int3((int) point.x, (int) point.y, (int) point.z);
    }
    int Index3D(const gpu::int3 voxel) {
        return voxel.x + voxel.y * (_dimSize.x) + voxel.z * (_dimSize.x * _dimSize.y);
    }
    int WorldToIndex3D(gpu::float3 point) {
        return Index3D(WorldToVoxel(point));
    }
    void MarkAsUsed(int point);
    bool CheckValidManifold(FrontEdge* edge, int newPoint);
    void performJoin(FrontEdge* edge, int newPoint, gpu::float3 newCenter, FrontEdge** edgeIK, FrontEdge** edgeKJ);
    bool Join(FrontEdge* edge, int newPoint, gpu::float3 newCenter, FrontEdge** edgeIK, FrontEdge** edgeKJ);
    void Glue(FrontEdge* edgeAB, long& ABIndex, FrontEdge* edgeBA, long& BAIndex);
    void Glue(FrontEdge* edgeAB, FrontEdge* edgeBA, long& frontIndex);

    void InsertEdges(int seedTrianglePointI, int seedTrianglePointJ, int seedTrianglePointK, gpu::float3 center);
    void MarkAsBoundary(FrontEdge* edge);
    void MarkAsActive(FrontEdge* edge);
    bool FindEdge(int pointI, int pointJ, FrontEdge** pointer, long& frontIndex);
    bool FindEdgeOnFront(int pointI, int pointJ, long frontIndex, FrontEdge** pointer);
    bool SphereIsEmpty(gpu::float3& queryPoint, float radius, int p1, int p2, int p3, double scale=1.);
    bool GetBallCenter(gpu::float3 p0, gpu::float3 n0, gpu::float3 p1, gpu::float3 n1, gpu::float3 p2, gpu::float3 n2, const float ballRadius, gpu::float3& center, bool& swap);
    void GenerateSphereCenter(gpu::float3& p1, gpu::float3& p2, gpu::float3& p3, float sphereRadius, gpu::float3& center, float& circleRadius);
    bool CheckNormalConsistency(gpu::float3& tempNormalA, gpu::float3& tempNormalB, gpu::float3& tempNormalC, gpu::float3& normal, const float normalThreshold=0);
    bool GetActiveEdge(FrontEdge** activeEdge=NULL);
    bool NotUsed(int pointK);
    bool OnFront(int pointK);
    void BucketSort(float ballRadius);
    void ProjectPointOnPlane(gpu::float3 normal, gpu::float3 origin, gpu::float3 point, gpu::float3& projected_point);
    bool BallPivot3(FrontEdge* EdgeAB, float ballRadius, int& pointC, gpu::float3& newCenter);
    bool FindSeedTriangle(int& pointA, int& pointB, int& pointC, const float ballRadius, gpu::float3& center);
    void GetColour(int& colour, int p1, int p2, int p3);
public:
    BallPivotAlgorithm(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr cloud);
    BallPivotAlgorithm(gpu::float3* vertexMap, gpu::float3* normalMap, unsigned long size);
    ~BallPivotAlgorithm();
    void GetFrontListsAsGPUFloat(std::vector<std::vector<gpu::float3>>& listOfFronts);
    bool CheckNextIteration(float radius);

    void OpenViewer();
    void WaitForViewer();
    void PclFrontViewerSetup();
    void UpdatePCLViewer(bool showEdgeState=false);
    void ViewFront();
    void CloseViewer();
    void EnableViewer();
    void DisableViewer();
    void EnableEndViewer();
    void operator() (float* radii, int size);
    void operator() (float ballRadius);
    void Reformat();
    void SaveAsMesh(std::string meshName);
    void SaveAsPCD(std::string meshName);
public:
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr _cloud;

    bool _allocatedPointsAndNormals = false;
    gpu::float3* _points = NULL;
    gpu::float3* _normals = NULL;
    unsigned long _pointSize = 0;


    std::vector<std::list<FrontEdge>> _front;
    long _currentFrontSet = -1;
    std::vector<std::pair<gpu::float3*, gpu::float3*>>* _buckets = NULL;
    gpu::int3 _dimSize;
    gpu::float3 _min;
    int _bucketSize = 0;
    float _voxelSize = 0;

    pcl::KdTreeFLANN<pcl::PointXYZRGBNormal> _kdtree;


    bool* _mask = NULL;
    std::vector<std::tuple<int, int, int, int>> _triangles;
    std::vector<gpu::float3> _resultingPoints;
    std::vector<std::tuple<long, long, long, int>> _resultingFaces;

    static const short NONE;
    static const short ACTIVE;
    static const short BOUNDARY;
    static const short FROZEN;


    //Viewer stuff and thread
    int testNumber = 10;
    int _viewerDelayTime = 0;
    int _viewerIteration = 0;
    int _viewerStopIteration = -1;
    bool _viewerEnable = false;
    bool _viewerEndEnabled = false;
    pcl::visualization::PCLVisualizer::Ptr _viewer;
    std::mutex _mutexViewer;
    std::thread* _viewerThread = 0;
    pcl::PointCloud<pcl::PointXYZRGB>::Ptr _viewerPointCloud;

};
bool operator ==(BallPivotAlgorithm::FrontEdge a, BallPivotAlgorithm::FrontEdge b);
//TODO Note These operators only work on the xyz part of the point
bool operator ==(pcl::PointXYZRGBNormal a, pcl::PointXYZRGBNormal b);
pcl::PointXYZRGBNormal operator +(pcl::PointXYZRGBNormal a, pcl::PointXYZRGBNormal b);
pcl::PointXYZRGBNormal operator -(pcl::PointXYZRGBNormal a, pcl::PointXYZRGBNormal b);
pcl::PointXYZRGBNormal operator *(pcl::PointXYZRGBNormal a, float b);
pcl::PointXYZRGBNormal operator *(float a, pcl::PointXYZRGBNormal b);

gpu::float3 makeXYZGPUFloat(pcl::PointXYZRGBNormal p);
gpu::float3 makeNormalGPUFloat(pcl::PointXYZRGBNormal p);
pcl::PointXYZRGBNormal makePCLXYZ(gpu::float3 p);
#endif //VSLAM_BALLPIVOTALGORITHM_HPP
