/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/05/26.
//

#include <fstream>
#include "UtilityFunctions.hpp"

namespace Utility {
    void GenerateXRotationMatrix(double theta, Eigen::Matrix4d& outMatrix) {

        Eigen::Matrix4d temp = Eigen::Matrix4d::Identity();
        double ct = cos(theta);
        double st = sin(theta);
        temp(1, 1) = ct;
        temp(1, 2) = -st;
        temp(2, 1) = st;
        temp(2, 2) = ct;
        temp(3, 3) = 1.;
//        std::cout << temp << std::endl;
//        std::cout << outMatrix * temp << std::endl;

        outMatrix *= temp;
//        std::cout << outMatrix << std::endl;

    }
    void GenerateYRotationMatrix(double theta, Eigen::Matrix4d& outMatrix) {
        Eigen::Matrix4d temp = Eigen::Matrix4d::Identity();
        double ct = cos(theta);
        double st = sin(theta);
        temp(0, 0) = ct;
        temp(0, 2) = st;
        temp(2, 0) = -st;
        temp(2, 2) = ct;
        temp(3, 3) = 1.;
        outMatrix *= temp;
    }

    void GenerateZRotationMatrix(double theta, Eigen::Matrix4d& outMatrix) {
        Eigen::Matrix4d temp = Eigen::Matrix4d::Identity();
        double ct = cos(theta);
        double st = sin(theta);
        temp(0, 0) = ct;
        temp(0, 1) = -st;
        temp(1, 0) = st;
        temp(1, 1) = ct;
        temp(3, 3) = 1.;
//        std::cout << "TESTING" << std::endl;
//        std::cout << temp << std::endl;
//        temp = outMatrix * temp;
//        std::cout << temp << std::endl;
//        outMatrix = temp;
//        std::cout << outMatrix << std::endl;
        outMatrix *= temp;
    }

    Eigen::Matrix4d GenerateRotationMatrix(double xRot, double yRot, double zRot, double x, double y, double z) {
        Eigen::Matrix4d rotationMatrix = Eigen::Matrix4d::Identity();
        GenerateZRotationMatrix(zRot, rotationMatrix);
        GenerateYRotationMatrix(yRot, rotationMatrix);
        GenerateXRotationMatrix(xRot, rotationMatrix);
        rotationMatrix(0, 3) = x;
        rotationMatrix(1, 3) = y;
        rotationMatrix(2, 3) = z;
        return rotationMatrix;
    }




    void GenerateXRotationMatrix33(double theta, gpu::matrix33& outMatrix) {

        gpu::matrix33 temp;
        temp.SetIdentity();
        float ct = cosf(theta);
        float st = sinf(theta);

        temp.row1.y = ct;
//        temp(1, 1) = ct;
        temp.row1.z = -st;
//        temp(1, 2) = -st;
        temp.row2.y = st;
//        temp(2, 1) = st;
        temp.row2.z = ct;
//        temp(2, 2) = ct;
//        temp(3, 3) = 1.;
//        std::cout << temp << std::endl;
//        std::cout << outMatrix * temp << std::endl;

        outMatrix = outMatrix * temp;
//        std::cout << outMatrix << std::endl;

    }
    void GenerateYRotationMatrix33(float theta, gpu::matrix33& outMatrix) {
        gpu::matrix33 temp;
        temp.SetIdentity();
        float ct = cosf(theta);
        float st = sinf(theta);
        temp.row0.x = ct;
//        temp(0, 0) = ct;
        temp.row0.z = st;
//        temp(0, 2) = st;
        temp.row2.x = -st;
//        temp(2, 0) = -st;
        temp.row2.z = ct;
//        temp(2, 2) = ct;
//        temp(3, 3) = 1.;
        outMatrix = outMatrix * temp;
    }

    void GenerateZRotationMatrix33(float theta, gpu::matrix33& outMatrix) {
        gpu::matrix33 temp;// = Eigen::Matrix4d::Identity();
        temp.SetIdentity();
        float ct = cosf(theta);
        float st = sinf(theta);
        temp.row0.x = ct;
//        temp(0, 0) = ct;
        temp.row0.y = -st;
//        temp(0, 1) = -st;
        temp.row1.x = st;
//        temp(1, 0) = st;
        temp.row1.y = ct;
//        temp(1, 1) = ct;
//        temp(3, 3) = 1.;
//        std::cout << "TESTING" << std::endl;
//        std::cout << temp << std::endl;
//        temp = outMatrix * temp;
//        std::cout << temp << std::endl;
//        outMatrix = temp;
//        std::cout << outMatrix << std::endl;
        outMatrix = outMatrix * temp;
    }

    gpu::matrix33 GenerateRotationMatrix33(float xRot, float yRot, float zRot) {
        gpu::matrix33 rotationMatrix;
        rotationMatrix.SetIdentity();
        GenerateZRotationMatrix33(zRot, rotationMatrix);
        GenerateYRotationMatrix33(yRot, rotationMatrix);
        GenerateXRotationMatrix33(xRot, rotationMatrix);
        return rotationMatrix;
    }

    Eigen::Matrix4d RotationMatrixDifference(Eigen::Matrix4d& rotation1, Eigen::Matrix4d& rotation2) {
        Eigen::Matrix3d r1, r2;
        Eigen::Vector3d t1, t2;
        r1 = rotation1.block<3, 3>(0, 0);
        r2 = rotation2.block<3, 3>(0, 0);
        r1 = r1 * r2.inverse();
        t1 = rotation1.block<3, 1>(0, 3);
        t2 = rotation2.block<3, 1>(0, 3);
//        t1 = t1 - t2;
        Eigen::Matrix4d res;
        res.setZero();
        res.block<3, 3>(0, 0) = r1;
        res.block<3, 1>(0, 3) = t1;
        return  res;
    }

    Eigen::Matrix4d QuaternionToRotationMatrix(Eigen::Vector4d& q) {
        return QuaternionToRotationMatrix(q(1), q(2), q(3), q(0));
    }

    Eigen::Matrix4d QuaternionToRotationMatrix(double xi, double yj, double zk, double w, double x, double y, double z) {
        Eigen::Matrix4d rotationMatrix;
        rotationMatrix.setZero();
         double xx = pow(xi, 2);
         double xy = xi * yj;
         double xz = xi * zk;
         double xw = xi * w;
    //
         double yy = pow(yj, 2);
         double yz = yj * zk;
         double yw = yj * w;
    //
         double zz = pow(zk, 2);
         double zw = zk * w;
    //
        rotationMatrix(0,0) = 1 - 2 * (yy + zz);
        rotationMatrix(0,1) = 2 * (xy - zw);
        rotationMatrix(0,2) = 2 * (xz + yw);

        rotationMatrix(1,0) = 2 * (xy + zw);
        rotationMatrix(1,1) = 1 - 2 * (xx + zz);
        rotationMatrix(1,2) = 2 * (yz - xw);

        rotationMatrix(2,0) = 2 * (xz - yw);
        rotationMatrix(2,1) = 2 * (yz + xw);
        rotationMatrix(2,2) = 1 - 2 * (xx + yy);

        rotationMatrix(0, 3) = x;
        rotationMatrix(1, 3) = y;
        rotationMatrix(2, 3) = z;
        rotationMatrix(3,3) = 1;

        return rotationMatrix;
    }
    //
    void RotationMatrixToQuaternion(gpu::matrix33 &array, double &w, double &x, double &y, double &z) {

        double trace = array(0, 0) + array(1, 1) + array(2, 2);
        double r, s;
        if (trace >= 0) {
            r = sqrt(1+trace);
            s = 0.5/r;
            w = 0.5*r;
            // x = np.abs(w)
            x = (array(2, 1) - array(1, 2))*s;
            // y = np.abs(w)
            y = (array(0, 2) - array(2, 0))*s;
            // z = np.abs(w)
            z = (array(1, 0) - array(0, 1))*s;
        } else {
            r = sqrt(1 + array(0, 0) - array(1, 1) - array(2, 2));
            s = 0.5/r;

            w = (array(2, 1) - array(1, 2))*s;
            x = 0.5*r;
            y = (array(0, 1) + array(1, 0))*s;
            z = (array(2, 0) + array(0, 2))*s;
        }
    }
    //

    void EulerToQuaternion(double xRot, double yRot, double zRot, double& w, double& x, double& y, double& z) {
        double t0 = cos(zRot * 0.5);
        double t1 = sin(zRot * 0.5);
        double t2 = cos(xRot * 0.5);
        double t3 = sin(xRot * 0.5);
        double t4 = cos(yRot * 0.5);
        double t5 = sin(yRot * 0.5);

        w = t0 * t2 * t4 + t1 * t3 * t5;
        x = t0 * t3 * t4 - t1 * t2 * t5;
        y = t0 * t2 * t5 + t1 * t3 * t4;
        z = t1 * t2 * t4 - t0 * t3 * t5;
    }
    void EulerToQuaternion(float xRot, float yRot, float zRot, float& w, float& x, float& y, float& z) {
        float t0 = cosf(zRot * 0.5f);
        float t1 = sinf(zRot * 0.5f);
        float t2 = cosf(xRot * 0.5f);
        float t3 = sinf(xRot * 0.5f);
        float t4 = cosf(yRot * 0.5f);
        float t5 = sinf(yRot * 0.5f);

        w = t0 * t2 * t4 + t1 * t3 * t5;
        x = t0 * t3 * t4 - t1 * t2 * t5;
        y = t0 * t2 * t5 + t1 * t3 * t4;
        z = t1 * t2 * t4 - t0 * t3 * t5;
    }
    void QuaternionToEuler(double w, double x, double y, double z, double& xRot, double& yRot, double& zRot) {

        xRot = atan2(2 * (w*x + y * z), 1 - 2 * (x*x + y * y));
        yRot = asin(2 * (w * y - z * x));
        zRot = atan2(2 * (w * z + x * y), 1 - 2 * (y * y + z * z));

    }
    void QuaternionToEuler(float w, float x, float y, float z, float& xRot, float& yRot, float& zRot) {

        xRot = atan2f(2 * (w*x + y * z), 1 - 2 * (x*x + y * y));
        yRot = asinf(2 * (w * y - z * x));
        zRot = atan2f(2 * (w * z + x * y), 1 - 2 * (y * y + z * z));

    }
    //RotationMatrix to Euler
    void RotationMatrixToEuler(gpu::matrix33& rotationMatrix, double& xRot, double& yRot, double& zRot) {


        if(rotationMatrix.row2.x < + 1) {
            if(rotationMatrix.row2.x > - 1) {
                yRot = asin(-rotationMatrix.row2.x);
                zRot = atan2(rotationMatrix.row1.x, rotationMatrix.row0.x);
                xRot = atan2(rotationMatrix.row2.y, rotationMatrix.row2.z);
            } else {
                yRot = M_PI/2.;
                zRot = -atan2(rotationMatrix.row1.z, rotationMatrix.row1.y);
                xRot = 0;
            }
        } else {
            yRot = -M_PI / 2;
            zRot = atan2(-rotationMatrix.row1.z, rotationMatrix.row1.y);
            xRot = 0;
        }
    }
    void RotationMatrixToEuler(gpu::matrix33& rotationMatrix, float& xRot, float& yRot, float& zRot) {

        if(rotationMatrix.row2.x < + 1) {
            if(rotationMatrix.row2.x > - 1) {
                yRot = asinf(-rotationMatrix.row2.x);
                zRot = atan2f(rotationMatrix.row1.x, rotationMatrix.row0.x);
                xRot = atan2f(rotationMatrix.row2.y, rotationMatrix.row2.z);
            } else {
                yRot = (float) M_PI/2.f;
                zRot = -atan2f(rotationMatrix.row1.z, rotationMatrix.row1.y);
                xRot = 0;
            }
        } else {
            yRot = (float) -M_PI / 2.f;
            zRot = atan2f(-rotationMatrix.row1.z, rotationMatrix.row1.y);
            xRot = 0;
        }
    }



    Eigen::Matrix3d& GetCameraParameters() {
        return _cameraParameters;
    }

    void SetCameraParameters(double fx, double fy, double ox, double oy) {
        _cameraParameters <<    fx, 0, ox,
                                0, fy ,oy,
                                0, 0, 1;
    }

    std::string Generate3DPlotFile(std::string prefix, std::vector<std::string> files) {
        std::ofstream plotFile;
        std::string plotFileName = prefix + "Plot.gnuplot";

        plotFile.open(plotFileName);

        plotFile << "set xrange [-5:5]" << std::endl << "set yrange [-5:5]" << std::endl;
        int i;
        plotFile << "splot ";
        for (i = 0; i < files.size();i++) {
            plotFile << "\"" << files[i] <<"\" using 1:2:3 with lines, \\" << std::endl;
        }
        plotFile.close();

        return plotFileName;
    }
    std::string Generate2DPlotFile(std::string prefix, std::vector<std::string> files) {
        std::ofstream plotFile;
        std::string plotFileName = prefix + "Plot.gnuplot";

        plotFile.open(plotFileName);

        plotFile << "set xrange [-5:5]" << std::endl << "set yrange [-5:5]" << std::endl;
        int i;
        plotFile << "plot ";
        for (i = 0; i < files.size();i++) {
            plotFile << "\"" << files[i] <<"\" using 1:2 with lines, \\" << std::endl;
        }
        plotFile.close();

        return plotFileName;
    }

    std::string GeneratePlotDataFile(std::string prefix, std::vector<Eigen::Matrix4d> estimate) {
        std::ofstream xyPlotFile;
//        std::ofstream xzPlotFile;
        std::string xyPlotFileName = prefix + "XYMotion.data";
//        std::string xzPlotFileName = prefix + "XZMotion.data";

        xyPlotFile.open(xyPlotFileName);
//        xzPlotFile.open(xzPlotFileName);

        xyPlotFile << "#X   Y" << std::endl;
//        xzPlotFile << "#X   Z" << std::endl;

        int i;
        for(i = 0; i < estimate.size(); i++ ) {
            double x = estimate[i](0, 3);
            double y = estimate[i](1, 3);
            double z = estimate[i](2, 3);
            xyPlotFile << x << " " << y << " " << z << std::endl;
//            xzPlotFile << x << " " << z << std::endl;
        }
        xyPlotFile.close();
//        xzPlotFile.close();
        return xyPlotFileName;
    }
    //TODO take rotation into account somehow
    std::string GeneratePlotDataFile(std::string prefix, std::vector<gpu::matrix33> rotation, std::vector<gpu::float3> translations) {

        std::ofstream xyPlotFile;
//        std::ofstream xzPlotFile;
        std::string xyPlotFileName = prefix + "XYMotion.data";
//        std::string xzPlotFileName = prefix + "XZMotion.data";

        xyPlotFile.open(xyPlotFileName);
//        xzPlotFile.open(xzPlotFileName);

        xyPlotFile << "#X   Y" << std::endl;
//        xzPlotFile << "#X   Z" << std::endl;
        if(rotation.size() != translations.size()) {
            throw "Generating plot data rotation.size() != translations.size()";
        }
        int i;
        for(i = 0; i < rotation.size(); i++ ) {
            double x = translations[i].x;
            double y = translations[i].y;
            double z = translations[i].z;
            xyPlotFile << x << " " << y << " " << z << std::endl;
//            xzPlotFile << x << " " << z << std::endl;
        }
        xyPlotFile.close();
//        xzPlotFile.close();
        return xyPlotFileName;
    }


    void TransformationToVector(Eigen::Matrix4f& transform, Eigen::Matrix<float, 1, 6>& pointer) {
        gpu::matrix33 rotation;
        gpu::float3 translation;
        BuildRotationAndTranslationFromTransform(transform, rotation, translation);
        TransformationToVector(rotation, translation, pointer);
    }

    void TransformationToVector(gpu::matrix33& rotation, gpu::float3& translation, Eigen::Matrix<float, 1, 6>& pointer) {

    }


    void BuildTransformFromRotationAndTranslation(gpu::matrix33& rotation, gpu::float3& translation, Eigen::Matrix4f& transform) {
        transform(0, 0) = rotation.row0.x;transform(0, 1) = rotation.row0.y;transform(0, 2) = rotation.row0.z;
        transform(1, 0) = rotation.row1.x;transform(1, 1) = rotation.row1.y;transform(1, 2) = rotation.row1.z;
        transform(2, 0) = rotation.row2.x;transform(2, 1) = rotation.row2.y;transform(2, 2) = rotation.row2.z;
        transform(0, 3) = translation.x;
        transform(1, 3) = translation.y;
        transform(2, 3) = translation.z;
    }
    void BuildRotationAndTranslationFromTransform(Eigen::Matrix4f& transform, gpu::matrix33& rotation, gpu::float3& translation) {
        rotation.row0.x = transform(0, 0);  rotation.row0.y = transform(0, 1);    rotation.row0.z = transform(0, 2);
        rotation.row1.x = transform(1, 0);  rotation.row1.y = transform(1, 1);    rotation.row1.z = transform(1, 2);
        rotation.row2.x = transform(2, 0);  rotation.row2.y = transform(2, 1);    rotation.row2.z = transform(2, 2);
        translation.x = transform(0, 3);
        translation.y = transform(1, 3);
        translation.z = transform(2, 3);
    }

//    pcl::PointCloud<pcl::PointXYZRGB>::Ptr GeneratePCL(std::vector<Eigen::VectorXd>& points, bool color) {
//        pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud(new pcl::PointCloud<pcl::PointXYZRGB>);
//        cloud->points.resize(points.size());
//        int i;
//        for (i = 0; i < points.size(); i++) {
//            cloud->points[i].x = points[i](0);
//            cloud->points[i].y = points[i](1);
//            cloud->points[i].z = points[i](2);
//            if (color) {
//                cloud->points[i].r = points[i](4);
//                cloud->points[i].g = points[i](5);
//                cloud->points[i].b = points[i](6);
//            }
//        }
//        return cloud;
//    }

//    void ViewPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud, bool downsample) {
//
//        boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
////        boost::shared_ptr<pcl::PointCloud<pcl::PointXYZRGB>> pc = cloud.makeShared();
//        pcl::PointCloud<pcl::PointXYZRGB>::Ptr out(new pcl::PointCloud<pcl::PointXYZRGB>);
//        pcl::PointCloud<pcl::PointXYZRGB>::Ptr in = cloud;
//        downsample = true;
//        if (downsample) {
//            pcl::VoxelGrid<pcl::PointXYZRGB> sor;
//            sor.setInputCloud (in);
//            sor.setLeafSize (0.01f, 0.01f, 0.01f);
//            sor.filter (*out);
//        } else {
//
//        }
//
//
//        viewer->setBackgroundColor (0, 0, 0);
//        pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(out);
//        viewer->addPointCloud<pcl::PointXYZRGB> (out, rgb, "sample cloud");
//        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 3, "sample cloud");
//        viewer->addCoordinateSystem (1.0);
//        viewer->initCameraParameters ();
//        while (!viewer->wasStopped ())
//        {
//            viewer->spinOnce (100);
//            boost::this_thread::sleep (boost::posix_time::microseconds (100000));
//        }
//    }
//    void ViewPointClouds(pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud1Ptr, pcl::PointCloud<pcl::PointXYZRGB>::Ptr cloud2Ptr, bool downsample) {
//
//        boost::shared_ptr<pcl::visualization::PCLVisualizer> viewer (new pcl::visualization::PCLVisualizer ("3D Viewer"));
//
//        viewer->setBackgroundColor (0, 0, 0);
////        pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(out );
//        pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> red(cloud1Ptr, 255, 0, 0);
//        pcl::visualization::PointCloudColorHandlerCustom<pcl::PointXYZRGB> blue(cloud2Ptr, 0, 255, 0);
////        pcl::visualization::PointCloudColorHandlerRGBField<pcl::PointXYZRGB> rgb(out );
//        viewer->addPointCloud<pcl::PointXYZRGB> (cloud1Ptr, red, "Current Pose");
//        viewer->addPointCloud<pcl::PointXYZRGB> (cloud2Ptr, blue, "New Pose");
//        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "Current Pose");
//        viewer->setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 5, "New Pose");
//        viewer->addCoordinateSystem (1.0);
//        viewer->initCameraParameters ();
//        while (!viewer->wasStopped ())
//        {
//            viewer->spinOnce (100);
//            boost::this_thread::sleep (boost::posix_time::microseconds (100000));
//        }
////        exit(0);
//    }
//
//    void SavePointCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloud, std::string filename) {
//        filename += ".pcd";
//        pcl::io::savePCDFileASCII (filename, (*pointCloud.get()));
//        std::cerr << "Saved " << (*pointCloud.get()).points.size() << " data points to " << filename << std::endl;
//    }
//
//    pcl::PointCloud<pcl::PointXYZRGB>::Ptr LoadPointCloud(std::string filename) {
//        pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloud( new pcl::PointCloud<pcl::PointXYZRGB>);
//        pcl::io::loadPCDFile(filename, (*pointCloud.get()));
//        return pointCloud;
//    }
    inline bool eq(double a, double b, double eps=1.e-5) {
        return std::abs(a - b ) < eps;
    }
    void _TestUtilityRotationToQuaternionToRotation() {
        gpu::matrix33 rotationMatrix = GenerateRotationMatrix33(0.4, 0.7, 0.1);
        double w, x, y, z;

        RotationMatrixToQuaternion(rotationMatrix, w, x, y, z);

        Eigen::Matrix4d rotation = QuaternionToRotationMatrix(x, y, z, w);
        std::cout << rotation(0, 0) << " " << rotationMatrix(0, 0) << std::endl;
        std::cout << rotation << std::endl;
        std::cout << "Starting matrix " << std::endl
        << rotationMatrix.row0.x << " " << rotationMatrix.row0.y << " " << rotationMatrix.row0.z << " " <<  std::endl
        << rotationMatrix.row1.x << " " << rotationMatrix.row1.y << " " << rotationMatrix.row1.z << " " << std::endl
        << rotationMatrix.row2.x << " " << rotationMatrix.row2.y << " " << rotationMatrix.row2.z << " " << std::endl;
        assert(eq(rotation(0, 0), rotationMatrix(0, 0)));
        assert(eq(rotation(0, 1), rotationMatrix(0, 1)));
        assert(eq(rotation(0, 2), rotationMatrix(0, 2)));
        assert(eq(rotation(1, 0), rotationMatrix(1, 0)));
        assert(eq(rotation(1, 1), rotationMatrix(1, 1)));
        assert(eq(rotation(1, 2), rotationMatrix(1, 2)));
        assert(eq(rotation(2, 0), rotationMatrix(2, 0)));
        assert(eq(rotation(2, 1), rotationMatrix(2, 1)));
        assert(eq(rotation(2, 2), rotationMatrix(2, 2)));

        rotationMatrix = GenerateRotationMatrix33(M_PI_2, M_PI/2, M_PI);
        RotationMatrixToQuaternion(rotationMatrix, w, x, y, z);
        rotation = QuaternionToRotationMatrix(x, y, z, w);


        assert(eq(rotation(0, 0), rotationMatrix(0, 0)));
        assert(eq(rotation(0, 1), rotationMatrix(0, 1)));
        assert(eq(rotation(0, 2), rotationMatrix(0, 2)));
        assert(eq(rotation(1, 0), rotationMatrix(1, 0)));
        assert(eq(rotation(1, 1), rotationMatrix(1, 1)));
        assert(eq(rotation(1, 2), rotationMatrix(1, 2)));
        assert(eq(rotation(2, 0), rotationMatrix(2, 0)));
        assert(eq(rotation(2, 1), rotationMatrix(2, 1)));
        assert(eq(rotation(2, 2), rotationMatrix(2, 2)));
    }
    void _TestUtilityRotationToEulerToRotation() {
        float xRot, yRot, zRot;
        float estimatedXRot, estimatedYRot, estimatedZRot;
        gpu::matrix33 rotationMatrix, newRotationMatrix;


        xRot = 0.125f;
        yRot = 0.0625f;
        zRot = 0.5f;
        rotationMatrix = GenerateRotationMatrix33(xRot, yRot, zRot);
        RotationMatrixToEuler(rotationMatrix, estimatedXRot, estimatedYRot, estimatedZRot);
        newRotationMatrix = GenerateRotationMatrix33(estimatedXRot, estimatedYRot, estimatedZRot);
        assert(eq(xRot, estimatedXRot));
        assert(eq(yRot, estimatedYRot));
        assert(eq(zRot, estimatedZRot));
        assert(eq(newRotationMatrix(0, 0), rotationMatrix(0, 0)));
        assert(eq(newRotationMatrix(0, 1), rotationMatrix(0, 1)));
        assert(eq(newRotationMatrix(0, 2), rotationMatrix(0, 2)));
        assert(eq(newRotationMatrix(1, 0), rotationMatrix(1, 0)));
        assert(eq(newRotationMatrix(1, 1), rotationMatrix(1, 1)));
        assert(eq(newRotationMatrix(1, 2), rotationMatrix(1, 2)));
        assert(eq(newRotationMatrix(2, 0), rotationMatrix(2, 0)));
        assert(eq(newRotationMatrix(2, 1), rotationMatrix(2, 1)));
        assert(eq(newRotationMatrix(2, 2), rotationMatrix(2, 2)));


        xRot = -0.125f;
        yRot = -0.0625f;
        zRot = -0.5f;
        rotationMatrix = GenerateRotationMatrix33(xRot, yRot, zRot);
        RotationMatrixToEuler(rotationMatrix, estimatedXRot, estimatedYRot, estimatedZRot);
        newRotationMatrix = GenerateRotationMatrix33(estimatedXRot, estimatedYRot, estimatedZRot);
        assert(eq(xRot, estimatedXRot));
        assert(eq(yRot, estimatedYRot));
        assert(eq(zRot, estimatedZRot));
        assert(eq(newRotationMatrix(0, 0), rotationMatrix(0, 0)));
        assert(eq(newRotationMatrix(0, 1), rotationMatrix(0, 1)));
        assert(eq(newRotationMatrix(0, 2), rotationMatrix(0, 2)));
        assert(eq(newRotationMatrix(1, 0), rotationMatrix(1, 0)));
        assert(eq(newRotationMatrix(1, 1), rotationMatrix(1, 1)));
        assert(eq(newRotationMatrix(1, 2), rotationMatrix(1, 2)));
        assert(eq(newRotationMatrix(2, 0), rotationMatrix(2, 0)));
        assert(eq(newRotationMatrix(2, 1), rotationMatrix(2, 1)));
        assert(eq(newRotationMatrix(2, 2), rotationMatrix(2, 2)));

        xRot = (float) M_PI;
        yRot = (float) M_PI;
        zRot = (float) M_PI;
        rotationMatrix = GenerateRotationMatrix33(xRot, yRot, zRot);
        RotationMatrixToEuler(rotationMatrix, estimatedXRot, estimatedYRot, estimatedZRot);
        newRotationMatrix = GenerateRotationMatrix33(estimatedXRot, estimatedYRot, estimatedZRot);
        assert(eq(xRot - M_PI, estimatedXRot));
        assert(eq(yRot - M_PI, estimatedYRot));
        assert(eq(zRot - M_PI, estimatedZRot));
        assert(eq(newRotationMatrix(0, 0), rotationMatrix(0, 0)));
        assert(eq(newRotationMatrix(0, 1), rotationMatrix(0, 1)));
        assert(eq(newRotationMatrix(0, 2), rotationMatrix(0, 2)));
        assert(eq(newRotationMatrix(1, 0), rotationMatrix(1, 0)));
        assert(eq(newRotationMatrix(1, 1), rotationMatrix(1, 1)));
        assert(eq(newRotationMatrix(1, 2), rotationMatrix(1, 2)));
        assert(eq(newRotationMatrix(2, 0), rotationMatrix(2, 0)));
        assert(eq(newRotationMatrix(2, 1), rotationMatrix(2, 1)));
        assert(eq(newRotationMatrix(2, 2), rotationMatrix(2, 2)));



    }
    void _TestUtilityEulerToRotationToEuler() {
        float xRot, yRot, zRot;
        float estimatedXRot, estimatedYRot, estimatedZRot;
        gpu::matrix33 rotationMatrix, newRotationMatrix;


        xRot = 0.125f;
        yRot = 0.0625f;
        zRot = 0.5f;
        rotationMatrix = GenerateRotationMatrix33(xRot, yRot, zRot);
        RotationMatrixToEuler(rotationMatrix, estimatedXRot, estimatedYRot, estimatedZRot);
        newRotationMatrix = GenerateRotationMatrix33(estimatedXRot, estimatedYRot, estimatedZRot);
        assert(eq(xRot, estimatedXRot));
        assert(eq(yRot, estimatedYRot));
        assert(eq(zRot, estimatedZRot));
        assert(eq(newRotationMatrix(0, 0), rotationMatrix(0, 0)));
        assert(eq(newRotationMatrix(0, 1), rotationMatrix(0, 1)));
        assert(eq(newRotationMatrix(0, 2), rotationMatrix(0, 2)));
        assert(eq(newRotationMatrix(1, 0), rotationMatrix(1, 0)));
        assert(eq(newRotationMatrix(1, 1), rotationMatrix(1, 1)));
        assert(eq(newRotationMatrix(1, 2), rotationMatrix(1, 2)));
        assert(eq(newRotationMatrix(2, 0), rotationMatrix(2, 0)));
        assert(eq(newRotationMatrix(2, 1), rotationMatrix(2, 1)));
        assert(eq(newRotationMatrix(2, 2), rotationMatrix(2, 2)));
    }
    void _TestUtilityEulerToQuaternionToEuler() {
        float xRot, yRot, zRot;
        float estimatedXRot, estimatedYRot, estimatedZRot;
        float w, x, y, z;

        xRot = 0.125f;
        yRot = 0.0625f;
        zRot = 0.5f;
        EulerToQuaternion(xRot, yRot, zRot, w, x, y, z);
        QuaternionToEuler(w, x, y, z, estimatedXRot, estimatedYRot, estimatedZRot);
        assert(eq(xRot, estimatedXRot));
        assert(eq(yRot, estimatedYRot));
        assert(eq(zRot, estimatedZRot));


        xRot = (float) M_PI/2.f;
        yRot = (float) M_PI/4.f;
        zRot = 4 * (float) M_PI/6.f;
        EulerToQuaternion(xRot, yRot, zRot, w, x, y, z);
        QuaternionToEuler(w, x, y, z, estimatedXRot, estimatedYRot, estimatedZRot);
        assert(eq(xRot, estimatedXRot));
        assert(eq(yRot, estimatedYRot));
        assert(eq(zRot, estimatedZRot));

    }
    void _TestUtilityQuaternionToEulerToQuaternion() {
        std::cout << "TEST" << std::endl;
        float xRot1, yRot1, zRot1;
        float xRot2, yRot2, zRot2;
        float differenceXRot, differenceYRot, differenceZRot;
        float estimatedXRot, estimatedYRot, estimatedZRot;
        gpu::matrix33 rotationMatrix1, rotationMatrix2, rotationMatrix3, newRotationMatrix;


        xRot1 = 0.0625f;
        yRot1 = 0.0625f;
        zRot1 = 0.0625f;
//        yRot1 = 0.f;
//        zRot1 = 0.f;

        xRot2 = M_PI - 0.4;
        yRot2 = M_PI - 0.4;
        zRot2 = M_PI - 0.4;
//        yRot2 = 0.f;
//        zRot2 = 0.f;
        rotationMatrix1 = GenerateRotationMatrix33(xRot1, yRot1, zRot1);
        rotationMatrix2 = GenerateRotationMatrix33(xRot2, yRot2, zRot2);

        rotationMatrix3 = rotationMatrix1.transpose() * rotationMatrix2;

        RotationMatrixToEuler(rotationMatrix3, estimatedXRot, estimatedYRot, estimatedZRot);

        differenceXRot = xRot2 - xRot1;
        differenceYRot = yRot2 - yRot1;
        differenceZRot = zRot2 - zRot1;
        newRotationMatrix = GenerateRotationMatrix33(differenceXRot, differenceYRot, differenceZRot);
        std::cout << differenceXRot << " " << differenceYRot << " " << differenceZRot << std::endl;

        std::cout << estimatedXRot << " " << estimatedYRot << " " << estimatedZRot << std::endl;


        std::cout << "Rotation Matrix 3" << std::endl
        << rotationMatrix3.row0.x << " " << rotationMatrix3.row0.y << " " << rotationMatrix3.row0.z << " " <<  std::endl
        << rotationMatrix3.row1.x << " " << rotationMatrix3.row1.y << " " << rotationMatrix3.row1.z << " " << std::endl
        << rotationMatrix3.row2.x << " " << rotationMatrix3.row2.y << " " << rotationMatrix3.row2.z << " " << std::endl;

        std::cout << "Difference in angles generated from Euler" << std::endl
        << newRotationMatrix.row0.x << " " << newRotationMatrix.row0.y << " " << newRotationMatrix.row0.z << " " <<  std::endl
        << newRotationMatrix.row1.x << " " << newRotationMatrix.row1.y << " " << newRotationMatrix.row1.z << " " << std::endl
        << newRotationMatrix.row2.x << " " << newRotationMatrix.row2.y << " " << newRotationMatrix.row2.z << " " << std::endl;



//        exit(0);
    }
};