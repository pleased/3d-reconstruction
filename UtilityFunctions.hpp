/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/05/26.
//


#ifndef VSLAM_UTILITYFUNCTIONS_HPP
#define VSLAM_UTILITYFUNCTIONS_HPP
#include <eigen3/Eigen/Dense>
#include <cmath>
#include <iostream>
#include "KinectFusion/DeviceTypes.hpp"
//#include <pcl/point_types.h>
//#include <pcl/point_cloud.h>
//#include <pcl/visualization/pcl_visualizer.h>
//#include <boost/thread/thread.hpp>
//#include <pcl/point_types.h>
//#include <pcl/filters/voxel_grid.h>
//#include <pcl/io/pcd_io.h>
//#include <pcl/point_types.h>
//#include <pcl/filters/voxel_grid.h>
namespace Utility {

    //Rotation Matrix Generation
    void GenerateXRotationMatrix(double theta, Eigen::Matrix4d& outMatrix);
    void GenerateYRotationMatrix(double theta, Eigen::Matrix4d& outMatrix);
    void GenerateZRotationMatrix(double theta, Eigen::Matrix4d& outMatrix);
    Eigen::Matrix4d GenerateRotationMatrix(double xRot, double yRot, double zRot, double x=0, double y=0, double z=0);
    gpu::matrix33 GenerateRotationMatrix33(float xRot, float yRot, float zRot);
    //Rotation Matrix Error
    Eigen::Matrix4d RotationMatrixDifference(Eigen::Matrix4d&, Eigen::Matrix4d&);

    //Quaternion to rotation matrix
    Eigen::Matrix4d QuaternionToRotationMatrix(Eigen::Vector4d&);
    Eigen::Matrix4d QuaternionToRotationMatrix(double xi, double yj, double zk, double w, double x=0, double y=0, double z=0);
    //Rotation Matrix to Quaternion
    void RotationMatrixToQuaternion(gpu::matrix33 &array, double &w, double &x, double &y, double &z);
    //Quaternion to Euler
    void QuaternionToEuler(double w, double x, double y, double z, double& xRot, double& yRot, double& zRot);
    //RotationMatrix to Euler
    void RotationMatrixToEuler(gpu::matrix33& rotationMatrix, double& xRot, double& yRot, double& zRot);
    //Camera Parameters
    static Eigen::Matrix3d _cameraParameters = Eigen::Matrix3d::Identity();
    Eigen::Matrix3d& GetCameraParameters();
    void SetCameraParameters(double fx, double fy, double ox, double oy);
    std::string Generate3DPlotFile(std::string prefix, std::vector<std::string> files);
    std::string Generate2DPlotFile(std::string prefix, std::vector<std::string> files);
    std::string GeneratePlotDataFile(std::string prefix, std::vector<Eigen::Matrix4d> estimate);
    std::string GeneratePlotDataFile(std::string prefix, std::vector<gpu::matrix33> rotation, std::vector<gpu::float3> translations);

    //Converts from rotation matrix to radians and translation in vector form
    void TransformationToVector(Eigen::Matrix4f& transform, Eigen::Matrix<float, 1, 6>& pointer);
    void TransformationToVector(gpu::matrix33& rotation, gpu::float3& translation, Eigen::Matrix<float, 1, 6>& pointer);

    //Convert from Eigen to matrix33
    void BuildTransformFromRotationAndTranslation(gpu::matrix33& rotation, gpu::float3& translation, Eigen::Matrix4f& transform);
    void BuildRotationAndTranslationFromTransform(Eigen::Matrix4f& transform, gpu::matrix33& rotation, gpu::float3& translation);
    //PCL Stuff
//    pcl::PointCloud<pcl::PointXYZRGB>::Ptr GeneratePCL(std::vector<Eigen::VectorXd>& points, bool color=true);
//    void ViewPointCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr pc, bool downsample=true);
//    void ViewPointClouds(pcl::PointCloud<pcl::PointXYZRGB>::Ptr pc1, pcl::PointCloud<pcl::PointXYZRGB>::Ptr pc2, bool downsample=true);
//
//
//    void SavePointCloud(pcl::PointCloud<pcl::PointXYZRGB>::Ptr pointCloud, std::string filename);
//    pcl::PointCloud<pcl::PointXYZRGB>::Ptr LoadPointCloud(std::string filename);

    //Tests
    void _TestUtilityRotationToQuaternionToRotation();
    void _TestUtilityRotationToEulerToRotation();
    void _TestUtilityEulerToRotationToEuler();
    void _TestUtilityEulerToQuaternionToEuler();
    void _TestUtilityQuaternionToEulerToQuaternion();
};
#endif //VSLAM_UTILITYFUNCTIONS_HPP
