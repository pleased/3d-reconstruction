/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2017/02/27.
//

#include "AssociatedICP.hpp"
AssociatedICP::AssociatedICP() {
    //Set vars
}
/**
 * So we have a point Set in inliers
 * first is the source
 * second is the destination
 * we need to transform the source points to the destination
 * ie transformation from prev Node to current Node
 */

void AssociatedICP::ComputeICP(std::vector<std::pair<gpu::float3, gpu::float3>> newInliersSet, gpu::matrix33& rotation, gpu::float3& translation, bool print) {
    //Compute ICP
    Eigen::Vector4d meanSquaredError;
    meanSquaredError << INFINITY, INFINITY, INFINITY, INFINITY;
    double mse = INFINITY;
    Eigen::Matrix4d estimatedTransform, incTransform;
    estimatedTransform.setIdentity();
    incTransform.setIdentity();
    int i = 0;
    while(i < _maxIterations && mse > 0.01) {
        float tolerance = 1;
        incTransform.setIdentity();
        estimatedTransform = ComputeLeastSquaresRegistration(newInliersSet);
        ApplyRegistration(newInliersSet, incTransform, meanSquaredError);
        estimatedTransform = incTransform * estimatedTransform;
        mse = meanSquaredError(0) + meanSquaredError(1) + meanSquaredError(2);
        rotation.row0.x = estimatedTransform(0, 0);rotation.row0.y = estimatedTransform(0, 1);rotation.row0.z = estimatedTransform(0, 2);translation.x = estimatedTransform(0, 3);
        rotation.row1.x = estimatedTransform(1, 0);rotation.row1.y = estimatedTransform(1, 1);rotation.row1.z = estimatedTransform(1, 2);translation.y = estimatedTransform(1, 3);
        rotation.row2.x = estimatedTransform(2, 0);rotation.row2.y = estimatedTransform(2, 1);rotation.row2.z = estimatedTransform(2, 2);translation.z = estimatedTransform(2, 3);
        if(print) {
            std::cout << "Estimated Transform" << std::endl;
            std::cout << estimatedTransform << std::endl;
            std::cout << "Mean Squared Error: " << mse << std::endl;
        }
        i++;
    }
}
Eigen::Matrix4d AssociatedICP::ComputeLeastSquaresRegistration(std::vector<std::pair<gpu::float3, gpu::float3>>& inliers) {
    Eigen::Vector4d centerMass1, centerMass2;
    ComputeCenterMass(inliers, centerMass1, centerMass2);

    Eigen::Matrix3d sigma = ComputeCrossCovarianceMatrix(inliers, centerMass1, centerMass2);
    Eigen::Matrix4d Q = GenerateQMatrix(sigma);

    Eigen::Vector4d estimate = GetMaxEigenVector(Q);
    Eigen::Matrix4d rotationMatrix = Utility::QuaternionToRotationMatrix(estimate);


    Eigen::Vector4d translation = centerMass2 - (rotationMatrix * centerMass1); //TODO Check this


    rotationMatrix(0, 3) = translation(0);
    rotationMatrix(1, 3) = translation(1);
    rotationMatrix(2, 3) = translation(2);

//        std::cout << "Estimate" << std::endl << rotationMatrix << std::endl;
    return rotationMatrix;
}
void AssociatedICP::ComputeCenterMass(std::vector<std::pair<gpu::float3, gpu::float3>>& inliers, Eigen::Vector4d& center_mass1, Eigen::Vector4d& center_mass2) {
    gpu::float3 cm1;
    cm1.x = 0;
    cm1.y = 0;
    cm1.z = 0;
    gpu::float3 cm2;
    cm2.x = 0;
    cm2.y = 0;
    cm2.z = 0;
    int i;
    for (i = 0; i < inliers.size(); i++) {
        cm1 += inliers[i].first;
        cm2 += inliers[i].second;
    }

    cm1 = cm1 * (1./ inliers.size());
    cm2 = cm2 * (1./ inliers.size());
    center_mass1(0) = cm1.x;
    center_mass1(1) = cm1.y;
    center_mass1(2) = cm1.z;
    center_mass1(3) = 1;

    center_mass2(0) = cm2.x;
    center_mass2(1) = cm2.y;
    center_mass2(2) = cm2.z;
    center_mass2(3) = 1;
}
Eigen::Matrix3d AssociatedICP::ComputeCrossCovarianceMatrix(std::vector<std::pair<gpu::float3, gpu::float3>>& inliers, Eigen::Vector4d& centerMass1, Eigen::Vector4d& centerMass2) {
    Eigen::Matrix3d sigma;
    int i;
    sigma << 0., 0., 0., 0., 0., 0., 0., 0., 0.;
    Eigen::MatrixXd set_p_point(3, 1);
    Eigen::MatrixXd set_x_point(1, 3);
    gpu::float3 diff(0, 0, 0), cm1(centerMass1(0), centerMass1(1), centerMass1(2)), cm2(centerMass2(0), centerMass2(1), centerMass2(2));


    for (i = 0; i < inliers.size(); i++) {
        diff = (inliers[i].first) - cm1;
        set_p_point(0, 0) = diff.x;
        set_p_point(1, 0) = diff.y;
        set_p_point(2, 0) = diff.z;


        diff = (inliers[i].second) - cm2;
        set_x_point(0, 0) = diff.x;
        set_x_point(0, 1) = diff.y;
        set_x_point(0, 2) = diff.z;

        Eigen::Matrix3d result = set_p_point * set_x_point;
        sigma += result;
    }

    return sigma;
}
Eigen::Matrix4d AssociatedICP::GenerateQMatrix(Eigen::Matrix3d& sigma) {
    Eigen::Matrix3d sigmadouble = sigma.transpose();
    Eigen::Matrix3d sigma_difference = sigma - sigmadouble;
    Eigen::Vector3d delta;
    delta(0) = sigma_difference(1, 2);
    delta(1) = sigma_difference(2, 0);
    delta(2) = sigma_difference(0, 1);

    Eigen::Matrix4d Q;
    double tr = sigma.trace();

    Q(0, 0) = tr;
    Q.block<3, 1>(1, 0) = delta;

//    Q(0, 1:) = delta;
    Q.block<1, 3>(0, 1) = delta;

//    Q(1:, 1:) = sigma + sigmadouble - tr * np.eye(3);
    Q.block<3, 3>(1, 1) = sigma + sigmadouble - (tr * Eigen::Matrix3d::Identity());
    return Q;
}

Eigen::Vector4d AssociatedICP::GetMaxEigenVector(Eigen::Matrix4d& Q) {

    Eigen::EigenSolver<Eigen::Matrix4d> eigenSolver(Q);
    Eigen::EigenSolver<Eigen::Matrix<double, 4, 4, 0, 4, 4>>::EigenvectorsType vectors = eigenSolver.eigenvectors();
    Eigen::MatrixXd realVectors = vectors.real();

    Eigen::EigenSolver<Eigen::Matrix<double, 4, 1, 0, 4, 1>>::EigenvectorsType values = eigenSolver.eigenvalues();
    Eigen::MatrixXd realValues = values.real();
    int i, maxIndex = 0;
    double max =0;
    bool foundSolution = false;
    for (i = 0; i < 4; i++) {
        if (max < realValues(i)) {
            max = realValues(i);
            maxIndex = i;
            foundSolution = true;
        }
    }
    //TODO if foundSolution is false we have a problem
    Eigen::Vector4d maxVector = realVectors.col(maxIndex);
    return maxVector;
}

void AssociatedICP::ApplyRegistration(std::vector<std::pair<gpu::float3, gpu::float3>>& inliers, Eigen::Matrix4d& transformation, Eigen::Vector4d& mse) {

    gpu::matrix33 rotation;
    gpu::float3 translation(transformation(0, 3), transformation(1, 3), transformation(2, 3));
    rotation.row0.x = transformation(0, 0);    rotation.row0.y = transformation(0, 1);    rotation.row0.z = transformation(0, 2);
    rotation.row1.x = transformation(1, 0);    rotation.row1.y = transformation(1, 1);    rotation.row1.z = transformation(1, 2);
    rotation.row2.x = transformation(2, 0);    rotation.row2.y = transformation(2, 1);    rotation.row2.z = transformation(2, 2);
    gpu::float3 difference(0, 0, 0);
    int i;
    for(i = 0; i < inliers.size(); i++) {
        gpu::float3 source = inliers[i].first;
        gpu::float3 transformedPoint = rotation * source + translation;

        //Update inliers points
        inliers[i].first = transformedPoint;
        difference += transformedPoint - inliers[i].second;
        difference.x *= difference.x;
        difference.y *= difference.y;
        difference.z *= difference.z;
    }
    mse(0) = difference.x;
    mse(1) = difference.y;
    mse(2) = difference.z;
    mse(3) = 0;
}