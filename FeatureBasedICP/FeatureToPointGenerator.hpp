/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by jonny91289 on 2016/05/19.
//

#ifndef VSLAM_FEATURETOPOINTGENERATOR_HPP
#define VSLAM_FEATURETOPOINTGENERATOR_HPP
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/SVD>
#include <opencv2/xfeatures2d.hpp>
#include "opencv2/cudafeatures2d.hpp"
#include "opencv2/xfeatures2d/cuda.hpp"
#include <iostream>
#include <random>
#include "../KinectFusion/DeviceTypes.hpp"
#define FEATUREDETECTOR 1 //0 SIFT,  1 SURF, 3 CUDA SURF

class FeatureToPointGenerator {
public:
    FeatureToPointGenerator();
    FeatureToPointGenerator(int maxFeatures, float featureMetric, int RANSACMaxIterations, float RANSACMaxDistance, Eigen::Matrix3d& intrensicCamera);
    FeatureToPointGenerator(int maxFeatures, float featureMetric, int RANSACMaxIterations, float RANSACMaxDistance, double fx, double fy, double ox, double oy);
    void SetParameters(int maxFeatures, float featureMetric, int RANSACMaxIterations, float RANSACMaxDistance, Eigen::Matrix3d& intrensicCamera);
    void SetParameters(int maxFeatures, float featureMetric, int RANSACMaxIterations, float RANSACMaxDistance, double fx, double fy, double ox, double oy);
    void SetDepthParameters(double minDepth, double maxDepth);
    void GenerateFeaturePoints( cv::Mat& rgbSource,
                                std::vector<cv::KeyPoint>& keyPoints_1,
                                cv::Mat& descriptors_1);

    std::vector<std::pair<int, int>> GenerateMatches(std::vector<cv::KeyPoint>& keypoints_1,
                                                                              cv::Mat& descriptors_1,
                                                                              std::vector<cv::KeyPoint>& keypoints_2,
                                                                              cv::Mat& descriptors_2);
    std::vector<std::pair<gpu::float3, gpu::float3>> BuildPointPairs(std::vector<std::pair<int, int>>& association,
                                                                     cv::Mat_<double>& sourceImage, std::vector<cv::KeyPoint>& sourceKeyPoints,
                                                                     cv::Mat_<double>& destinationImage, std::vector<cv::KeyPoint>& destinationKeyPoints,
                                                                        std::vector<std::pair<int, int>>& depthFilteredMatches);
    std::vector<std::pair<Eigen::Vector4d, Eigen::Vector4d> > GeneratePointPairs(cv::Mat& rgbSource,
                                                                                 cv::Mat_<double>& depthSource,
                                                                                 Eigen::Matrix4d& initSourceTransform,
                                                                                 cv::Mat& rgbTarget,
                                                                                 cv::Mat_<double>& depthTarget,
                                                                                 Eigen::Matrix4d& initTargetTransform);
    std::vector<std::pair<cv::KeyPoint, cv::KeyPoint>> RANSAC(std::vector<std::pair<cv::KeyPoint, cv::KeyPoint>> pointSet);
    std::vector<std::pair<int, int>> RANSAC2(std::vector<std::pair<int, int>>& pointSet, std::vector<cv::KeyPoint>&, std::vector<cv::KeyPoint>&);

    void SplitPointPairs(std::vector<std::pair<Eigen::Vector4d, Eigen::Vector4d>> &pointsPairs,
                         std::vector<Eigen::Vector4d>& sourcePoints, std::vector<Eigen::Vector4d>& targetPoints);
    void ViewRANSAC(cv::Mat rgbSource, cv::Mat rgbTarget, std::vector<std::pair<cv::KeyPoint, cv::KeyPoint>> vector);
    void ViewRANSAC2(cv::Mat& rgbSource, cv::Mat& rgbTarget, std::vector<std::pair<int, int>>& keyPointMatches,
    std::vector<cv::KeyPoint>& kp1, std::vector<cv::KeyPoint>& kp2);
private:
    gpu::float3 GeneratePoint(cv::Point2f& point2f, cv::Mat_<double> depthImage);
    void GeneratePoint(cv::Point2f& point2f, double depth, Eigen::Vector4d& point);
protected:
    int _maxFeatures = 10;
    float _featureMetric = 50;
    double _minDepth, _maxDepth;
    double _fx, _fy, _invFx, _invFy, _ox, _oy;
    std::default_random_engine _generator;

    int _maxRANSACIterations = 100;

    double _deltaRANSAC = 10.;
};


#endif //VSLAM_FEATURETOPOINTGENERATOR_HPP
