/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2017/02/27.
//

#ifndef VSLAM_FEATUREBASEDICP_HPP
#define VSLAM_FEATUREBASEDICP_HPP
#include "../KinectFusion/DeviceTypes.hpp"
#include "../UtilityFunctions.hpp"
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/Eigenvalues>
/**
 * This is actually registration not ICP
 */
class AssociatedICP {
public:
    AssociatedICP();
    void ComputeICP(std::vector<std::pair<gpu::float3, gpu::float3>>, gpu::matrix33& rotation, gpu::float3& translation, bool print=false);
    Eigen::Matrix4d ComputeLeastSquaresRegistration(std::vector<std::pair<gpu::float3, gpu::float3>>& inliers);
    void ComputeCenterMass(std::vector<std::pair<gpu::float3, gpu::float3>>& inliers, Eigen::Vector4d& center_mass1, Eigen::Vector4d& center_mass2);
    Eigen::Matrix3d ComputeCrossCovarianceMatrix(std::vector<std::pair<gpu::float3, gpu::float3>>& inliers, Eigen::Vector4d& centerMass1, Eigen::Vector4d& centerMass2);
    Eigen::Matrix4d GenerateQMatrix(Eigen::Matrix3d& sigma);

    //Get new Estimate of Quaternions
    Eigen::Vector4d GetMaxEigenVector(Eigen::Matrix4d& sigma);

    void ApplyRegistration(std::vector<std::pair<gpu::float3, gpu::float3>>& inliers, Eigen::Matrix4d& transformation, Eigen::Vector4d& mse);
private:
    int _maxIterations = 10;
};


#endif //VSLAM_FEATUREBASEDICP_HPP
