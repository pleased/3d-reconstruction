/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by jonny91289 on 2016/05/19.
//

#include "FeatureToPointGenerator.hpp"

FeatureToPointGenerator::FeatureToPointGenerator() {

}

FeatureToPointGenerator::FeatureToPointGenerator(int maxFeatures, float featureMetric, int RANSACMaxIterations, float RANSACMaxDistance, Eigen::Matrix3d& intrensicCamera) {
    _maxFeatures = maxFeatures;
    _minDepth = 0.3;
    _maxDepth = 5.;
    _fx = intrensicCamera(0, 0);
    _fy = intrensicCamera(1, 1);
    _invFx = 1./_fx;
    _invFy = 1./_fy;
    _ox = intrensicCamera(0, 2);
    _oy = intrensicCamera(1, 2);
    _featureMetric = featureMetric;
    _maxRANSACIterations = RANSACMaxIterations;
    _deltaRANSAC = RANSACMaxDistance;

}

FeatureToPointGenerator::FeatureToPointGenerator(int maxFeatures, float featureMetric, int RANSACMaxIterations, float RANSACMaxDistance, double fx, double fy, double ox, double oy) {
    _maxFeatures = maxFeatures;
    _minDepth = 0.3;
    _maxDepth = 5.;
    _fx = fx;
    _fy = fy;
    _invFx = 1./_fx;
    _invFy = 1./_fy;
    _ox = ox;
    _oy = oy;
    _featureMetric = featureMetric;
    _maxRANSACIterations = RANSACMaxIterations;
    _deltaRANSAC = RANSACMaxDistance;
}
void FeatureToPointGenerator::SetParameters(int maxFeatures, float featureMetric, int RANSACMaxIterations, float RANSACMaxDistance, double fx, double fy, double ox, double oy) {
    _maxFeatures = maxFeatures;
    _minDepth = 0.3;
    _maxDepth = 5.;
    _fx = fx;
    _fy = fy;
    _invFx = 1./_fx;
    _invFy = 1./_fy;
    _ox = ox;
    _oy = oy;
    _featureMetric = featureMetric;
    _maxRANSACIterations = RANSACMaxIterations;
    _deltaRANSAC = RANSACMaxDistance;
}
void FeatureToPointGenerator::SetParameters(int maxFeatures, float featureMetric, int RANSACMaxIterations, float RANSACMaxDistance, Eigen::Matrix3d& intrensicCamera) {
    _maxFeatures = maxFeatures;
    _minDepth = 0.3;
    _maxDepth = 5.;
    _fx = intrensicCamera(0, 0);
    _fy = intrensicCamera(1, 1);
    _invFx = 1./_fx;
    _invFy = 1./_fy;
    _ox = intrensicCamera(0, 2);
    _oy = intrensicCamera(1, 2);
    _featureMetric = featureMetric;
    _maxRANSACIterations = RANSACMaxIterations;
    _deltaRANSAC = RANSACMaxDistance;
}

void FeatureToPointGenerator::SetDepthParameters(double minDepth, double maxDepth) {
    _minDepth = minDepth;
    _maxDepth = maxDepth;
}


//Using KinectFusion
void FeatureToPointGenerator::GenerateFeaturePoints(cv::Mat& rgbSource,
                                                    std::vector<cv::KeyPoint>& keypoints_1,
                                                    cv::Mat& descriptors_1 ) {
    #if FEATUREDETECTOR == 3
//    cv::cuda::printShortCudaDeviceInfo(cv::cuda::getDevice());
    clock_t  b, e;
    cv::cuda::SURF_CUDA surf_cuda;
    cv::cuda::GpuMat img1;
    cv::Mat greyMat;
    b = clock();
    cv::cvtColor(rgbSource, greyMat, cv::COLOR_BGR2GRAY);
    e = clock();
    std::cout << "Convert to grey Time: " << double(e - b) /CLOCKS_PER_SEC << std::endl;
    b = clock();
    img1.upload(greyMat);
    e = clock();
    std::cout << "Upload to gpu Time: " << double(e - b) /CLOCKS_PER_SEC << std::endl;
    cv::cuda::GpuMat keyPoints, descriptors;
    b = clock();
    surf_cuda(img1, cv::cuda::GpuMat(), keyPoints, descriptors, false);
    e = clock();
    std::cout << "CUDA SURF TIME: " << double(e - b) /CLOCKS_PER_SEC << std::endl;
    return;
    #endif
    cv::Ptr<cv::Feature2D> f2d;

    #if FEATUREDETECTOR == 0
    f2d = cv::xfeatures2d::SIFT::create();
    #elif FEATUREDETECTOR == 1
//    cv::Ptr<cv::Feature2D> f2d = cv::xfeatures2d::SURF::create();
    f2d = cv::xfeatures2d::SURF::create();

    #endif
    #if FEATUREDETECTOR == 0 || FEATUREDETECTOR == 1
    f2d->detectAndCompute(rgbSource, cv::noArray(), keypoints_1, descriptors_1);
//    f2d->detect( rgbSource, keypoints_1 );
//    std::vector<cv::KeyPoint> tempKP;
//    int i;
//    //TODO omp
//    for(i = 0; i < keypoints_1.size(); i++) {
//        float depth = (float) depthSource((int) keypoints_1[i].pt.y, (int) keypoints_1[i].pt.x);
//        if ( (_minDepth < depth && depth < _maxDepth) ) {
//            tempKP.push_back(keypoints_1[i]);
//            // Generate FeaturesPoints
//            gpu::float3 point;
//            point.x = (keypoints_1[i].pt.x - _ox) * depth * _invFx ;
//            point.y = (keypoints_1[i].pt.y - _oy) * depth * _invFy ;
//            point.z = depth;
//            featurePoints.push_back(point);
//        }
//    }
//    keypoints_1.clear();
//    keypoints_1 = tempKP;
//    f2d->compute( rgbSource, keypoints_1, descriptors_1 );
    #endif
}
std::vector<std::pair<int, int>> FeatureToPointGenerator::GenerateMatches(std::vector<cv::KeyPoint>& keypoints_1,
                                              cv::Mat& descriptors_1,
                                              std::vector<cv::KeyPoint>& keypoints_2,
                                              cv::Mat& descriptors_2) {
    //-- Step 3: Matching descriptor vectors using BFMatcher :
    cv::BFMatcher matcher;
    std::vector< cv::DMatch > matches;
    //              query           train
//    try {
    std::cout << descriptors_1.type() << " " << descriptors_2.type() << std::endl;
    std::cout << descriptors_1.cols << " " << descriptors_1.rows << std::endl;
    std::cout << descriptors_2.cols << " " << descriptors_2.rows << std::endl;
        matcher.match( descriptors_1, descriptors_2, matches );
//    } catch (cv::Exception & e ) {
//        std::cout << "In catch" << std::endl;
//        std::cout << e.func << " " << e.line <<  std::endl;
//        std::cout << e.msg << std::endl;
//        exit(0);
//    }
    int i;
    std::vector<std::pair<int, int>> keyPointMatches;
    std::cout << "feature Metric is " << _featureMetric << std::endl;
    for(i = 0; i < matches.size(); i++) {
        if (matches[i].distance > _featureMetric) {
            continue;
        }
//        std::cout << matches[i].distance << std::endl;
        int queryIndex = matches[i].queryIdx;
        int trainIndex = matches[i].trainIdx;
        keyPointMatches.push_back(std::pair<int, int>(queryIndex, trainIndex));
    }
    return keyPointMatches;
}
//
//std::vector<std::pair<gpu::float3, gpu::float3>> FeatureToPointGenerator::Generate3DPoints(std::vector<std::pair<cv::KeyPoint, cv::KeyPoint>> matches, cv::Mat_<float> depthCurrent, cv::Mat_<float> depthNext) {
//
//
//    int i;
//    std::vector<std::pair<gpu::float3, gpu::float3>> pointMatches;
//    for(i = 0; i < matches.size(); i++) {
//        std::pair<cv::KeyPoint, cv::KeyPoint> match = matches[i];
//
//        float depthC = depthCurrent((int) match.first.pt.y, (int) match.first.pt.x);
//
//        gpu::float3 pointCurrent;
//        pointCurrent.x = (match.first.pt.x - _ox) * depthC * _invFx ;
//        pointCurrent.y = (match.first.pt.y - _oy) * depthC * _invFy ;
//        pointCurrent.z = depthC;
//
//        float depthN = depthNext((int) match.second.pt.y, (int) match.second.pt.x);
//        gpu::float3 pointNext;
//        pointNext.x = (match.second.pt.x - _ox) * depthN * _invFx ;
//        pointNext.y = (match.second.pt.y - _oy) * depthN * _invFy ;
//        pointNext.z = depthN;
//
//        pointMatches.push_back(std::pair<gpu::float3, gpu::float3>(pointCurrent, pointNext));
//    }
//    return pointMatches;
//}
//



std::vector<std::pair<Eigen::Vector4d, Eigen::Vector4d> > FeatureToPointGenerator::GeneratePointPairs(cv::Mat& rgbSource,
                                                                                                      cv::Mat_<double>& depthSource,
                                                                                                      Eigen::Matrix4d& initSourceTransform,
                                                                                                      cv::Mat& rgbTarget,
                                                                                                      cv::Mat_<double>& depthTarget,
                                                                                                      Eigen::Matrix4d& initTargetTransform) {
    cv::Ptr<cv::Feature2D> f2d;
    #if FEATUREDETECTOR == 0
    f2d = cv::xfeatures2d::SIFT::create();
    #elif FEATUREDETECTOR == 1
    f2d = cv::xfeatures2d::SURF::create();
    #endif

    std::vector<cv::KeyPoint> keypoints_1, keypoints_2;
    f2d->detect( rgbSource, keypoints_1 );
    f2d->detect( rgbTarget, keypoints_2 );

    //-- Step 2: Calculate descriptors (feature vectors)
    cv::Mat descriptors_1, descriptors_2;
    f2d->compute( rgbSource, keypoints_1, descriptors_1 );
    f2d->compute( rgbTarget, keypoints_2, descriptors_2 );

    //-- Step 3: Matching descriptor vectors using BFMatcher :
    cv::BFMatcher matcher;
    std::vector< cv::DMatch > matches;
    //              query           train
    matcher.match( descriptors_1, descriptors_2, matches );

    //-- Step 4: Generate points and filter out those without depth info
    int i;
    std::vector< cv::DMatch > newMatches;
    std::vector<std::pair<cv::KeyPoint, cv::KeyPoint>> keyPointMatches;
    for (i = 0; i < matches.size(); i++) {
        if (matches[i].distance > 50.) {
            std::cout << matches[i].distance << std::endl;
            continue;
        }
        int queryIndex = matches[i].queryIdx;
        int trainIndex = matches[i].trainIdx;

//        keypoints_1[queryIndex] == keyPoints_2[trainIndex]

        cv::Point2f queryPoint = keypoints_1[queryIndex].pt;
        cv::Point2f trainPoint = keypoints_2[trainIndex].pt;
        double queryDepth = depthSource((int)queryPoint.y, (int)queryPoint.x);
        double trainDepth = depthTarget((int)trainPoint.y, (int)trainPoint.x);
        if ( (_minDepth < queryDepth && queryDepth < _maxDepth) &&
                (_minDepth < trainDepth && trainDepth < _maxDepth)) {
            keyPointMatches.push_back(std::pair<cv::KeyPoint, cv::KeyPoint>(keypoints_1[queryIndex], keypoints_2[trainIndex]));
//            std::cout << "Accepted feature pair" << std::endl;
//            Eigen::Vector4d queryLocal, queryWorld, trainLocal, trainWorld;
//            GeneratePoint(queryPoint, queryDepth, queryLocal);
//            GeneratePoint(trainPoint, trainDepth, trainLocal);
//            queryWorld = initSourceTransform * queryLocal;
//            trainWorld = initTargetTransform * trainLocal;
//            std::pair<Eigen::Vector4d, Eigen::Vector4d> point(queryWorld, trainWorld);
//            generatedPoints.push_back(point);
//            std::cout << matches[i].distance << std::endl;
//            newMatches.push_back(matches[i]);
        }
    }
    std::cout << "Size: " << keyPointMatches.size() << std::endl;
//    exit(0);
    //-- Step 5: Perform RANSAC on the generated point pairs to find largest concenous
    keyPointMatches = RANSAC(keyPointMatches);

    //-- Step 6: Generate world position of each feature using the initial transformations
    std::vector<std::pair<Eigen::Vector4d, Eigen::Vector4d>> generatedPointPairs;
//    ViewRANSAC(rgbSource, rgbTarget, keyPointMatches);
    generatedPointPairs.resize(keyPointMatches.size());
//    exit(0);
    for (i = 0; i < keyPointMatches.size(); i++) {
        cv::Point2f queryPoint = keyPointMatches[i].first.pt;
        cv::Point2f trainPoint = keyPointMatches[i].second.pt;
        double queryDepth = depthSource((int)queryPoint.y, (int)queryPoint.x);
        double trainDepth = depthTarget((int)trainPoint.y, (int)trainPoint.x);
        Eigen::Vector4d queryLocal, queryWorld, trainLocal, trainWorld;
        GeneratePoint(queryPoint, queryDepth, queryLocal);
        GeneratePoint(trainPoint, trainDepth, trainLocal);
        queryWorld = queryLocal;
        trainWorld = trainLocal;
        std::pair<Eigen::Vector4d, Eigen::Vector4d> points(queryWorld, trainWorld);
        generatedPointPairs[i] = points;
    }
    return generatedPointPairs;
}
std::vector<std::pair<gpu::float3, gpu::float3>> FeatureToPointGenerator::BuildPointPairs(
        std::vector<std::pair<int, int>>& association,
        cv::Mat_<double>& sourceImage, std::vector<cv::KeyPoint>& sourceKeyPoints,
        cv::Mat_<double>& destinationImage, std::vector<cv::KeyPoint>& destinationKeyPoints,
        std::vector<std::pair<int, int>>& depthFilteredMatches) {
    long i;
    std::vector<std::pair<gpu::float3, gpu::float3>> points;
    std::cout << "Iterating" << std::endl;
    std::cout << "Depth: " << _minDepth << " " << _maxDepth << std::endl;
    for(i = 0l; i < association.size(); i++) {
        int p1 = association[i].first;
        int p2 = association[i].second;
        float sourceDepth = (float) sourceImage((int) sourceKeyPoints[p1].pt.y, (int) sourceKeyPoints[p1].pt.x);
        float destinationDepth = (float) destinationImage((int) destinationKeyPoints[p2].pt.y, (int) destinationKeyPoints[p2].pt.x);
        if(_minDepth < sourceDepth && sourceDepth < _maxDepth
           && _minDepth < destinationDepth && destinationDepth < _maxDepth) {
            points.push_back(std::pair<gpu::float3, gpu::float3>(GeneratePoint(sourceKeyPoints[p1].pt, sourceImage),
                                                                 GeneratePoint(destinationKeyPoints[p2].pt,
                                                                               destinationImage)));
            depthFilteredMatches.push_back(std::pair<int, int>(p1, p2));
        } else {
            std::cout << "Failed depth values: " << sourceDepth << " " << destinationDepth << std::endl;
        }
    }
    std::cout << "Passing  ? " << std::endl;
    return points;
}
gpu::float3 FeatureToPointGenerator::GeneratePoint(cv::Point2f& point2f, cv::Mat_<double> depthImage) {
    float depth = (float) depthImage((int) point2f.y, (int) point2f.x);
    gpu::float3 point;
    point.x = (float)((point2f.x - _ox) * depth * _invFx) ;
    point.y = (float)((point2f.y - _oy) * depth * _invFy) ;
    point.z = depth;
//    std::cout << point.x << " " << point.y << " " << point.z << std::endl;
    return point;
}
void FeatureToPointGenerator::GeneratePoint(cv::Point2f& point2f, double depth, Eigen::Vector4d& point) {
    point(0) = (point2f.x - _ox) * depth * _invFx ;
    point(1) = (point2f.y - _oy) * depth * _invFy ;
    point(2) = depth;
    point(3) = 1.;
}

std::vector<std::pair<cv::KeyPoint, cv::KeyPoint>> FeatureToPointGenerator::RANSAC(std::vector<std::pair<cv::KeyPoint, cv::KeyPoint>> featureSet) {
    int i, j;
    std::uniform_int_distribution<int> distribution(0, featureSet.size() - 1);
    std::pair<cv::KeyPoint, cv::KeyPoint> pairs[4];
    assert(featureSet.size() > 0);

    std::vector<std::vector<std::pair<cv::KeyPoint, cv::KeyPoint>>> inlierSet;
    for (i = 0; i < _maxRANSACIterations; i++) {
//        std::cout << distribution(_generator) << " " << distribution(_generator) << " " << distribution(_generator) << std::endl;
        //Pick 4 random feature pairs
        pairs[0] = featureSet[distribution(_generator)];
        pairs[1] = featureSet[distribution(_generator)];
        pairs[2] = featureSet[distribution(_generator)];
        pairs[3] = featureSet[distribution(_generator)];
        //Generate H matrix
        Eigen::Matrix<double, 8, 9> H;
        for (j = 0; j < 4; j++) {
//            H.row(j * 2 )       <<  -pairs[j].first.pt.x, -pairs[j].first.pt.y, -1, 0, 0, 0, pairs[j].first.pt.x * pairs[j].second.pt.x,
//                    pairs[j].first.pt.y * pairs[j].second.pt.x, pairs[j].second.pt.x;
//            H.row(j * 2 + 1)    << 0, 0, 0, -pairs[j].first.pt.x, -pairs[j].first.pt.y, -1, pairs[j].first.pt.x * pairs[j].second.pt.y,
//                    pairs[j].first.pt.y * pairs[j].second.pt.y, pairs[j].second.pt.y;
            H.row(j * 2 )       <<  pairs[j].first.pt.x, pairs[j].first.pt.y, 1, 0, 0, 0, -pairs[j].first.pt.x * pairs[j].second.pt.x,
                    -pairs[j].first.pt.y * pairs[j].second.pt.x, -pairs[j].second.pt.x;
            H.row(j * 2 + 1)    << 0, 0, 0, pairs[j].first.pt.x, pairs[j].first.pt.y, 1, -pairs[j].first.pt.x * pairs[j].second.pt.y,
                    -pairs[j].first.pt.y * pairs[j].second.pt.y, -pairs[j].second.pt.y;
        }

//        std::cout << "Homography " << std::endl;
//        std::cout << H << std::endl;
        // SVD to compute homography
        Eigen::JacobiSVD<Eigen::Matrix<double, 8, 9>> svd(H, Eigen::ComputeFullU | Eigen::ComputeFullV);
        Eigen::Matrix<double, 9, 9> V = svd.matrixV();
        Eigen::Matrix<double, 9, 1> solutionCol = V.col(V.cols() - 1);
        Eigen::Matrix<double, 3, 3> homography(solutionCol.data());
        homography.transposeInPlace();
        std::vector<std::pair<cv::KeyPoint, cv::KeyPoint>> inliers;
        //compute inliers
        for (j = 0; j < featureSet.size(); j++) {
            Eigen::Vector3d point, pointPrime;
            point << featureSet[j].first.pt.x, featureSet[j].first.pt.y, 1.;
            pointPrime << featureSet[j].second.pt.x, featureSet[j].second.pt.y, 1;
            Eigen::Vector3d transformed = homography * point;
            transformed = transformed / transformed(2);
            double distance = sqrt(pow(transformed(0) - pointPrime(0), 2) + pow(transformed(1) - pointPrime(1), 2));//euclidean distance
            if (distance < _deltaRANSAC) {
                inliers.push_back(featureSet[j]);
            }
        }
        inlierSet.push_back(inliers);
    }
    std::vector<std::pair<cv::KeyPoint, cv::KeyPoint>> newFeatureSet;
//    std::cout << "Selecting new set" << std::endl;
    for( i = 0 ;i < inlierSet.size(); i++) {
        if (inlierSet[i].size() > newFeatureSet.size()) {
            newFeatureSet = inlierSet[i];
        }
    }
    return newFeatureSet;
}

std::vector<std::pair<int, int>> FeatureToPointGenerator::RANSAC2(std::vector<std::pair<int, int>>& featureSet, std::vector<cv::KeyPoint>& kp1, std::vector<cv::KeyPoint>& kp2) {
    int i, j;
    std::uniform_int_distribution<int> distribution(0, featureSet.size() - 1);
    std::pair<int, int> pairs[4];
    assert(featureSet.size() > 0);

    std::vector<std::vector<std::pair<int, int>>> inlierSet;
    for (i = 0; i < _maxRANSACIterations; i++) {
//        std::cout << distribution(_generator) << " " << distribution(_generator) << " " << distribution(_generator) << std::endl;
        //Pick 4 random feature pairs
        pairs[0] = featureSet[distribution(_generator)];
        pairs[1] = featureSet[distribution(_generator)];
        pairs[2] = featureSet[distribution(_generator)];
        pairs[3] = featureSet[distribution(_generator)];

        //Generate H matrix
        Eigen::Matrix<double, 8, 9> H;
        for (j = 0; j < 4; j++) {
//            H.row(j * 2 )       <<  -pairs[j].first.pt.x, -pairs[j].first.pt.y, -1, 0, 0, 0, pairs[j].first.pt.x * pairs[j].second.pt.x,
//                    pairs[j].first.pt.y * pairs[j].second.pt.x, pairs[j].second.pt.x;
//            H.row(j * 2 + 1)    << 0, 0, 0, -pairs[j].first.pt.x, -pairs[j].first.pt.y, -1, pairs[j].first.pt.x * pairs[j].second.pt.y,
//                    pairs[j].first.pt.y * pairs[j].second.pt.y, pairs[j].second.pt.y;
            H.row(j * 2 )       <<  kp1[pairs[j].first].pt.x, kp1[pairs[j].first].pt.y, 1, 0, 0, 0, -kp1[pairs[j].first].pt.x * kp2[pairs[j].second].pt.x,
                    -kp1[pairs[j].first].pt.y * kp2[pairs[j].second].pt.x, -kp2[pairs[j].second].pt.x;
            H.row(j * 2 + 1)    << 0, 0, 0, kp1[pairs[j].first].pt.x, kp1[pairs[j].first].pt.y, 1, -kp1[pairs[j].first].pt.x * kp2[pairs[j].second].pt.y,
                    -kp1[pairs[j].first].pt.y * kp2[pairs[j].second].pt.y, -kp2[pairs[j].second].pt.y;
        }

//        std::cout << "Homography " << std::endl;
//        std::cout << H << std::endl;
        // SVD to compute homography
        Eigen::JacobiSVD<Eigen::Matrix<double, 8, 9>> svd(H, Eigen::ComputeFullU | Eigen::ComputeFullV);
        Eigen::Matrix<double, 9, 9> V = svd.matrixV();
        Eigen::Matrix<double, 9, 1> solutionCol = V.col(V.cols() - 1);
        Eigen::Matrix<double, 3, 3> homography(solutionCol.data());
        homography.transposeInPlace();
        std::vector<std::pair<int, int>> inliers;
        //compute inliers
        for (j = 0; j < featureSet.size(); j++) {
            Eigen::Vector3d point, pointPrime;
            point << kp1[featureSet[j].first].pt.x, kp1[featureSet[j].first].pt.y, 1.;
            pointPrime << kp2[featureSet[j].second].pt.x, kp2[featureSet[j].second].pt.y, 1;
            Eigen::Vector3d transformed = homography * point;
            transformed = transformed / transformed(2);
            double distance = sqrt(pow(transformed(0) - pointPrime(0), 2) + pow(transformed(1) - pointPrime(1), 2));//euclidean distance
            if (distance < _deltaRANSAC) {
                inliers.push_back(featureSet[j]);
            }
        }
        inlierSet.push_back(inliers);
    }
    std::vector<std::pair<int, int>> newFeatureSet;
//    std::cout << "Selecting new set" << std::endl;
    for( i = 0 ;i < inlierSet.size(); i++) {
        if (inlierSet[i].size() > newFeatureSet.size()) {
            newFeatureSet = inlierSet[i];
        }
    }
    return newFeatureSet;
}

void FeatureToPointGenerator::SplitPointPairs(std::vector<std::pair<Eigen::Vector4d, Eigen::Vector4d>> &pointsPairs,
                                                std::vector<Eigen::Vector4d>& sourcePoints, std::vector<Eigen::Vector4d>& targetPoints) {

    sourcePoints.resize(pointsPairs.size());
    targetPoints.resize(pointsPairs.size());
    int i;
    for (i = 0; i < pointsPairs.size(); i++ ) {
        sourcePoints[i] = pointsPairs[i].first;
        targetPoints[i] = pointsPairs[i].second;
    }
}

void FeatureToPointGenerator::ViewRANSAC(cv::Mat rgbSource, cv::Mat rgbTarget, std::vector<std::pair<cv::KeyPoint, cv::KeyPoint>> keyPointMatches) {
    //Build DMATCHES
    std::vector<cv::DMatch> dmatches(keyPointMatches.size());
    std::vector<cv::KeyPoint> query(keyPointMatches.size());
    std::vector<cv::KeyPoint> train(keyPointMatches.size());
    int i = 0;
    for (i = 0; i < dmatches.size(); i++) {
        cv::DMatch match;
        match.queryIdx = i;
        match.trainIdx = i;
        query[i] = keyPointMatches[i].first;
        train[i] = keyPointMatches[i].second;
        dmatches[i] = match;
    }
    cv::Mat matchImage;
    cv::drawMatches(rgbSource, query, rgbTarget, train, dmatches, matchImage);
    cv::imshow("Matches", matchImage);
    cv::waitKey(0);
}
void FeatureToPointGenerator::ViewRANSAC2(cv::Mat& rgbSource, cv::Mat& rgbTarget, std::vector<std::pair<int, int>>& keyPointMatches,
                                          std::vector<cv::KeyPoint>& kp1, std::vector<cv::KeyPoint>& kp2) {
    //Build DMATCHES
    std::vector<cv::DMatch> dmatches(keyPointMatches.size());
    std::vector<cv::KeyPoint> query(keyPointMatches.size());
    std::vector<cv::KeyPoint> train(keyPointMatches.size());
    int i = 0;
    for (i = 0; i < dmatches.size(); i++) {
        cv::DMatch match;
        match.queryIdx = i;
        match.trainIdx = i;
        query[i] = kp1[keyPointMatches[i].first];
        train[i] = kp2[keyPointMatches[i].second];
        dmatches[i] = match;
    }
    cv::Mat matchImage;
    cv::drawMatches(rgbSource, query, rgbTarget, train, dmatches, matchImage);
    cv::imshow("Matches", matchImage);
    cv::waitKey(0);
}
















