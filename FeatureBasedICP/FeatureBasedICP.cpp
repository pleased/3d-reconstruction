/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2017/07/12.
//

#include "FeatureBasedICP.hpp"
FBICP::FBICP(int maxFeatures, float featureMetric, int RANSACMaxIterations, float RANSACDistance,
             float minDepth, float maxDepth, double fx, double fy, double ox, double oy) {
    _maxFeatures = maxFeatures;
    _fx = fx;
    _fy = fy;
    _ox = ox;
    _oy = oy;
    _minDepth = minDepth;
    _maxDepth = maxDepth;
    _featureMetric = featureMetric;
    _RANSACMaxIterations = RANSACMaxIterations;
    _RANSACDistance = RANSACDistance;
    featureToPointGenerator.SetParameters(_maxFeatures, featureMetric, RANSACMaxIterations, RANSACDistance, fx, fy, ox, oy);
    featureToPointGenerator.SetDepthParameters(_minDepth, _maxDepth);
}
void FBICP::SetRaycasterImage(cv::Mat rgb, cv::Mat_<double> depth) {
    _previousRGB = rgb;
    _previousDepth = depth;
    _iteration++;
}
bool FBICP::EstimateMotion(cv::Mat rgb, cv::Mat_<double> depth, gpu::matrix33& estimatedRotation, gpu::float3& estimateTranslation) {

//    std::cout << "Types " << rgb.type() << " " << _previousRGB.type() << std::endl;
//    cv::imshow("Raycasted ", _previousRGB);
//    cv::imshow("New ", rgb);
//    cv::waitKey(0);
//    cv::imwrite("TESTA.png", _previousRGB);
//    cv::imwrite("TESTB.png", rgb);
    std::vector<cv::KeyPoint> keyPointsSource;
    std::vector<gpu::float3> featurePointsSource;
    cv::Mat descriptorsSource;
    clock_t begin, end;
    begin = clock();
    featureToPointGenerator.GenerateFeaturePoints(_previousRGB, keyPointsSource, descriptorsSource);
    end = clock();
    std::cout << "Function call: " << double(end - begin)/CLOCKS_PER_SEC << std::endl;
//    _featureKeyPointDatabase.push_back(keyPointsSource);
//    _featurePointsDatabase.push_back(featurePointsSource);
//    _featureDescriptorDatabase.push_back(descriptorsSource);



    std::vector<cv::KeyPoint> keyPointsDestination;
    std::vector<gpu::float3> featurePointsDestination;
    cv::Mat descriptorsDestination;
    begin = clock();
    featureToPointGenerator.GenerateFeaturePoints(rgb, keyPointsDestination, descriptorsDestination);
    end = clock();
    std::cout << "Function call: " << double(end - begin)/CLOCKS_PER_SEC << std::endl;


    std::vector<std::pair<int, int>> matches;
    begin = clock();
    matches = featureToPointGenerator.GenerateMatches(keyPointsSource, descriptorsSource,
                                                      keyPointsDestination, descriptorsDestination);

    end = clock();
    cv::imshow("Source", _previousRGB);
    cv::imshow("Destination", rgb);
    cv::waitKey(0);
    std::cout << double(end - begin)/CLOCKS_PER_SEC << std::endl;
    std::vector<std::pair<int, int>> filteredMatches, depthFilteredMatches;
    begin = clock();
    filteredMatches = featureToPointGenerator.RANSAC2(matches, keyPointsSource, keyPointsDestination);
    end = clock();
//    featureToPointGenerator.ViewRANSAC2(_previousRGB, rgb, filteredMatches, keyPointsSource, keyPointsDestination);
    std::cout << double(end - begin)/CLOCKS_PER_SEC << std::endl;
    std::vector<std::pair<gpu::float3, gpu::float3>> icpPoints;
    icpPoints = featureToPointGenerator.BuildPointPairs(filteredMatches, _previousDepth, keyPointsSource, depth, keyPointsDestination, depthFilteredMatches);

    featureToPointGenerator.ViewRANSAC2(_previousRGB, rgb, depthFilteredMatches, keyPointsSource, keyPointsDestination);
    if(icpPoints.size() < 5) {
        return false;
    }
    associatedICP.ComputeICP(icpPoints, estimatedRotation, estimateTranslation, false);
    return true;
}
bool FBICP::CheckLoopClosure() {

}
