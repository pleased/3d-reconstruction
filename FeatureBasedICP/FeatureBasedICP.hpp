/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2017/07/12.
//

#ifndef VSLAM_FBICP_HPP
#define VSLAM_FBICP_HPP
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>
#include <eigen3/Eigen/Dense>
#include <eigen3/Eigen/SVD>
#include <opencv2/xfeatures2d.hpp>
#include <opencv2/features2d.hpp>
#include <iostream>
#include <random>
#include "AssociatedICP.hpp"
#include "FeatureToPointGenerator.hpp"
/**
 * Use the only the raycasters features to store in the database
 */
class FBICP {
public:
    FBICP() {};
    void SetRaycasterImage(cv::Mat, cv::Mat_<double> depth);
    bool EstimateMotion(cv::Mat rgb, cv::Mat_<double> depth, gpu::matrix33& estimatedRotation, gpu::float3& estimateTranslation);
    bool CheckLoopClosure();
    FBICP(int maxFeatures, float featureMetric, int RANSACMaxIterations, float RANSACDistance,
          float minDepth, float maxDepth, double fx, double fy, double ox, double oy);


    FeatureToPointGenerator featureToPointGenerator;
    AssociatedICP associatedICP;

    long _iteration = -1;
    std::vector<std::vector<cv::KeyPoint>>  _featureKeyPointDatabase;
    std::vector<std::vector<gpu::float3>>   _featurePointsDatabase;
    std::vector<cv::Mat>                    _featureDescriptorDatabase;
    bool _ready = false;

    cv::Mat _previousRGB;
    cv::Mat_<double> _previousDepth;

    //FEATURE TO POINT GENERATOR PARAMETERS
    double _fx, _fy, _ox, _oy;
    float _minDepth = 0.3;
    float _maxDepth = 5;
    int _maxFeatures;
    float _featureMetric;
    int _RANSACMaxIterations;
    float _RANSACDistance;
};


#endif //VSLAM_FBICP_HPP
