# README #

This repository contains code for rebuilding a 3D environment and outputting a mesh. vSlam runs the rebuilding process based off the KinectFusion paper by Newcombe.
This system has the additions of Kinitious that allows this system to move to different areas during reconstruction.
This system has three modes for the Kinect Fusion, the first is the online mode, this allows the system to take raw image data from the Kinect for xbox one and rebuild the environment.
The second mode is allows the system to record data from the Kinect for Xbox one and save it to be used in the thrid mode.
The thrid mode allows the system to take data prerecorded data and pass it through the reconstruction proccess. 
After the first or thrid mode has exectued the Ball Pivot algorithm can be executed optionally, the system can save the results from the reconstruction process and can be loaded to rebuild the model later.
For the mesh visualisation blender is used. The script 'GenerateMesh.sh' is used to visualize the output of the BPA reconstruction.

# Building the applications #
This application is built using cmake 3.5. The following dependencies are required for this application
## dependencies ##
* PCL 1.8 <=
* CUDA 7.5
* OpenCV 3.1 with contrib module
* VTKRendering5.1
* libfreenect2
* Boost1.61
* Eigen3
* python3
* Blender 2.73 >=

Create a build folder and run cmake to produce the make file and then use make to build the binary.	
vSlam
	* navigate to the vSlam source folder
	* Create a 'build' directory
	* Execute 'cmake'
	* Then execute the 'make' command
	
# Running vSlam #
Executing vSlam requires a configuration file as an input parameter. An example of a configuration file is provided as sample.conf. An explaination of the configuration is described below.

* KinectFusion = *bool* if true runs the rebuilding process. If false skips to the Model rebuilding algorithm with a loaded pointcloud from *PointCloud*.
* online = *bool* if true ignores the *baseDir* and *dataset* parameters and captures its data directly from a Kinect V2
* V2 = *bool* if true uses camera parameters for the Kinect V2, instead of the Kinect V1
* EnableRecordModeOnly = *bool* enables record mode only uses *RecordModeDestination* as the output directory
* RecordModeFrameRate = *int* the fps for the systemt to record
* RecordModeDestination = *directory* the output directory for the record mode
* breakPoint = *int* Stops the building the model after *int* images, default -1
* baseDir = *dir* base directory where all the datasets are located
* dataSet = *dir* relative directory to *baseDir* of a dataset
* PointCloud = *file.pcl* If KinectFusion is true, stores the rebuilt model.
* ICPModeFrameToFrame = *bool* not used
* WeightFactor = *float* WeightFactor for each new depth map integrated 1
* SetManualMode = *bool* not used 
* IntegrationThreshold= *float* threshold for itergrating a new depth image 0.05
* ICPRotation = *float* max rotation between each iteration of ICP
* ICPTranslation = *float* max translation between each iteration of ICP

* voxelGrid = *bool* If KinectFusion is true, filters the pointcloud that has been generated from the system, If it is false filters the pointcloud that has been loaded.
* filterSize = *float* The voxel filter size, default 0.01
* filterSave = *file.pcl* Saves the filtered pointcloud

* BPA = *bool* If true runs the mesh reconstruction. 
* BPALoad = *file.pcl* If KinectFusion is False, loads a .pcd file to rebuild the mesh.
* BPASave = *file* saves the data to a file
* BPAKDTree = *bool* Not used
* BPALevels = *float sequence* indicates the mesh reconstruction ball radii 

The main configuration of this system is as follows
Enabling the reconstruction mode is used with *KinectFusion* in this mode an offline or online reconstruction can be performed.
*online* enables the online reconstruction if set false this will default to the offline mode.
*baseDir* and *dataSet* is used to determine the directory of the prerecorded dataset.
For the offline mode the *EnableRecordModeOnly* can be used to prerecord a data set this will prevent the reconstruction and the BPA from executing.
If the system has *KinectFusion* enabled the Ball Pivot alogirithm can be run after the point cloud has been generated from the data set by enabling *BPA* with the required parameters. Similarly, if *KinectFusion* is disabled you can load a model using *BPALoad* and execute the BPA, but the *BPA* must be set to true.





# Displaying the reconstructed mesh #
Blender is used to visualise the mesh the script GenerateMesh.sh takes the output of 'BPASave' and builds the model in the .blend format this also exports a fbx object that can be used in game engines.