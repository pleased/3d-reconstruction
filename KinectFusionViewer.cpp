/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include <cv.hpp>
#include "KinectFusionViewer.hpp"
KinectFusionViewer::KinectFusionViewer(int displays, int imageCols, int imageRows, bool record, std::string filename) {
    imageRows_ = imageRows, imageCols_ = imageCols;
    displays_ = displays;
    record_ = record;
    if(record) {
        filename_ = filename;
        recorderBuffer_ = (unsigned char *) calloc(sizeof(unsigned char), 1280 * 960 * 4);
    }
    if(!glfwInit()) {
        std::cerr << "Failed to Init" << std::endl;
        return ;
    }

    glfwWindowHint(GLFW_SAMPLES, 4); // 4x antialiasing
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3); // We want OpenGL 3.3
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE); //We don't want the old OpenGL
    window_ = glfwCreateWindow( imageCols * 2, imageRows * 2, "Kinect Fusion Viewer V2", NULL, NULL);
    if( window_ == NULL ){
        std::cerr << "Couldn't Create window" << std::endl;
        glfwTerminate();
        return ;
    }
    glfwMakeContextCurrent(window_); // Initialize GLEW
    glewExperimental=true; // Needed in core profile
    if (glewInit() != GLEW_OK) {
        std::cerr << "Failed to init GLEW" << std::endl;
        return;
    }
    //Setup input
    glfwSetInputMode(window_, GLFW_STICKY_KEYS, GL_TRUE);
    //Setup Textures
    textureShader= LoadShaders( "Common/TextureVertexShader.vertexshader", "Common/TextureFragmentShader.fragmentshader");
    GenerateBuffers();
    gpuTextures = new int*[displays_];
    TextureID  = glGetUniformLocation(textureShader, "myTextureSampler");

    MatrixID = glGetUniformLocation(textureShader, "MVP");        // Projection matrix : 45° Field of View, 4:3 ratio, display range : 0.1 unit <-> 100 units
    //glm::mat4 Projection = glm::perspective(45.0f, 4.0f / 3.0f, 0.1f, 100.0f);
    Projection = glm::mat4(1.0f);
    //         // Camera matrix^M
    //glm::mat4 View = glm::lookAt(   glm::vec3(4,3,3), // Camera is at (4,3,3), in World Space^M
    //                                glm::vec3(0,0,0), // and looks at the origin^M
    //                                glm::vec3(0,1,0)  // Head is up (set to 0,-1,0 to look upside-down)^M
    //                                );
    View = glm::mat4(1.0f);
    Model      = glm::mat4(1.0f);
    //                                                                                                                                                    // Our ModelViewProjection : multiplication of our 3 matrices^M
    MVP        = Projection * View * Model; // Remember, matrix multiplication is the other way around^M
    GenerateTextureBuffers();

}
KinectFusionViewer::~KinectFusionViewer() {
    DeleteBuffers();
    free(VertexArrayID);
    free(vertexbuffer);
    int i;
    for(i = 0; i < displays_; i++) {
        //cudaFree(gpuTextures[i]);
    }
    free(gpuTextures);
}
void KinectFusionViewer::GenerateBuffers() {
    VertexArrayID= new GLuint[displays_];
    glGenVertexArrays(displays_, VertexArrayID);

    // This will identify our vertex buffer
    vertexbuffer = new GLuint[displays_];
    glGenBuffers(displays_, vertexbuffer);




    //TODO make dynamic
    //Bind Buffers TOP_RIGHT == 0
    glBindVertexArray(VertexArrayID[0]);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer[0]);
    // Give our vertices to OpenGL. UPLOAD
    glBufferData(GL_ARRAY_BUFFER, sizeof(WINDOW_TOP_RIGHT), WINDOW_TOP_RIGHT, GL_DYNAMIC_DRAW);

    //BindBuffers TOP_LEFT
    glBindVertexArray(VertexArrayID[1]);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer[1]);
    // Give our vertices to OpenGL.
    glBufferData(GL_ARRAY_BUFFER, sizeof(WINDOW_TOP_LEFT), WINDOW_TOP_LEFT, GL_DYNAMIC_DRAW);

    //BindBuffers BOTTOM_LEFT
    glBindVertexArray(VertexArrayID[2]);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer[2]);
    // Give our vertices to OpenGL.
    glBufferData(GL_ARRAY_BUFFER, sizeof(WINDOW_BOTTOM_LEFT), WINDOW_BOTTOM_LEFT, GL_DYNAMIC_DRAW);

    //BindBuffers BOTTOM_RIGHT
    glBindVertexArray(VertexArrayID[3]);
    glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer[3]);
    // Give our vertices to OpenGL.
    glBufferData(GL_ARRAY_BUFFER, sizeof(WINDOW_BOTTOM_RIGHT), WINDOW_BOTTOM_RIGHT, GL_DYNAMIC_DRAW);



    //Create UvMap buffer

    glGenBuffers(1, &uvMap_);
    glBindBuffer(GL_ARRAY_BUFFER, uvMap_);
    glBufferData(GL_ARRAY_BUFFER, sizeof(UVMAPPING), UVMAPPING, GL_STATIC_DRAW);
}
void KinectFusionViewer::DeleteBuffers() {
    glDeleteBuffers(displays_, VertexArrayID);
    glDeleteBuffers(displays_, vertexbuffer);
    glDeleteBuffers(1, &uvMap_);
}
bool KinectFusionViewer::AnyKeyPressed() {
    std::cout << "Doesnt work" << std::endl;
    return (glfwGetKey(window_, GLFW_KEY_SPACE) == GLFW_PRESS);
}
bool KinectFusionViewer::GetKeyPressed(int key) {
    return glfwGetKey(window_, key) == GLFW_PRESS;
}
bool KinectFusionViewer::GetKeyReleased(int key) {
    return glfwGetKey(window_, key) == GLFW_RELEASE;
}
bool KinectFusionViewer::CloseWindow() {
    return (glfwGetKey(window_, GLFW_KEY_ESCAPE ) != GLFW_PRESS && glfwWindowShouldClose(window_) == 0);
}
void KinectFusionViewer::SaveVideo() {
    if(record_) {
        //TODO recompile brokencv
//        cv::VideoWriter videoWriter(filename_, CV_FOURCC('M','J','P','G'), 1, cv::Size(1280, 960));
        int i;
        for(i = 0; i < images.size(); i++) {
            std::ostringstream oss;
            oss << "Frame";
            oss << i;
            oss << ".png";
//            videoWriter << images[i];
            cv::imwrite(oss.str(), images[i]);
        }
//        videoWriter.release();
    }
}
GLuint KinectFusionViewer::LoadShaders(const char * vertex_file_path,const char * fragment_file_path){

    // Create the shaders
    GLuint VertexShaderID = glCreateShader(GL_VERTEX_SHADER);
    GLuint FragmentShaderID = glCreateShader(GL_FRAGMENT_SHADER);

    // Read the Vertex Shader code from the file
    std::string VertexShaderCode;
    std::ifstream VertexShaderStream(vertex_file_path, std::ios::in);
    if(VertexShaderStream.is_open()){
        std::string Line = "";
        while(getline(VertexShaderStream, Line))
            VertexShaderCode += "\n" + Line;
        VertexShaderStream.close();
    }else{
        printf("Impossible to open %s. Are you in the right directory ? Don't forget to read the FAQ !\n", vertex_file_path);
        getchar();
        return 0;
    }

    // Read the Fragment Shader code from the file
    std::string FragmentShaderCode;
    std::ifstream FragmentShaderStream(fragment_file_path, std::ios::in);
    if(FragmentShaderStream.is_open()){
        std::string Line = "";
        while(getline(FragmentShaderStream, Line))
            FragmentShaderCode += "\n" + Line;
        FragmentShaderStream.close();
    }

    GLint Result = GL_FALSE;
    int InfoLogLength;


    // Compile Vertex Shader
    printf("Compiling shader : %s\n", vertex_file_path);
    char const * VertexSourcePointer = VertexShaderCode.c_str();
    glShaderSource(VertexShaderID, 1, &VertexSourcePointer , NULL);
    glCompileShader(VertexShaderID);

    // Check Vertex Shader
    glGetShaderiv(VertexShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(VertexShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> VertexShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(VertexShaderID, InfoLogLength, NULL, &VertexShaderErrorMessage[0]);
        printf("%s\n", &VertexShaderErrorMessage[0]);
    }



    // Compile Fragment Shader
    printf("Compiling shader : %s\n", fragment_file_path);
    char const * FragmentSourcePointer = FragmentShaderCode.c_str();
    glShaderSource(FragmentShaderID, 1, &FragmentSourcePointer , NULL);
    glCompileShader(FragmentShaderID);

    // Check Fragment Shader
    glGetShaderiv(FragmentShaderID, GL_COMPILE_STATUS, &Result);
    glGetShaderiv(FragmentShaderID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> FragmentShaderErrorMessage(InfoLogLength+1);
        glGetShaderInfoLog(FragmentShaderID, InfoLogLength, NULL, &FragmentShaderErrorMessage[0]);
        printf("%s\n", &FragmentShaderErrorMessage[0]);
    }



    // Link the program
    printf("Linking program\n");
    GLuint ProgramID = glCreateProgram();
    glAttachShader(ProgramID, VertexShaderID);
    glAttachShader(ProgramID, FragmentShaderID);
    glLinkProgram(ProgramID);

    // Check the program
    glGetProgramiv(ProgramID, GL_LINK_STATUS, &Result);
    glGetProgramiv(ProgramID, GL_INFO_LOG_LENGTH, &InfoLogLength);
    if ( InfoLogLength > 0 ){
        std::vector<char> ProgramErrorMessage(InfoLogLength+1);
        glGetProgramInfoLog(ProgramID, InfoLogLength, NULL, &ProgramErrorMessage[0]);
        printf("%s\n", &ProgramErrorMessage[0]);
    }


    glDetachShader(ProgramID, VertexShaderID);
    glDetachShader(ProgramID, FragmentShaderID);

    glDeleteShader(VertexShaderID);
    glDeleteShader(FragmentShaderID);

    return ProgramID;
}
void KinectFusionViewer::Render() {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    int i;
    //Draw each buffer
    for(i = 0; i < displays_; i++) {




        //First apply Shader
        glUseProgram(textureShader);
        glUniformMatrix4fv(MatrixID, 1, GL_FALSE, &MVP[0][0]);
        glActiveTexture(GL_TEXTURE0);
        glBindTexture(GL_TEXTURE_2D, gpuTextureId_[i]);
        //glBindTexture(GL_TEXTURE_2D, textureID[i]);
        glUniform1i(TextureID, 0);

        //Then Draw
        //
        // 1rst attribute buffer : vertices
        glEnableVertexAttribArray(0);
        glBindBuffer(GL_ARRAY_BUFFER, vertexbuffer[i]);
        glVertexAttribPointer(0,                  // attribute 0. No particular reason for 0, but must match the layout in the shader.
                              3,                  // size
                              GL_FLOAT,           // type
                              GL_FALSE,           // normalized?
                              0,                  // stride
                              (void*)0            // array buffer offset
        );
        // 2nd attribute buffer : colors
        glEnableVertexAttribArray(1);
        glBindBuffer(GL_ARRAY_BUFFER, uvMap_);
        glVertexAttribPointer(1,                                // attribute. No particular reason for 1, but must match the layout in the shader.
                              2,                                // size
                              GL_FLOAT,                         // type
                              GL_FALSE,                         // normalized?
                              0,                                // stride
                              (void*)0                          // array buffer offset
        );
        // Draw the triangle !
        glDrawArrays(GL_TRIANGLES, 0, 6); // Starting from vertex 0; 6 vertices total -> 2 triangle
        glDisableVertexAttribArray(0);
        glDisableVertexAttribArray(1);

    }
    glfwSwapBuffers(window_);

    // Should launch as a thread though with a Lock TODO
    if(record_) {
        cv::Mat img(960, 1280, CV_8UC3, recorderBuffer_);
        glPixelStorei(GL_PACK_ALIGNMENT, (img.step & 3) ? 1 : 4);
        glPixelStorei(GL_PACK_ROW_LENGTH, img.step/img.elemSize());
        glReadBuffer(GL_FRONT);
        glReadPixels(0, 0, img.cols, img.rows, GL_BGR, GL_UNSIGNED_BYTE, img.data);
        images.push_back(img);
    }

    glfwPollEvents();
}

void KinectFusionViewer::GenerateTextureBuffers() {
    gpuBuffer_ = new GLuint[displays_];
    gpuTextureId_ = new GLuint[displays_];
    //Step 1
    glGenBuffers(displays_, gpuBuffer_);
    // Make this the current UNPACK buffer (OpenGL is state-based)
    int i = 0;
    for(i = 0; i < displays_; i++) {
        glBindBuffer(GL_PIXEL_UNPACK_BUFFER, gpuBuffer_[i]);
        // // Allocate data for the buffer. 4-channel 8-bit image
        glBufferData(GL_PIXEL_UNPACK_BUFFER, imageRows_ * imageCols_ * sizeof(int), NULL, GL_DYNAMIC_COPY);
        cudaGLRegisterBufferObject( gpuBuffer_[i] );
    }
    //Step 2

// Generate a texture ID
    glGenTextures(displays_, gpuTextureId_);
// // Make this the current texture (remember that GL is state-based)
    for(i = 0; i< displays_; i++) {
        glBindTexture( GL_TEXTURE_2D, gpuTextureId_[i]);
// // Allocate the texture memory. The last parameter is NULL since we only
// // want to allocate memory, not initialize it
        glTexImage2D( GL_TEXTURE_2D, 0, GL_RGBA, imageCols_, imageRows_, 0, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
// // Must set the filter mode, GL_LINEAR enables interpolation when scaling
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
    }
}
void KinectFusionViewer::MapCuda() {
    void** texture;
    int index;
    for(index = 0; index < displays_; index++) {
        texture = (void**) &(gpuTextures[index]);
        cudaGLMapBufferObject(texture, gpuBuffer_[index]);
    }
}
int* KinectFusionViewer::GetGPUPointer(int index) {
    return gpuTextures[index];
}
int** KinectFusionViewer::GetGPUPointers() {
    return gpuTextures;
}
void KinectFusionViewer::CopyToCuda(int index, void *src) {
    cudaMemcpy(gpuTextures[index], src, sizeof(int) * imageCols_ * imageRows_, cudaMemcpyHostToDevice);
}
void KinectFusionViewer::UnMapCuda() {
    int index;
    for(index = 0; index < displays_; index++)
        cudaGLUnmapBufferObject(gpuBuffer_[index]);
}
void KinectFusionViewer::CreateTextureFromCuda() {
    int i;
    for(i = 0; i < displays_; i++) {
        glBindBuffer( GL_PIXEL_UNPACK_BUFFER, gpuBuffer_[i]);
    // Select the appropriate texture
        glBindTexture( GL_TEXTURE_2D, gpuTextureId_[i]);
    // // Make a texture from the buffer
        glTexSubImage2D( GL_TEXTURE_2D, 0, 0, 0, imageCols_, imageRows_, GL_RGBA, GL_UNSIGNED_BYTE, NULL);
    }
}
void KinectFusionViewer::close() {
    glfwDestroyWindow(window_);
}
