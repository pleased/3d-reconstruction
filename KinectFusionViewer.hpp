/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#ifndef VSLAM_KINECTFUSIONVIEWER_HPP
#define VSLAM_KINECTFUSIONVIEWER_HPP
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <string.h>
#include <fstream>
#include <vector>
#include <iostream>
#include <cuda_runtime_api.h>
#include <device_launch_parameters.h>
#include <cuda_gl_interop.h>
#include <opencv2/core/core.hpp>

static const GLfloat WINDOW_TOP_RIGHT[] = {
        0.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        1.0f, 1.0f, 0.0f,

        0.0f, 0.0f, 0.0f,
        1.0f, 1.0f, 0.0f,
        1.0f, 0.0f, 0.0f
};
static const GLfloat WINDOW_TOP_LEFT[] = {
        -1.0f, 0.0f, 0.0f,
        -1.0f, 1.0f, 0.0f,
        0.0f, 1.0f, 0.0f,

        -1.0f, 0.0f, 0.0f,
        0.0f, 1.0f, 0.0f,
        0.0f, 0.0f, 0.0f
};
static const GLfloat WINDOW_BOTTOM_LEFT[] = {
        -1.0f, -1.0f, 0.0f,
        -1.0f, 0.0f, 0.0f,
        0.0f, 0.0f, 0.0f,

        -1.0f, -1.0f, 0.0f,
        0.0f, 0.0f, 0.0f,
        0.0f, -1.0f, 0.0f
};
static const GLfloat WINDOW_BOTTOM_RIGHT[] = {
        0.0f, -1.0f, 0.0f,
        0.0f, 0.0f, 0.0f,
        1.0f, 0.0f, 0.0f,

        0.0f, -1.0f, 0.0f,
        1.0f, 0.0f, 0.0f,
        1.0f, -1.0f, 0.0f
};
static const GLfloat UVMAPPING[] = {
        0.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 0.0f,

        0.0f, 1.0f,
        1.0f, 0.0f,
        1.0f, 1.0f



//        0.0f, 0.0f,
//        1.0f, 1.0f,
//        0.0f, 1.0f,
//
//        1.0f, 0.0f,
//        0.0f, 0.0f,
//        1.0f, 1.0f
};
class KinectFusionViewer {
public:
    KinectFusionViewer(int displays=4, int cols=640, int rows=480, bool record=false, std::string filename="recording.mp4");
    ~KinectFusionViewer();
    int* GetGPUPointer(int index);
    int** GetGPUPointers();
    void MapCuda();
    void CopyToCuda(int index, void *src);
    void UnMapCuda();
    void CreateTextureFromCuda();
    void Render();
    bool GetKeyPressed(int key);
    bool GetKeyReleased(int key);
    bool CloseWindow();
    bool AnyKeyPressed();
    void SaveVideo();
    void close();
private:
    GLuint LoadShaders(const char*, const char*);
    void GenerateBuffers();
    void DeleteBuffers();
    void GenerateTextureBuffers();




    //class vars
private:
    std::string filename_;
    bool record_;
    std::vector<cv::Mat> images;
    unsigned char* recorderBuffer_;

    int displays_;
    GLuint textureShader;
    GLuint* VertexArrayID;
    GLuint* vertexbuffer;
    GLuint uvMap_;
    GLuint TextureID;
    GLuint MatrixID;
    glm::mat4 Projection;

    glm::mat4 View;
    glm::mat4 Model;
    //                                                                                                                                                    // Our ModelViewProjection : multiplication of our 3 matrices^M
    glm::mat4 MVP; // Remember, matrix multiplication is the other way around^M


    GLFWwindow* window_;
//    int* redTexture;
//    int* greenTexture;
//    int* blueTexture;
//    int* whiteTexture;
    int** gpuTextures;
    GLuint* gpuBuffer_;
    GLuint* gpuTextureId_;
    int imageRows_, imageCols_;

};
#endif