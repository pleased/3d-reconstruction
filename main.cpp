/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
#include <iostream>
#include <eigen3/Eigen/Dense>
#include <opencv2/core/core.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <chrono>

#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/registration.h>
#include <libfreenect2/packet_pipeline.h>
#include <libfreenect2/logger.h>



#include <pcl/filters/voxel_grid.h>
//#include "VisualOdometry.hpp"
//#include "FeatureToPointGenerator.hpp"
//#include "PointCloud.hpp"
//#include "IterativeClosestPoint.hpp"
#include "FileParser.hpp"
//#include "UnitTests/PointCloudUnitTests.hpp"
//#include "UnitTests/IterativeClosestPointUnitTests.hpp"
#include "UnitTests/BallPivotAlgorithmUnitTests.hpp"
#include "UnitTests/MeshGenerationUnitTests.hpp"
#include "UnitTests/ProjectiveDataAssociationUnitTests.hpp"
//#include "UnitTests/PoseGraphOptimisationUnitTests.hpp"
#include "UnitTests/FeatureBasedICPUnitTests.hpp"
#include "UnitTests/FileParserUnitTests.hpp"
#include "UnitTests/KinectFusionUnitTests.hpp"
#include "KinectFusion/DeviceTypes.hpp"
#include "UnitTests/DeviceTypesUnitTests.hpp"
#include "UnitTests/GlobalDeviceUnitTests.hpp"
#include "KinectFusionViewer.hpp"
#include "KinectFusion/KinectFusionViewerMemCopy.hpp"
#include <chrono>
#include <thread>
#include <pcl/point_types.h>
#include <pcl/features/normal_3d.h>
#include <libfreenect2/libfreenect2.hpp>
#include <libfreenect2/frame_listener_impl.h>
#include <libfreenect2/registration.h>
#include <libfreenect2/packet_pipeline.h>
#include <libfreenect2/logger.h>

void memReport() {
    // show memory usage of GPU
    cudaError cuda_status;
    size_t free_byte ;

    size_t total_byte ;
    cuda_status = cudaMemGetInfo( &free_byte, &total_byte ) ;

    if ( cudaSuccess != cuda_status ){
        printf("Error: cudaMemGetInfo fails, %s \n", cudaGetErrorString(cuda_status) );
        exit(1);
    }



    double free_db = (double)free_byte ;

    double total_db = (double)total_byte ;

    double used_db = total_db - free_db ;

    printf("GPU memory usage: used = %f, free = %f MB, total = %f MB\n",

           used_db/1024.0/1024.0, free_db/1024.0/1024.0, total_db/1024.0/1024.0);
    exit(0);
}





void KinectFusionTests() {
    std::cout << "Starting Kinect Fusion Tests" << std::endl;
//    _TestKinectFusionCudaTsdfVolumeAndRayCasterTogether();
//    _TestKinectFusionCudaResetTsdfVolumeSection();
//    _TestKinectFusionCudaSectionRayCastingStartingFromZeroAllAxes();
//    _TestKinectFusionCudaSectionRayCastingFromEndGridAllAxes();


//    _TestKinectFusionCudaSectionRayCastingXAxisTest();

//    _TestKinectFusionCudaSectionRayCastingFullVolume();

//    _TestKinectFusionCudaSectionRayCastingFullVolumeHoleFilling();
    _TestKinectFusionCudaSectionRayCastingFullVolumeHoleFillingFromTwoAngle();
//    _TestKinectFusionCudaSectionRayCastingNormalsWithCircle();
//    _TestKinectFusionCudaSectionRayCastingNormalsCheckAllAxisWithCircle();
//    _TestKinectFusionCudaSectionRayCastingFullVolumeIgnorePlaneExtraction();
//    _TestRestRaycastingStorageVolume();
    exit(0);
    std::cout << "Ending Kinect Fusion Tests" << std::endl;
}
void UtilityTests() {
    std::cout << "Starting Utility Tests" << std::endl;
    Utility::_TestUtilityRotationToQuaternionToRotation();
    Utility::_TestUtilityRotationToEulerToRotation();
    Utility::_TestUtilityEulerToRotationToEuler();
    Utility::_TestUtilityEulerToQuaternionToEuler();
    Utility::_TestUtilityQuaternionToEulerToQuaternion();
    std::cout << "Ending Utility Tests" << std::endl;
}
void DeviceTypesTests() {
    std::cout << "Testing DeviceTypesTests" << std::endl;
    _TestDeviceTypesFloat3();
    _TestDeviceTypesMatrix33();
    _TestDeviceTypesDepthMapDestructors();
    _TestDeviceTypesDepthMap();
    _TestDeviceTypesDepthMapVector();
    _TestDeviceTypesDepthMapVector2();
    _TestDeviceTypesImage();
    std::cout << "Ending DeviceTypesTests" << std::endl;
}
void GlobalDeviceTests() {
    std::cout << "Testing GlobalDeviceTests" << std::endl;
    _TestVoxelInteractions();
    _TestUploads();
    _TestTsdfUploads();
    std::cout << "Ending GlobalDeviceTests" << std::endl;
}
void MeshTests() {
    _TestBallPivotAlgorithm();
    _TestConsistentMeshGrid();
    _TestInconsistentMeshGrid();
    _TestRGBDMeshGrid();
//    _TestMeshGeneration();
    exit(0);
}
void GraphTests() {
//    _TestFeatureBasedICP();
    _TestFeatureBasedICPSystem();
    _TestFeatureBasedICPSystemModifiedImages();
    _TestFeatureBasedICPSystem2();
//    _TestPoseGraph();
}
void UnitTests() {
    UtilityTests();
    DeviceTypesTests();
    GlobalDeviceTests();
    KinectFusionTests();
    MeshTests();
//    _TestBallPivotAlgorithm();
//    _TestMeshGeneration();
    GraphTests();
}



std::string _KeyEnableTestMode = "EnableTestMode";
bool _EnableTestMode = false;

//Record Mode old Parameters
std::string _KeyEnableRecordOnlyMode = "EnableRecordModeOnly";
bool _EnableRecordOnlyMode = false;
std::string _KeyRecordModeFrameRate = "RecordModeFrameRate";
int _RecordModeFrameRate = 10;
std::string _KeyRecordModeDestination = "RecordModeDestination";
std::string _RecordModeDestination = "";

std::string _KeyKinectFusionReconstruction = "EnableKinectFusionReconstruction";
bool _EnableKinectFusionReconstruction = false;
std::string _KeyCameraParameters = "SetCameraParameters";
float _fx = NAN, _fy = NAN, _ox = NAN, _oy = NAN, _k1 = NAN, _k2 = NAN, _k3 = NAN;
std::string _KeyEnableOnlineMode = "EnableOnlineMode";
bool _EnableOnlineMode = false;

//KF System
std::string _KeyBaseDir = "BaseDir";
std::string _KeyDataSetDir = "DataSetDir";
std::string _BaseDir = "", _DataSet = "";
std::string _KeyDepthMapIntegrationThreshold = "DepthMapIntegrationThreshold";
float _IntegrationMetric = 0.05;
std::string _KeyKinectFusionWeightFactor = "KinectFusionWeightFactor";
float _WeightFactor = 1.f;
std::string _KeyStopIteration = "StopIteration";
int _StopIteration = -1;
std::string _KeyVolumeDistance = "VolumeDistance";
float _VolumeDistance = 3.f;
//KF ICP
std::string _KeyEnableFeatureBasedICP = "FeatureBasedICP";
bool _FeatureBasedICP = false;
std::string _KeyICPRotationLimit = "ICPRotationLimit";
float _ICPRotationLimit = 0.05;
std::string _KeyICPTranslationLimit = "ICPTranslationLimit";
float _ICPTranslationLimit = 0.05;
std::string _KeyMovingVolumeVoxelThreshold = "MovingVolumeVoxelThreshold";
int _MovingVolumeVoxelThreshold = 60;
std::string _KeyEnableRaycasterPointExtraction = "EnableRaycasterPointExtraction";
bool _EnableRaycasterPointExtraction = false;
std::string _KeyKinectFusionGeneratedPointCloudOutput = "KinectFusionGeneratedPointCloudOutput";
std::string _KinectFusionGeneratedPointCloudOutput = "ReconstructedPointCloudOutput";


//Point Cloud preparation, load if KF wasnt used
std::string _KeyBPALoadPointCloud = "BPALoadPointCloud";
std::string _LoadedPointCloud = "";
//Filtering
std::string _KeyEnableVoxelGridFilter = "EnableVoxelGridFilter";
bool _EnableVoxelGridFilter = false;
std::string _KeyVoxelGridFilterSize = "VoxelGridFilterSize";
float _VoxelGridFilterSize = 0.01f;

std::string _KeyDisplayPointCloudBeforeBPA = "DisplayPointCloudBeforeBPA";
bool _DisplayPointCloudBeforeBPA = true;
std::string _KeyDisplayNormals = "DisplayNormals";
bool _DisplayNormals = false;
//BPA
std::string _KeyEnableBPA = "EnableBPA";
bool _EnableBPA = false;
std::string _KeyBPAOutputMesh = "BPAOutputMesh";
std::string _BPAOutputMesh = "BPAGeneratedMesh";
std::string _KeyUsePCLNormalEstimate = "UsePCLNormalEstimation";
bool _UsePCLNormalEstimate = false;
std::string _KeyPCLNormalEstimationSize = "PCLNormalEstimationSize";
float _PCLNormalEstimationSize = _VoxelGridFilterSize;
std::string _KeyBPABallRadii = "BPABallRadii";
std::vector<float> _BPABallRadii;

std::string _KeyBPAEnableOnlineViewer = "BPAEnableOnlineViewer";
bool _BPAEnableOnlineViewer = false;
std::string _KeyBPAOnlineViewerDelay = "BPAOnlineViewerDelay";
int BPAOnlineViewerDelay = 0;
std::string _KeyBPAOnlineViewerStopIteration = "BPAOnlineViewerStopIteration";
int BPAOnlineViewerStopIteration = -1;
std::string _KeyBPAEnableEndViewer = "BPAEnableEndViewer";
bool _BPAEnableEndViewer = false;


std::string tempPointCloud = "";
std::string filterSave = "";
//KinectFusion parameters
bool ICPframeToFrame = false;
bool configPass = false;
bool BPAKDTree = true;



void saveimages(std::vector<cv::Mat>& rgb_list, std::vector<cv::Mat>& depth_list, std::vector<cv::Mat>& ir_list, std::vector<double>& time_list,
                std::string rgb_dir, std::string depth_dir, std::string ir_dir, std::string base_dir) {
    std::string depth_folder = "depth/";
    std::string rgb_folder = "rgb/";
    std::string ir_folder = "ir/";
    std::ofstream rgbFile(base_dir + "rgb.txt");
    std::ofstream depthFile(base_dir + "depth.txt");
    std::ofstream irFile(base_dir + "ir.txt");
    int i;
    for(i = 0;i < rgb_list.size(); i++ ) {
        double timeStamp = time_list[i];
        cv::Mat rgb_image = rgb_list[i];
        cv::Mat depth_image = depth_list[i];
        cv::Mat ir_image = ir_list[i];
        std::ostringstream oss_depth;
        oss_depth << depth_dir << std::fixed << timeStamp << ".png";
        std::ostringstream oss_rgb;
        oss_rgb << rgb_dir << std::fixed << timeStamp << ".png";
        std::ostringstream oss_ir;
        oss_ir << ir_dir << std::fixed << timeStamp << ".png";
        cv::imwrite(oss_rgb.str(), rgb_image);
        cv::imwrite(oss_depth.str(), depth_image);
        cv::imwrite(oss_ir.str(), ir_image);


        depthFile << std::fixed << timeStamp << " ";
        depthFile << depth_folder;
        depthFile << std::fixed << timeStamp << ".png" << std::endl;

        rgbFile << std::fixed << timeStamp << " ";
        rgbFile << rgb_folder;
        rgbFile << std::fixed << timeStamp << ".png" << std::endl;

        irFile << std::fixed << timeStamp << " ";
        irFile << ir_folder ;
        irFile << std::fixed << timeStamp << ".png" << std::endl;
    }
    rgbFile.close();
    depthFile.close();
}

int KinectCapture() {
    std::string rgb_dir = "rgb";
    std::string depth_dir = "depth";
    std::string ir_dir = "ir";

    //Create Directory Structure for output data
    std::cout << "Output Directory" << _RecordModeDestination << std::endl;
    struct stat sb;
    bool create_dir = true;
    bool exists = false;
    if (stat(_RecordModeDestination.c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)) {
        create_dir = false;
        exists = true;
    }
    if (create_dir) {
        if (_RecordModeDestination.back() == '/') {
            _RecordModeDestination.pop_back();
        }
        exists = boost::filesystem::create_directories(_RecordModeDestination.c_str());

    }
    if (_RecordModeDestination.back() != '/') {
        _RecordModeDestination += '/';
    }
    if (stat((_RecordModeDestination + rgb_dir).c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)) {
    } else {
        if (!boost::filesystem::create_directories((_RecordModeDestination + rgb_dir).c_str())) {
            std::cout << "Failed to create " << _RecordModeDestination + rgb_dir << std::endl;
            return EXIT_FAILURE;
        }
    }
    if (stat((_RecordModeDestination + depth_dir).c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)) {
    } else {
        if (!boost::filesystem::create_directories((_RecordModeDestination + depth_dir).c_str())) {
            std::cout << "Failed to create " << _RecordModeDestination + depth_dir << std::endl;
            return EXIT_FAILURE;
        }
    }
    if (stat((_RecordModeDestination + ir_dir).c_str(), &sb) == 0 && S_ISDIR(sb.st_mode)) {
    } else {
        if (!boost::filesystem::create_directories((_RecordModeDestination + ir_dir).c_str())) {
            std::cout << "Failed to create " << _RecordModeDestination + ir_dir << std::endl;
            return EXIT_FAILURE;
        }
    }
    rgb_dir += '/';
    depth_dir += '/';
    ir_dir += '/';

    if (exists) { //Returns false in there is a / at the end
        //Add the / after dir is created
        std::cout << "Data set recording in " << _RecordModeDestination << std::endl;
        rgb_dir = _RecordModeDestination + rgb_dir;
        depth_dir = _RecordModeDestination + depth_dir;
        ir_dir = _RecordModeDestination + ir_dir;
        std::cout << "RGB dir " << rgb_dir << std::endl;
        std::cout << "Depth dir " << depth_dir << std::endl;
        std::cout << "ir dir " << ir_dir << std::endl;
    } else {
        std::cerr << "Failed to create path " << _RecordModeDestination << std::endl;
        return EXIT_FAILURE;
    }

    //Constants
    const int target_speed = (1. / _RecordModeFrameRate) * 1000;
    const std::chrono::milliseconds capture_interval(target_speed);
    const std::chrono::seconds second(1);

    libfreenect2::Freenect2 freenect2; // Context of all devices
    libfreenect2::Freenect2Device *dev = 0; // A device
    libfreenect2::PacketPipeline *pipeline = 0;

    if (freenect2.enumerateDevices() == 0) {
        std::cout << "no device connected!" << std::endl;
        return EXIT_FAILURE;
    }
    std::string serial = freenect2.getDefaultDeviceSerialNumber();

    pipeline = new libfreenect2::CudaPacketPipeline(); // CPU Packet Pipeline


    dev = freenect2.openDevice(serial, pipeline); // open device

    //Sync'ed frame listener waits for all frames to be synchronized before getting them
    //Configuration
    libfreenect2::SyncMultiFrameListener listener(
            libfreenect2::Frame::Color | libfreenect2::Frame::Ir | libfreenect2::Frame::Depth);
    libfreenect2::FrameMap frames;
//    if (cameraParamsSet) {
//        dev->setIrCameraParams(irCameraParams);
//    }
    dev->setColorFrameListener(&listener);
    dev->setIrAndDepthFrameListener(&listener);

    //starting the device
    dev->start();
    std::cout << "device serial: " << dev->getSerialNumber() << std::endl;
    std::cout << "device firmware: " << dev->getFirmwareVersion() << std::endl;

    libfreenect2::Freenect2Device::IrCameraParams irCameraParams;
    if(std::isnan(_fx) || std::isnan(_fy) ||std::isnan(_ox) ||std::isnan(_oy) ||std::isnan(_k1) ||std::isnan(_k2) ||std::isnan(_k3)) {
        irCameraParams = dev->getIrCameraParams();
    } else {
        irCameraParams.fx = _fx;
        irCameraParams.fy = _fy;
        irCameraParams.cx = _ox;
        irCameraParams.cy = _oy;
        irCameraParams.k1 = _k1;
        irCameraParams.k2 = _k2;
        irCameraParams.k3 = _k3;
    }

    //Depth registration
    libfreenect2::Registration *registration = new libfreenect2::Registration(irCameraParams,
                                                                              dev->getColorCameraParams());
    libfreenect2::Frame undistorted(512, 424, 4), registered(512, 424, 4);
    //starting the 'stop' thread
//    std::thread recording(stopRecording);

    std::chrono::milliseconds time_stamp = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch());
    std::chrono::milliseconds current_time = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch());
    std::chrono::milliseconds start = std::chrono::duration_cast<std::chrono::milliseconds>(
            std::chrono::system_clock::now().time_since_epoch());



    int iterations = 0;
    //Getting the frames
    std::vector<cv::Mat> rgbList, depthList, irList;
    std::vector<double> timeList;
    int rows = 424, cols = 512;
    KinectFusionViewer viewer(4, cols, rows);
    unsigned char* _gpuColourImage = 0;
    unsigned short *_gpuIrImage = 0, *_gpuDepthImage = 0;
    cudaMalloc(&(_gpuColourImage), sizeof(int) * rows * cols);
    cudaMalloc(&(_gpuIrImage), sizeof(unsigned short) * rows * cols);
    cudaMalloc(&(_gpuDepthImage), sizeof(unsigned short) * rows * cols);
//    gpu::host::KinectFusionViewerMemCopy::InitViewerMemory(rows, cols);
    bool useVisualizer = true;
    while (true) {
        //grab frames
        listener.waitForNewFrame(frames);
        time_stamp = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch());

        libfreenect2::Frame *rgb = frames[libfreenect2::Frame::Color];
        libfreenect2::Frame *ir = frames[libfreenect2::Frame::Ir];
        libfreenect2::Frame *depth = frames[libfreenect2::Frame::Depth];
//        std::cout << std::fixed << time_stamp.count() /1000. << std::endl;
        cv::Mat rgb_image;
        cv::Mat depth_image;
        cv::Mat ir_image;


        //Register frames
        registration->apply(rgb, depth, &undistorted, &registered, true);
        rgb_image = cv::Mat((int)registered.height, (int)registered.width, CV_8UC4, registered.data);

        cv::Mat unscaledDepthImage = cv::Mat((int) undistorted.height, (int)undistorted.width, CV_32F, undistorted.data);
        cv::Mat flipped;
        unscaledDepthImage.convertTo(flipped, CV_16UC1, 5);
        cv::flip(flipped, depth_image, 1);
        cv::Mat rgbflipped;
        cv::flip(rgb_image, rgbflipped, 1);
        rgb_image = rgbflipped;
//
//
        cv::Mat unscaled_ir_image = cv::Mat((int) ir->height, (int) ir->width, CV_32F, ir->data);
        cv::Mat irflipped;
        unscaled_ir_image.convertTo(irflipped, CV_16UC1);
        cv::flip(irflipped, ir_image, 1);

        rgbList.push_back(rgb_image);
        depthList.push_back(depth_image);
        irList.push_back(ir_image);
        timeList.push_back(time_stamp.count()/1000.);

        if(useVisualizer) {
            //TODO Check if viewer works
            cudaMemcpy(_gpuColourImage, rgb_image.data, sizeof(unsigned char) * rgb_image.rows * rgb_image.cols * rgb_image.channels(), cudaMemcpyHostToDevice);
            cudaMemcpy(_gpuIrImage, ir_image.data, sizeof(unsigned short) * ir_image.rows * ir_image.cols, cudaMemcpyHostToDevice);
            cudaMemcpy(_gpuDepthImage, depth_image.data, sizeof(unsigned short) * depth_image.rows * depth_image.cols, cudaMemcpyHostToDevice);
            //Cuda mapping
            viewer.MapCuda();
            //Write to Cuda
//            gpu::host::KinectFusionViewerMemCopy::CopyToViewers(viewer.GetGPUPointers());
            gpu::host::KinectCaptureViewerMemCopy::CopyToViewer(_gpuColourImage, _gpuIrImage, _gpuDepthImage, viewer.GetGPUPointers());
            viewer.UnMapCuda();
            viewer.CreateTextureFromCuda();
            viewer.Render();
        }
        if (!viewer.CloseWindow()) {
            break;
        }

        listener.release(frames);
        iterations++;
        if (time_stamp - start > second) {
            std::cout << "Running at " << iterations << "HZ" << std::endl;
            iterations = 0;
            start = time_stamp;
        }

        current_time = std::chrono::duration_cast<std::chrono::milliseconds>(
                std::chrono::system_clock::now().time_since_epoch());
        while (time_stamp + capture_interval > current_time) {
            std::this_thread::sleep_for(time_stamp + capture_interval - current_time);
            current_time = std::chrono::duration_cast<std::chrono::milliseconds>(
                    std::chrono::system_clock::now().time_since_epoch());
        }

    }
//    recording.join();
    //Stop the kinect
    dev->stop();
    dev->close();
    cudaError_t cudaErrorCode;

    cudaErrorCode = cudaFree(_gpuColourImage);
    if(cudaErrorCode != cudaSuccess) {
        std::cout << "Failed to free: " << cudaGetErrorString(cudaErrorCode) << std::endl;
    }
    cudaErrorCode = cudaFree(_gpuDepthImage);
    if(cudaErrorCode != cudaSuccess) {
        std::cout << "Failed to free: " << cudaGetErrorString(cudaErrorCode) << std::endl;
    }
    cudaErrorCode = cudaFree(_gpuIrImage);
    if(cudaErrorCode != cudaSuccess) {
        std::cout << "Failed to free: " << cudaGetErrorString(cudaErrorCode) << std::endl;
    }
    std::cout << "Writing dataset to disk" << std::endl;
    saveimages(rgbList, depthList, irList, timeList, rgb_dir, depth_dir, ir_dir, _RecordModeDestination);
    return EXIT_SUCCESS;
}

void RunSlamFusionCudaOnline() {
    //KinectFusion Setup
    int rows = 424, cols = 512;
    Slam::KinectFusion kinectFusion; //TODO uncomment
    KinectFusionViewer viewer(4, cols, rows);
    gpu::matrix33 rotationMatrix;
    rotationMatrix.SetIdentity();
    gpu::float3 translation;

    gpu::DepthMap depthImage(rows, cols);
    gpu::Image rgbImage(rows, cols, 3);

    bool fusionRunning = true;
//    std::thread stopThread(Stop);

    libfreenect2::Freenect2 freenect2; // Context of all devices
    libfreenect2::Freenect2Device *dev = 0; // A device
    libfreenect2::PacketPipeline *pipeline = 0;

    if (freenect2.enumerateDevices() == 0) {
        std::cout << "no device connected!" << std::endl;
        return;
    }
    std::string serial = freenect2.getDefaultDeviceSerialNumber();
    pipeline = new libfreenect2::CudaPacketPipeline(); // Cuda Packet Pipeline
    dev = freenect2.openDevice(serial, pipeline); // open device

    //Sync'ed frame listener waits for all frames to be synchronized before getting them
    //Configuration
    libfreenect2::SyncMultiFrameListener listener(
//            libfreenect2::Frame::Color | libfreenect2::Frame::Ir | libfreenect2::Frame::Depth);
            libfreenect2::Frame::Color | libfreenect2::Frame::Depth);
    libfreenect2::FrameMap frames;
    libfreenect2::Freenect2Device::IrCameraParams irCameraParams;
//    //369.54291259088183 370.20970328011168 259.715809081801 205.65796886324938 0.092541225321552262 -0.213810190734055 0.0019636839510653948
    if(std::isnan(_fx) || std::isnan(_fy) ||std::isnan(_ox) ||std::isnan(_oy) ||std::isnan(_k1) ||std::isnan(_k2) ||std::isnan(_k3)) {
        irCameraParams = dev->getIrCameraParams();
    } else {
        irCameraParams.fx = _fx;
        irCameraParams.fy = _fy;
        irCameraParams.cx = _ox;
        irCameraParams.cy = _oy;
        irCameraParams.k1 = _k1;
        irCameraParams.k2 = _k2;
        irCameraParams.k3 = _k3;
    }



//    dev->setIrCameraParams(irCameraParams);

    dev->setColorFrameListener(&listener);
    dev->setIrAndDepthFrameListener(&listener);

    //starting the device
    dev->start();
    std::cout << "device serial: " << dev->getSerialNumber() << std::endl;
    std::cout << "device firmware: " << dev->getFirmwareVersion() << std::endl;

    //Depth registration
//    libfreenect2::Registration *registration = new libfreenect2::Registration(irCameraParams,
//                                                                              dev->getColorCameraParams());
    libfreenect2::Registration *registration = new libfreenect2::Registration(irCameraParams, dev->getColorCameraParams()); //TODO seems to have fixed black registration
    libfreenect2::Frame undistorted(cols, rows, 4), registered(cols, rows, 4);
    int iteration = 0;

//    TODO uncomment
    kinectFusion.SetupCuda(dev->getIrCameraParams().fx, dev->getIrCameraParams().fy, dev->getIrCameraParams().cx, dev->getIrCameraParams().cy, rows, cols, 3, rotationMatrix, translation, _WeightFactor, _VolumeDistance, _FeatureBasedICP);
    //Configuring Kinect Fusion system
    kinectFusion.SetICPLimits(_ICPRotationLimit, _ICPTranslationLimit);
    kinectFusion.SetIntegrationMetric(_IntegrationMetric);
    kinectFusion.SetMovingVoxelThreshold(_MovingVolumeVoxelThreshold);

    std::cout << "Starting Fusion " << std::endl;
    bool initial = true;
    std::chrono::time_point<std::chrono::system_clock> startTime, endTime;
    startTime = std::chrono::system_clock::now();
    endTime = startTime;
    std::chrono::duration<double> elapsed_seconds = endTime - startTime;
    int hzCounter = 0;
    while (fusionRunning) {
        //grab frames
        listener.waitForNewFrame(frames);
        libfreenect2::Frame *rgb = frames[libfreenect2::Frame::Color];
        libfreenect2::Frame *depth = frames[libfreenect2::Frame::Depth];
        registration->apply(rgb, depth, &undistorted, &registered, true);
        cv::Mat flippedRgb((int) registered.height, (int) registered.width, CV_8UC4, registered.data);

        cv::Mat flippedDepth;
        cv::Mat unscaled_depth_image((int) undistorted.height, (int) undistorted.width, CV_32F, undistorted.data);
        unscaled_depth_image.convertTo(flippedDepth, CV_64FC1,
                                       1); // scale the depth image and convert to 16bit image requires divison by
        //TODO Fix here expects a double image in cudakinectv2
        cv::Mat_<double> depth_image;
        cv::flip(flippedDepth, depth_image, 1);
        cv::Mat rgb_image;
        cv::flip(flippedRgb, rgb_image, 1);
        depthImage.reset(rows, cols, depth_image.datastart);


        rgbImage.resetBGRAToBGR(rows, cols, rgb_image.channels(), rgb_image.datastart);
        kinectFusion.CudaKinectV2(depthImage, rgbImage,depth_image,rgb_image, initial); //TODO uncomment
        initial = false;
        listener.release(frames);


        viewer.MapCuda();
        gpu::host::KinectFusionViewerMemCopy::CopyToViewers(viewer.GetGPUPointers());
        viewer.UnMapCuda();
        viewer.CreateTextureFromCuda();
        viewer.Render();
        if (!viewer.CloseWindow()) {
            break;
        }

        iteration++;
        hzCounter++;
        //fps controller
        endTime = std::chrono::system_clock::now();
        elapsed_seconds = endTime - startTime;
        if(elapsed_seconds.count() > 1.) {
            std::cout << "Running at " << (hzCounter/elapsed_seconds.count()) << "HZ" << std::endl;
            startTime = std::chrono::system_clock::now();
            hzCounter = 0;
        }
//        break;
    }
//    stopThread.join();
    //Stop the kinect
    dev->stop();
    dev->close();
    kinectFusion.CudaKinectV2Finish();
    kinectFusion.viewPointCloud(kinectFusion._globalPointCloud);

}

pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr RunSlamFusionCudaOffline(std::string baseDir, std::string dataSet) {
    Utility::SetCameraParameters(_fx, _fy, _ox, _oy);

    std::string dataSetDir = baseDir + dataSet + "/", rgbFileName("rgb.txt"), depthFileName("depth.txt"),
            groundTruthFileName("groundtruth.txt");
    IO::FileParser parser(dataSetDir, rgbFileName, depthFileName, 5.e-2);
//    if(!kinectV2) {
//        parser.EnableGroundTruth(groundTruthFileName);
//    }
    std::vector<std::string> fileNames;


    double depthCurrentTime, rgbCurrentTime, groundTruthCurrentTime, depthPreviousTime, rgbPreviousTime, groundTruthPreviousTime;
    std::string depthFile, rgbFile;
    Eigen::Matrix4d groundTruth;
    gpu::matrix33 groundTruthRotation, groundTruthStartRotation;
    gpu::float3 groundTruthTranslation, groundTruthStartTranslation;


    int i;
//    for(i = 0 ;i < 20; i ++) {
    if (parser.ReadData(fileNames) != 1) {
        std::cerr << "Couldn't read data for first frame pairs" << std::endl;
    }
    parser.GetImageTimeAndLocation(fileNames[IO::RGB], rgbPreviousTime, rgbFile);
    parser.GetImageTimeAndLocation(fileNames[IO::Depth], depthPreviousTime, depthFile);
    parser.GetGroundTruthTimeAndRotationMatrix(fileNames[IO::GrouthTruth], groundTruthPreviousTime, groundTruth);
//    groundTruthRotation = ExtractRotation(groundTruth);
//    groundTruthTranslation = ExtractTranslation(groundTruth);
//    groundTruthStartRotation = ExtractRotation(groundTruth);
//    groundTruthStartTranslation = ExtractTranslation(groundTruth);
//    }
    cv::Mat_<double> newDepthImage;
    newDepthImage = cv::imread(depthFile, -1);
    newDepthImage = newDepthImage * (1. / 5.);
    gpu::DepthMap currentDepth(newDepthImage.rows, newDepthImage.cols, newDepthImage.data);
    int rows = newDepthImage.rows, cols = newDepthImage.cols;
//    std::cout << "DEPTH MAP: " << newDepthImage(431, 255) << std::endl;
    std::cout << "DEPTH MAP: " << newDepthImage(283, 433) << std::endl;
    cv::Mat colourImage;
    colourImage = cv::imread(rgbFile);
    std::cout << colourImage.channels() << std::endl;
//    exit(0);
    gpu::Image currentColourImage(colourImage.rows, colourImage.cols, colourImage.channels(), colourImage.datastart);

    int icpLevels = 3;
    Slam::KinectFusion kinectFusionCuda;
    gpu::matrix33 cameraMatrix(_fx, 0, _ox, 0, _fy, _oy, 0, 0, 1),
            rotationMatrix(1, 0, 0, 0, 1, 0, 0, 0, 1), inverseRotation = rotationMatrix.transpose();
    gpu::float3 translation(0, 0, 0);
//    gpu::float3 translation(-2.5, -2.5, -2.5);
    assert(colourImage.channels() == 3);
    kinectFusionCuda.SetupCuda(_fx, _fy, _ox, _oy, rows, cols, icpLevels, rotationMatrix, translation, _WeightFactor, _VolumeDistance, _FeatureBasedICP);
    //Configuring Kinect Fusion system
    kinectFusionCuda.SetICPLimits(_ICPRotationLimit, _ICPTranslationLimit);
    kinectFusionCuda.SetIntegrationMetric(_IntegrationMetric);
    kinectFusionCuda.SetMovingVoxelThreshold(_MovingVolumeVoxelThreshold);

    std::cout << "Starting Kinect cuda" << std::endl;
//    memReport();

    std::vector<gpu::matrix33> rotationMatrices;
    std::vector<gpu::float3> translationVectors;

    rotationMatrices.push_back(rotationMatrix);
    translationVectors.push_back(translation);
    double avgTime = 0;
//    kinectFusionCuda.CudaKinect(currentDepth, currentColourImage, true);
    bool v2Debug = false;
    clock_t startClock = clock();
    bool reset = false;
    bool analyseTrajector = true;
    ofstream traj;
    if(analyseTrajector) {
        traj.open(dataSetDir + "reconstructedTrajectory.tj");
    }
    if(v2Debug) {
        kinectFusionCuda.CudaKinectV2Debug(currentDepth, currentColourImage, groundTruthStartTranslation, groundTruthStartRotation, reset, true);
    } else {
        double w, x, y, z;
        Utility::RotationMatrixToQuaternion(kinectFusionCuda._rotationMatrix, w, x, y, z);
        traj << 0 << " " << std::setprecision (4) << kinectFusionCuda._translation.x << " " << kinectFusionCuda._translation.y << " " << kinectFusionCuda._translation.z << " " <<
             x << " " << y << " " << z << " " << w << std::endl;
        rotationMatrices.push_back(kinectFusionCuda._rotationMatrix);
        translationVectors.push_back(kinectFusionCuda._translation);
        kinectFusionCuda.CudaKinectV2(currentDepth, currentColourImage, newDepthImage, colourImage, true);
    }
    clock_t endClock = clock();
    avgTime = double(endClock - startClock)/CLOCKS_PER_SEC;
    int iteration = 1;
    KinectFusionViewer viewer(4, cols, rows);
    bool forceIntegration = false, forceExtractPointCloud = false;
    bool wasPressedForceIntegration = false, wasPressedForceExtractPointCloud = false;
    while(parser.ReadData(fileNames) == 1) {

        std::cout << "\x1B[35m" << "Starting Iteration: " << iteration << "\x1B[0m" << std::endl;
        parser.GetImageTimeAndLocation(fileNames[IO::RGB], rgbCurrentTime, rgbFile);
        parser.GetImageTimeAndLocation(fileNames[IO::Depth], depthCurrentTime, depthFile);
        parser.GetGroundTruthTimeAndRotationMatrix(fileNames[IO::GrouthTruth], groundTruthCurrentTime, groundTruth);
//        groundTruthRotation = ExtractRotation(groundTruth);
//        groundTruthTranslation = ExtractTranslation(groundTruth);

        newDepthImage = cv::imread(depthFile, -1);
        newDepthImage = newDepthImage * (1. / 5.);
        currentDepth.reset(newDepthImage.rows, newDepthImage.cols, newDepthImage.data);

        colourImage = cv::imread(rgbFile);
        currentColourImage.reset(colourImage.rows, colourImage.cols, colourImage.channels(), colourImage.data);

//        bool icpCompleted = kinectFusionCuda.CudaKinect(currentDepth, currentColourImage);

        //Pressing keys here and Key bounce
        if(!wasPressedForceIntegration) {
            forceIntegration = viewer.GetKeyPressed(GLFW_KEY_F);
            wasPressedForceIntegration = forceIntegration;
        } else if(wasPressedForceIntegration) {
            forceIntegration = false;
            if(viewer.GetKeyReleased(GLFW_KEY_F)) {
                wasPressedForceIntegration = false;
            }
        }

        if(!wasPressedForceExtractPointCloud) {
            forceExtractPointCloud = viewer.GetKeyPressed(GLFW_KEY_E);
            wasPressedForceExtractPointCloud = forceIntegration;
        } else if(wasPressedForceExtractPointCloud) {
            forceExtractPointCloud = false;
            if(viewer.GetKeyReleased(GLFW_KEY_E)) {
                wasPressedForceExtractPointCloud = false;
            }
        }

        //End
        startClock = clock();
        bool icpCompleted;
        if(v2Debug) {
            std::cout << "Translation: " << groundTruthTranslation.x << ", " << groundTruthTranslation.y << ", " << groundTruthTranslation.z << std::endl;
            std::cout << "Rotation" << std::endl;
            std::cout << groundTruthRotation.row0.x << ", " << groundTruthRotation.row0.y << ", " << groundTruthRotation.row0.z << std::endl;
            std::cout << groundTruthRotation.row1.x << ", " << groundTruthRotation.row1.y << ", " << groundTruthRotation.row1.z << std::endl;
            std::cout << groundTruthRotation.row2.x << ", " << groundTruthRotation.row2.y << ", " << groundTruthRotation.row2.z << std::endl;
            icpCompleted = kinectFusionCuda.CudaKinectV2Debug(currentDepth, currentColourImage, groundTruthTranslation, groundTruthRotation, reset);

        } else {
            icpCompleted =  kinectFusionCuda.CudaKinectV2(currentDepth, currentColourImage, newDepthImage, colourImage, false, forceIntegration,
                                                          forceExtractPointCloud);
        }
        endClock = clock();
        if(analyseTrajector) {
            double w, x, y, z;
            Utility::RotationMatrixToQuaternion(kinectFusionCuda._rotationMatrix, w, x, y, z);
            traj << iteration << " " << std::setprecision (4) << kinectFusionCuda._translation.x << " " << kinectFusionCuda._translation.y << " " << kinectFusionCuda._translation.z << " " <<
            x << " " << y << " " << z << " " << w << std::endl;
            rotationMatrices.push_back(kinectFusionCuda._rotationMatrix);
            translationVectors.push_back(kinectFusionCuda._translation);
        }

        avgTime = (avgTime + double(endClock - startClock)/CLOCKS_PER_SEC)/2;

        //Viewer Stuff
        viewer.MapCuda();
        //Write to Cuda
        gpu::host::KinectFusionViewerMemCopy::CopyToViewers(viewer.GetGPUPointers());
        viewer.UnMapCuda();
        viewer.CreateTextureFromCuda();
        viewer.Render();
        if (!viewer.CloseWindow()) {
            break;
        }


        if(!icpCompleted) {
            std::cout << "\tIcp failed breaking" << std::endl;
            break;
        }

        rotationMatrices.push_back(kinectFusionCuda.GetLastRotationMatrix());
        translationVectors.push_back(kinectFusionCuda.GetLastTranslationVector());

        gpu::matrix33 groundTruthRotationMotion = groundTruthRotation.transpose() * groundTruthStartRotation;
        gpu::float3 groundTruthTranslationMotion = groundTruthTranslation - groundTruthStartTranslation;


        if(iteration == _StopIteration) {
            break;
        }
        iteration++;
    }
    if(analyseTrajector) {
        std::string name = Utility::GeneratePlotDataFile("TrajectorPlotData", rotationMatrices, translationVectors);
        std::vector<std::string> files;
        files.push_back(name);
        Utility::Generate2DPlotFile("TrajectorPlot", files);
        traj.close();
    }

    kinectFusionCuda.CudaKinectV2Finish();
    return kinectFusionCuda._globalPointCloud;
//    return kinectFusionCuda._globalPointCloud;
//    std::cout << "Average time per image: " << avgTime << std::endl;
//    return kinectFusionCuda._globalPointCloud;
}



bool _CheckConfig = true;


void setParams(std::string key, std::string value) {
    if(key == _KeyEnableTestMode) {
        if ((value == "true") || (value == "True") || (value == "TRUE") || (value == "1")) {
            _EnableTestMode = true;
            configPass = true;
        } else if ((value == "false") || (value == "False") || (value == "FALSE") || (value == "0")) {
            _EnableTestMode = false;
        }
        if (_CheckConfig) {
            std::cout << key << " " << _EnableTestMode << std::endl;
        }
    } else if (key == _KeyEnableRecordOnlyMode) {
        if ((value == "true") || (value == "True") || (value == "TRUE") || (value == "1")) {
            _EnableRecordOnlyMode = true;
        } else if ((value == "false") || (value == "False") || (value == "FALSE") || (value == "0")) {
            _EnableRecordOnlyMode = false;
        }
        if (_CheckConfig) {
            std::cout << key << " " << _EnableRecordOnlyMode << std::endl;
        }
    } else if (key == _KeyRecordModeFrameRate) {
        _RecordModeFrameRate = atoi(value.c_str());
        if (_CheckConfig) {
            std::cout << key << " " << _RecordModeFrameRate << std::endl;
        }
    } else if (key == _KeyRecordModeDestination) {
        _RecordModeDestination = value;
        if (_CheckConfig) {
            std::cout << key << " " << _RecordModeDestination << std::endl;
        }
    } else if(key == _KeyKinectFusionReconstruction) {
        if((value == "true") || (value == "True") || (value == "TRUE") || (value == "1")) {
            _EnableKinectFusionReconstruction = true;
        } else if((value == "false") || (value == "False") || (value == "FALSE") || (value == "0")) {
            _EnableKinectFusionReconstruction = false;
        }
        if(_CheckConfig) {
            std::cout << key << " " << _EnableKinectFusionReconstruction << std::endl;
        }
    } else if(key == _KeyEnableOnlineMode) {
        if((value == "true") || (value == "True") || (value == "TRUE") || (value == "1")) {
            _EnableOnlineMode = true;
            configPass = true;
        } else if((value == "false") || (value == "False") || (value == "FALSE") || (value == "0")) {
            _EnableOnlineMode = false;
        }
        if(_CheckConfig) {
            std::cout << key << " " << _EnableKinectFusionReconstruction << std::endl;
        }
    } else if(key == _KeyCameraParameters) {
        //TODO Populate _fx, _fy, _ox, _oy values | if in online/kinectCapture mode take k1, k2, k3
        std::istringstream iss(value);
        std::string v;
        iss >> v;
        _fx = strtof(v.c_str(), 0);
        iss >> v;
        _fy = strtof(v.c_str(), 0);
        iss >> v;
        _ox = strtof(v.c_str(), 0);
        iss >> v;
        _oy = strtof(v.c_str(), 0);

        if(!iss.eof()) {
            iss >> v;
            _k1 = strtof(v.c_str(), 0);
            iss >> v;
            _k2 = strtof(v.c_str(), 0);
            iss >> v;
            _k3 = strtof(v.c_str(), 0);
        }
        if(_CheckConfig) {
            std::cout << key << " " << _fx << " " << _fy << " " << _ox << " " << _oy << " " << _k1 << " " << _k2 << " " << _k3 << std::endl;
        }
        //KF system
    } else if(key == _KeyBaseDir) {
        _BaseDir = value;
        configPass = true;
        if(_CheckConfig) {
            std::cout << key << " " << _BaseDir << std::endl;
        }
    } else if(key == _KeyDataSetDir) {
        _DataSet = value;
        configPass = true;
        if(_CheckConfig) {
            std::cout << key << " " << _DataSet << std::endl;
        }
    } else if (key == _KeyDepthMapIntegrationThreshold) {
        _IntegrationMetric = strtof(value.c_str(), 0);
        if(_CheckConfig) {
            std::cout << key << " " << _IntegrationMetric << std::endl;
        }
    } else if(key == _KeyKinectFusionWeightFactor) {
        _WeightFactor = strtof(value.c_str(), 0);
        if(_CheckConfig) {
            std::cout << key << " " << _WeightFactor << std::endl;
        }
    } else if (key == _KeyStopIteration) {
        _StopIteration = atoi(value.c_str());
        if(_CheckConfig) {
            std::cout << key << " " << _StopIteration << std::endl;
        }
    } else if (key == _KeyVolumeDistance) {
        _VolumeDistance = strtof(value.c_str(), 0);
        if(_CheckConfig) {
            std::cout << key << " " << _VolumeDistance << std::endl;
        }
        //ICP Configs
    } else if (key == _KeyEnableFeatureBasedICP) {
        if ((value == "true") || (value == "True") || (value == "TRUE") || (value == "1")) {
            _FeatureBasedICP = true;
        } else if((value == "false") || (value == "False") || (value == "FALSE") || (value == "0")) {
            _FeatureBasedICP = false;
        }
        if(_CheckConfig) {
            std::cout << key << " " << _FeatureBasedICP << std::endl;
        }
    } else if (key == _KeyICPRotationLimit) {
        _ICPRotationLimit = strtof(value.c_str(), 0);
        if(_CheckConfig) {
            std::cout << key << " " << _ICPRotationLimit << std::endl;
        }
    } else if (key == _KeyICPTranslationLimit) {
        _ICPTranslationLimit = strtof(value.c_str(), 0);
        if(_CheckConfig) {
            std::cout << key << " " << _ICPTranslationLimit << std::endl;
        }
        //Raycasting and voxel limit
    } else if (key == _KeyMovingVolumeVoxelThreshold) {
        _MovingVolumeVoxelThreshold = atoi(value.c_str());
        if(_CheckConfig) {
            std::cout << key << " " << _MovingVolumeVoxelThreshold << std::endl;
        }
    } else if (key == _KeyEnableRaycasterPointExtraction) {
        if ((value == "true") || (value == "True") || (value == "TRUE") || (value == "1")) {
            _EnableRaycasterPointExtraction = true;
        } else if ((value == "false") || (value == "False") || (value == "FALSE") || (value == "0")) {
            _EnableRaycasterPointExtraction = false;
        }
        if (_CheckConfig) {
            std::cout << key << " " << _EnableRaycasterPointExtraction << std::endl;
        }
    } else if (key == _KeyKinectFusionGeneratedPointCloudOutput) {
        _KinectFusionGeneratedPointCloudOutput = value;
        if (_CheckConfig) {
            std::cout << key << " " << _KinectFusionGeneratedPointCloudOutput << std::endl;
        }
        //Point Cloud Prep, load if KF wasnt used
    } else if (key == _KeyBPALoadPointCloud) {
        _LoadedPointCloud = value;
        if(_CheckConfig) {
            std::cout << key << " " << _LoadedPointCloud << std::endl;
        }
    //Filtering
    } else if (key == _KeyEnableVoxelGridFilter) {
        if((value == "true") || (value == "True") || (value == "TRUE") || (value == "1")) {
            _EnableVoxelGridFilter = true;
        } else if((value == "false") || (value == "False") || (value == "FALSE") || (value == "0")) {
            _EnableVoxelGridFilter = false;
        }

        if(_CheckConfig) {
            std::cout << key << " " << _EnableVoxelGridFilter << std::endl;
        }
    } else if(key == _KeyVoxelGridFilterSize) {
        _VoxelGridFilterSize = strtof(value.c_str(), 0);
        if(_CheckConfig) {
            std::cout << key << " " << _VoxelGridFilterSize << std::endl;
        }
    } else if(key == _KeyDisplayPointCloudBeforeBPA) {
        if((value == "true") || (value == "True") || (value == "TRUE") || (value == "1")) {
            _DisplayPointCloudBeforeBPA = true;
        } else if((value == "false") || (value == "False") || (value == "FALSE") || (value == "0")) {
            _DisplayPointCloudBeforeBPA = false;
        }
        if(_CheckConfig) {
            std::cout << key << " " << _DisplayPointCloudBeforeBPA << std::endl;
        }
        //BPA
    } else if(key == _KeyEnableBPA) {
        if ((value == "true") || (value == "True") || (value == "TRUE") || (value == "1")) {
            _EnableBPA = true;
        } else if ((value == "false") || (value == "False") || (value == "FALSE") || (value == "0")) {
            _EnableBPA = false;
        }
        if(_CheckConfig) {
            std::cout << key << " " << _EnableBPA << std::endl;
        }
    } else if (key == _KeyBPAOutputMesh) {
        _BPAOutputMesh = value;
        if(_CheckConfig) {
            std::cout << key << " " << _BPAOutputMesh << std::endl;
        }
    } else if (key == _KeyUsePCLNormalEstimate) {
        if ((value == "true") || (value == "True") || (value == "TRUE") || (value == "1")) {
            _UsePCLNormalEstimate = true;
        } else if ((value == "false") || (value == "False") || (value == "FALSE") || (value == "0")) {
            _UsePCLNormalEstimate = false;
        }
        if(_CheckConfig) {
            std::cout << key << " " << _UsePCLNormalEstimate << std::endl;
        }
    } else if(key == _KeyPCLNormalEstimationSize) {
        _PCLNormalEstimationSize = strtof(value.c_str(), 0);
        if(_CheckConfig) {
            std::cout << key << " " << _PCLNormalEstimationSize << std::endl;
        }
    } else if(key == _KeyBPABallRadii) {
        std::istringstream iss(value);
        while (!iss.eof()) {
            std::string v;
            iss >> v;
            _BPABallRadii.push_back(strtof(v.c_str(), 0));
        }
        if (_CheckConfig) {
            int i;
            std::cout << key << " ";
            for (i = 0; i < _BPABallRadii.size(); i++) {
                std::cout << _BPABallRadii[i] << " ";
            }
            std::cout << std::endl;
        }
    } else if (key == _KeyBPAEnableOnlineViewer) {
        if ((value == "true") || (value == "True") || (value == "TRUE") || (value == "1")) {
            _BPAEnableOnlineViewer = true;
        } else if ((value == "false") || (value == "False") || (value == "FALSE") || (value == "0")) {
            _BPAEnableOnlineViewer = false;
        }
        if(_CheckConfig) {
            std::cout << key << " " << _DisplayNormals << std::endl;
        }
    } else if (key == _KeyBPAEnableEndViewer) {
        if ((value == "true") || (value == "True") || (value == "TRUE") || (value == "1")) {
            _BPAEnableEndViewer = true;
        } else if ((value == "false") || (value == "False") || (value == "FALSE") || (value == "0")) {
            _BPAEnableEndViewer = false;
        }
        if(_CheckConfig) {
            std::cout << key << " " << _KeyBPAEnableEndViewer << std::endl;
        }
    } else if (key == _KeyBPAOnlineViewerDelay) {
        BPAOnlineViewerDelay = atoi(value.c_str());
        if(_CheckConfig) {
            std::cout << key << " " << BPAOnlineViewerDelay << std::endl;
        }
    } else if (key == _KeyDisplayNormals) {
        if ((value == "true") || (value == "True") || (value == "TRUE") || (value == "1")) {
            _DisplayNormals = true;
        } else if ((value == "false") || (value == "False") || (value == "FALSE") || (value == "0")) {
            _DisplayNormals = false;
        }
        if(_CheckConfig) {
            std::cout << key << " " << _DisplayNormals << std::endl;
        }
    } else if (key == _KeyBPAOnlineViewerStopIteration) {
        BPAOnlineViewerStopIteration = atoi(value.c_str());
        if(_CheckConfig) {
            std::cout << key << " " << BPAOnlineViewerStopIteration << std::endl;
        }
    } else {
        std::cout << "Invalid key: " << key << std::endl;
    }
}
// trim from start (in place)
static inline void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(),
                                    std::not1(std::ptr_fun<int, int>(std::isspace))));
}
// trim from end (in place)
static inline void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(),
                         std::not1(std::ptr_fun<int, int>(std::isspace))).base(), s.end());
}
// trim from both ends (in place)
static inline void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}
void ConfigParser(std::string configFile) {
    std::cout << "Parsing Config" << std::endl;
    ifstream file;
    file.open(configFile);
    std::string line;
    while(std::getline(file, line)) {
        std::cout << line << std::endl;
        if(line.find("#") == 0) {
            std::cout << "Skipping line" << std::endl;
            continue;
        }
        std::istringstream isline(line);
        std::string key;
        if(std::getline(isline, key, '=')) {
            trim(key);
            std::string value;
            if(std::getline(isline, value)) {
                trim(value);
                setParams(key, value);
            }
        }
    }
}
bool ValidateParamsKinectFusionRecorder() {
    //TODO
    return true;
}
bool ValidateParamsKinectFusionOnline() {
    //TODO
    return true;
}
bool ValidParamsKinectFusionOffline() {
    if(std::isnan(_fx) || std::isnan(_fy) || std::isnan(_ox) || std::isnan(_oy)) {
        std::cerr << "Please verify camera parameters have been set. It can be set using " << _KeyCameraParameters << " as the key followed by 'fx fy ox oy'" << std::endl;
        return false;
    } if (_BaseDir == "" && _DataSet == "") {
        std::cerr << "Base Directory and Dataset directory have not been set, please select the data using either " << _KeyBaseDir << " or " << _KeyDataSetDir << std::endl;
        return false;
    } if (_KinectFusionGeneratedPointCloudOutput == "ReconstructedPointCloudOutput") {
        std::cerr << "Kinect Fusion generated output point cloud name has not been set reverting to default output name 'ReconstructedPointCloudOutput.pcl'" << std::endl;
    } if (_IntegrationMetric <= 0) {
        std::cerr << "Please ensure the DepthMap integration metric is greater than 0. The default value is 0.05" << std::endl;
        return false;
    } if (_WeightFactor < 1.e-3) {
        std::cerr << "Warning " << _KeyKinectFusionWeightFactor << " is close to 0" << std::endl;
    } if (_StopIteration >= 0) {
        if(_StopIteration == 0) {
            std::cerr << "Warning " << _KeyStopIteration << " is set to 0. No images will be integrated" << std::endl;
        } else {
            std::cerr << "The data set will end at iteration " << _StopIteration << std::endl;
        }
    } if (_VolumeDistance < 1.e-3) {
        std::cerr << "Warning " << _KeyVolumeDistance << " is less than 1.e-3 " << std::endl;
    } if (_ICPRotationLimit > 0.1) {
        std::cerr << "Warning " << _ICPRotationLimit << " is greater than 0.1. default value is  0.05" << std::endl;
    } if (_ICPTranslationLimit > 0.1) {
        std::cerr << "Warning " << _KeyVolumeDistance << " is greater than 0.1. default value is 0.05" << std::endl;
    } if (_MovingVolumeVoxelThreshold < 20) {
        std::cerr << "Warning " << _KeyMovingVolumeVoxelThreshold << " is less than 20 This may cause constant extraction of point clouds default value is 60" << std::endl;
    }

    return true;
}
bool ValidateParamsVoxelFilter() {
    return true;
}
bool ValidateParamsNormalEstimation() {
    return true;
}
bool ValidateParamsBPA() {
    return true;
}
int main(int argc, char** argv) {

    if(argc < 2) {
        std::cout << "Please provide config file as parameter" << std::endl;
        return EXIT_FAILURE;
    }
    ConfigParser(argv[1]);
    if (_EnableTestMode) {
        UnitTests();
        return EXIT_SUCCESS;
    }
    if(_EnableRecordOnlyMode) {
        return KinectCapture();
    }

    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr generatedPointCloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
    if(_EnableKinectFusionReconstruction) {
        if(_EnableOnlineMode) {
            //TODO fix for online mode
            RunSlamFusionCudaOnline();
//            exit(0);
        } else {
            assert(ValidParamsKinectFusionOffline());
            generatedPointCloud = RunSlamFusionCudaOffline(_BaseDir, _DataSet);
        }
        if(_KinectFusionGeneratedPointCloudOutput != "") {
            std::cout << "Saving generated point cloud as " << _KinectFusionGeneratedPointCloudOutput << std::endl;
            std::cout << "Dimensions: " << generatedPointCloud.get()->width << " " << generatedPointCloud.get()->height << std::endl;
            pcl::io::savePCDFile(_KinectFusionGeneratedPointCloudOutput, (*generatedPointCloud.get()));
        }
    } else {
        std::cout << "Loading model: " << _LoadedPointCloud << std::endl;
        pcl::io::loadPCDFile(_LoadedPointCloud, (*generatedPointCloud.get()));
    }
    std::cout << "PointCloud Size: " << generatedPointCloud.get()->size() << std::endl;


    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr filteredPointCloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
    if(_EnableVoxelGridFilter) {
        pcl::VoxelGrid<pcl::PointXYZRGBNormal> sor;
        sor.setInputCloud (generatedPointCloud);
        sor.setLeafSize (_VoxelGridFilterSize, _VoxelGridFilterSize, _VoxelGridFilterSize);
        sor.filter (*filteredPointCloud.get());

    } else {
        filteredPointCloud = generatedPointCloud;
    }

    if(_UsePCLNormalEstimate) {
        std::cout << "PCL normal estimation" << std::endl;
        pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr temp(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
        pcl::NormalEstimation<pcl::PointXYZRGBNormal, pcl::Normal> ne;
        ne.setInputCloud (filteredPointCloud);

        // Create an empty kdtree representation, and pass it to the normal estimation object.
        // Its content will be filled inside the object, based on the given input dataset (as no other search surface is given).
        pcl::search::KdTree<pcl::PointXYZRGBNormal>::Ptr tree (new pcl::search::KdTree<pcl::PointXYZRGBNormal> ());
        ne.setSearchMethod (tree);

        // Output datasets
        pcl::PointCloud<pcl::Normal>::Ptr cloud_normals (new pcl::PointCloud<pcl::Normal>);

        // Use all neighbors in a sphere of radius 3cm
        ne.setRadiusSearch (_PCLNormalEstimationSize);

        // Compute the features
        ne.compute (*cloud_normals);
        pcl::concatenateFields(*filteredPointCloud,  *cloud_normals, *temp);
        filteredPointCloud = temp;
    }
    int i;
    for(i = 0; i < filteredPointCloud->points.size(); i++) {
        if(filteredPointCloud->points[i].r == 0 && filteredPointCloud->points[i].g == 0 && filteredPointCloud->points[i].b == 0) {
            filteredPointCloud->points[i].r = 255;
            filteredPointCloud->points[i].g = 255;
            filteredPointCloud->points[i].b = 255;
        }
    }
    //removing points

//    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr ptr(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
//
//    for(i = 0; i < filteredPointCloud->points.size(); i++) {
//        //-0.25, -0.5
////        if(filteredPointCloud->points[i].x < -0.3 && filteredPointCloud->points[i].y < -0.3 && filteredPointCloud->points[i].x > -0.4 && filteredPointCloud->points[i].y > -0.4) {
//        if(filteredPointCloud->points[i].x < -0.25 && filteredPointCloud->points[i].y < -0.25 && filteredPointCloud->points[i].x > -0.5 && filteredPointCloud->points[i].y > -0.5) {
//            ptr->points.push_back(filteredPointCloud->points[i]);
//        }
//    }
////    ptr->points[19].r = 255; ptr->points[19].g = 0; ptr->points[19].b = 0;
////    ptr->points[17].r = 0; ptr->points[17].g = 255; ptr->points[17].b = 0;
////    ptr->points[0].r = 255; ptr->points[0].g = 0; ptr->points[0].b = 0;
////    ptr->points[1].r = 180; ptr->points[1].g = 0; ptr->points[1].b = 0;
////    ptr->points[2].r = 128; ptr->points[2].g = 0; ptr->points[2].b = 0;
////    ptr->points[3].r = 0; ptr->points[3].g = 255; ptr->points[3].b = 0;
////    ptr->points[4].r = 0; ptr->points[4].g = 180; ptr->points[4].b = 0;
////    ptr->points[5].r = 0; ptr->points[5].g = 128; ptr->points[5].b = 0;
////    ptr->points[6].r = 0; ptr->points[6].g = 0; ptr->points[6].b = 255;
////    ptr->points[7].r = 0; ptr->points[7].g = 0; ptr->points[7].b = 180;
//    ptr->width = ptr->points.size();
//    ptr->height = 1;
//    std::cout << "Ptr: " << ptr->points.size() << " fpc: " << filteredPointCloud->points.size() << std::endl;
////    std::string s;
////    std::cin >> s;
//    filteredPointCloud = ptr;

    if(_DisplayPointCloudBeforeBPA) {

        pcl::visualization::PCLVisualizer viewer;
        viewer.setBackgroundColor (0, 0, 0);
        viewer.addPointCloud<pcl::PointXYZRGBNormal> (filteredPointCloud, "sample cloud");
        viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 4, "sample cloud");
        if(_DisplayNormals) {
            viewer.addPointCloudNormals<pcl::PointXYZRGBNormal>(filteredPointCloud, 1, 0.02, "normals");
        }
        viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_LINE_WIDTH, 4, "sample cloud");
//        viewer.addCoordinateSystem (1.0);
        viewer.initCameraParameters ();
        std::cout << "Stopped ? " << viewer.wasStopped() << std::endl;
        while (!viewer.wasStopped ()) {
            viewer.spinOnce (100);
        }
    }

    if(_EnableBPA) {
        std::cout << "Reconstruction starting" << std::endl;
        std::string wait;
        std::cout << "Point Cloud size " << filteredPointCloud.get()->size() << std::endl;
        std::cout << "Press enter to continue" << std::endl;
        std::cin >> wait;
        float* radius = new float[_BPABallRadii.size()];
        int i;
        for(i = 0; i < _BPABallRadii.size(); i++) {
            radius[i] = _BPABallRadii[i];
        }


        BallPivotAlgorithm BPA(filteredPointCloud);
//        BPA._viewerEnable = true;
        if(_BPAEnableOnlineViewer) {
            BPA.EnableViewer();
            BPA._viewerDelayTime = BPAOnlineViewerDelay;
            BPA._viewerStopIteration = BPAOnlineViewerStopIteration;
//            std::cout << "DELAY " << BPAOnlineViewerDelay << " " << BPA._viewerDelayTime << std::endl;
//            std::this_thread::sleep_for(std::chrono::seconds(20));
        } if (_BPAEnableEndViewer) {
            BPA.EnableEndViewer();
        }


        BPA(radius, _BPABallRadii.size());
        BPA.SaveAsMesh(_BPAOutputMesh);
        delete[] radius;
    }
    return EXIT_SUCCESS;
}