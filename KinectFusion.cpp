/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/06/10.
//

#include <thread>
#include "KinectFusion.hpp"
namespace Slam {



    KinectFusion::~KinectFusion() {
        if (_enableCudaTesting) {
            gpu::host::DeallocateAll();
            free(_gpuVolumePtr);
            free(bilateralFilterTargetPtr);
            free(bilateralFilterSourcePtr);
            free(_icpSourceVertexMap);
            free(_icpSourceNormalMap);
            free(_icpLocalTargetVertexMap);
            free(_icpLocalTargetNormalMap);
            free(_icpGlobalTargetVertexMap);
            free(_icpGlobalTargetNormalMap);
            free(_colourMap);
        }
    }
    Eigen::Matrix4d KinectFusion::ComputeRigidBodyMotion(cv::Mat &rgbSource, cv::Mat &graySource,
                                                         cv::Mat_<double> &depthSource, cv::Mat &rgbTarget,
                                                         cv::Mat &grayTarget, cv::Mat_<double> &depthTarget) {
        Eigen::Matrix4d transformation = Eigen::Matrix4d::Identity();

        return transformation;
    }

    Eigen::Matrix<double, 6, 1> KinectFusion::CholeskyDecomposition(Eigen::Matrix<double, Eigen::Dynamic, 6>& A, Eigen::Matrix<double, Eigen::Dynamic, 1>& b) {
        Eigen::Matrix<double, 6, Eigen::Dynamic> Astar = A.transpose();
        Eigen::Matrix<double, 6, 6> AstarA = Astar * A;
        // AstarA is the new matrix A
        Eigen::Matrix<double, 6, 1> AstarB = Astar * b;

        Eigen::Matrix<double, 6, 1> solution = AstarA.llt().solve(AstarB);
        return solution;
    }

    //Main Methods
    void KinectFusion::SetupCuda(double fx, double fy, double cx, double cy, int rows, int cols, int numICPLevels,
                                 gpu::matrix33 rotationMatrix, gpu::float3 translation, float weightFactor, float volumeSize, bool featureBasedICP) {
        _featureBasedICPEnabled = featureBasedICP;
        _weightFactor = weightFactor;
        counter = 0;
        iteration = 0;
        _cameraMatrix = gpu::matrix33(fx, 0, cx, 0, fy, cy, 0, 0, 1);
        gpu::matrix33 inverseRotation = rotationMatrix.transpose();
        _translation = translation;
        _rotationMatrix = rotationMatrix;
        _lastIntegrationRotationMatrix = _rotationMatrix;
        _lastIntegrationTranslation = _translation;
        _lastPointCloudExtractionRotationMatrix = _rotationMatrix;
        _lastPointCloudExtractionTranslation = _translation;
        _voxelGridSize.x = 512;
        _voxelGridSize.y = 512;
        _voxelGridSize.z = 512;

        _voxelCenter.x = 0;
        _voxelCenter.y = 0;
        _voxelCenter.z = 0;
        _voxelCellSize = volumeSize / _voxelGridSize;
        _volumeSize = volumeSize;
//        _voxelCellSize = 3.f / _voxelGridSize;
        std::cout << "Size is " << _voxelCellSize.x << " " << _voxelCellSize.y << " " << _voxelCellSize.z << std::endl;
//        _voxelCellSize.x = 0.01;
//        _voxelCellSize.y = 0.01;
//        _voxelCellSize.z = 0.01;

        _voxelThreshold.x = 60;
        _voxelThreshold.y = 60;
        _voxelThreshold.z = 60;
//        _voxelThreshold.x = 150;
//        _voxelThreshold.y = 150;
//        _voxelThreshold.z = 150;

        _pointIndexGridSize.x = 512;
        _pointIndexGridSize.y = 512;
        _pointIndexGridSize.z = 64;
//        SetVoxelGridSize(gridSizeX, gridSizeY, gridSizeZ);
//        SetVoxelCellSize(0.01);

        //Allocations needed for operations
        _gpuPointIndexVolume =  new int[_pointIndexGridSize.x * _pointIndexGridSize.y * _pointIndexGridSize.z];
        _gpuPointIndexColourVolume =  new int[_pointIndexGridSize.x * _pointIndexGridSize.y * _pointIndexGridSize.z];
        _gpuPointIndexNormalVolume = new gpu::float3[_pointIndexGridSize.x * _pointIndexGridSize.y * _pointIndexGridSize.z];

        _poseGraph._voxelCellSize = _voxelCellSize;
        _poseGraph._voxelSize = _voxelGridSize;
        int bilateralFilterAllImagesSize = 0;
        int i;
        for(i = 0; i < numICPLevels; i++)
            bilateralFilterAllImagesSize += (rows * pow(2, -i)) * (cols * pow(2, -i));

        _featureBasedICP = FBICP(10, 10, 20, 5, 0.3, _volumeSize, fx, fy, cx, cy);
        //vars for download
        if(_enableCudaTesting) {
            _gpuVolumePtr = new int[(_voxelGridSize.x * _voxelGridSize.y * _voxelGridSize.z)];
            bilateralFilterTargetPtr = new float[bilateralFilterAllImagesSize];
            bilateralFilterSourcePtr = new float[bilateralFilterAllImagesSize];
            _icpSourceVertexMap = new gpu::float3[bilateralFilterAllImagesSize];
            _icpSourceNormalMap = new gpu::float3[bilateralFilterAllImagesSize];
            _icpLocalTargetVertexMap = new gpu::float3[bilateralFilterAllImagesSize];
            _icpLocalTargetNormalMap = new gpu::float3[bilateralFilterAllImagesSize];
            _icpGlobalTargetVertexMap = new gpu::float3[bilateralFilterAllImagesSize];
            _icpGlobalTargetNormalMap = new gpu::float3[bilateralFilterAllImagesSize];
            _colourMap = new uchar[bilateralFilterAllImagesSize * 3];
        }
        gpu::host::init(_cameraMatrix, rotationMatrix, inverseRotation, translation, _voxelCenter, _voxelGridSize, _voxelCellSize, _pointIndexGridSize, rows, cols, 3, numICPLevels);
        CudaResetPointIndexVolume();
        CudaDownloadRayCastTsdfSection();
    }
    void KinectFusion::SetICPLimits(float rotationlimit, float translationlimit) {
        _rotationMag = rotationlimit;
        _translationMag = translationlimit;
    }
    void KinectFusion::SetIntegrationMetric(float metric) {
        _integration_metric_threshold = metric;
    }
    void KinectFusion::SetMovingVoxelThreshold(int voxels) {
        _voxelThreshold.x = voxels;
        _voxelThreshold.y = voxels;
        _voxelThreshold.z = voxels;
    }
    bool KinectFusion::CudaKinect(gpu::DepthMap& img, bool initial) {
        CudaUploadImageData(img);
        CudaBilateralFilterAndResize();
        bool icpComplete = true, integrationCheckPassed = false, pointcloudExtractionCheckPass = false;
        if(!initial) {
            icpComplete = CudaIterativeClosestPoint(integrationCheckPassed, pointcloudExtractionCheckPass);
        }
        if (icpComplete) {
            CudaProjectiveTsdf();
            CudaRayCaster();
        } else {

        }
        return icpComplete;
//        CudaTsdfVisualizer();
//        std::cout << "ICP Pass Complete" << std::endl;

//        gpu::host::SwitchSourceAndTarget();

    }
    bool KinectFusion::CudaKinect(gpu::DepthMap& img, gpu::Image& colourImg, bool initial) {
        CudaUploadImageData(img);
        CudaUploadColourImageData(colourImg);

        CudaBilateralFilterAndResize();
        bool icpComplete = true, integrationCheckPassed = false, pointcloudExtractionCheckPass = false;
        if(!initial) {
            icpComplete = CudaIterativeClosestPoint(integrationCheckPassed, pointcloudExtractionCheckPass);
        }
        if (icpComplete) {
            CudaProjectiveTsdf();
            CudaRayCaster();
        } else {

        }
        return icpComplete;
//        CudaTsdfVisualizer();
//        std::cout << "ICP Pass Complete" << std::endl;

//        gpu::host::SwitchSourceAndTarget();

    }
    void KinectFusion::CudaBilateralFilterAndResize() {
        bool enableBilatFilter = true;

        if(enableBilatFilter) {
            gpu::host::BilateralFilter::BilateralFilter();
        } else {
            cudaMemcpy(gpu::host::GetGPUTargetImages(), gpu::host::GetGPUDepthMap(), sizeof(float) * 640 * 480, cudaMemcpyDeviceToDevice);
        }
        gpu::host::BilateralFilter::ResizeImageIntoPyramid();
        CudaDownloadBilateralFilterAndResize();
    }
    bool KinectFusion::CudaIterativeClosestPoint(bool& integrationCheckPass, bool& pointcloudExtractionPassed) {
        bool printExtra = false;
        bool downloadExtra = false;

        clock_t begin = clock();
        gpu::host::IterativeClosestPoint::BuildTargetVertexAndNormalMaps();
        int levelIterations[] = {10, 5, 4};
        int i, iteration;
        int imageRows = gpu::host::GetImageRows(), imageCols = gpu::host::GetImageCols();
        int icpLevels = gpu::host::GetICPLevels();
        int rows = imageRows, cols = imageCols;

        int skip = 0;
        for(i = 0 ; i < icpLevels - 1; i++) {
            skip += rows * cols;
            rows /= 2;
            cols /= 2;
        }

        gpu::matrix33 rotationMatrix, inverseRotationMatrix, predictedRotation, predictedInverseRotation;
        gpu::float3 translation, predictedTranslation;
        gpu::host::Download(gpu::host::GetGPURotation(), &rotationMatrix, sizeof(gpu::matrix33));
        gpu::host::Download(gpu::host::GetGPUTranslation(), &translation, sizeof(gpu::float3));
        inverseRotationMatrix = rotationMatrix.transpose();

        predictedRotation = rotationMatrix;
        predictedInverseRotation = inverseRotationMatrix;
        predictedTranslation = translation;

        gpu::host::UploadTransformation(predictedRotation, predictedInverseRotation, predictedTranslation,
                                        gpu::host::GetGPUPredictedRotation(), gpu::host::GetGPUPredictedInverseRotation(), gpu::host::GetGPUPredictedTranslation());
        float* multiplied = new float[42];
        float errorValue = std::numeric_limits<float>::infinity();
        std::memset(multiplied, 0, sizeof(float) * 42);
        int equationsAccepted = 0;
        for(i = icpLevels - 1; i >= 0; i--) {
            errorValue = std::numeric_limits<float>::infinity();
//            skip = 0;
//            std::cout << "ICP LEVEL " << i << " iteration stuff " << i <<  std::endl;
            rows = (int) (gpu::host::GetImageRows() * powf(2, -i));
            cols = (int) (gpu::host::GetImageCols() * powf(2, -i));
//            std::cout << rows << " " << cols << " " << skip << std::endl;
            iteration = 0;

            while (iteration < levelIterations[i]) {


                gpu::host::IterativeClosestPoint::IterativeClosestPoint(i, rows, cols, skip);
                float tempError = 0;
                gpu::host::Download(gpu::host::GetGPUICPError(), &tempError, sizeof(float));
//                std::cout << "Downloaded Error " << tempError << std::endl;

                gpu::host::Download(gpu::host::GetGPUICPMaskCount(), &equationsAccepted, sizeof(int));
//                std::cout << "Equations Accepted " << equationsAccepted << std::endl;
                //Download equations and solve on cpu TODO fix reduction so this doesnt have to be done
//                gpu::float7* AV = new gpu::float7[rows * cols];
//                gpu::host::Download(gpu::host::GetGPUICPEquations() + skip, AV, sizeof(gpu::float7) * rows * cols);
//                int l;
//                Eigen::Matrix<float, Eigen::Dynamic, 6> A;
//                A.resize(rows * cols, Eigen::NoChange);
//                Eigen::Matrix<float, Eigen::Dynamic, 1> B;
//                B.resize(rows * cols, Eigen::NoChange);
//                for(l = 0; l < rows * cols; l++) {
//                    A(l, 0) = AV[l](0);
//                    A(l, 1) = AV[l](1);
//                    A(l, 2) = AV[l](2);
//                    A(l, 3) = AV[l](3);
//                    A(l, 4) = AV[l](4);
//                    A(l, 5) = AV[l](5);
//
//                    B(l, 0) = AV[l](6);
//                }
//                free(AV);
//                Eigen::Matrix<float, 6, 6> AstarA = A.transpose() * A;
//                Eigen::Matrix<float, 6, 1> AstarB = A.transpose() * B;

                //
                cudaMemcpy(multiplied, gpu::host::GetGPUICPReductionMatrix(), sizeof(float) * 42, cudaMemcpyDeviceToHost);

                Eigen::Matrix<float, 6, 6> AAstarAUpperTriangular;
                Eigen::Matrix<float, 6, 1> AAstarB;
                AAstarAUpperTriangular.setZero();
                AAstarB.setZero();
                int ii, jj;
                for(ii = 0; ii < 6; ii++) {
                    for(jj = ii; jj < 7; jj++ ) {
                        if(jj == 6) {
                            AAstarB(ii, 0) = multiplied[ii * 7 + jj];
                        } else {
                            AAstarAUpperTriangular(ii, jj) = multiplied[ii * 7 + jj];
                        }
                    }
                }
                Eigen::Matrix<float, 6, 6> AAstarA = AAstarAUpperTriangular.selfadjointView<Eigen::Upper>();
                if(fabsf(AAstarA.determinant()) < 1.e-15 || isnanf(AAstarA.determinant())) {
                    std::cerr << "\x1B[31m" << "Null space check failed" << "\x1B[0m" << std::endl;
                    break;
                }
//                std::cout << test << std::endl;
//                exit(0);

                //Test
//                if(!AstarA.isApprox(AAstarA)) {
//                    std::cout << "Failed Not Approx ==" << std::endl;
//                    std::cout << AstarA << std::endl;
//                    std::cout << AAstarA << std::endl;
//                    exit(0);
//                } if (!AAstarB.isApprox(AAstarB)) {
//                    std::cout << "Failed Not Approx ==" << std::endl;
//                    std::cout << AstarB << std::endl;
//                    std::cout << AAstarB << std::endl;
//                    exit(0);
//                }
                //Test
                Eigen::Matrix<float, 6, 1> x = AAstarA.llt().solve(AAstarB);
                if(x.hasNaN()) {
                    std::cerr << "\x1B[31m" << "Nan detected in estimate please relocalising" << "\x1B[0m" << std::endl;
                    gpu::host::UploadTransformation(rotationMatrix, inverseRotationMatrix, translation,
                                                    gpu::host::GetGPUPredictedRotation(),
                                                    gpu::host::GetGPUPredictedInverseRotation(),
                                                    gpu::host::GetGPUPredictedTranslation());
                    return false;
                }
                //TODO create checks for icp small angle assumption and motion
                //Magnitude check of transform params
                if(x(0) > _rotationMag || x(1) > _rotationMag || x(2) > _rotationMag) {
                    std::cerr << "\x1B[31m" << "Rotation Magnitude too high Threshold: " << _rotationMag << "\x1B[0m" << std::endl;
                    std::cout << x(0) << " " << x(1) << " " << x(2) << std::endl;
                    break;
                }
                if(x(3) > _translationMag || x(4) > _translationMag || x(5) > _translationMag) {
                    std::cerr << "\x1B[31m" << "Translation Magnitude too high Threshold: " << _translationMag << "\x1B[0m" << std::endl;
                    std::cout << x(3) << " " << x(4) << " " << x(5) << std::endl;
                    break;
                }
                //ERROR check
//                if(errorValue < tempError) {
//                    //Error value too high compared to previous
//                    std::cerr << "\x1B[31m" << "Error too high" << "\x1B[0m" << std::endl;
//                    break;
//                }
//                errorValue = tempError;
//                exit(0);


//                std::cout << AstarA << std::endl;
//                std::cout << AstarB << std::endl;
//                gpu::matrix33 incRotation(1, x(2), x(1),
//                                          -x(2), 1, x(0),
//                                          x(1), -x(0), 1);
                gpu::matrix33 incRotation = Utility::GenerateRotationMatrix33(x(0), x(1), x(2));
                gpu::float3 incTranslation(x(3), x(4), x(5));

                gpu::host::Download(gpu::host::GetGPUPredictedRotation(), &predictedRotation, sizeof(gpu::matrix33));
                gpu::host::Download(gpu::host::GetGPUPredictedInverseRotation(), &predictedInverseRotation, sizeof(gpu::matrix33));
                gpu::host::Download(gpu::host::GetGPUPredictedTranslation(), &predictedTranslation, sizeof(gpu::float3));

                predictedTranslation = incRotation * predictedTranslation + incTranslation;
                predictedRotation = incRotation * predictedRotation;
                predictedInverseRotation = predictedRotation.transpose();

                gpu::host::UploadTransformation(predictedRotation, predictedInverseRotation, predictedTranslation,
                                                gpu::host::GetGPUPredictedRotation(),
                                                gpu::host::GetGPUPredictedInverseRotation(),
                                                gpu::host::GetGPUPredictedTranslation());

                iteration++;
//                std::cout << "Finished " << std::endl;
            }
            skip -= (4 * rows * cols);
        }


        if(false) {
            gpu::matrix33 prevRotationMatrix;
            gpu::float3 prevTranslation;
            gpu::host::Download(gpu::host::GetGPURotation(), &prevRotationMatrix, sizeof(gpu::matrix33));
            gpu::host::Download(gpu::host::GetGPUTranslation(), &prevTranslation, sizeof(gpu::float3));

            gpu::matrix33 finalRotationMatrix;
            gpu::float3 finalTranslation;
            gpu::host::Download(gpu::host::GetGPUPredictedRotation(), &finalRotationMatrix, sizeof(gpu::matrix33));
            gpu::host::Download(gpu::host::GetGPUPredictedTranslation(), &finalTranslation, sizeof(gpu::float3));

            std::cout << "Previous Rotation Matrix " << std::endl
            << prevRotationMatrix.row0.x << " " << prevRotationMatrix.row0.y << " " << prevRotationMatrix.row0.z << " " << prevTranslation.x << std::endl
            << prevRotationMatrix.row1.x << " " << prevRotationMatrix.row1.y << " " << prevRotationMatrix.row1.z << " " << prevTranslation.y <<std::endl
            << prevRotationMatrix.row2.x << " " << prevRotationMatrix.row2.y << " " << prevRotationMatrix.row2.z << " " << prevTranslation.z <<std::endl;

            std::cout << "Predicted Rotation Matrix " << std::endl
            << finalRotationMatrix.row0.x << " " << finalRotationMatrix.row0.y << " " << finalRotationMatrix.row0.z << " " << finalTranslation.x << std::endl
            << finalRotationMatrix.row1.x << " " << finalRotationMatrix.row1.y << " " << finalRotationMatrix.row1.z << " " << finalTranslation.y <<std::endl
            << finalRotationMatrix.row2.x << " " << finalRotationMatrix.row2.y << " " << finalRotationMatrix.row2.z << " " << finalTranslation.z <<std::endl;
        }
        if(downloadExtra) {
            CudaDownloadIterativeClosestPointVertexAndNormalMap();
        }
//        Eigen::Matrix3f rcurr, rprev;
//        rcurr << predictedRotation.row0.x, predictedRotation.row0.y, predictedRotation.row0.z,
//                predictedRotation.row1.x, predictedRotation.row1.y, predictedRotation.row1.z,
//                predictedRotation.row2.x, predictedRotation.row2.y, predictedRotation.row2.z;

        //Integration Check
        gpu::float3 translationDifference = predictedTranslation - _lastIntegrationTranslation;
        gpu::matrix33 result = predictedRotation.transpose() * _lastIntegrationRotationMatrix;
        cv::Mat rotationalDifference(3, 3, CV_32F), rotationVector(1, 3, CV_32F, cv::Scalar(0));
        rotationalDifference.at<float>(0, 0) = result.row0.x;     rotationalDifference.at<float>(0, 1) = result.row0.y;     rotationalDifference.at<float>(0, 2) = result.row0.z;
        rotationalDifference.at<float>(1, 0) = result.row1.x;     rotationalDifference.at<float>(1, 1) = result.row1.y;     rotationalDifference.at<float>(1, 2) = result.row1.z;
        rotationalDifference.at<float>(2, 0) = result.row2.x;     rotationalDifference.at<float>(2, 1) = result.row2.y;     rotationalDifference.at<float>(2, 2) = result.row2.z;
        CvMat rotationalDifferenceCvMat = rotationalDifference;
        CvMat rotationVectorCvMat(rotationVector);
        cvRodrigues2(&rotationalDifferenceCvMat, &rotationVectorCvMat);
        gpu::float3 vector(rotationVector.at<float>(0),rotationVector.at<float>(1),rotationVector.at<float>(2));
        float rnorm = vector.norm();
        float tnorm = translationDifference.norm();
        const float alpha = 1.f;
        integrationCheckPass = (rnorm + alpha * tnorm)/2 >= _integration_metric_threshold;
        std::cout << "Integration Metric value " << (rnorm + alpha * tnorm)/2 << std::endl;
        if(integrationCheckPass == true) {
            std::cout << "\033[38mShould Integrating DepthMap\033[39m" << std::endl;
            _lastIntegrationRotationMatrix = predictedRotation;
            _lastIntegrationTranslation = predictedTranslation;
        }


        //Extraction Check
        translationDifference = predictedTranslation - _lastPointCloudExtractionTranslation;
        result = predictedRotation.transpose() * _lastPointCloudExtractionRotationMatrix;

        rotationalDifference.at<float>(0, 0) = result.row0.x;     rotationalDifference.at<float>(0, 1) = result.row0.y;     rotationalDifference.at<float>(0, 2) = result.row0.z;
        rotationalDifference.at<float>(1, 0) = result.row1.x;     rotationalDifference.at<float>(1, 1) = result.row1.y;     rotationalDifference.at<float>(1, 2) = result.row1.z;
        rotationalDifference.at<float>(2, 0) = result.row2.x;     rotationalDifference.at<float>(2, 1) = result.row2.y;     rotationalDifference.at<float>(2, 2) = result.row2.z;
        rotationalDifferenceCvMat = rotationalDifference;
        rotationVectorCvMat = rotationVector;
        cvRodrigues2(&rotationalDifferenceCvMat, &rotationVectorCvMat);
        vector.x = rotationVector.at<float>(0);
        vector.y = rotationVector.at<float>(1);
        vector.z = rotationVector.at<float>(2);
        rnorm = vector.norm();
        tnorm = translationDifference.norm();
        pointcloudExtractionPassed = (rnorm + alpha * tnorm)/2 >= _pointcloud_extraction_metric_threshold;
        std::cout << "PointCloud Extraction Metric value " << (rnorm + alpha * tnorm)/2 << std::endl;
        if(pointcloudExtractionPassed == true) {
            std::cout << "\033[38mShould Integrating DepthMap\033[39m" << std::endl;
            _lastPointCloudExtractionRotationMatrix = predictedRotation;
            _lastPointCloudExtractionTranslation = predictedTranslation;
        }





//        std::cout << "\033[34mAccepted Equations: " << equationsAccepted << "\033[39m" << std::endl;

        _rotationMatrix = predictedRotation;
        gpu::matrix33 inverse = _rotationMatrix.transpose();
        _translation = predictedTranslation;
        gpu::host::UploadTransformation(_rotationMatrix, inverse, _translation, gpu::host::GetGPURotation(), gpu::host::GetGPUInverseRotation(), gpu::host::GetGPUTranslation());
        //GPU swap;
//        std::cout << "Predicted Rotation Matrix " << std::endl
//        << _rotationMatrix.row0.x << " " << _rotationMatrix.row0.y << " " << _rotationMatrix.row0.z << " " << _translation.x << std::endl
//        << _rotationMatrix.row1.x << " " << _rotationMatrix.row1.y << " " << _rotationMatrix.row1.z << " " << _translation.y <<std::endl
//        << _rotationMatrix.row2.x << " " << _rotationMatrix.row2.y << " " << _rotationMatrix.row2.z << " " << _translation.z <<std::endl;





        clock_t end = clock();
//        std::cout << "Iterative Closest Point: " << double(end - begin)/ CLOCKS_PER_SEC << std::endl;
        free(multiplied);
        return true;
    }

    bool KinectFusion::FeatureBasedICP(cv::Mat& rgb, cv::Mat_<double>& depth, bool& integrationCheckPass, bool& pointcloudExtractionPassed) {
        CudaDownloadRayCaster(-1);
        int x, y;
        cv::Mat raycasterRGB(gpu::host::GetImageRows(), gpu::host::GetImageCols(), CV_8UC3);
        //TODO OMP
        for(x = 0; x < gpu::host::GetImageCols(); x++) {
            for(y = 0; y < gpu::host::GetImageRows(); y++) {
                raycasterRGB.data[gpu::device::Index2D(x, y, raycasterRGB.cols) * 3 + 2] = _colourMap[gpu::device::Index2D(x, y, raycasterRGB.cols) * 3 + 0];
                raycasterRGB.data[gpu::device::Index2D(x, y, raycasterRGB.cols) * 3 + 1] = _colourMap[gpu::device::Index2D(x, y, raycasterRGB.cols) * 3 + 1];
                raycasterRGB.data[gpu::device::Index2D(x, y, raycasterRGB.cols) * 3 + 0] = _colourMap[gpu::device::Index2D(x, y, raycasterRGB.cols) * 3 + 2];
            }
        }

        cv::Mat_<double> raycasterDepth(gpu::host::GetImageRows(), gpu::host::GetImageCols());
        //TODO OMP
        for(x = 0; x < gpu::host::GetImageCols(); x++) {
            for(y = 0; y < gpu::host::GetImageRows(); y++) {
                if(std::isnan(_icpSourceVertexMap[gpu::device::Index2D(x, y, raycasterDepth.cols)].z) ||
                        _icpSourceVertexMap[gpu::device::Index2D(x, y, raycasterDepth.cols)].z == 0.f) {
                    ((double*)raycasterDepth.data)[gpu::device::Index2D(x, y, raycasterDepth.cols)] = 0;
                    continue;
                }
                gpu::float3 point = _rotationMatrix.transpose() * (_icpSourceVertexMap[gpu::device::Index2D(x, y, raycasterDepth.cols)] - _translation);
                ((double*)raycasterDepth.data)[gpu::device::Index2D(x, y, raycasterDepth.cols)] = point.z;
            }
        }

//        cv::Mat raycasterRGB2 = cv::imread("TESTA.png");
//        cv::Mat rgb2 = cv::imread("TESTB.png");
//        cv::imshow("TEST", raycasterRGB2);
//        cv::imshow("TEST2", rgb2);
//        cv::waitKey(0);

////        cv::Mat prevRgb = cv::imread("TESTA.png");
//        cv::Mat_<double> prevDepth = cv::imread("test/1D.png", CV_LOAD_IMAGE_ANYDEPTH);
//        prevDepth = prevDepth * (1./5000);
////    prevDepth = prevDepth / 65535.;
////        cv::Mat rgb = cv::imread("TESTB.png");
//        cv::Mat_<double> depth = cv::imread("test/2D.png", CV_LOAD_IMAGE_ANYDEPTH);
//        depth = depth * (1./5000);
        depth = depth/1000.;

//        cv::imshow("RaycastedDepth", raycasterDepth);
//        cv::imshow("LoadedDepth", depth);
//        cv::waitKey(0);
//        exit(0);
//        clock_t begin, end;
//        std::cout << "\033[1;33mBasic Test with noise\033[0m" << std::endl;
        _featureBasedICP.SetRaycasterImage(raycasterRGB, raycasterDepth);
        gpu::matrix33 rotation;
        gpu::float3 translation;
//        begin = clock();
        bool icp;
        icp = _featureBasedICP.EstimateMotion(rgb, depth, rotation, translation);
        if(!icp) {
            return false;
        }



//        float fx = 525.0f, // default
//                fy = 525.0f,
//                cx = 319.5f,
//                cy = 239.5f;
//        FBICP fbicp(10, 0.07, 10, 10, 0.3, 1.5, fx, fy, cx, cy);
//        //TODO Create images above
//        std::cout << "TYPES " << rgb2.type() << " " << raycasterRGB2.type() << std::endl;
        gpu::matrix33 estimatedRotation;
        gpu::float3 estimatedTranslation;
//        fbicp.SetRaycasterImage(raycasterRGB2, raycasterDepth);
//        fbicp.EstimateMotion(rgb2, depth, estimatedRotation, estimatedTranslation);



        gpu::float3 translationDifference = estimatedTranslation - _lastIntegrationTranslation;
        gpu::matrix33 result = estimatedRotation.transpose() * _lastIntegrationRotationMatrix;
        cv::Mat rotationalDifference(3, 3, CV_32F), rotationVector(1, 3, CV_32F, cv::Scalar(0));
        rotationalDifference.at<float>(0, 0) = result.row0.x;     rotationalDifference.at<float>(0, 1) = result.row0.y;     rotationalDifference.at<float>(0, 2) = result.row0.z;
        rotationalDifference.at<float>(1, 0) = result.row1.x;     rotationalDifference.at<float>(1, 1) = result.row1.y;     rotationalDifference.at<float>(1, 2) = result.row1.z;
        rotationalDifference.at<float>(2, 0) = result.row2.x;     rotationalDifference.at<float>(2, 1) = result.row2.y;     rotationalDifference.at<float>(2, 2) = result.row2.z;
        CvMat rotationalDifferenceCvMat = rotationalDifference;
        CvMat rotationVectorCvMat(rotationVector);
        cvRodrigues2(&rotationalDifferenceCvMat, &rotationVectorCvMat);
        gpu::float3 vector(rotationVector.at<float>(0),rotationVector.at<float>(1),rotationVector.at<float>(2));
        float rnorm = vector.norm();
        float tnorm = translationDifference.norm();
        const float alpha = 1.f;
        integrationCheckPass = (rnorm + alpha * tnorm)/2 >= _integration_metric_threshold;
        std::cout << "Integration Metric value " << (rnorm + alpha * tnorm)/2 << std::endl;
        if(integrationCheckPass == true) {
            std::cout << "\033[38mShould Integrating DepthMap\033[39m" << std::endl;
            _lastIntegrationRotationMatrix = estimatedRotation;
            _lastIntegrationTranslation = estimatedTranslation;
        }


        //Extraction Check
        translationDifference = estimatedTranslation - _lastPointCloudExtractionTranslation;
        result = estimatedRotation.transpose() * _lastPointCloudExtractionRotationMatrix;

        rotationalDifference.at<float>(0, 0) = result.row0.x;     rotationalDifference.at<float>(0, 1) = result.row0.y;     rotationalDifference.at<float>(0, 2) = result.row0.z;
        rotationalDifference.at<float>(1, 0) = result.row1.x;     rotationalDifference.at<float>(1, 1) = result.row1.y;     rotationalDifference.at<float>(1, 2) = result.row1.z;
        rotationalDifference.at<float>(2, 0) = result.row2.x;     rotationalDifference.at<float>(2, 1) = result.row2.y;     rotationalDifference.at<float>(2, 2) = result.row2.z;
        rotationalDifferenceCvMat = rotationalDifference;
        rotationVectorCvMat = rotationVector;
        cvRodrigues2(&rotationalDifferenceCvMat, &rotationVectorCvMat);
        vector.x = rotationVector.at<float>(0);
        vector.y = rotationVector.at<float>(1);
        vector.z = rotationVector.at<float>(2);
        rnorm = vector.norm();
        tnorm = translationDifference.norm();
        pointcloudExtractionPassed = (rnorm + alpha * tnorm)/2 >= _pointcloud_extraction_metric_threshold;
        std::cout << "PointCloud Extraction Metric value " << (rnorm + alpha * tnorm)/2 << std::endl;
        if(pointcloudExtractionPassed == true) {
            std::cout << "\033[38mShould Integrating DepthMap\033[39m" << std::endl;
            _lastPointCloudExtractionRotationMatrix = estimatedRotation;
            _lastPointCloudExtractionTranslation = estimatedTranslation;
        }
        _rotationMatrix = estimatedRotation;
        gpu::matrix33 inverse = _rotationMatrix.transpose();
        _translation = estimatedTranslation;
        gpu::host::UploadTransformation(_rotationMatrix, inverse, _translation, gpu::host::GetGPURotation(), gpu::host::GetGPUInverseRotation(), gpu::host::GetGPUTranslation());
        //GPU swap;
        return true;
    }
    void KinectFusion::CudaDownloadIterativeClosestPointEquations() {
        gpu::host::Download(gpu::host::GetGPUICPMatrixAstarA(), _icpAstarA, sizeof(float) * 36);
        gpu::host::Download(gpu::host::GetGPUICPMatrixAstarB(), _icpAstarB, sizeof(float) * 6);
    }
    void KinectFusion::CudaProjectiveTsdf() {
//        gpu::host::UploadImageData(depthTarget.data, depthTarget.rows, depthTarget.cols);
        gpu::host::Tsdf::ProjectiveTSDFKernel(_weightFactor);
    }
    void KinectFusion::CudaRayCaster() {
        std::cout << "Raycaster running" << std::endl;
        gpu::host::RayCaster::RayCasterKernel();
    }
    //Main Methods V2
    bool KinectFusion::CudaKinectV2(gpu::DepthMap& img, bool initial) {
        CudaUploadImageData(img);
        CudaBilateralFilterAndResize();
        bool icpComplete = true, integrationCheckPass = false, pointcloudExtractionCheckPass = false;
        if(!initial) {
            icpComplete = CudaIterativeClosestPoint(integrationCheckPass, pointcloudExtractionCheckPass);
        }
        if (icpComplete) {
            CudaCheckVolumeBounds();
            CudaProjectiveTsdf();
            CudaRayCaster();
        } else {

        }
        return icpComplete;
    }
    bool KinectFusion::CudaKinectV2(gpu::DepthMap& img, gpu::Image& colourImg, cv::Mat_<double>& depth, cv::Mat& rgb, bool initial, bool forceIntegrationNow, bool extractPointCloudNow) {
        bool time = false;
        std::clock_t s, e;
        s = clock();
        CudaUploadImageData(img);
        e = clock();
        if(time) {
            std::cout << "Image Upload " << (double(e - s) / CLOCKS_PER_SEC) << std::endl;
        }
        s = clock();
        CudaUploadColourImageData(colourImg);
        e = clock();
        if(time) {
            std::cout << "Image Upload " << (double(e - s) / CLOCKS_PER_SEC) << std::endl;
        }
        s = clock();
        CudaBilateralFilterAndResize();
        e = clock();
        if(time) {
            std::cout << "Bilat filter " << (double(e - s) / CLOCKS_PER_SEC) << std::endl;
        }
        bool icpPassed = true;
        bool integrationCheckPassed = false;
        bool extractPointCloudNowMetricPassed = false;
        if(!initial) {
//            s = clock();
            if(_featureBasedICPEnabled) {
                icpPassed = FeatureBasedICP(rgb, depth, integrationCheckPassed, extractPointCloudNowMetricPassed);
            } else {
                icpPassed = CudaIterativeClosestPoint(integrationCheckPassed, extractPointCloudNowMetricPassed);
            }
//            integrationCheckPassed = false;
            if(icpPassed) {
                CudaCheckVolumeBounds();
                if(_useRayCasterAsPointCloudGenerator) {
                    if(extractPointCloudNow) {
                        std::cout << "\033[35mExtracting PointCloud (forced)\033[39m" << std::endl;
                        _poseGraph.AddNode(_rotationMatrix, _translation, _voxelCenter, GeneratePointCloud(), _icpSourceVertexMap, _icpSourceNormalMap, _colourMap, gpu::host::GetImageRows(), gpu::host::GetImageCols());
                    } else if (extractPointCloudNowMetricPassed) {
                        std::cout << "\033[35mExtracting PointCloud (Metric)\033[39m" << std::endl;
                        _poseGraph.AddNode(_rotationMatrix, _translation, _voxelCenter, GeneratePointCloud(), _icpSourceVertexMap, _icpSourceNormalMap, _colourMap, gpu::host::GetImageRows(), gpu::host::GetImageCols());
                    }
                }
//                if(!CudaCheckVolumeBounds()) {
//
//                } else {
//                    if(_useRayCasterAsPointCloudGenerator) {
//                       std::cout << "\033[36mExtracting PointCloud\033[39m" << std::endl;
//                        _poseGraph.AddNode(_rotationMatrix, _translation, _voxelCenter, GeneratePointCloud(), _icpSourceVertexMap, _icpSourceNormalMap, _colourMap, gpu::host::GetImageRows(), gpu::host::GetImageCols());
//                    }
//                }
            }
//            exit(0);
            if(icpPassed && (integrationCheckPassed || forceIntegrationNow)) {
//                std::cout << "It is going to project" << std::endl;
//                exit(0);
                if(integrationCheckPassed) {
                    std::cout << "\033[36mIntegrating DepthMap\033[39m" << std::endl;
                } else if(forceIntegrationNow){
                    std::cout << "\033[35mIntegrating DepthMap (forced)\033[39m" << std::endl;
                }
                s = clock();
                CudaProjectiveTsdf();
                e = clock();
                if(time) {
                    std::cout << "Projective Tsdf: " << (double(e - s) / CLOCKS_PER_SEC) << std::endl;
                }
            }
//            e = clock();
//            std::cout << "ICP " << (double(e - s) / CLOCKS_PER_SEC) << std::endl;
        } else {
//            icpComplete = false;
            s = clock();
            CudaProjectiveTsdf();
            e = clock();
            if(time) {
                std::cout << "Projective Tsdf: " << (double(e - s) / CLOCKS_PER_SEC) << std::endl;
            }

        }
        s = clock();
        CudaRayCaster();
        e = clock();
        if(initial && _useRayCasterAsPointCloudGenerator) {
            std::cout << "\033[35mExtracting PointCloud (Initial)\033[39m" << std::endl;
            _poseGraph.AddNode(_rotationMatrix, _translation, _voxelCenter, GeneratePointCloud(), _icpSourceVertexMap, _icpSourceNormalMap, _colourMap, gpu::host::GetImageRows(), gpu::host::GetImageCols());
        }
        if(time) {
            std::cout << "Raycaster: " << (double(e - s) / CLOCKS_PER_SEC) << std::endl;
        }
//        //find black pixels in raycaster
//        int x, y;
//        float min = 10000;
//        for(x = 200; x < gpu::host::GetImageCols() - 200; x++) {
//            for(y = 150; y < gpu::host::GetImageRows() - 150; y++) {
//                float x1 =_icpSourceVertexMap[gpu::device::Index2D(x, y, gpu::host::GetImageCols())].x;
//                float y1 =_icpSourceVertexMap[gpu::device::Index2D(x, y, gpu::host::GetImageCols())].y;
//                float z1 =_icpSourceVertexMap[gpu::device::Index2D(x, y, gpu::host::GetImageCols())].z;
//                if(std::isnan(z1)) {
////                    std::cout << "Blank Pixel: " << x << ", " << y << std::endl;
//                }
//            }
//        }
//        std::cout << "Minimum found: " << min << std::endl;
//        CudaSaveRayCaster(100);
//        std::cout << "Exiting" << std::endl;
//        exit(0);
//      TODO how it should be done uncomment
//        if (icpComplete) {
////            CudaCheckVolumeBounds();
////            s = clock();
////            CudaProjectiveTsdf();
////            e = clock();
////            std::cout << "Tsdf " << (double(e - s) / CLOCKS_PER_SEC) << std::endl;
////            s = clock();
//            CudaRayCaster();
////            e = clock();
////            std::cout << "Raycast " << (double(e - s) / CLOCKS_PER_SEC) << std::endl;
//        } else {
//
//        }
        return icpPassed;
    }
    bool KinectFusion::CudaKinectV2Debug(gpu::DepthMap& img, gpu::Image& colourImg, gpu::float3& translation, gpu::matrix33& rotation,  bool& proc, bool initial) {

        CudaUploadImageData(img);
        CudaUploadColourImageData(colourImg);

        CudaBilateralFilterAndResize();
        _rotationMatrix = rotation;
        gpu::matrix33 inverse = _rotationMatrix.transpose();
        _translation = translation;
        //GPU swap;
        gpu::host::UploadTransformation(_rotationMatrix, inverse, _translation, gpu::host::GetGPURotation(), gpu::host::GetGPUInverseRotation(), gpu::host::GetGPUTranslation());


        proc = CudaCheckVolumeBounds();
//        if(initial) {
//        if(iteration == 0 ) {
            CudaProjectiveTsdf();
//        }
//        }
        if(!initial) {
            CudaRayCaster();
        }
        iteration++;
        return true;
    }
    void KinectFusion::CudaKinectV2Finish() {

        if(_useRayCasterAsPointCloudGenerator) {
//            pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
//            GeneratePointCloudUsingRayCaster(pointCloud);
//            viewPointCloud(pointCloud);
//            _globalPointCloud += (*pointCloud.get());
            _poseGraph.AddNode(_rotationMatrix, _translation, _voxelCenter, GeneratePointCloud(), _icpSourceVertexMap, _icpSourceNormalMap, _colourMap, gpu::host::GetImageRows(), gpu::host::GetImageCols());
            _globalPointCloud = _poseGraph.BuildPointCloud();
        } else {
            //Raycast the leftover volume
            int i, iterations, stepSize;
            gpu::int3 start, end;
            gpu::int3 volumeSize = _voxelGridSize;
            if(volumeSize.x != volumeSize.y || volumeSize.y != volumeSize.z) {
                std::cerr << " Volume Sizes arent consistant throughout the tsdf for computing Tsdf raycast remainder" << std::endl;
                exit(0);
            }
            stepSize = (int) (_pointIndexGridSize.z * 0.7);
            iterations = (volumeSize.x / stepSize) + 1;
//            iterations = 1;
            std::cout << iterations << std::endl;
            std::cout << (iterations * _pointIndexGridSize.z) << std::endl;
            pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
            pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloudPtr;

            //temp
//            start.z = 484; end.z = 484;
            for(i = 0 ; i < iterations; i++) {
                end.z += stepSize;
                end.z = std::min(end.z, _voxelGridSize.z);
                std::cout << "X: " << start.x << " " << end.x << std::endl;
                std::cout << "Y: " << start.y << " " << end.y << std::endl;
                std::cout << "Z: " << start.z << " " << end.z << std::endl;
                CudaRayCastTsdfSection(start, end);
                CudaDownloadRayCastTsdfSection(true);






//                pointCloudPtr = GeneratePointCloud();
//                *(pointCloud.get()) += *(pointCloudPtr.get());
                //
                start = end;
                _poseGraph.AddNode(_rotationMatrix, _translation, _voxelCenter, GeneratePointCloud(), _icpSourceVertexMap, _icpSourceNormalMap, _colourMap, gpu::host::GetImageRows(), gpu::host::GetImageCols());
            }
            _globalPointCloud = _poseGraph.BuildPointCloud();

//            _poseGraph.add(_translation, _rotationMatrix, pointCloud);
        }
        //Build point clouds
//        _poseGraph.Optimize();
//        _poseGraph.viewPointCloud();
        //Loop closure
        int i;
        //TODO fix below
//        for(i = 0; i < _poseGraph._pointCloud.size(); i++) {
//            _globalPointCloud += *((_poseGraph._pointCloud[i]).get());
//            viewPointCloud(_poseGraph._pointCloud[i]);
//        }

//        pcl::io::saveVTKFile("test.vtk", _globalPointClou);
    }
    bool KinectFusion::CudaCheckVolumeBounds() {
//        if(counter == 1) {
//            return false;
//        }
        gpu::float3& cameraPos = _translation;
        gpu::int3& volumeCenter = _voxelCenter;

        gpu::float3 tempF = cameraPos/_voxelCellSize;
        gpu::int3 u((int) tempF.x, (int) tempF.y, (int) tempF.z);
        gpu::int3 movedVoxels =  u - volumeCenter;
//        std::cout << _voxelThreshold.x << " " << _voxelThreshold.y << " " << _voxelThreshold.z << std::endl;
        if(std::abs(movedVoxels.x) >= _voxelThreshold.x ||
           std::abs(movedVoxels.y) >= _voxelThreshold.y ||
           std::abs(movedVoxels.z) >= _voxelThreshold.z) {

            std::cout << "\x1B[31mVolume Moving Triggered" << std::endl << "Triggered " << counter << " times \x1B[0m" << std::endl;
            //'Move' volume
            //std::cout << "TRIGGERED" << std::endl;
            //std::cout << "Moved Voxels: " << movedVoxels.x << " " << movedVoxels.y << " " << movedVoxels.z << std::endl;
            //gpu::float3 tPrime = cameraPos - _voxelCellSize * u; // TODO I'm not going to use this, since I take care of the indexing based on center volume
            //std::cout << "New of Camera: " << tPrime.x << " " << tPrime.y << " " << tPrime.z << std::endl;
            // Generate start and end points

            gpu::int3 start;
            gpu::int3 end = movedVoxels;
            if(movedVoxels.x < 0) {
                start.x = _voxelGridSize.x + movedVoxels.x;
                end.x = _voxelGridSize.x;
            } if (movedVoxels.y < 0) {
                start.y = _voxelGridSize.y + movedVoxels.y;
                end.y = _voxelGridSize.y;
            } if (movedVoxels.z < 0) {
                start.z = _voxelGridSize.z + movedVoxels.z;
                end.z = _voxelGridSize.z;
            }
            CudaRayCastTsdfSection(start, end);
            CudaDownloadRayCastTsdfSection(true);
            if(!_useRayCasterAsPointCloudGenerator) {
                pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointcloud = GeneratePointCloud();
                if(pointcloud->points.size() != 0) {
                    _poseGraph.AddNode(_rotationMatrix, _translation, _voxelCenter, pointcloud, _icpSourceVertexMap, _icpSourceNormalMap, _colourMap, gpu::host::GetImageRows(), gpu::host::GetImageCols());
                }
            }

            CudaResetTsdfSection(start, end);
            volumeCenter = volumeCenter + movedVoxels;
            cudaMemcpy(gpu::host::GetGPUVoxelCenter(), &volumeCenter, sizeof(gpu::int3), cudaMemcpyHostToDevice);
            counter++;
            return true;
        }
        return false;
    }
    void KinectFusion::CudaUpdateVolumeBoundsAndMoveVolume() {

    }
    void KinectFusion::CudaResetTsdfSection(gpu::int3 &start, gpu::int3& end) {
        gpu::host::Tsdf::ResetVolume(start, end);
    }
    void KinectFusion::CudaRayCastTsdfSection(gpu::int3 &start, gpu::int3& end) {
        gpu::host::RayCaster::RayCasterKernel(start, end);
    }
//    /*
//     * This will raycast in the z dir only
//     */
//    void KinectFusion::CudaRayCastTsdfRemainder() {
//        int i, iterations, stepSize;
//        gpu::int3 start, end;
//        gpu::int3 volumeSize = _voxelGridSize;
//        if(volumeSize.x != volumeSize.y || volumeSize.y != volumeSize.z) {
//            std::cerr << " Volume Sizes arent consistant throughout the tsdf for computing Tsdf raycast remainder" << std::endl;
//            exit(0);
//        }
//        stepSize = (int) (_pointIndexGridSize.z * 0.7);
//        iterations = (volumeSize.x / stepSize) + 1;
//        std::cout << iterations << std::endl;
//        std::cout << (iterations * _pointIndexGridSize.z) << std::endl;
//        for(i = 0 ; i < iterations; i++) {
//            end.z += stepSize;
//            end.z = std::min(end.z, _voxelGridSize.z);
//            std::cout << "X: " << start.x << " " << end.x << std::endl;
//            std::cout << "Y: " << start.y << " " << end.y << std::endl;
//            std::cout << "Z: " << start.z << " " << end.z << std::endl;
//            gpu::host::RayCaster::RayCasterKernel(start, end);
//            start = end;
//        }
//    }
    void KinectFusion::CudaDownloadRayCastTsdfSection(bool reset) {
        gpu::host::Download(gpu::host::GetGPUPointIndex(), _gpuPointIndexVolume, sizeof(int) * _pointIndexGridSize.x * _pointIndexGridSize.y * _pointIndexGridSize.z);
        gpu::host::Download(gpu::host::GetGPUPointIndexColour(), _gpuPointIndexColourVolume, sizeof(int) * _pointIndexGridSize.x * _pointIndexGridSize.y * _pointIndexGridSize.z);
        gpu::host::Download(gpu::host::GetGPUPointIndexNormal(), _gpuPointIndexNormalVolume, sizeof(gpu::float3) * _pointIndexGridSize.x * _pointIndexGridSize.y * _pointIndexGridSize.z);
        if (reset) {
            CudaResetPointIndexVolume();
        }
    }
    void KinectFusion::CudaResetPointIndexVolume() {
        std::cout << "Reseting Axes extraction gpu volume " << std::endl;
        gpu::host::RayCaster::ResetPointIndexVolume();
    }
    void KinectFusion::viewPointCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr ptr) {
        if(ptr->points.size() == 0 ) {
            std::cerr << " Can't view point cloud, size is 0" << std::endl;
            return;
        }
        pcl::visualization::PCLVisualizer viewer;
        viewer.setBackgroundColor (0, 0, 0);
        viewer.addPointCloud<pcl::PointXYZRGBNormal> (ptr, "sample cloud");
        viewer.setPointCloudRenderingProperties (pcl::visualization::PCL_VISUALIZER_POINT_SIZE, 1, "sample cloud");
//        viewer.addPointCloudNormals<pcl::PointXYZRGBNormal>(ptr, 1);
//        viewer.addCoordinateSystem (1.0);
        viewer.initCameraParameters ();
        std::cout << "Stopped ? " << viewer.wasStopped() << std::endl;
        while (!viewer.wasStopped ()) {
            viewer.spinOnce (100);
        }
    }
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr KinectFusion::GeneratePointCloud(bool setBasicColour) {
        pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud(new pcl::PointCloud<pcl::PointXYZRGBNormal>);
        clock_t startTime = clock();

        if(_useRayCasterAsPointCloudGenerator) {
            GeneratePointCloudUsingRayCaster(pointCloud, setBasicColour);
        } else {
            GeneratePointCloudUsingAxesRayCaster(pointCloud, setBasicColour);
        }


        clock_t endTime = clock();
        std::cout << "Time taken to generate points: " << double(endTime - startTime) / CLOCKS_PER_SEC << " Found " << pointCloud.get()->points.size() << std::endl;
        pointCloud.get()->width = (unsigned int) pointCloud.get()->points.size();
        pointCloud.get()->height = 1;
//        viewPointCloud(pointCloud);
//        _pointCloud += *(pointCloud.get());
        return pointCloud;
    }

    void KinectFusion::GeneratePointCloudUsingRayCaster(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr& pointCloud, bool basicColour) {
        int i;
        int size = gpu::host::GetImageRows() * gpu::host::GetImageCols();
        std::cout << gpu::host::GetImageCols() << " " << gpu::host::GetImageRows() << " " << size << std::endl;
        CudaDownloadRayCaster();
        //TODO Parallel exe omp
        for(i = 0; i < size; i++) {
            gpu::float3 p = _icpSourceVertexMap[i];
            gpu::float3 n = _icpSourceNormalMap[i];
            if(std::isnan(p.x)) {
                continue;
            }
            pcl::PointXYZRGBNormal pointXYZRGBNormal;
            uchar* colourMap = _colourMap + i * gpu::host::GetImageChannels();
            pointXYZRGBNormal.x = p.x;
            pointXYZRGBNormal.y = p.y;
            pointXYZRGBNormal.z = p.z;
            //TODO uncomment this colour assignment
            pointXYZRGBNormal.r = colourMap[0];
            pointXYZRGBNormal.g = colourMap[1];
            pointXYZRGBNormal.b = colourMap[2];
            if(basicColour) {
                pointXYZRGBNormal.r = 244;
                pointXYZRGBNormal.g = 0;
                pointXYZRGBNormal.b = 0;
            }
            gpu::float3 globalN = _rotationMatrix * n;
            //Normal Assignment
            pointXYZRGBNormal.normal_x = n.x;
            pointXYZRGBNormal.normal_y = n.y;
            pointXYZRGBNormal.normal_z = n.z;
            pointXYZRGBNormal.curvature = 0;

            pointXYZRGBNormal.normal_x = globalN.x;
            pointXYZRGBNormal.normal_y = globalN.y;
            pointXYZRGBNormal.normal_z = globalN.z;
            pointXYZRGBNormal.curvature = 0;
//                        pointXYZRGB.r = 255;
//                        pointXYZRGB.g = 0;
//                        pointXYZRGB.b = 0;
            pointCloud.get()->points.push_back(pointXYZRGBNormal);
        }
    }
    void KinectFusion::GeneratePointCloudUsingAxesRayCaster(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr& pointCloud, bool basicColour) {

        //396 440
        int x, y, z, nextIndex;
        for(x = 0; x < _pointIndexGridSize.x; x++) {
            for(y = 0; y < _pointIndexGridSize.y; y++) {
                int gx, gy, gz;
                for(nextIndex = 0; nextIndex < _pointIndexGridSize.z; nextIndex++) {
                    gpu::device::LocalVoxelToGlobalVoxel(x, y, nextIndex, &_voxelCenter, &_voxelGridSize, &_voxelCellSize, gx, gy, gz);

                    int index = gpu::device::Index3D(gx - _voxelCenter.x, gy - _voxelCenter.y, nextIndex, &_voxelCenter, &_pointIndexGridSize);
//                if(index >= maxIndex) {
//                    std::cout << "ERROR" << std::endl;
//                }
                    gz = _gpuPointIndexVolume[index];
                    if( gz == -2147483648) {
                        break;
                    } else {
                        //Generate point at x, y, z[]
                        gpu::float3 p = gpu::device::VoxelToWorld(gx, gy, gz, &_voxelGridSize, &_voxelCenter, &_voxelCellSize);
                        gpu::float3 normal = _gpuPointIndexNormalVolume[index];
//                        std::cout << "Generating Point: " << p.x << " " << p.y << " " << p.z <<  std::endl <<
//                                "From: " << gx << " " << gy << " " << gz << std::endl;
                        pcl::PointXYZRGBNormal pointXYZRGBNormal;
                        uchar* colourPtr = (uchar*) &_gpuPointIndexColourVolume[index];
                        pointXYZRGBNormal.x = p.x;
                        pointXYZRGBNormal.y = p.y;
                        pointXYZRGBNormal.z = p.z;

                        pointXYZRGBNormal.normal_x = normal.x;
                        pointXYZRGBNormal.normal_y = normal.y;
                        pointXYZRGBNormal.normal_z = normal.z;
                        //TODO uncomment this colour assignment
                        pointXYZRGBNormal.r = colourPtr[1];
                        pointXYZRGBNormal.g = colourPtr[2];
                        pointXYZRGBNormal.b = colourPtr[3];
                        if(colourPtr[1] == 0 && colourPtr[2] == 0 && colourPtr[3] == 0) {
                            pointXYZRGBNormal.r = 255;
                            pointXYZRGBNormal.g = 255;
                            pointXYZRGBNormal.b = 255;
                        }
//                        pointXYZRGB.g = 0;
//                        pointXYZRGB.b = 0;
                        pointCloud.get()->points.push_back(pointXYZRGBNormal);
                    }
                }
            }
        }
        std::cout << "Point generated " << pointCloud.get()->points.size() << std::endl;
    }
    //Upload Image data
    void KinectFusion::CudaUploadImageData(gpu::DepthMap& depthTarget) {
        gpu::host::UploadImageData(depthTarget.data, depthTarget.rows, depthTarget.cols);
    }
    void KinectFusion::CudaUploadColourImageData(gpu::Image& image) {
        gpu::host::UploadColourImageData(image.data, image.rows, image.cols, image.channels);
    }
    //Downloads for Testing
    void KinectFusion::CudaDownloadBilateralFilterAndResize() {
        int size = 0, i, rows = gpu::host::GetImageRows(), cols = gpu::host::GetImageCols();
        for(i = 0; i < gpu::host::GetICPLevels(); i++)
            size += (rows * pow(2, -i)) * (cols * pow(2, -i));

//        printf("size: %d\n", size);
//        gpu::host::Download(gpu::host::GetGPUTargetImages(), bilateralFilterTargetPtr, size * sizeof(float));
        gpu::host::Download(gpu::host::GetGPUTargetImages(), bilateralFilterSourcePtr, size * sizeof(float));
    }
    void KinectFusion::CudaDownloadIterativeClosestPointVertexAndNormalMap() {
        int size = 0, i, rows = gpu::host::GetImageRows(), cols = gpu::host::GetImageCols();
        for(i = 0; i < gpu::host::GetICPLevels(); i++)
            size += (rows * pow(2, -i)) * (cols * pow(2, -i));
        gpu::host::Download(gpu::host::GetGPUSourceVertexMap(), _icpSourceVertexMap, size * sizeof(float3));
        gpu::host::Download(gpu::host::GetGPUSourceNormalMap(), _icpSourceNormalMap, size * sizeof(float3));
        gpu::host::Download(gpu::host::GetGPULocalTargetVertexMap(), _icpLocalTargetVertexMap, size * sizeof(float3));
        gpu::host::Download(gpu::host::GetGPULocalTargetNormalMap(), _icpLocalTargetNormalMap, size * sizeof(float3));
//        gpu::host::Download(gpu::host::GetGPUGlobalTargetVertexMap(), _icpGlobalTargetVertexMap, size * sizeof(float3));
//        gpu::host::Download(gpu::host::GetGPUGlobalTargetNormalMap(), _icpGlobalTargetNormalMap, size * sizeof(float3));
    }
    void KinectFusion::CudaDownloadTsdf() {
        unsigned int size = (unsigned int)(_voxelGridSize.x * _voxelGridSize.y * _voxelGridSize.z) * sizeof(int);
        gpu::host::Download(gpu::host::GetGPUTsdfVolume(), _gpuVolumePtr, size);
    }
    /**
     * Downloads from level 0 to level <levels>
     */
    void KinectFusion::CudaDownloadRayCaster(int levels) {
        std::cout << "DOWNLOADING RAYCASTER LEVEL " << levels << std::endl;
//        return;
        int size = 0, i, rows = gpu::host::GetImageRows(), cols = gpu::host::GetImageCols();
        if(levels < 0) {
            levels = gpu::host::GetICPLevels() - 1;
        }
        for(i = 0; i < levels + 1; i++)
            size += (rows * pow(2, -i)) * (cols * pow(2, -i));
        gpu::host::Download(gpu::host::GetGPUSourceVertexMap(), _icpSourceVertexMap, size * sizeof(float3));
        gpu::host::Download(gpu::host::GetGPUSourceNormalMap(), _icpSourceNormalMap, size * sizeof(float3));
        gpu::host::Download(gpu::host::GetGPUSourceColourMap(), _colourMap, size * gpu::host::GetImageChannels() * sizeof(uchar));
//        CudaSaveRayCaster();
    }
    //Save for Testing
    void KinectFusion::CudaSaveBilateralFilterAndResize() {
        int size = 0, i, r = gpu::host::GetImageRows(), c = gpu::host::GetImageCols();
        int skip = 0, imgNum;
        int x, y;
        for(imgNum = 0; imgNum < gpu::host::GetICPLevels(); imgNum++) {
            cv::Mat_<ushort> img(r, c);
            for(x = 0; x < c; x++) {
                for(y = 0; y < r; y++) {
                    img(y, x) = (ushort)((bilateralFilterTargetPtr[skip + y * c + x]) * 5);
//                    assert(y * c + x < r * c);
                }
            }
            skip += r * c;
            r /= 2.;
            c /= 2.;
            std::ostringstream oss;
            oss << "BilateralFilter";
            oss << imgNum ;
            oss << ".png";
            std::cout << oss.str() << std::endl;
            cv::imwrite(oss.str(), img);
//        cv::imwrite("BilateralFilter.png", img);
//            cv::imshow("Cuda BilateralFilter", img);
        }
    }
    void KinectFusion::CudaSaveIterativeClosestPointVertexAndNormalMap() {
        int imgNum, icpLevels = gpu::host::GetICPLevels(), skip = 0, rows = gpu::host::GetImageRows(), cols = gpu::host::GetImageRows(), x, y;
        for (imgNum = 0; imgNum < icpLevels; imgNum++) {
            std::cout << "CHECKING LEVEL " << imgNum << std::endl;
            gpu::matrix33 rotationMatrix(0, 0, 1, 0, 1, 0, -1, 0, 0), inverseRotation = rotationMatrix.transpose();
            gpu::float3 translation(2.56, 2.56, 2.56);

            float scale = powf(2, -imgNum);
            gpu::float3 f;
            gpu::float3 c;
            f.x = _cameraMatrix(0, 0) * scale;
            f.y = _cameraMatrix(1, 1) * scale;
            c.x = _cameraMatrix(0, 2) * scale;
            c.y = _cameraMatrix(1, 2) * scale;

            gpu::DepthMap bilateralFilteredMap;
            bilateralFilteredMap.rows = rows;
            bilateralFilteredMap.cols = cols;
            bilateralFilteredMap.data = &(bilateralFilterTargetPtr[skip]);
            int blkX = cols;
            int blkY = rows;
            int startX = 0;
            int startY = 0;
            for (x = startX; x < startX + blkX; x++) {
                for (y = startY; y < startY + blkY; y++) {
                    int index = y * cols + x;
                    gpu::float3 v3 = _icpLocalTargetVertexMap[index  + skip];


                }
            }

            skip += rows * cols;
            rows /= 2;
            cols /= 2;
//        return;
        }
    }
    void KinectFusion::CudaTsdfVisualizer(std::string filename) {
        std::cout << "Visualizer" << std::endl;
        int i, j, z;
        std::cout << _voxelGridSize.z << " " << _voxelGridSize.y << " " << _voxelGridSize.x << std::endl;
        for ( z = 0; z < (int)_voxelGridSize.z; z++) {
//        for ( z = 100; z < 101; z++) {
            cv::Mat img2((int)_voxelGridSize.x, (int)_voxelGridSize.y, CV_8UC1,cv::Scalar(0));
            for (i = 0; i < (int)_voxelGridSize.x; i++) {
                for (j = 0; j < (int) _voxelGridSize.y; j++) {
                    int test = (*(_gpuVolumePtr + (int) (i*_voxelGridSize.y*_voxelGridSize.z + j*_voxelGridSize.z + z)));
//                    float v = 0;//CudaReadTsdf(i, j, z);
                    short* vv = (short*)(_gpuVolumePtr + i + j * (_voxelGridSize.x) + z * (_voxelGridSize.x * _voxelGridSize.y));
                    float v = (float) vv[1];
//                    if ( test != 1 ) {
//                        std::cout << "Error "<< i << " " << j << " " << z << std::endl;
//                    }
                    if (v < 0) {
//                        printf("WUT\n");
//                        printf("Visualizer negative %f %d, %d evaluation %d\n",v, i, j,(int)(_gpuVoxelGridSize.y * _gpuVoxelGridSize.x*z + _gpuVoxelGridSize.x*j + i));
                        v = 0;
                    } else {
                        v = 255;
                    }
//                    v += 1;
//                    v *=128;
                    img2.at<uchar>(i, j) = (uchar) v;

                }
            }
            std::ostringstream oss2;
            oss2 << filename;
            oss2 << z;
            oss2 << ".png";
            cv::imwrite(oss2.str(), img2);
        }
    }

    void KinectFusion::CudaSaveRayCaster(int iteration) {
        int i = gpu::host::GetICPLevels(), r = gpu::host::GetImageRows(), c = gpu::host::GetImageCols(), x, y, skip = 0;
        float scale = 1;
        gpu::matrix33 inverseRotation;
        gpu::float3 translation;
        gpu::host::Download(gpu::host::GetGPUInverseRotation(), &inverseRotation, sizeof(gpu::matrix33));
        gpu::host::Download(gpu::host::GetGPUTranslation(), &translation, sizeof(gpu::float3));
        float* image  = new float[r * c];
        gpu::host::Download(gpu::host::GetGPUDepthMap(), image, sizeof(float) * r * c);
        for(i = 0; i < gpu::host::GetICPLevels(); i++) {
            scale = powf(2, -i);
            cv::Mat_<ushort> generatedImage(r, c, (ushort)0);
            cv::Mat_<ushort> generatedImage2(r, c, (ushort)0);
            cv::Mat_<ushort> filteredImage(r, c, (ushort)0);

            for( x = 0 ; x < c; x++) {
                for ( y= 0 ; y < r; y++) {

                    filteredImage(y, x) = (ushort) (5 * image[y * c + x]);
                    gpu::float3 eq = _icpSourceVertexMap[y * c + x + skip];
                    gpu::float3 localPoint = inverseRotation * (eq - translation);

                    if ( localPoint.z <= 0 ) {
                        continue;
                    }
                    generatedImage2(y, x) = (ushort)(5 * (int)(localPoint.z * 1000.));

                    int2 p;
                    p.x = (int)((localPoint.x * (_cameraMatrix(0, 0) * scale))/localPoint.z + (_cameraMatrix(0, 2) * scale));
                    p.y = (int)((localPoint.y * (_cameraMatrix(1, 1) * scale))/localPoint.z + (_cameraMatrix(1, 2) * scale));
//                    if(x == 0 && y == 0) {
//                        std::cout << eq.x << " " << eq.y << " " << eq.z << std::endl;
//                        std::cout << localPoint.x << " " << localPoint.y << " " << localPoint.z << std::endl;
//                        std::cout << p.x << " " << p.y << std::endl;
//                        exit(0);
//                    }
                    if( p.y >= r || p.y < 0 || p.x >= c || p.x < 0) {
                        continue;
                    }
//                    std::cout << "Before" << std::endl;
                    generatedImage(p.y, p.x) = (ushort)(5 * ((localPoint.z * 1000.)));
//                    generatedImage(y, x) = (ushort)(5 * ((int)(localPoint.z * 1000.)));
//                    std::cout << "After" << std::endl;
                }
            }
            skip += r * c;
            r /= 2;
            c /= 2;

            //    std::cout << counter << std::endl;
            std::ostringstream oss;
            oss << "RaycastImageIteration";
            oss << iteration;
            oss << "ALevel";
            oss << i;
            oss << ".png";
            std::ostringstream osss;
            osss << "RaycastImageIteration";
            osss << iteration;
            osss << "BLevel";
            osss << i;
            osss << ".png";
            std::ostringstream os;
            os << "RaycastImageIteration";
            os << iteration;
            os << "CLevel";
            os << i;
            os << ".png";
//            cv::imwrite(oss.str(), generatedImage);
//            cv::imwrite(os.str(), filteredImage);
            cv::imwrite(osss.str(), generatedImage2);
        }

    }
    void KinectFusion::CudaSaveRayCasterColour(int i) {
        int x, y;
        cv::Mat img(gpu::host::GetImageRows(), gpu::host::GetImageCols(), CV_8UC3);
        for(x = 0; x < gpu::host::GetImageCols(); x++) {
            for(y = 0; y < gpu::host::GetImageRows(); y++) {
                img.data[gpu::device::Index2D(x, y, img.cols) * 3 + 0] = _colourMap[gpu::device::Index2D(x, y, img.cols) * 3 + 0];
                img.data[gpu::device::Index2D(x, y, img.cols) * 3 + 1] = _colourMap[gpu::device::Index2D(x, y, img.cols) * 3 + 1];
                img.data[gpu::device::Index2D(x, y, img.cols) * 3 + 2] = _colourMap[gpu::device::Index2D(x, y, img.cols) * 3 + 2];
            }
        }
        std::ostringstream oss;
        oss << "RaycastColourImage";
        oss << i;
        oss << ".png";
        cv::imwrite(oss.str(), img);
    }

    void KinectFusion::CudaRayCastPosition(std::vector<gpu::matrix33>& rotations, std::vector<gpu::float3>& translations, bool save) {
        int i;
        assert(rotations.size() == translations.size());
        for(i = 0; i < rotations.size(); i++) {
            gpu::matrix33 rotationMatrix = rotations[i];
            gpu::matrix33 inverseRotationMatrix = rotationMatrix.transpose();
            gpu::float3 translation = translations[i];
            gpu::host::UploadTransformation(rotationMatrix, inverseRotationMatrix, translation,
                                            gpu::host::GetGPURotation(), gpu::host::GetGPUInverseRotation(), gpu::host::GetGPUTranslation());
            CudaRayCaster();
            CudaDownloadRayCaster();
            if(save) {
                CudaSaveRayCaster(i);
                CudaSaveRayCasterColour(i);
            }
        }

    }
    gpu::float3 KinectFusion::GetLastTranslationVector() {
        return _translation;
    }
    gpu::matrix33 KinectFusion::GetLastRotationMatrix() {
        return _rotationMatrix;
    }

//    void KinectFusion::icptest() {
//        int num = 10;
//        int i, j, counter = 0;
//        Eigen::Matrix<float, Eigen::Dynamic, 6> A;
//        A.resize(num, Eigen::NoChange);
//        for(i = 0; i < num; i++) {
//            Eigen::Matrix<float, 1, 6> subA;
//            for(j = 0 ;j < 6;j++) {
//                subA(j) = counter;
//                counter++;
//            }
//            counter++;
//            A.block(i, 0, 1, 6) = subA;
//        }
//        Eigen::Matrix<float, 6, 6> AtA = A.transpose() * A;
////        return;
//        gpu::host::IterativeClosestPoint::equationTests(num);
//        std::cout << AtA << std::endl;
//        std::cout << "GPU " << std::endl;
//        float* gpuAtA = new float[36];
//        for(i = 0 ;i < 36; i ++) {
//            gpuAtA[i] = 0;
//        }
//        gpu::host::Download(gpu::host::GetGPUICPMatrixAstarA(), gpuAtA, sizeof(float) * 36);
//        for(i = 0; i < 6;i++) {
//            for(j = 0;j < 6; j++) {
//                std::cout << gpuAtA[i + j * 6 ] << " ";
//            }
//            std::cout << std::endl;
//        }
//        // AstarB
//
//        float *gpuAstarB = new float[6];
//        for(i = 0; i < 6; i++) {
//            gpuAstarB[i] = 0;
//        }
//        gpu::host::Download(gpu::host::GetGPUICPMatrixAstarB(), gpuAstarB, sizeof(float) * 6);
//        Eigen::Matrix<float, 6, Eigen::Dynamic> At = A.transpose();
//        Eigen::Matrix<float, Eigen::Dynamic, 1> b;
//        b.resize(num, Eigen::NoChange);
//        counter = 6;
//        for(i = 0; i < num; i++) {
//            b(i) = counter;
//            counter += 7;
//        }
//        Eigen::Matrix<float, 6, 1> AstarB = At * b;
//
//        std::cout << "AstarB Eigen " << std::endl;
//        std::cout << AstarB << std::endl;
//        std::cout << "AstarB GPU" << std::endl;
//        for(i = 0; i < 6; i++) {
//            std::cout << gpuAstarB[i] << std::endl;
//        }
//
//
//        free(gpuAtA);
//        free(gpuAstarB);
//    }

    Eigen::Matrix4d KinectFusion::OpenCVRgbd(cv::Mat &prevRgbRef, cv::Mat &prevDepthRef, cv::Mat &currentRgbRef, cv::Mat & currentDepthRef) {
        cv::Ptr<cv::rgbd::OdometryFrame> frame_prev = cv::Ptr<cv::rgbd::OdometryFrame>(new cv::rgbd::OdometryFrame()),
                frame_curr = cv::Ptr<cv::rgbd::OdometryFrame>(new cv::rgbd::OdometryFrame());
        cv::Ptr<cv::rgbd::Odometry> odometry = cv::rgbd::Odometry::create( "RgbdOdometry");
        cv::Mat cameraMatrix = cv::Mat::eye(3,3,CV_32FC1);
        cameraMatrix.at<float>(0,0) = _K(0, 0);
        cameraMatrix.at<float>(1,1) = _K(1, 1);
        cameraMatrix.at<float>(0,2) = _K(0, 2);
        cameraMatrix.at<float>(1,2) = _K(1, 2);
        odometry->setCameraMatrix(cameraMatrix);

        cv::Mat prevRgb, currentRgb, currentDepth, prevDepth;
        prevRgbRef.copyTo(prevRgb);
        currentRgbRef.copyTo(currentRgb);
        currentDepthRef.copyTo(currentDepth);
        prevDepthRef.copyTo(prevDepth);



        cv::Mat depth_flt;
        currentDepth.convertTo(depth_flt, CV_32FC1, 1.f/5000.f);
//        currentDepth.release();
        depth_flt.setTo(std::numeric_limits<float>::quiet_NaN(), currentDepth == 0);
        currentDepth = depth_flt;
        depth_flt.release();

        prevDepth.convertTo(depth_flt, CV_32FC1, 1.f/5000.f);
//        prevDepth.release();
        depth_flt.setTo(std::numeric_limits<float>::quiet_NaN(), prevDepth == 0);
        prevDepth = depth_flt;
        depth_flt.release();

        cv::Mat currentGray;
        cv::cvtColor(currentRgb, currentGray, cv::COLOR_BGR2GRAY);
        frame_curr->image = currentGray;
        frame_curr->depth = currentDepth;
        cv::Mat prevGray;
        cv::cvtColor(prevRgb, prevGray, cv::COLOR_BGR2GRAY);
        frame_prev->image = prevGray;
        frame_prev->depth = prevDepth;



        cv::Mat Rt;
        bool res = odometry->compute(frame_curr, frame_prev, Rt);

        //deallocation
        if (!frame_prev.empty()) {
            frame_prev->release();
        }
        if (!frame_curr.empty()) {
            frame_curr->release();
        }
        odometry.release();
        prevRgb.release();
        currentRgb.release();

        Eigen::Matrix4d rt;
        cv::cv2eigen(Rt, rt);
//        std::cout << Rt << std::endl << rt << std::endl;
        return rt;
    }


};
