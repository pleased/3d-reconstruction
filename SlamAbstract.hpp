/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/06/10.
//

#ifndef VSLAM_SLAMABSTRACT_HPP
#define VSLAM_SLAMABSTRACT_HPP
#include <eigen3/Eigen/Dense>
#include <opencv2/core/core.hpp>
namespace Slam {
    class SlamAbstract {
    public:
        SlamAbstract();
        void SetCameraMatrix(Eigen::Matrix3d);
        virtual Eigen::Matrix4d ComputeRigidBodyMotion(cv::Mat& rgbSource, cv::Mat& graySource, cv::Mat_<double>& depthSource,
                                    cv::Mat& rgbTarget, cv::Mat& grayTarget, cv::Mat_<double>& depthTarget){ Eigen::Matrix4d a; return a;};

        Eigen::Matrix3d _K;
        Eigen::Matrix4d _K4;
//        pcl::PointCloud<pcl::PointXYZRGB>::Ptr _worldState;
        Eigen::Matrix4d _transformEstimate;
    };
};


#endif //VSLAM_SLAMABSTRACT_HPP
