/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/05/30.
//

#ifndef VSLAM_FILEPARSER_HPP
#define VSLAM_FILEPARSER_HPP
#include <iostream>
#include <fstream>
#include <string>
#include <sstream>
#include <vector>
#include <cmath>
#include <algorithm>
#include <boost/algorithm/string.hpp>
#include <eigen3/Eigen/Dense>
#include "UtilityFunctions.hpp"
namespace IO {
    enum IOType {
        RGB = 0, Depth = 1, IMU = 2, GrouthTruth = 3
    };
    class FileParser {
    public:
        FileParser(std::string& baseDir, std::string& rgbFile, std::string& depthFile, double tol=5.e-2);
        ~FileParser();

        // Usage Functions //
        int ReadData(std::vector<std::string>&);
        void EnableGroundTruth(std::string& groundTruthFile);
        void DisableGroundTruth();
        void EnableIMU(std::string& imuFile);
        void DisableIMU();

        void GetImageTimeAndLocation(std::string&, double&, std::string&);
        void GetGroundTruthTimeAndRotationMatrix(std::string&, double&, Eigen::Matrix4d&);

        //Private Functions
    private:
        int ReadSynchronisedRGBDImages(std::string&, std::string&, double&);
        int ReadSynchronisedGroundTruth(double time, std::string&);
        int ReadSynchronisedIMU(double time, std::string&);

        int ReadRGB(std::string&);
        int ReadDepth(std::string&);
        int ReadGroundTruth(std::string&);
        int ReadIMU(std::string&);
        int PeekLine(std::ifstream&, std::string&);
        void GetTime(std::string& in, double& time);

        void BuildDirectory(std::string&);
        void StripLine(std::string&);

        bool _groundTruthEnabled = false, _imuEnabled = false;
        std::ifstream _rgbFileStream, _depthFileStream, _imuFileStream, _groundTruthFileStream;
        std::string _baseDir, _rgbFileLocation, _depthFileLocation, _imuFileLocation, _groundTruthFileLocation;
        double _tolerance;
    };
};


#endif //VSLAM_FILEPARSER_HPP
