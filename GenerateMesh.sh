if [[ $# -lt 1 ]]
then
    echo "Requires 1 arguments, Filename"
else
    blender -P GenerateTriangleMesh.py -- $1
fi
