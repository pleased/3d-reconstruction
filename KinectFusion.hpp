/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2016/06/10.
//

#ifndef VSLAM_KINECTFUSION_HPP
#define VSLAM_KINECTFUSION_HPP
#include <iostream>
#include <limits>
#include <Eigen/Cholesky>
#include <opencv2/rgbd.hpp>
#include <algorithm>
#include "SlamAbstract.hpp"
#include "UtilityFunctions.hpp"
#include "FeatureBasedICP/FeatureBasedICP.hpp"
#include "KinectFusion/GlobalDevice.hpp"
#include "KinectFusion/DeviceTypes.hpp"
#include "KinectFusion/GlobalHost.hpp"
#include "KinectFusion/BilateralFilter.hpp"
#include "KinectFusion/IterativeClosestPoint.hpp"
#include "KinectFusion/TsdfVolume.hpp"
#include "KinectFusion/RayCaster.hpp"
#include "PoseGraph.hpp"
#include <opencv2/rgbd.hpp>

#include <opencv2/highgui.hpp>
#include <opencv2/calib3d.hpp>
#include <opencv2/imgproc.hpp>
#include <opencv2/core/utility.hpp>
#include <opencv2/core/eigen.hpp>

#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <pcl/visualization/cloud_viewer.h>
#include <pcl/io/ply_io.h>
#include <pcl/io/vtk_io.h>
#include <pcl/io/pcd_io.h>
#include <limits.h>

namespace Slam {
    class KinectFusion : public SlamAbstract {
        struct double3 {
            double x = 0, y = 0, z = 0;
        };
    public:
        ~KinectFusion();
        Eigen::Matrix4d ComputeRigidBodyMotion(cv::Mat& rgbSource, cv::Mat& graySource, cv::Mat_<double>& depthSource,
                                               cv::Mat& rgbTarget, cv::Mat& grayTarget, cv::Mat_<double>& depthTarget);


        Eigen::Matrix4d VisualOdometry(cv::Mat_<double>& depthSource, cv::Mat_<double>& depthTarget);
        // ICP stuff
        // Computes solution to Ax = b
        Eigen::Matrix<double, 6, 1> CholeskyDecomposition(Eigen::Matrix<double, Eigen::Dynamic, 6>& A, Eigen::Matrix<double, Eigen::Dynamic, 1>& b);


        // ICP vars

        //ICP magnitudes
        float _rotationMag = 0.05f,_translationMag = 0.04f, _integration_metric_threshold = 0.005, _pointcloud_extraction_metric_threshold = 0.4;

        //
//        double _normalThreshold = 0.1, _vertexThreshold = 0.1;

        //Projective TSDF Integration Methods

        //Local Vars
        gpu::int3 _voxelGridSize;
        gpu::int3 _voxelCenter;
        gpu::float3 _voxelCellSize;
        float _volumeSize;
        gpu::matrix33 _cameraMatrix;
        gpu::matrix33 _rotationMatrix;
        gpu::float3 _translation;
        gpu::matrix33 _lastIntegrationRotationMatrix;
        gpu::float3 _lastIntegrationTranslation;
        gpu::matrix33 _lastPointCloudExtractionRotationMatrix;
        gpu::float3 _lastPointCloudExtractionTranslation;
        float _icpAstarB[6];// 6 by 1
        float _icpAstarA[36];// 6 by 6
        bool _enableCudaTesting = true;

        //KinectV2 Local Vars
        gpu::int3 _voxelThreshold;
        gpu::int3 _pointIndexGridSize;
        int* _gpuPointIndexVolume;
        int* _gpuPointIndexColourVolume;
        gpu::float3* _gpuPointIndexNormalVolume;
        //Point Cloud Stuff
        pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr _globalPointCloud;
        bool _useRayCasterAsPointCloudGenerator = false;
//        std::vector<pcl::PointCloud<pcl::PointXYZRGB>::Ptr> _pointClouds;
        PoseGraph _poseGraph;

        //Local Vars for CUDA Downloading
        int* _gpuVolumePtr;
        gpu::int3 _gpuVoxelGridSize;
        gpu::int3 _gpuVoxelCenter;
        gpu::float3 _gpuVoxelCellSize;
        float* bilateralFilterTargetPtr;
        float* bilateralFilterSourcePtr;
        gpu::float3* _icpSourceVertexMap;
        gpu::float3* _icpSourceNormalMap;
        gpu::float3* _icpLocalTargetVertexMap;
        gpu::float3* _icpLocalTargetNormalMap;
        gpu::float3* _icpGlobalTargetVertexMap;
        gpu::float3* _icpGlobalTargetNormalMap;
        uchar* _colourMap;

        bool _featureBasedICPEnabled = false;

        FBICP _featureBasedICP;
        float _weightFactor = 1.f;
        //Test
        int counter;
        int iteration;
        //Main Methods
        void SetupCuda(double fx, double fy, double cx, double cy, int rows, int cols, int numLevels,
                       gpu::matrix33 rotationMatrix, gpu::float3 translation, float weightFactor, float volumeSize=3.f,
                        bool featureBasedICP = false);
        void SetICPLimits(float rotationlimit = 0.05, float translationlimit = 0.05);
        void SetIntegrationMetric(float metric = 0.05);
        void SetMovingVoxelThreshold(int voxels = 60);
        bool CudaKinect(gpu::DepthMap& img, bool initial=false);
        bool CudaKinect(gpu::DepthMap& img, gpu::Image& colourImg, bool initial=false);
        void CudaBilateralFilterAndResize();
        bool CudaIterativeClosestPoint(bool& integrationCheckPass, bool& extractPointCloudMetric);
        bool FeatureBasedICP(cv::Mat& rgb, cv::Mat_<double>& depth, bool& integrationCheckPass, bool& pointcloudExtractionPassed);
        void CudaDownloadIterativeClosestPointEquations();
        void CudaProjectiveTsdf();
        void CudaRayCaster();
        //Main Methods V2 addition
        bool CudaKinectV2(gpu::DepthMap& img, bool initial=false);
        bool CudaKinectV2(gpu::DepthMap& img, gpu::Image& colourImg, cv::Mat_<double>& depth, cv::Mat& rgb, bool initial=false, bool forceIntegrationNow=false, bool extractPointCloudNow=false);
        void CudaKinectV2Finish();
        bool CudaKinectV2Debug(gpu::DepthMap& img, gpu::Image& colourImg, gpu::float3& translation, gpu::matrix33& rotation, bool& proc, bool initial=false);
        bool CudaCheckVolumeBounds();
        void CudaUpdateVolumeBoundsAndMoveVolume();
        void CudaResetTsdfSection(gpu::int3 &start, gpu::int3& end);
        void CudaRayCastTsdfSection(gpu::int3 &start, gpu::int3& end);
        void CudaRayCastTsdfRemainder();
        void CudaDownloadRayCastTsdfSection(bool reset=true);

        pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr GeneratePointCloud(bool setBasicColour=false);
        void GeneratePointCloudUsingRayCaster(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr&, bool basicColour=false);
        void GeneratePointCloudUsingAxesRayCaster(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr&, bool basicColour=false);

        void CudaResetPointIndexVolume();
        //Upload Image data
        void CudaUploadImageData(gpu::DepthMap& depthTarget);

        void CudaUploadColourImageData(gpu::Image& image);
        //Downloads for Testing
        void CudaDownloadBilateralFilterAndResize();
        void CudaDownloadIterativeClosestPointVertexAndNormalMap();
        void CudaDownloadTsdf();
        void CudaDownloadRayCaster(int level=-1);

        //Save for Testing
        void CudaSaveBilateralFilterAndResize();
        void CudaSaveIterativeClosestPointVertexAndNormalMap();
        void CudaSaveRayCaster(int i);
        void CudaSaveRayCasterColour(int i);
        void viewPointCloud(pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr ptr);

        void icptest();
        gpu::matrix33 GetLastRotationMatrix();
        gpu::float3 GetLastTranslationVector();
        void CudaTsdfVisualizer(std::string filename="TsdfAZ");
        void CudaRayCastPosition(std::vector<gpu::matrix33>&, std::vector<gpu::float3>&, bool save=true);

        Eigen::Matrix4d OpenCVRgbd(cv::Mat &prevRgbRef, cv::Mat &prevDepthRef, cv::Mat &currentRgbRef, cv::Mat & currentDepthRef);
    };
};


#endif //VSLAM_KINECTFUSION_HPP
