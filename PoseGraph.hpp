/**
 * MIT License

Copyright (c) 2017 Javonne Jason Martin

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.
 */
//
// Created by Javonne Martin on 2017/02/07.
//

#ifndef VSLAM_POSEGRAPH_HPP
#define VSLAM_POSEGRAPH_HPP

#include "KinectFusion/DeviceTypes.hpp"
#include "FeatureBasedICP/FeatureToPointGenerator.hpp"
#include "Node.hpp"
#include <list>
#include <pcl/point_types.h>
#include <pcl/point_cloud.h>
#include <unordered_set>
//#include "AssociatedICP.hpp"
//class Rectangle*;
//class Rectangle {
//    Rectangle(float xmin, float ymin, float zmin, float xmax, float ymax, float zmax) {
//
//    }
//    bool intersection(Rectangle r2) {
//
//    }
//
//};
//struct rect
//{
//    float x;
//    float y;
//    float z;
//    float width;
//    float height;
//    float depth;
//};
//
//bool valueInRange(float value, float min, float max)
//{ return (value >= min) && (value <= max); }
//
//bool rectOverlap(rect A, rect B)
//{
//    bool xOverlap = valueInRange(A.x, B.x, B.x + B.width) ||
//                    valueInRange(B.x, A.x, A.x + A.width);
//
//    bool yOverlap = valueInRange(A.y, B.y, B.y + B.height) ||
//                    valueInRange(B.y, A.y, A.y + A.height);
//
//    return xOverlap && yOverlap;
//}
class PoseGraph {
public:
    void AddNode(gpu::matrix33 rotation, gpu::float3 translation, gpu::int3 center, pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr pointCloud,
                 gpu::float3* vertexMap, gpu::float3* normalMap, uchar* image, int rows, int cols);
    bool Reintegrate();
    pcl::PointCloud<pcl::PointXYZRGBNormal>::Ptr BuildPointCloud();
    std::list<Node> _nodes;
    gpu::int3 _voxelSize;
    gpu::float3 _voxelCellSize;
};


#endif //VSLAM_POSEGRAPH_HPP
